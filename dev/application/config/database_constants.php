<?php

define('USERS','users');
define('USER_GROUPS','user_groups');
define('USER_GROUP_REFERENCE','user_group_reference');
define('USER_LOGINS','user_logins');
define('USER_ACTIVITY','user_activity');

//payroll
define("PRL","payroll");
define("PRL_MNT","payroll_maintainables");

//EMPLOYEE
define('EMP_DETAILS','employee_details'); 
define("EMP_PAYROLL",'employee_payroll');
define("EMP_PRL",'employee_payroll');
define('EMP_DEPT','department_employee_reference');
define('EMP_SAVING','employee_saving');

//COMPANY
define('COMPANY','company');
define('COMPANY_SETTING','company_setting');
define('COMPANY_MEMBERS','company_users');

//
define('EXPENSE','expenses');


//maintainables
define("MNT_DEPT",'mnt_departments');
define("MNT_DEDUCT_INC",'mnt_deductions_incentives');

define('TIMEZONE','timezone');