<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	var $data = array();

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->template->check_if_logged_in();
	}

	public function login()
	{
		$this->template->check_if_logged_in();

		// load library
		$this->load->library('form_validation');
		$this->load->model(array("mduser"));

		$data = array();
		if($this->input->is_ajax_request())
		{
			// set validation rules
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('password', 'Password', 'required');
			if($this->form_validation->run())
			{
				$username = $this->input->post("username");
				$password = $this->input->post("password");
				if($this->mduser->check_user_login($username, $password))
				{
					$filters = array(
						"username" => $username,
						"is_active" => 1
					);
					$user_id = $this->mduser->get_user_id($filters);
					if($user_id)
					{
						$filters = array(
							"u_id" => $user_id
						);
						$user  = $this->mduser->get_user_info($filters);
						$session_data = array(
							"user_id" => encrypt($user['id']),
							"company_id" => encrypt($user['company_id']),
							"username" => $user['username'],
							"email" => $user['email'],
							"user_type" => $user['user_type'],
							"logged_in" => (bool)true,
						); 
						$this->session->set_userdata($session_data);
						
						if(is_super_admin())
						{
							$data['url'] = base_url()."admin/company";
						}else if(is_admin_alone())
						{
							$data['url'] = base_url()."admin/employees";
						}else
						{
							$data['url'] = base_url()."profile";
						}

						if(isset($_GET['redirect_url']))
						{
							$data['url'] = urldecode($_GET['redirect_url']);
						}

						$this->template->set_error(0);
						$this->template->set_message("Login successful.");
						$this->template->set_response_data($data);
					}else
					{
						$this->template->set_message("Sorry! User not found.");
					}
				}else
				{
					$this->template->set_message("Wrong username or password or user inactive.");
				}
			}else
			{
				$this->template->set_message(validation_errors('<p>','</p>'));
			}
		}
		$this->template->response(false);
	}

	public function logout($user_id="")
	{
		$id = decrypt($user_id);
		if(!empty($id))
		{
			if($this->session->has_userdata("logged_in"))
			{
				if($this->session->userdata("logged_in")==(bool)true)
				{
					$this->session->sess_destroy();
					redirect(base_url(),'refresh');
				}
			}
		}else
		{
			$this->session->sess_destroy();
			redirect(base_url(),'refresh');
		}
		// if not successful
		if(isset($_GET['redirect']))
		{
			$redirect_url = urldecode($_GET['redirect']);
			redirect($redirect_url,'refresh');
		}
	}

	public function register_notary()
	{
		$this->load->library("form_validation");
		$this->load->model("mdnotary");

		if($this->input->is_ajax_request())
		{
			$this->form_validation->set_rules("name","Name","required|is_unique[users.name]",array("is_unique" => "The %s already exists. Please choose another one."));
			$this->form_validation->set_rules("username","Username","required|min_length[6]|is_unique[users.username]",array("is_unique" => "The %s already exists. Please choose another one."));
			$this->form_validation->set_rules("email","Email","required|valid_email|is_unique[users.email]",array("is_unique" => "The %s is already exists. Please choose another one."));
			$this->form_validation->set_rules("password","Password","min_length[6]");
			$this->form_validation->set_rules("confirm_password","Confirm Password","min_length[6]|matches[password]");
			$this->form_validation->set_rules("address","Address","required");
			$this->form_validation->set_rules("longitude","Longitude","required");
			$this->form_validation->set_rules("latitude","Latitude","required");
			$this->form_validation->set_rules("address","Address","required");
			$this->form_validation->set_rules("state_short","State Short Name","required");
			$this->form_validation->set_rules("state","State","required");
			if($this->form_validation->run())
			{
				$timezone = "";

				// get timezone curl based on latitude and longitude
				$timezone_result = get_curl_json("https://maps.googleapis.com/maps/api/timezone/json?location=".$this->input->post("latitude").",".$this->input->post("longitude")."&timestamp=".strtotime(date("Y-m-d H:i:s")));
				$timezone_result = (array)$timezone_result;
				if(!empty($timezone_result))
				{
					if($timezone_result['status']=="OK")
					{
						$timezone = get_correct_timezone($timezone_result['timeZoneName']);
					}else
					{
						return $this->template->set_message("Cannot get timezone. Please try again.");
					}
				}

				$data = array(
					"name" => $this->input->post("name"),
					"email" => $this->input->post("email"),
					"status" => 1,
					"longitude" => $this->input->post("longitude"),
					"latitude" => $this->input->post("latitude"),
					"state" => $this->input->post("state"),
					"address" => $this->input->post("address"),
					"timezone" => $timezone,
					"home_phone" => $this->input->post("phone_number"),
					"username" => $this->input->post("username"),
					"password" => $this->input->post("password"),
					"created_at" => date("Y-m-d H:i:s"),
					"updated_at" => date("Y-m-d H:i:s")
				);

				$locality = $this->input->post("locality");

				if($locality!="")
				{
					$data['city'] = $this->input->post("locality").", ".$this->input->post("state_short");
				}else
				{
					$data['city'] = $this->input->post("state").", ".$this->input->post("state_short");
				}

				$response = $this->mdnotary->add($data,5);

				if($response)
				{
					// send email
					$this->load->library('email');

					$this->data['first_name'] = strtok($this->input->post("name"), " ");
					$this->data['username'] = $this->input->post("email");
					$this->data['password'] = $this->input->post("password");

					$content = $this->template->load("common","auth/user-credentials",$this->data,TRUE,TRUE);

					$this->email->to($this->input->post("email"));
        			$this->email->from('no-reply@notarycloud.com',"Notary Cloud");

       				$this->email->subject('REGISTRATION CREDENTIALS - Notary Cloud');
        			$this->email->message($content);

        			$this->email->send(FALSE);

					$this->template->set_error(0);
					$this->template->set_message("You have succesfully registered!");
				}
			}else
			{
				$this->template->set_message(validation_errors('<p>','</p>'));
			}
		}

		$this->template->response(false);
	}

	public function forgot_password()
	{
		$this->load->library("form_validation");
		$this->load->model(array("mduser"));

		if($this->input->is_ajax_request())
		{
			$this->form_validation->set_rules("email","Email","required|valid_email");
			if($this->form_validation->run())
			{
				$filters = array(
					"email" => $this->input->post("email")
				);
				if($this->mduser->check_user($filters)==TRUE)
				{
					$response = $this->mduser->forgot_password($this->input->post("email"));
					if($response['update'])
					{
						// send email
						$this->load->library('email');

						$this->data['code'] = $response['data']['forgotten_password_code'];
						$content = $this->template->load("common","auth/forgot-password",$this->data,TRUE,TRUE);

						$this->email->to($this->input->post("email"));
        				$this->email->from('no-reply@notarycloud.com',"Notary Cloud");

       					$this->email->subject('FORGOT PASSWORD - Notary Cloud');
        				$this->email->message($content);

        				if($this->email->send(FALSE)==TRUE)
        				{
        					$this->template->set_error(0);
							$this->template->set_message("A link to reset your password is emailed to <strong>".$this->input->post("email")."</strong>! Check it in your inbox or in the spam.");
        				}else
        				{
							$this->template->set_message("Email sending failed. Please try again.");
        				}
					}
				}else
				{
					$this->template->set_message("The email you entered is not found in the record.");
				}
			}else
			{
				$this->template->set_message(validation_errors('<p>','</p>'));
			}
		}

		$this->template->response(false);
	}

	public function reset_password($forgotten_password_code)
	{
		$this->load->model("mduser");
		$this->load->helper("form");

		$this->template->set_scripts(base_url('assets/js/reset-password.js'));

		$user = $this->mduser->check_forgotten_password($forgotten_password_code);

		if($user)
		{
			$this->data['code'] = $forgotten_password_code;

			$this->template->load("landing","auth/reset-password",$this->data);
		}else
		{
			// redirect back to landing page if not found or code expired
			redirect(base_url(),"refresh");
		}
	}

	public function change_password($code)
	{
		$this->load->library("form_validation");
		$this->load->model("mduser");

		if($this->input->is_ajax_request())
		{
			$this->form_validation->set_rules("new_password","New Password","required|min_length[6]");
			$this->form_validation->set_rules("confirm_new_password","Confirm New Password","required|min_length[6]|matches[new_password]");
			if($this->form_validation->run())
			{
				$data = array(
					"password" => $this->input->post("new_password"),
					"forgotten_password_code" => NULL,
					"forgotten_password_time" => "0000-00-00 00:00:00"
				);
				$response = $this->mduser->reset_password($data, $code);
				if($response)
				{
					$this->template->set_error(0);
					$this->template->set_message("You have successfully reset your password!");
				}
			}else
			{
				$this->template->set_message(validation_errors('<p>','</p>'));
			}
		}

		$this->template->response(false);
	}

}