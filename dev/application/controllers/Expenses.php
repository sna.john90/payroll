<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Expenses extends CI_Controller {

	var $data = array();

	public function __construct()
	{
		parent::__construct();
		$this->template->set_scripts(base_url('assets/js/expenses.js?_='.date("YmdHis")));
		
		$this->template->check_if_not_logged_in();
		$this->load->library("form_validation");
		$this->load->model("mdexpenses");
	}

	public function index()
	{
		$this->template->load("common","expenses/expenses_list",$this->data);
	}

	public function operating()
	{
		$year   = date("Y");
		if(isset($_GET['year']) && $_GET['year']!="")
		{
			$year = $_GET['year'];
		}
		$categories = get_expenses_category(); 
		$listings = array();
		$overall_total = array();
		for($i=1;$i<=12;$i++)
		{
			$overall_total[$i] = 0;
		}


		//personnel
		$personnelF = array();
		$personnelT = array();
		$personnelRowTotal = 0;
		for($i=1;$i<=12;$i++)
		{

			$totalPersonnel = $this->mdexpenses->get_expenses_total_view("view_total_incentives",$i,$year);

			$overall_total[$i] = $overall_total[$i]+$totalPersonnel;

			$personnelT[] = array(
								"total" => $totalPersonnel,
								"year"  => $year,
								"month" => $i
							);
			$personnelRowTotal = $personnelRowTotal+$totalPersonnel;
		}
		$personnelF['name'] 			= "Salaries & Wages";
		$personnelF['data_permonth']	= $personnelT;
		$personnelF['row_total']		= $personnelRowTotal;

		$listings[] = $personnelF;
		
		//other expenses
		foreach($categories as $key=>$value)
		{ 
			if(!empty($value['children']))
			{
				foreach($value['children'] as $svalue)
				{ 
					$temp = array();
					$expenses_month = array();
					$temp = $svalue;
					$row_total = 0;
					for($i=1;$i<=12;$i++)
					{
						$filter = array(
						  "YEAR(receipt_date)" 		=> $year,
						  "MONTH(receipt_date)"		=> $i
						);	
						$tTotal = $this->mdexpenses->get_expenses_total($filter,$svalue['id']);
						$expenses_month[] = array(
												"total" => $tTotal,
												"year"	=> $year,
												"month" => $i,
												"percent" => (($svalue['estimated_amount']>0 && $tTotal>0)?($tTotal/$svalue['estimated_amount'])*100 : 0)
											);
						$overall_total[$i] = $overall_total[$i]+$tTotal;

						$row_total = $row_total+$tTotal;
					}
					$temp['data_permonth'] = $expenses_month;
					$temp['row_total']	   = $row_total;
					$listings[] = $temp;
				}
			}
			else 
			{ 
				$temp = array();
				$expenses_month = array();
				$row_total = 0;
				$temp = $value;
				for($i=1;$i<=12;$i++)
				{
					$filter = array(
					  "YEAR(receipt_date)" 		=> $year,
					  "MONTH(receipt_date)"		=> $i
					);	
					$tTotal = $this->mdexpenses->get_expenses_total($filter,$value['id']);
					$expenses_month[] = array(
											"total" => $tTotal,
											"year"	=> $year,
											"month" => $i,
											"percent" => (($value['estimated_amount']>0 && $tTotal>0)?($tTotal/$value['estimated_amount'])*100 : 0)
										);
					$overall_total[$i] = $overall_total[$i]+$tTotal;

					$row_total = $row_total+$tTotal;
				}
				$temp['data_permonth'] = $expenses_month;
				$temp['row_total']	   = $row_total;
				$listings[] = $temp;
			}
		}

		//calculate row total for grand total
		$grand_total = 0;
		foreach ($overall_total as $key => $value) {
			$grand_total = $grand_total+$value;
		}

		$this->data['lists'] = $listings;
		$this->data['column_total'] = $overall_total;
		$this->data['grand_total'] = $grand_total;
		$this->data['year']	= $year;	
		$this->template->load("common","expenses/expenses_operating",$this->data);
	}
	public function personnel()
	{
		$this->load->model("mduser");


		$output = array();
		$year   = date("Y");
		if(isset($_GET['year']) && $_GET['year']!="")
		{
			$year = $_GET['year'];
		}
		

		$employees = $this->mduser->get_company_employees();


		$total_pmonth = array();
		for($i=1;$i<=12;$i++){ $total_pmonth[$i]=0;} //generate default
		foreach($employees as $value)
		{
			$incentives_pmonth = array();

			//January to December 
			
			for($i=1;$i<=12;$i++)
			{
				//15
				$period_from 	= date("Y-m-d",strtotime("{$year}-{$i}-01"));
				$period_to 		= date("Y-m-d H:i:s",strtotime("{$year}-{$i}-15 23:59:59"));
				$result 		= $this->mdexpenses->personnel_expenses($value['id'],$period_from,$period_to); 
				$incentives_pmonth[] = $result;

				if(!empty($result))
				{
					$total_pmonth[$i] = (isset($total_pmonth[$i]))? $total_pmonth[$i]+$result['total_incentives'] : 0;
				} 
			}
			$value['month_duration']   = "15th";
			$value['total_incentives'] = $incentives_pmonth;
			$output[] = $value;

			$incentives_pmonth = array();
			//30
			for($i=1;$i<=12;$i++)
			{
				$period_from 	= date("Y-m-d",strtotime("{$year}-{$i}-16"));
				$period_to 		= date("Y-m-t H:i:s",strtotime("{$year}-{$i}-16 23:59:59"));
				$result 		= $this->mdexpenses->personnel_expenses($value['id'],$period_from,$period_to);

				$incentives_pmonth[] =  $result; 

				if(!empty($result))
				{
					$total_pmonth[$i] = (isset($total_pmonth[$i]))? $total_pmonth[$i]+$result['total_incentives'] : 0;
				} 
			}
			$value['month_duration']   = "30th";
			$value['total_incentives'] = $incentives_pmonth;
			$output[] = $value;
		}


		// get personnel report every month 
		$this->data['total_pmonth'] = $total_pmonth;
		$this->data['lists'] = $output;
		$this->data['selected_year'] = $year;
		$this->template->load("common","expenses/expenses_personnel",$this->data);
	}

	public function monthly()
	{
		$month 	= date("m");
		$year 	= date("Y");
		if(isset($_GET['month']) && $_GET['month']!="")
		{
			$month = $_GET['month'];
		}
		if(isset($_GET['year']) && $_GET['year']!="")
		{
			$year = $_GET['year'];
		}
		$this->data['month'] = $month;
		$this->data['year']  = $year;
 		$this->data['expenses_content_monthly']	= $this->monthly_listing(true);
		$this->template->load("common","expenses/expenses_monthly",$this->data);
	}
	public function monthly_listing($return=false)
	{
		$month 	= date("m");
		$year 	= date("Y");
		if(isset($_GET['month']) && $_GET['month']!="")
		{
			$month = $_GET['month'];
		}
		if(isset($_GET['year']) && $_GET['year']!="")
		{
			$year = $_GET['year'];
		}

		$filter = array(
					  "YEAR(receipt_date)" 		=> $year,
					  "MONTH(receipt_date)"		=> $month
					);


		$categories = get_expenses_category();
		//print_r($categories);
		$lists = array();
		$overall_total = array(
							"budget" 	=> 0,
							"actual"	=> 0,
							"difference" => 0,
							"difference_percentage" => 100
						); 
		foreach($categories as $key=>$value)
		{
			
			
			if(!empty($value['children']))
			{
				foreach($value['children'] as $svalue)
				{
					$temp = array();
					$temp = $svalue;
					$actual							= $this->mdexpenses->get_expenses_total($filter,$svalue['id']);
					$temp['actual']					= (empty($actual))? 0 : $actual;
					$temp['difference']				= $svalue['estimated_amount']-$temp['actual'];
					$temp['difference_percentage']	= ($temp['actual']>0 && $svalue['estimated_amount']>0)? ($temp['actual']/$svalue['estimated_amount'])*100 : 100;

					$overall_total['budget']		= $overall_total['budget']+$svalue['estimated_amount'];
					$overall_total['actual']		= $overall_total['actual']+$temp['actual'];
					$overall_total['difference']	= $overall_total['difference']+$temp['difference'];
					$temp['type'] = "OPERATING";
					$lists[] = $temp;
				}
			}
			else 
			{
				$temp = array();
				$temp = $value;
				$actual							= $this->mdexpenses->get_expenses_total($filter,$value['id']);
				$temp['actual']					= (empty($actual))? 0 : $actual;
				$temp['difference']				= $value['estimated_amount']-$temp['actual'];
				$temp['difference_percentage']	= ($temp['actual']>0 && $value['estimated_amount']>0)? ($temp['actual']/$value['estimated_amount'])*100 : 100;

				$overall_total['budget']		= $overall_total['budget']+$value['estimated_amount'];
				$overall_total['actual']		= $overall_total['actual']+$temp['actual'];
				$overall_total['difference']	= $overall_total['difference']+$temp['difference'];

				$temp['type'] = "OPERATING";
				$lists[] = $temp;
			}
		}

		//get the personnel monthly summary 
		$budgetT 		= $this->mdexpenses->get_expenses_total_view("view_total_basicpay",$month,$year);
		$actualT 		= $this->mdexpenses->get_expenses_total_view("view_total_incentives",$month,$year);
		$differenceT 	= $budgetT - $actualT;
		$difference_percentageT = 100;
		if($budgetT>0 && $actualT>0)
		{
			$difference_percentageT = ($actualT/$budgetT)*100;
		}

		$personnel_summary = array(
								"type" 					=> "SALARIES & WAGES",
								"name"					=> "Salaries",
								"estimated_amount"		=> $budgetT,
								"actual"				=> $actualT,
								"difference"			=> $differenceT,
								"difference_percentage" => $difference_percentageT
							);

		$overall_total['budget'] = $overall_total['budget']+$budgetT;
		$overall_total['actual'] = $overall_total['actual']+$actualT;
		$overall_total['difference'] = $overall_total['difference']+$differenceT;


		$lists[] = $personnel_summary;
		$this->data['lists'] = $lists;
		if($overall_total['budget']>0 && $overall_total['actual']>0)
		{
			$overall_total['difference_percentage'] = ($overall_total['actual']/$overall_total['budget'])*100;
		}
		$this->data['overall_total'] = $overall_total;
		return $this->template->load("common","expenses/content/expenses_content_monthly",$this->data,$return,TRUE);
	}

	/** ADD expenses **/
	public function add()  // a modal
	{
		$this->template->load("common","expenses/modals/add_expense",$this->data,FALSE,TRUE);
	}
	public function do_add()
	{ 
		if(!is_admin())
		{
			redirect(base_url("profile"),"refresh");
		}
		if($this->input->post())
		{
			$this->form_validation->set_rules("entry_date","Entry Date","required");
			$this->form_validation->set_rules("receipt_from","Receipt From","required");
			$this->form_validation->set_rules("receipt_date","Receipt Date","required"); 
			$this->form_validation->set_rules("amount","Amount","required");
			$this->form_validation->set_rules("category","Category","required"); 
			if($this->form_validation->run())
			{
				$entry_date 	= $this->input->post("entry_date");
				$receipt_from 	= $this->input->post("receipt_from");
				$receipt_date 	= $this->input->post("receipt_date"); 
				$amount 		= $this->input->post("amount");
				$category 		= $this->input->post("category");
				$description 	= $this->input->post("description");
				$note 			= $this->input->post("note");

				$data_to_save  = array(
										"company_id" 	=> get_company_id(),
										"entry_date"	=> date("Y-m-d",strtotime($entry_date)),
										"receipt_from"	=> $receipt_from,
										"receipt_date"	=> date("Y-m-d",strtotime($receipt_date)),
										"amount"		=> $amount,
										"category_id"		=> $category,
										"description"	=> $description, 
										"addedby"		=> 	user_id()
								 ); 
				$response = $this->mdexpenses->add($data_to_save);
				if($response)
				{
					$this->mdexpenses->add_note($response,$note);
				}
				$this->template->evaluate($response,1); 
			}
			else
			{
				$this->template->set_message(validation_errors("",""));
			}
		}
		$this->template->response(false);
	}
	public function do_update()
	{ 
		if(!is_admin())
		{
			redirect(base_url("profile"),"refresh");
		}
		if($this->input->post())
		{
			$this->form_validation->set_rules("entry_date","Entry Date","required");
			$this->form_validation->set_rules("receipt_from","Receipt From","required");
			$this->form_validation->set_rules("receipt_date","Receipt Date","required"); 
			$this->form_validation->set_rules("amount","Amount","required");
			$this->form_validation->set_rules("category","Category","required"); 
			$this->form_validation->set_rules("id","ID","required"); 
			if($this->form_validation->run())
			{ 
				$entry_date 	= $this->input->post("entry_date");
				$receipt_from 	= $this->input->post("receipt_from");
				$receipt_date 	= $this->input->post("receipt_date"); 
				$amount 		= $this->input->post("amount");
				$category 		= $this->input->post("category");
				$description 	= $this->input->post("description"); 

				$id 	= $this->input->post("id"); 
				$id = decrypt($id);
				if(empty($id))
				{
					$this->template->response(false);exit;
				}
				$data_to_save  = array(
										"entry_date"	=> date("Y-m-d",strtotime($entry_date)),
										"receipt_from"	=> $receipt_from,
										"receipt_date"	=> date("Y-m-d",strtotime($receipt_date)),
										"amount"		=> $amount,
										"category_id"		=> $category,
										"description"	=> $description
								 ); 
				$response = $this->mdexpenses->update($data_to_save,$id); 
				$this->template->evaluate($response,1); 
			}
			else
			{
				$this->template->set_message(validation_errors("",""));
			}
		}
		$this->template->response(false);
	}
	// Listing for Datatables
	public function default_listing()
	{
		$response_data = array(
			"data" => array(),
			"draw" => 1,
			"recordsFiltered" => 0,
			"recordsTotal" => 0
		);
		if($this->input->is_ajax_request())
		{	
			$datatable = $this->input->get();
			$start = $datatable['start'];
			$limit = $datatable['length'];
			$filters = array( 
							"month_year" 	=> date("Y-m")
						);

			// if isset filters
			if(isset($_GET['filters']))
			{
				$temp = json_decode($this->input->get('filters'));
                foreach ($temp as $key => $value) {
                    $filters[$key] = $value;
                }
			} 
			$column = array("receipt_date","description","receipt_from","category_id","amount");
			$filters['order_column'] = array(
										  "field" 	=> $column[$datatable["order"][0]["column"]],
										  "ordered"	=> $datatable["order"][0]["dir"]
										); 
			if(isset($datatable['columns'][3]['search']['value']))
			{
				if(!empty($datatable['columns'][3]['search']['value']) && $datatable['columns'][3]['search']['value']!="all")
				{ 
					$filters['by_category'] = $datatable['columns'][3]['search']['value'];
				} 
			}
			if(isset($column[$datatable["order"][0]["column"]])=="receipt_date" && $datatable['columns'][$datatable["order"][0]["column"]]['search']['value']!="")
			{
				$filters['month_year'] = date("Y-m",strtotime($datatable['columns'][$datatable["order"][0]["column"]]['search']['value']));
			}
			$result = $this->mdexpenses->listing($filters,$start,$limit);
			if($result['totalCount']>0)
			{
				foreach ($result['result'] as $key => $value)
				{
					$value['id'] 			= $value['id'];
					$value['entry_date'] 	= $value['entry_date'];
					$value['receipt_from'] 	= $value['receipt_from'];
					$value['particular'] 	= $value['description'];
					$value['date'] 			= $value['receipt_date'];
					$value['amount'] 		= $value['amount']; 
					$value['category'] 		= get_expenses_category($value['category_id'])['name'];
					$value['receipt_date'] 	= $value['receipt_date'];
					 $hasnote				= $this->mdexpenses->get_notes($value['id'],true);  
					 $note_color 			= ($hasnote)? "text-success" : "text-gray";
					// $value['note'] 			= ($hasnote)? '<a href="#"  class="note-entry" data-id="'.encrypt($value['id']).'"><i class="fa fa-wpforms"></i></a>' : ""; 
					$value['note'] 			= '<a href="#"  class="note-entry '.$note_color.'" data-id="'.encrypt($value['id']).'"><i class="fa fa-paperclip"></i></a>'; 
					$value['action'] = '<div class="btn-groupX btn-group-xs"><a href="javascript:void(0)" class="btn ml5 btn-success view-btn" data-toggle="tooltip" data-id="'.encrypt($value['id']).'" data-container="body" data-trigger="hover" title="View details"><i class="fa fa-search-plus "></i></a><a href="javascript:void(0)" class="btn ml5 btn-primary edit-btn" data-toggle="tooltip" data-id="'.encrypt($value['id']).'" data-container="body" data-trigger="hover" title="View details"><i class="fa fa-pencil "></i></a><a href="javascript:void(0)" class="btn ml5 btn-danger remove-btn" data-toggle="tooltip" data-id="'.encrypt($value['id']).'" data-container="body" data-trigger="hover" title="Delete"><i class="fa fa-close "></i></a></div>';
					$response_data['data'][] = $value;
				}
			}
			$response_data['draw'] = $datatable['draw'];
			$response_data['recordsFiltered'] = $result['totalCount'];
			$response_data['recordsTotal'] = $result['totalCount'];
		}
		$this->template->array_to_json($response_data);
	}
	/* -- end --*/
	public function do_delete()
	{
		if(isset($_GET['id']) AND $_GET['id']!="")
		{
			$id = decrypt($_GET['id']);
			if(!empty($id))
			{
				$response = $this->mdexpenses->delete($id);
				if($response)
				{
					$this->template->set_error(0);
					$this->template->set_message("Successfully deleted.");
				}
			}
		}
		$this->template->response(false);
	}
	public function edit()  // a modal
	{
		$this->template->load("common","expenses/modals/edit_expense",$this->data,FALSE,TRUE);
	}

	public function view()  // a modal
	{
		if(isset($_GET['id']) && $_GET['id']!="")
		{ 
			$id = $_GET['id']; 
			$expense_id = decrypt($id); 

			if(!empty($expense_id))
			{
				$filters['id'] 	= $expense_id;
				$result 		= $this->mdexpenses->listing($filters,0,1); 
				if(count($result['result'])>0)
				{
					$this->data = $result['result'][0];
				} 
				$this->template->load("common","expenses/modals/view_expense",$this->data,FALSE,TRUE);
			} 
		}
	}
	public function edit_expense()  // a modal
	{
		if(isset($_GET['id']) && $_GET['id']!="")
		{ 
			$id = $_GET['id']; 
			$expense_id = decrypt($id); 

			if(!empty($expense_id))
			{
				$filters['id'] 	= $expense_id;
				$result 		= $this->mdexpenses->listing($filters,0,1); 
				if(count($result['result'])>0)
				{
					$this->data = $result['result'][0];
				} 
				$this->template->load("common","expenses/modals/edit_expense-basic",$this->data,FALSE,TRUE);
			} 
		}
	}
	public function view_details()  // a modal for expense list monthly
	{
		$year 	= date("Y");
		$month 	= date("m");

		if(isset($_GET['month']) && $_GET['month']!="")
		{
			$month = $_GET["month"];
		}

		if(isset($_GET['year']) && $_GET['year']!="")
		{
			$year = $_GET["year"];
		}

		$filters = array(
						"month_year" => $year."-".$month
					);
		$category_name = "All";
		if(isset($_GET['category']) && $_GET['category']!="")
		{
			$filters['by_category'] = $_GET['category'];
			$category = $this->mdexpenses->get_categories(0,$_GET['category']);

			if(!empty($category))
			{
				$category_name = $category[0]['name'];
			}
		}

		
		$result = $this->mdexpenses->listing($filters);
		
		$this->data['category_name'] = $category_name;
		$this->data['month_name']	 = date("F",strtotime("{$year}-{$month}-01"));
		$this->data['lists'] = $result['result'];
		$this->template->load("common","expenses/modals/expenses_list_detail",$this->data,FALSE,TRUE);
	}

	//
	public function add_vendor()  // a modal:adds vendor from dropdown.select button
	{
		//$this->data['add_contact']	=$this->template->load("common","contacts/modals/add_contact",$this->data,TRUE,TRUE);
		$this->template->load("common","contacts/modals/add_contact",$this->data,FALSE,TRUE);
	}
	
	public function notes()
	{ 
		if(isset($_GET['id']) && $_GET['id']!="")
		{ 
			$id = $_GET['id']; 
			$expense_id = decrypt($id); 
			$lists = $this->mdexpenses->get_notes($expense_id);  
			$this->data['notes']  = $lists;
			$this->data['target'] = $id;
			$this->data['note_lists'] = $this->template->load("common","expenses/content/notes-list",$this->data,TRUE,TRUE);
			$this->template->load("common","expenses/modals/notes",$this->data,FALSE,TRUE);
		} 
	}
	public function do_addnotes()
	{
		if($this->input->post())
		{
			$this->form_validation->set_rules("notes","Notes","required");
			$this->form_validation->set_rules("expenseid","Target Expense","required");
			if($this->form_validation->run())
			{
				$expense_id = $this->input->post("expenseid");
				$notes = $this->input->post("notes");

				$id = decrypt($expense_id);
				if(!empty($id))
				{
					$response = $this->mdexpenses->add_note($id,$notes); 
					if($response)
					{
						$this->template->set_error(0);
						$this->template->set_message("Successfully added");
						$lists = $this->mdexpenses->get_notes($id);
						$this->data['notes']  = $lists;
						$this->data['target'] = $id;
						$datalists = $this->template->load("common","expenses/content/notes-list",$this->data,TRUE,TRUE); 
						$this->template->set_response_data($datalists);
					}
					else
					{
						$this->template->set_error(1);
						$this->template->set_message("Failed to add.");
					}
				}
			}
			else
			{
				$this->template->set_message(validation_errors("",""));
			}
		}
		$this->template->response(false);
	}

	public function categories()
	{
		$this->data['expenses_category_content'] = $this->categories_content(TRUE);
		$this->template->load("common","expenses/categories",$this->data);
	}
	public function categories_content($return=false)
	{
		$this->data['lists'] = get_expenses_category();
		return $this->template->load("common","expenses/content/category-list",$this->data,TRUE,TRUE);
	}
	public function add_category()  // a modal:adds cagetory from dropdown.select button
	{
		$this->data['expenses_categories']	=$this->template->load("common","admin/content/expenses_categories_content",$this->data,TRUE,TRUE);
		$this->template->load("common","expenses/modals/add_category",$this->data,FALSE,TRUE);
	}
	public function doAdd_category()
	{
		if($this->input->post())
		{
			$this->form_validation->set_rules("name","Name","required");
			$this->form_validation->set_rules("estimated_amount","Budget","required");
			$this->form_validation->set_rules("parent","Parent","required");
			if($this->form_validation->run())
			{
				$name 				= $this->input->post("name");
				$estimated_amount 	= $this->input->post("estimated_amount");
				$parent 			= $this->input->post("parent");  
				$company_id 		= get_company_id();
				$user_id			= user_id();

				$raw_data = array(
								"name" 				=> $name,
								"estimated_amount"	=> $estimated_amount,
								"parent"			=> $parent,
								"company_id"		=> $company_id,
								"addedby"			=> $user_id
							);

				$result = $this->mdexpenses->add_category($raw_data);
				$this->template->evaluate($result,1); 
			}
			else 
			{
				$this->template->set_message(validation_errors("",""));
			}
		}
		$this->template->response(false,TRUE);
	}
	public function update_category()  // a modal:adds cagetory from dropdown.select button
	{ 
		$id = "";
		if(isset($_GET['id']))
		{
			$id = decrypt($_GET['id']);
		}
		if(!empty($id))
		{
			$result = $this->mdexpenses->get_categories(0,$id);
			$final_data = array(
								"name" => "",
								"estimated_amount" => 0,
								"parent" => 0
							);
		//	print_r($result);
			if(!empty($result))
			{
				$final_data = $result[0];
			}
			$this->data = $final_data;
			$this->template->load("common","expenses/modals/update_category",$this->data,FALSE,TRUE);
		}
	}
	public function doUpdate_category()
	{ 
		if($this->input->post())
		{
			$this->form_validation->set_rules("name","Name","required");
			$this->form_validation->set_rules("estimated_amount","Budget","required");
			$this->form_validation->set_rules("parent","Parent","required");
			$this->form_validation->set_rules("id","ID","required");
			if($this->form_validation->run())
			{
				$enc_id 				= $this->input->post("id");
				$name 				= $this->input->post("name");
				$estimated_amount 	= $this->input->post("estimated_amount");
				$parent 			= $this->input->post("parent");  
				$company_id 		= get_company_id(); 

				$raw_data = array(
								"name" 				=> $name,
								"estimated_amount"	=> $estimated_amount,
								"parent"			=> $parent
							);
				$id = decrypt($enc_id);
				if(!empty($id))
				{
					$result = $this->mdexpenses->update_category($raw_data,$id);
					$this->template->evaluate($result,2);
				}
				 
			}
			else 
			{
				$this->template->set_message(validation_errors("",""));
			}
		} 
		$this->template->response(false,true);
	}
	public function doDelete_category()
	{
		if(isset($_GET['id']))
		{
			$id = decrypt($_GET['id']);
			if(!empty($id))
			{
				$response = $this->mdexpenses->delete_category($id);
				if($response)
				{
					$this->template->set_error(0);
					$this->template->set_message("Successfully deleted.");
				}
			}
		}
		$this->template->response(false,true);
	}
}

