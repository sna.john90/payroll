<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	var $data = array();

	public function __construct()
	{
		parent::__construct();
		$this->template->check_if_logged_in();
		$this->load->helper(array("form"));
		// scripts
		$this->template->set_scripts(base_url('assets/js/home.js'));
	}

	public function index()
	{
		$this->data['redirect_url'] = "";
		if(isset($_GET['redirect_url']))
		{
			$this->data['redirect_url'] = "?redirect_url=".$_GET['redirect_url'];
		}

		$this->template->set_styles(base_url('assets/lib/selectize/selectize.css'));
		$this->template->set_scripts(base_url('assets/lib/selectize/selectize.min.js'));
		$this->template->set_scripts(base_url('assets/lib/selectize/plugins/no_results.js'));

		$this->template->set_scripts(base_url("assets/lib/jquery-input-mask/jquery.inputmask.bundle.min.js"));
		$this->template->set_scripts("http://maps.googleapis.com/maps/api/js?key=".$this->config->item("google_api_key")."&libraries=places");
		$this->template->set_scripts(base_url("assets/lib/jquery-geocomplete/jquery.geocomplete.min.js"));

		$this->template->set_scripts(base_url('assets/js/home.js'));

		$this->data['google_map_api_key'] = $this->config->item("google_map_api_key");
		
		$this->template->load("landing","index",$this->data);
	}
}
