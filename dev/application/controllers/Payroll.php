<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Payroll extends CI_Controller {

	var $data = array();

	public function __construct()
	{
		parent::__construct();

		$this->template->set_scripts(base_url('assets/js/payroll.js')."?_=".date("YmdHis"));
		$this->template->check_if_not_logged_in();
		$this->load->library("form_validation");
		$this->load->model("mdpayroll");
		$this->template->set_scripts(base_url('assets/js/tax_calculator.js'));
	}


	/*
	| PAYRUN DETAILS
	|  @description : adding of payruns details
	|
	*/
	public function index()
	{
		if(!is_admin())
		{
			redirect(base_url("profile"));
		}
		$this->data['payruns'] = $this->mdpayroll->listing();
		$this->data['payroll_content_list']	= $this->template->load("common","payroll/content/payroll_content_list",$this->data,TRUE,TRUE);
		$this->template->load("common","payroll/payroll_list",$this->data);
	}
	public function create_payroll()  // a modal
	{
		$this->template->load("common","payroll/modals/create_payrun",$this->data,FALSE,TRUE);
	}
	 /*
	  | @func        - do_remove()
	  | @description - remove payrun and its associated data
	  | @params      - payrun id
		| @requests   - GET
	  | @return - boolean
	  */
	 public function do_remove($id="")
	 {
	 		$payrun_id = decrypt($id);
			if(empty($payrun_id) && !is_admin())
			{
					redirect(base_url('profile'),'refresh');
			}
			$result = $this->mdpayroll->delete_payrun($payrun_id);
			if($result)
			{
				$this->template->set_error(0);
				$this->template->set_message("Successfully removed.");
			}
			$this->template->response(FALSE);
	 }
	 /* END OF functionname */
	//create payroll details
	public function do_create()
	{
		if(!is_admin())
		{
			redirect(base_url("profile"));
		}
		if($this->input->post())
		{
			$this->form_validation->set_rules("period_from","Pay Period From","required");
			$this->form_validation->set_rules("period_to","Pay Period To","required");
			$this->form_validation->set_rules("payroll_date","Payroll Date","required");
			$this->form_validation->set_rules("description","Description","required");
			$this->form_validation->set_rules("notes","Notes","required");
			if($this->form_validation->run())
			{
				$period_from 	= $this->input->post("period_from");
				$period_to 		= $this->input->post("period_to");
				$payroll_date 	= $this->input->post("payroll_date");
				$description 	= $this->input->post("description");
				$notes 			= $this->input->post("notes");

				$data 			= array(
									"period_from" 	=> $period_from,
									"period_to"		=> $period_to,
									"payroll_date"	=> $payroll_date,
									"description"	=> $description,
									"notes"			=> $notes,
									"addedby"		=> user_id(),
									"company_id"	=> get_company_id(),
									"payroll_number"=> get_payroll_number()
								  );
				$response = $this->mdpayroll->create_payrun($data);
				if($response)
				{
					$this->template->set_error(0);
					$this->template->set_message("Successfully added.");
					$this->template->set_response_data(encrypt($response));
				}
				else
				{
					$this->template->set_message("Fail to add.");
				}
			}
			else
			{
				$this->template->set_message(validation_errors("",""));
			}
			$this->template->response(false);
		}
	}
	public function do_update_payrun_details()
	{
		if($this->input->post())
		{
			$this->form_validation->set_rules("value","Field","required");
			$this->form_validation->set_rules("primarykey","Payrun ID","required");
			$this->form_validation->set_rules("container","Field","required");
			if($this->form_validation->run())
			{
				$id 	= $this->input->post("primarykey");
				$value	= $this->input->post("value");
				$field  = $this->input->post("container");

				$id = decrypt($id);
				if(!empty($id))
				{
					$response = $this->mdpayroll->update_payrun_details($id,$value,$field);
					if($response)
					{
						$this->template->set_error(0);
						$this->template->set_message("All changes has been saved.");
					}
				}
			}
			else
			{
				$this->template->set_message(validation_errors("",""));
			}
		}
		$this->template->response(FALSE,TRUE);
	}

	public function view_payrun($payrun_id="",$lists="")
	{
		$payrunID = decrypt($payrun_id);

		//get payrun entries
		$payrun_data = $this->mdpayroll->get_payrun_details($payrunID);
		if(empty($payrunID) || !is_admin() || empty($payrun_data))
		{
			redirect(base_url('profile'),'refresh');
		}

		if($lists=="lists")
		{
			$this->template->set_error(0);
			$this->template->set_message("employee lists");
			$this->template->set_response_data($this->_payrun_employees($payrun_id,"payroll/content/payrun-view-part-listing"));
			$this->template->response(FALSE,TRUE);
			exit;
		}

		$payrun_data['payrun_id_enc']	= $payrun_id;

		$payrun_data['total_earnings'] 		= 0;
		$payrun_data['total_salaries'] 		= 0;
		$payrun_data['total_taxes'] 			= 0;
		$payrun_data['total_incentives'] 	= 0;
		$payrun_data['total_deductions'] 	= 0;
		$payrun_data['total_takehome'] 		= 0;

		$this->data['payrun_content']	= $this->template->load("common","payroll/content/payrun-view-part-content",$payrun_data,TRUE,TRUE);

		//checking the first employees
		$this->data['first_employee'] = "0";
		$response = $this->mdpayroll->employees();
		if(!empty($response))
		{
			$this->data['first_employee'] = encrypt($response[0]['id']);
		}
		$this->data['payrun_id_enc'] 	= $payrun_id;
		$this->data['description']		= ucfirst($payrun_data['description']);
		$this->template->load("common","payroll/content/view_payrun",$this->data);
	}
	public function employees()
	{
		$this->data['payroll_content']			= $this->template->load("common","payroll/content/payrun_content",$this->data,TRUE,TRUE);
		$this->template->load("common","payroll/employees",$this->data);
	}
	public function payroll_employee_view()  // a modal
	{
		@$this->data['profile_widget']		= $this->template->load("common","profile/includes/inc-profile-widget",$this->data,TRUE,TRUE);
		$this->data['profile_content']		= $this->template->load("common","profile/includes/inc-profile-content",$this->data,TRUE,TRUE);

		$this->data['salary_dedecutions']	= $this->template->load("common","payroll/content/payroll_content_salary_deduction",$this->data,TRUE,TRUE);
		$this->data['payroll_payslip']		= $this->template->load("common","payroll/content/payroll_content_payslip",$this->data,TRUE,TRUE);

		$this->template->load("common","payroll/modals/payroll_employee",$this->data,FALSE,TRUE);
	}
	/***** END OF PAYRUN DETAILS ****/

	/*
	| PAYRUN EMPOLYEES
	|	@description : this is for payrun employees processes
	|
	*/
	public function payrun_employees($payrun_id="",$lists="")
	{
		$payrunID = decrypt($payrun_id);

		//get payrun entries
		$payrun_data = $this->mdpayroll->get_payrun_details($payrunID);
		if(empty($payrunID) || !is_admin() || empty($payrun_data))
		{
			redirect(base_url('profile'),'refresh');
		}

		if($lists=="lists")
		{
			$this->template->set_error(0);
			$this->template->set_message("employee lists");
			$this->template->set_response_data($this->_payrun_employees($payrun_id));
			$this->template->response(FALSE,TRUE);
			exit;
		}

		$payrun_data['payrun_id_enc']	= $payrun_id;

		$payrun_data['total_earnings'] 		= 0;
		$payrun_data['total_salaries'] 		= 0;
		$payrun_data['total_taxes'] 		= 0;
		$payrun_data['total_incentives'] 	= 0;
		$payrun_data['total_deductions'] 	= 0;
		$payrun_data['total_takehome'] 		= 0;

		$this->data['payrun_content']	= $this->template->load("common","payroll/content/payrun_content",$payrun_data,TRUE,TRUE);

		//checking the first employees
		$this->data['first_employee'] = "0";
		$response = $this->mdpayroll->employees();
		if(!empty($response))
		{
			$this->data['first_employee'] = encrypt($response[0]['id']);
		}
		$this->data['payrun_id_enc'] 	= $payrun_id;
		$this->data['description']		= ucfirst($payrun_data['description']);
		$this->template->load("common","payroll/payrun",$this->data);
	} 

	// _payrun_employees()
	//   * this is the lists of employees under the company
	// 	@params : payrunid
	//  @return : html lists
	private function _payrun_employees($id,$view="payroll/content/payrun-employee-lists")
	{
		if(!empty($id))
		{
			$data = "";
			$response = $this->mdpayroll->employees();
			$this->data['employees'] = $response;
			$this->data['payrun_id_enc'] 	= $id;
			$payrun_id = decrypt($id);

			$overall_total = array(
							"company_share" 	=> 0,
							"salaries"			=> 0,
							"taxes"				=> 0,
							"incentives"		=> 0,
							"deductions"		=> 0,
							"takehomepay"		=> 0
						);
			$data = "<tr><td colspan='9' class='text-center'>No employees.</td></tr>";
			//recalculate tax if basic pay or incentives are added
			$lists_income_taxable = get_payroll_mnt("incentive",array(
														"is_taxable" => "yes"
													),"name",true);
			$lists_deduction_taxable = get_payroll_mnt("deduction",array(
														"is_taxable" => "yes"
													),"name",true);
			if(!empty($response))
			{
				$data = "";
				foreach($response as $key => $value)
				{
								$gross_income 			= 0;
								$deduction_intax		= 0;
								$total_deductions  	= 0;
								$total_earnings		 	= 0;
								$takehomepay				= 0;
								$incentives = 0;
								$basicpay   				= $value["basic_pay"];
								if($value['payment_type']=="semi-monthly")
								{
										$basicpay = $value["basic_pay"]/2;
								}
								$income_tax 					= 0;
								$paid 							= false;
								if(is_payrun_emp_exists($value['id'],$payrun_id))
								{
								 	$temp_data = $this->mdpayroll->get_emp_payroll_details($payrun_id,$value['id']);
									foreach($temp_data as $skey=>$svalue)
									{
											if($svalue['pay_type']=="deduction")
											{
												$total_deductions = $total_deductions + $svalue['value'];
											}
											else if($svalue['pay_type']=="incentive" && $svalue['mnt_di_id']!="basic_pay")
											{
												$incentives = $incentives + $svalue['value'];
											}
											if($svalue['status']==1)
											{
													$paid = true;
											}
											if($svalue['mnt_di_id']=="income_tax")
											{
													$income_tax = $svalue['value'];
											}
											if($svalue['mnt_di_id']=="basic_pay")
											{
												$basicpay = $svalue['value'];
											}
									}
								}
								else
								{
										$sss        = getSSSContribution($basicpay,"employed",$value['payment_type'],"all");
										$plh        = getPhilhealthContribution($basicpay,$value['payment_type'],"all");
										$pgb        = getPagibigContribution($basicpay);
										$otherdeduction = 0; //for late and other
										$deductions = $sss['share'] + $plh['share'] + $pgb + $otherdeduction;

										$deductions_notrelatedtotax = 0; //deduction after tax calculation

										$incentives_taxable   = 0;
										$incentives_notaxable = 0;

										$total_incentives_taxable = $basicpay + $incentives_taxable;

										$gross_income  = $total_incentives_taxable + $incentives_notaxable;
										$income_tax          = getWithholdingTax($total_incentives_taxable,$deductions,getCivilStatus($value['gender_status']),$value['payment_type']);
										$total_deductions = $deductions + $income_tax + $deductions_notrelatedtotax;
								}
								$gross_income = $basicpay+$incentives;
								$takehomepay = $gross_income - $total_deductions; //get the take home pay 

			          $response_data = $value;
			          $response_data['payrun_id_enc'] 		= $id;
					  $response_data['is_paid']				= $paid;
			          $response_data['total_deductions']	= number_format($total_deductions,2);
			          $response_data['incentives']			= number_format($incentives,2);
			          $response_data['take_homepay']		= number_format($takehomepay,2);
			          $response_data['enc_id']				= encrypt($value['id']);
			          $response_data['last_payperiod']		= get_last_payperiod($value['id']);
			          $response_data['name']				= ucfirst($value['firstname'])." ".ucfirst($value['lastname']);
			          $response_data['employee_pic']		= profile_picture($value['profile_pic']);
			          $response_data['designation']			= ucfirst(trim($value["designation"]));
			          $response_data['basic_pay']			= number_format($basicpay,2);

			          $data .= $this->template->load("common",$view,$response_data,TRUE,TRUE);

			          //$overall_total['company_share']		+= ($sss['company'] + $plh['company']);
			          $overall_total['salaries']			+= $gross_income;
			          $overall_total['incentives']			+= $incentives;
			          $overall_total['deductions']			+= $total_deductions;
			          $overall_total['takehomepay']			+= $takehomepay;
			          $overall_total['taxes']				+= $income_tax;
				}
			}
			return array(
						"employee_list_html" => $data,
						"statistics"		 => $overall_total
					);
		}
	}


	public function review_payment($payrun_id="",$user_id="",$media="all")  // a modal
	{
		$payrunID 	= decrypt($payrun_id);
		$userID 	= decrypt($user_id);
		if(empty($payrunID) || empty($userID))
		{
			echo "not found";exit;
		}
		$this->data['editable'] = true;
		$referer = $_SERVER['HTTP_REFERER']; 

		if($referer==base_url("payroll/view-payrun/{$payrun_id}"))
		{
				$this->data['editable'] = false;
		}
		$this->data['payrun_id'] 	= $payrun_id;
		$this->data['user_id']		= $user_id;
		$this->data['id']			= $userID; //user decryted id

		$this->data['media'] = $media;
		//get previous and next employee
		$prev =	$this->mdpayroll->prev_employee($userID);
		$next = $this->mdpayroll->next_employee($userID);
		$this->data['prev_id'] = ($prev!=0)? encrypt($prev) : "0";
		$this->data['next_id'] = ($next!=0)? encrypt($next) : "0";

		//widgets
		$this->data['profile_widget']		= $this->template->load("common","profile/includes/inc-profile-widget",$this->data,TRUE,TRUE);
		$this->data['profile_content']		= $this->template->load("common","profile/includes/inc-profile-content",$this->data,TRUE,TRUE);
		$this->data['active_tab'] = "payslip";
		if(isset($_GET['profile_content']))
		{
			 $this->data['active_tab'] = "profile";
		}
		//
		// $this->data['salary_dedecutions']	= $this->template->load("common","payroll/content/payroll_content_salary_deduction",$this->data,TRUE,TRUE);

		$this->data['company_details']		= get_company_details();
		$this->data['get_employee_details']	= get_employee_details($userID);
		$this->data['payrun_details']		= $this->mdpayroll->get_payrun_details($payrunID);

		//get default data
		$temp_emp_details = $this->data['get_employee_details'];

		$emp_basic_pay = ($temp_emp_details['payment_type']=="semi-monthly")? $temp_emp_details['basic_pay']/2 : $temp_emp_details['basic_pay'];
		$earnings  = array(
						"basic_pay" => number_format($emp_basic_pay,2)
					);
		$deductions = array(
						"sss" 			=> 0,
						"philhealth"	=> 0,
						"pagibig"		=> 0,
						"income_tax"=> 0
					);
		//recalculate tax if basic pay or incentives are added
		$lists_income_taxable = get_payroll_mnt("incentive",array(
													"is_taxable" => "yes"
												),"name",true);
		$lists_deduction_taxable = get_payroll_mnt("deduction",array(
													"is_taxable" => "yes"
												),"name",true);
		// query from data existing data here
		$get_existing_data = $this->mdpayroll->get_emp_payroll_details($payrunID,$userID);

		if(empty($get_existing_data))
		{ 
			$government_deductions = get_employee_government_details($temp_emp_details['basic_pay'],$emp_basic_pay,$temp_emp_details['payment_type'],$temp_emp_details['gender_status'],0);
			foreach($government_deductions as $key=>$value)
			{
				if(is_array($value))
				{
					$deductions[$key] = number_format($value["share"],2);
				}
				else
				{
					$deductions[$key] = number_format($value,2);
				}
			}
		}
		else {
			foreach ($get_existing_data as $key => $value) {
				if ($value['pay_type']=="deduction") {
					$deductions[$value['mnt_di_id']] = $value['value'];
				}
				else {
					$earnings[$value['mnt_di_id']] = $value['value'];
				}
			}
		}

		$total_deductions 	= 0;
		$total_income		= 0;
		foreach ($earnings as $key => $value)
		{
			$total_income += remove_number_format($value);
		}
		foreach ($deductions as $key => $value)
		{
			$total_deductions += remove_number_format($value);
		}
		$this->data['pay_types']		= get_payroll_mnt("incentive",array(),array("key"=>"name","value"=>"description"),true,"all");
		$this->data['deductions'] 			= $deductions;
		$this->data['earnings']				= $earnings;
		$this->data['total_deductions']		= number_format($total_deductions,2);
		$this->data['total_income'] 		= number_format($total_income,2);
		$this->data['net_pay']				= number_format(get_netpay($total_income,$total_deductions),2);

		$this->data['payroll_payslip']		= $this->template->load("common","payroll/content/payroll_content_payslip",$this->data,TRUE,TRUE);

		$this->template->load("common","payroll/modals/review_payment",$this->data,FALSE,TRUE);
	}
	public function rpayment_review_update($payrun_id="",$user_id="")  // a modal
	{
		$payrunID 	= decrypt($payrun_id);
		$userID 	= decrypt($user_id);
		if(empty($payrunID) || empty($userID))
		{
			echo "not found";exit;
		}

		if($this->input->post())
		{
			$trigger 	= (isset($_GET['trigger']) && $_GET['trigger']=="incentive")? "incentive" : "";
			$type 		= (isset($_GET['type']) && $_GET['type']=="basic_pay")? "calculate" : "";

			//employee details
			$emp_details = get_employee_details($userID);

			//deductions and incomes
			$grossincome_taxable 	= 0;
			$deductable_taxable  	= 0;

			$total_deductions 		= 0;
			$total_income			= 0;

			$deductions = $this->input->post("deductions");
			$income 	= $this->input->post("incentives");

			//recalculate tax if basic pay or incentives are added
			$lists_income_taxable = get_payroll_mnt("incentive",array(
														"is_taxable" => "yes"
													),"name",true);
			$lists_deduction_taxable = get_payroll_mnt("deduction",array(
														"is_taxable" => "yes"
													),"name",true);

			//variables for saving to database
			$db_incentives = array();
			$db_deductions = array();

			$lists_income_taxable[] = "basic_pay";
			foreach($income as $key=>$value)
			{
				$temp = array(
										"company_id" 	=> get_company_id(),
										"payrun_id" 	=> $payrunID,
										"user_id"		=> $userID,
										"mnt_di_id"		=> $key,
										"value"			=> remove_number_format($value),
										"company_share"	=> 0,
										"status"		=> 0 //draft
									);
				if(in_array($key, $lists_income_taxable))
				{
					$grossincome_taxable = (float)remove_number_format($value) + $grossincome_taxable;
				}
				$total_income += (float)remove_number_format($value);

				$db_incentives[] = $temp;
			}
			foreach($deductions as $key=>$value)
			{
				// if($key == "income_tax")
				// {
				// 	continue;
				// }
				$temp = array(
										"company_id" 	=> get_company_id(),
										"payrun_id" 	=> $payrunID,
										"user_id"		=> $userID,
										"mnt_di_id"		=> $key,
										"value"			=> remove_number_format($value),
										"company_share"	=> 0,
										"status"		=> 0 //draft
									);
				//if basic pay is trigger, change the sss,etc
				// if($trigger=="incentive" && $type=="basic_pay")
				// {
				// 	$tempval = "";
				// 	$basicpay = $income['basic_pay'];
				// 	if($key=="sss")
				// 	{
				// 		$tempval	= getSSSContribution($income['basic_pay'],"employed",$emp_details['payment_type'],"all");
				// 	}
				// 	if($key=="philhealth")
				// 	{
				// 		$tempval    = getPhilhealthContribution($basicpay,$emp_details['payment_type'],"all");
				// 	}
				// 	if($key=="pagibig")
				// 	{
				// 		$tempval   = getPagibigContribution($basicpay);
				// 	}
				// 	if(!empty($tempval))
				// 	{
				// 		if(is_array($tempval))
				// 		{
				// 			$value = $tempval['share'];
				// 			$temp['company_share'] = $tempval['company'];
				// 			$deductions[$key] = $value;
				// 		}
				// 		else
				// 		{
				// 			$value = $tempval;
				// 			$deductions[$key] = $tempval;
				// 		}

				// 	}
				// }
				if(in_array($key, $lists_deduction_taxable))
				{
						$deductable_taxable = (float)remove_number_format($value) + $deductable_taxable;
				}
				$total_deductions = (float)remove_number_format($value) + $total_deductions;

				$db_incentives[] = $temp;
			}
			//calculate tax
			//$tax = getWithholdingTax($grossincome_taxable,$deductable_taxable,getCivilStatus($emp_details['gender_status']), $emp_details['payment_type']);
				// $db_deductions[] = array(
				// 						"company_id" 	=> get_company_id(),
				// 						"payrun_id" 	=> $payrunID,
				// 						"user_id"		=> $userID,
				// 						"mnt_di_id"		=> "income_tax",
				// 						"value"			=> remove_number_format($tax),
				// 						"company_share"	=> 0,
				// 						"status"		=> 0 //draft
				// 					);

			$data_to_save = array_merge($db_incentives,$db_deductions);

			$result = $this->mdpayroll->update_payrun_empdetails($payrunID,$data_to_save,$userID);

			if($result)
			{
				//$deductions['income_tax'] = $tax;
				//$total_deductions = $total_deductions+$tax;
				$this->template->set_error(0);
				$this->template->set_message("All changes has been saved.");
				$this->template->set_response_data(array(
														"incentives" 		=> $income,
														"deductions" 		=> $deductions,
														"total_earnings"	=> number_format($total_income,2),
														"total_deductions"	=> number_format($total_deductions,2),
														"total_netpay"		=> number_format(get_netpay($total_income,$total_deductions),2)
													));
			}
		}
		$this->template->response(FALSE,TRUE);

	}
	 /*
	  | @func        - generate()
	  | @description - save the employees payroll, save data if not exists
	  | @params      - no parameters but type is post
	  | @return - boolean
	  */
	 public function generate($payrunID="")
	 {
		  $payrun_id = decrypt($payrunID);
			$this->template->set_message("Please select an employee.");
			if(empty($payrun_id) && !is_admin())
			{
				return $this->template->response(FALSE,TRUE);
			}
	 		if($this->input->post())
			{
				 $this->form_validation->set_rules("employees[]","Employees","required");
				 if ($this->form_validation->run()) {
				 		$employees = $this->input->post("employees");
						//decryted employees
						$user_ids_not_exists_inpayroll =  array();
						$user_ids_exists_inpayroll = array();
						foreach ($employees as $key => $value) {
							 //decruption process
							 $try_decrypt = decrypt($value);
							 if (!empty($try_decrypt)) {
							 	 if(is_payrun_emp_exists($try_decrypt,$payrun_id))
								 {
									 $user_ids_exists_inpayroll[] = $try_decrypt;
								 }
								 else {
									 $user_ids_not_exists_inpayroll[] = $try_decrypt;
								 }
							 }
						}
						$return 		 = false;
						$return_generate = false; 
						if(!empty($user_ids_exists_inpayroll))
						{
							$return = $this->mdpayroll->set_emp_payroll_paid($payrun_id,$user_ids_exists_inpayroll);
						}
						if(!empty($user_ids_not_exists_inpayroll))
						{ 
							$return_generate = generate_default_payroll($user_ids_not_exists_inpayroll,$payrun_id,true); //generate if not exist and change automatic to paid
						}

						if($return || $return_generate)
						{
								$this->template->set_error(0);
								$this->template->set_message("Payroll has been successfully generated. Redirecting in a while ...");
						}
				 }
				 else {
				 		$this->template->set_message(validation_errors("",""));
				 }
			}
			$this->template->response(FALSE,TRUE);
	 }
	 /* END OF generate */

	public function payrun_message()  // a modal
	{
		$this->template->load("common","payroll/modals/message_payrun",$this->data,FALSE,TRUE);
	}

	//notes
	public function notes()
	{
		if(isset($_GET['id']) && $_GET['id']!="")
		{
			$id = $_GET['id'];
			$payrun_id = decrypt($id);
			$lists = $this->mdpayroll->get_notes($payrun_id);
			$this->data['notes']  = $lists;
			$this->data['target'] = $id;
			$this->data['comment_setting'] = false;
			$this->data['note_lists'] = $this->template->load("common","expenses/content/notes-list",$this->data,TRUE,TRUE);
			$this->template->load("common","expenses/modals/notes",$this->data,FALSE,TRUE);
		}
	}


     /*TEST*/
	public function tax_calculator()  // a modal
	{
		@$this->data['profile_widget']		= $this->template->load("common","profile/includes/inc-profile-widget",$this->data,TRUE,TRUE);
		$this->data['profile_content']		= $this->template->load("common","profile/includes/inc-profile-content",$this->data,TRUE,TRUE);
		$this->data['salary_dedecutions']	= $this->template->load("common","payroll/content/payroll_content_salary_deduction",$this->data,TRUE,TRUE);
		$this->data['payroll_payslip']		= $this->template->load("common","payroll/content/payroll_content_payslip",$this->data,TRUE,TRUE);

		$this->template->load("common","payroll/tax_calculator",$this->data);
	}
	/*** END OF PAYRUN EMPLOYEES ***/
	public function test()
	{
		$this->load->helper("taxcalculator");

		$basicpay = 15000;

		$sss = getSSSContribution($basicpay);
		$plh = getPhilhealthContribution($basicpay,"monthly","share");
		$pgb = getPagibigContribution($basicpay);

		//echo "sss =".$sss.", phl = ".$plh.", pgb = ".$pgb;
		$deductions = $sss + $plh + $pgb;

		echo getWithholdingTax($basicpay,$deductions,"S1/ME1", $period="monthly");

		gender_status_types();

	}
}
