<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	var $data = array();

	public function __construct()
	{
		parent::__construct();
		// set timezone
		$this->template->set_timezone();
		$this->template->check_if_not_logged_in();
		$this->load->helper("form");
		$this->template->set_scripts(base_url('assets/js/profile.js'));
		$this->load->model("mdprofile");
		$this->load->model("mdpayroll");
	}
	public function index()
	{
		$this->employee();
	}
	 /*
	  | @func        - user()
	  | @description - description
	  | @params      - param1,param2
	  | @return - boolean
	  */
	 public function employee($id="")
	 {
			 $check_id = decrypt($id);
			 $user_id = (empty($check_id))? user_id() : $check_id;
			 $this->data['id'] 	  = (empty($check_id))? user_id() : $check_id;
			 $this->data['widget'] = $this->template->load("common","profile/includes/inc-profile-widget",$this->data,TRUE,TRUE);
			 $this->data['content'] = $this->template->load("common","profile/includes/inc-profile-content",$this->data,TRUE,TRUE);
			 $this->data['my_payroll'] = $this->_payroll($this->data['id']);
			 $this->data['my_savings'] = $this->_savings($user_id);
			 $this->data['forms'] = "";//$this->template->load("common","profile/includes/inc-forms",$this->data,TRUE,TRUE);
			 $this->data['settings'] = $this->template->load("common","profile/includes/inc-settings",$this->data,TRUE,TRUE);
			 $this->template->load("common","profile/index",$this->data);
	 }
	 public function savings_list($id="")
	 {
	 	$check_id = decrypt($id);
		$user_id = (empty($check_id))? user_id() : $check_id;
		$this->_savings($user_id,false);
	 }
	 /* END OF employee */


	 /*
	  | @func        - employee()
	  | @description - description
	  | @params      - param1,param2
	  | @return - boolean
	  */
	 private function _payroll($id="")
	 {
		 	if(!is_admin() && $id!=user_id())
			{
				 echo "not found";
				 exit;
			}
			$return = $this->mdprofile->list_payroll($id);
			$data = array();
			$html_response = "";
			foreach($return as $key=>$value)
			{
				 $data 				 	= $value;
				 $data['enc_id']		= encrypt($id);
				 $data['payrun_id_enc'] = encrypt($value['payrun_id']);
				 $data['date'] 	= format_date($value['period_from'],"M. j, Y")." - ".format_date($value['period_to'],"M. j, Y");
				 $data['total']	= number_format($value['total_incentives'] + $value['total_deductions'],2);
				 $data['basic_pay'] = number_format($value['basic_pay'],2);
				 $data['total_deductions'] = number_format($value['total_deductions'],2);
				 $data['total_incentives'] = number_format($value['total_incentives'],2);
				 $data['payroll_date'] = format_date($value['payroll_date'],"M. j, Y");
				 $html_response .= $this->template->load("common","profile/content/payroll-lists",$data,TRUE,TRUE);
			}

			 $current_date = date("Y-m-d");
			 //15
			 $begin15  = date('Y-m-d',strtotime(date("Y-m")."-01"));
             $end15    = date('Y-m-d',strtotime(date("Y-m")."-15"));

             //end month
              $beginLast    = date('Y-m-d',strtotime(date("Y-m")."-16"));
              $endLast		= date("Y-m-t");

              $this->data['next_payday'] = "";
              if (($current_date > $begin15) && ($current_date < $end15))
              {
              		$this->data['next_payday'] = date("F j Y",strtotime($end15));
              }
              if (($current_date > $beginLast) && ($current_date < $endLast))
              {
              		$this->data['next_payday'] = date("F j Y",strtotime($endLast));
              }

			$this->data['payroll_lists'] = $html_response;
			$this->data['total_alltime_earning'] = number_format($this->mdprofile->earning_total("",$id),2);
			$this->data['total_year_earning'] = number_format($this->mdprofile->earning_total("",$id),2);
			$this->data['total_month_earning'] = number_format($this->mdprofile->earning_total("",$id),2);
	 		return $this->template->load("common","profile/includes/inc-mypayroll",$this->data,TRUE,TRUE);
	 }
	 /* END OF _payroll */
	 /*
	  | @func        - payslip()
	  | @description - payslip lists
	  | @params      - payrun id, user id
	  | @return - boolean
	  */
	 public function payslip($payrun_id="",$user_id="")
	 {
		 		$payrunID = decrypt($payrun_id);
	 			if(!empty($payrunID))
				{
					// use employee id if it is empty
					 $user_id = decrypt($user_id);
					 if(empty($user_id))
					 {
						 $user_id = user_id();
					 }
					 //return none if employee and not equal to current logged in employee
					 if(!is_admin() && $user_id!=user_id())
					 {
						  return "";
					 }
					 $this->data['company_details']		= get_company_details();
					 $this->data['get_employee_details']	= get_employee_details($user_id);
					 $this->data['payrun_details']		= $this->mdpayroll->get_payrun_details($payrunID);

					 //list of incentives or earnings
					 $incentives 											=	$this->mdprofile->payroll_details($payrunID,$user_id);
					 $total_earnings = 0;
					 $incentives_content = "";
					 if(!empty($incentives))
					 {
						 foreach($incentives as $key=>$value)
						 {
							 	$incentives_content .= $this->template->load("common","profile/content/payslip-content-earnings",$value,TRUE,TRUE);
								$total_earnings = $total_earnings + $value['value'];
						 }
					 }
					 //lists of deductions
					$deductions				= $this->mdprofile->payroll_details($payrunID,$user_id,"deduction");
					$total_deductions = 0;
					$deductions_content = "";
					if(!empty($deductions))
					{
						foreach($deductions as $key=>$value)
						{
							 $total_deductions = $total_deductions + $value['value'];
							 $deductions_content .= $this->template->load("common","profile/content/payslip-content-earnings",$value,TRUE,TRUE);
						}
					}
					$this->data['total_deductions'] = $total_deductions;
					$this->data['deduction_content'] = $deductions_content;

					$this->data['total_earnings']   = $total_earnings;
					$this->data['incentive_content'] = $incentives_content;

					$this->data['total_takehome'] = number_format($total_earnings - $total_deductions,2);

					 $this->template->load("common","profile/content/payslip-content",$this->data,FALSE,TRUE);
				}
	 }
	 /* END OF payslip */

	 private function _savings($user_id,$response=true)
	 {
	 	$this->load->model("mdsavings");
	 	$listings = array();

	 	$return_lists = $this->mdsavings->get($user_id);

	 	$savings = "";
	 	$encrypt_user_id = encrypt($user_id);
	 	foreach($return_lists as $value)
	 	{
	 		$amount 		= $value['value'];
	 		$value['total'] = number_format($amount,2);
	 		$value['amount'] = number_format($amount,2);
	 		$value['notes'] = (empty($value['notes']))? "---" : $value['notes'];
	 		$value['payroll_date'] = format_date($value['payroll_date'],"M. j, Y");
	 		if($value['type']=="deduction")
	 		{
	 			$value['type'] = "payroll - deposit";
	 		}
	 		if($value['type']=="incentive")
	 		{
	 			$value['type'] = "payroll - widthraw";
	 		}
	 		$value['user_id_enc'] = $encrypt_user_id;
	 		$savings .= $this->template->load("common","profile/content/savings-content-list",$value,TRUE,TRUE);
	 	}
	 	$this->data['mysavings_content'] = $savings;
	 	//$this->data['this_month'] = $this->mdsavings->get_total($user_id,"month");
	 	//$this->data['this_year']  = $this->mdsavings->get_total($user_id,"year");
	 	$this->data['overall_savings']	  = $this->mdsavings->get_total($user_id,"all");
	 	$this->data['user_id_enc'] = encrypt($user_id);
	 	return $this->template->load("common","profile/includes/inc-mysavings",$this->data,$response,TRUE);
	 }

	public function view_payslip() //mockups
	{
		$this->template->load("common","profile/content/payslip-content-mockup",$this->data,FALSE,TRUE);
	}
	public function create_form()  // a modal
	{
		$this->template->load("common","profile/modals/create-general-form",$this->data,FALSE,TRUE);
	}
		public function view_form()  // a modal
		{
			$this->template->load("common","profile/modals/view-general-form",$this->data,FALSE,TRUE);
		}
	public function create_leave_form()  // a modal
	{
		$this->template->load("common","profile/modals/create-leave-form",$this->data,FALSE,TRUE);
	}
		public function view_leave_form()  // a modal
		{
			$this->template->load("common","profile/modals/view-leave-form",$this->data,FALSE,TRUE);
		}
	public function edit()
	{
		// load js
		$this->template->set_scripts(base_url("assets/lib/jquery-input-mask/jquery.inputmask.bundle.min.js"));
		$this->template->set_scripts("http://maps.googleapis.com/maps/api/js?key=".$this->config->item("google_api_key")."&libraries=places");
		$this->template->set_scripts(base_url("assets/lib/jquery-geocomplete/jquery.geocomplete.min.js"));
		$this->template->set_styles(base_url("assets/lib/cropper/cropper.min.css"));
		$this->template->set_scripts(base_url("assets/lib/cropper/cropper.min.js"));

		$this->load->model("mduser");
		// get profile information
		$filters = array(
			"u_id" => user_id()
		);
		$result = $this->mduser->get_user_and_company($filters);
		if(!empty($result))
		{
			foreach ($result as $key => $value)
			{
				if($key=="id" || $key=="user_group_id")
				{
					$value = encrypt($value);
				}
				if($key=="description" && $value=="")
				{
					$value = "Some awesome quote goes here...";
				}
				if($key=="profile_pic")
				{
					$value = profile_picture($value);
				}
				$this->data[$key] = $value;
			}
			$this->data['personal_info_content'] = $this->template->load("common","profile/content/personal-info-content",$this->data,TRUE,TRUE);
			$this->data['contact_info_content'] = $this->template->load("common","profile/content/contact-info-content",$this->data,TRUE,TRUE);

			$this->data['notary_info_content'] = "";
			if(is_notary())
			{
				$this->data['notary_info_content'] = $this->template->load("common","profile/content/notary-info-content",$this->data,TRUE,TRUE);
			}

			$this->data['crop_modal'] = $this->template->load("common","modals/crop-picture-modal",$this->data,TRUE,TRUE);

			$this->template->load("common","profile/index",$this->data);
		}else
		{
			show_404();
		}
	}
	public function download_w9()
	{
		$this->load->helper("download");

		if(isset($_GET['file']) && isset($_GET['name']))
		{
			$filepath = urldecode($this->input->get("file"));
			$filename = urldecode($this->input->get("name"));

			if(file_exists("./uploads/w9/".$filepath))
			{
				$path  =  file_get_contents("./uploads/w9/".$filepath);
				force_download($filename,$path);
			}else
			{
				redirect(base_url()."profile/edit");
			}
		}
	}

	public function user_accounts()
	{
		$this->load->model("mduser");
		// get profile information
		$filters = array(
			"u_id" => user_id()
		);
		$select = "u.id,u.username";
		$result = $this->mduser->get_user($filters,$select);
		if(!empty($result))
		{
			foreach ($result as $key => $value)
			{
				if($key=="id")
				{
					$value = encrypt($value);
				}
				$this->data[$key] = $value;
			}
			$this->data['account_info_content'] = $this->template->load("common","profile/content/account-info-content",$this->data,TRUE,TRUE);

			$this->template->load("common","profile/user-account",$this->data);
		}else
		{
			show_404();
		}
	}

	// PERSONAL INFORMATION
	public function edit_personal_view($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$this->load->model("mduser");
			$filters = array(
				"u_id" => $id
			);
			$select = "u.id,u.name,u.address,u.description,u.profile_pic,u.website,u.timezone";
			$result = $this->mduser->get_user($filters,$select);
			if(!empty($result))
			{
				foreach ($result as $key => $value) {
					if($key=="id")
					{
						$value = encrypt($value);
					}
					if($key=="profile_pic")
					{
						$value = profile_picture($value);
					}
					if($key=="description" && $value=="")
					{
						$value = "Some awesome quote goes here...";
					}
					if($key=="timezone" && $value=="")
					{
						$value = "PST – Pacific Standard Time";
					}
					$this->data[$key] = $value;
				}
			}
			$this->template->load("common","profile/content/edit-personal-info",$this->data,FALSE,TRUE);
		}
	}
	public function personal_view($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$this->load->model("mduser");
			$filters = array(
				"u_id" => $id
			);
			$select = "u.id,u.name,u.address,u.description,u.profile_pic,u.timezone,u.website,ur.user_group_id,ug.name as user_type,ug.description as user_type_description,co.name as company_name";
			$result = $this->mduser->get_user_and_company($filters,$select);
			if(!empty($result))
			{
				foreach ($result as $key => $value) {
					if($key=="id" || $key=="user_group_id")
					{
						$value = encrypt($value);
					}
					if($key=="profile_pic")
					{
						$value = profile_picture($value);
					}
					if($key=="description" && $value=="")
					{
						$value = "Some awesome quote goes here...";
					}
					if($key=="timezone" && $value=="")
					{
						$value = "PST – Pacific Standard Time";
					}
					$this->data[$key] = $value;
				}
			}
			$this->template->load("common","profile/content/personal-info-content",$this->data,FALSE,TRUE);
		}
	}
	public function update_personal_info($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$this->load->model("mduser");
			// load library
			$this->load->library("form_validation");
			$this->form_validation->set_rules("name","Name","required");
			$this->form_validation->set_rules("longitude","Longitude","required");
			$this->form_validation->set_rules("latitude","Latitude","required");
			$this->form_validation->set_rules("state_short","State Short Name","required");
			$this->form_validation->set_rules("state","State","required");
			$this->form_validation->set_rules("address","Address","required");
			$this->form_validation->set_rules("description","Description","required");
			if($this->form_validation->run())
			{
				$timezone = "";

				// get timezone curl based on latitude and longitude
				$timezone_result = get_curl_json("https://maps.googleapis.com/maps/api/timezone/json?location=".$this->input->post("latitude").",".$this->input->post("longitude")."&timestamp=".strtotime(date("Y-m-d H:i:s")));
				$timezone_result = (array)$timezone_result;
				if(!empty($timezone_result))
				{
					if($timezone_result['status']=="OK")
					{
						$timezone = get_correct_timezone($timezone_result['timeZoneName']);
					}else
					{
						return $this->template->set_message("Cannot get timezone. Please try again.");
					}
				}

				$filters = array(
					"id" => $id
				);
				$data = array(
					"name" => ucwords($this->input->post("name")),
					"description" => ucfirst($this->input->post("description")),
					"longitude" => $this->input->post("longitude"),
					"latitude" => $this->input->post("latitude"),
					"state" => $this->input->post("state"),
					"address" => $this->input->post("address"),
					"timezone" => $timezone,
					"address" => $this->input->post("address"),
					"website" => $this->input->post("website")
				);

				$locality = $this->input->post("locality");

				if($locality!="")
				{
					$data['city'] = $this->input->post("locality").", ".$this->input->post("state_short");
				}else
				{
					$data['city'] = $this->input->post("state").", ".$this->input->post("state_short");
				}

				// check if there is profile file upload
				$image_url = $this->input->post("profile_picture");
				if(isset($image_url) && !empty($image_url))
				{
					$filename = random_string(40);
					//save image
		            $img_trimmed = substr(explode(";",$image_url)[1], 7);
		            $image_path = './uploads/profile/'.$filename.'.png';
		            file_put_contents($image_path, base64_decode($img_trimmed));
		            if(file_exists($image_path))
            		{
            			$data['profile_pic'] = $filename.".png";
            		}else{
            			return $this->template->set_message("<p>Could not create image file. Please try another one.</p>");
            		}
	            }
				$response = $this->mduser->edit_user($filters, $data);
				if($response)
				{
					$offset = $this->mduser->get_timezone_offset(array("name" => $timezone));
					$this->session->set_userdata("timezone", get_timezone($timezone));
					$this->session->set_userdata("timezone_offset", intval($offset));
				}
				$this->template->evaluate($response, 2);
			}else
			{
				$this->template->set_message(validation_errors("<p>","</p>"));
			}
		}
		$this->template->response(false);
	}

	// CONTACT INFORMATION
	public function edit_contact_view($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$this->load->model("mduser");
			$filters = array(
				"u_id" => $id
			);
			$select = "u.id,u.email,u.home_phone,u.office_phone,u.cell_phone,u.efax";
			$result = $this->mduser->get_user($filters,$select);
			if(!empty($result))
			{
				foreach ($result as $key => $value) {
					if($key=="id")
					{
						$value = encrypt($value);
					}
					$this->data[$key] = $value;
				}
			}
			$this->template->load("common","profile/content/edit-contact-info",$this->data,FALSE,TRUE);
		}
	}
	public function contact_view($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$this->load->model("mduser");
			$filters = array(
				"u_id" => $id
			);
			$select = "u.id,u.email,u.home_phone,u.office_phone,u.cell_phone,u.efax";
			$result = $this->mduser->get_user($filters,$select);
			if(!empty($result))
			{
				foreach ($result as $key => $value) {
					if($key=="id")
					{
						$value = encrypt($value);
					}
					$this->data[$key] = $value;
				}
			}
			$this->template->load("common","profile/content/contact-info-content",$this->data,FALSE,TRUE);
		}
	}
	public function update_contact_info($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$this->load->model("mduser");
			// load library
			$this->load->library("form_validation");

			$original_email = $this->input->post("original_email");
			$email = $this->input->post("email");

			if($email != $original_email) {
			   $is_unique =  '|is_unique[users.email]';
			}else
			{
			   $is_unique =  '';
			}

			$this->form_validation->set_rules("email","Email","required|valid_email".$is_unique,array("is_unique" => "The %s already exists. Please choose another one."));
			if($this->form_validation->run())
			{
				$filters = array(
					"id" => $id
				);
				$data = array(
					"email" => $this->input->post("email"),
					"home_phone" => $this->input->post("home_phone"),
					"office_phone" => $this->input->post("office_phone"),
					"cell_phone" => $this->input->post("cell_phone"),
					"efax" => $this->input->post("efax")
				);
				$response = $this->mduser->edit_user($filters, $data);
				$this->template->evaluate($response, 2);
			}else
			{
				$this->template->set_message(validation_errors("<p>","</p>"));
			}
		}
		$this->template->response(false);
	}

	// NOTARY INFORMATION
	public function update_notary_info($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$this->load->model("mduser");
			// load library
			$this->load->library("form_validation");

			$eo_insurance = $this->input->post("eo_insurance");
			$commission = $this->input->post("commission");
			$w9_form_file = $this->input->post("w9_form_file");

			if($eo_insurance!="")
			{
				$this->form_validation->set_rules("eo_insurance","E&O Insurance","greater_than[0]");
			}
			if($commission!="")
			{
				$this->form_validation->set_rules("commission","Commmission","greater_than[0]");
			}

			if($eo_insurance=="" && $commission=="")
			{
				$filters = array(
					"id" => $id
				);

				$data = array(
					"eo_insurance" => $this->input->post("eo_insurance"),
					"commission" => $this->input->post("commission")
				);

				if(isset($_FILES['w9_form']))
				{
					$config['upload_path']          = './uploads/w9/';
	                $config['allowed_types']        = 'pdf|docx|doc|docs';
	                $config['encrypt_name']			= TRUE;

	                $this->load->library('upload', $config);

	                if($this->upload->do_upload('w9_form'))
	                {
	                	$filedata = $this->upload->data();
	                	$data['w9_form'] = $filedata['orig_name'];
	                	$data['w9_form_file'] = $filedata['file_name'];
	                }
	                else
	                {
	                	return $this->template->set_message("<p>".strip_tags($this->upload->display_errors())."</p>");
	                }
				}

				$response = $this->mduser->edit_user($filters, $data);
				if($response)
				{
					if($w9_form_file!="" && isset($data['w9_form']))
					{
						if(file_exists("./uploads/w9/".$w9_form_file))
						{
							unlink("./uploads/w9/".$w9_form_file);
						}
					}
				}
				$this->template->evaluate($response, 2);
			}else
			{
				if($this->form_validation->run())
				{
					$filters = array(
						"id" => $id
					);
					$data = array(
						"eo_insurance" => $this->input->post("eo_insurance"),
						"commission" => $this->input->post("commission")
					);

					if(isset($_FILES['w9_form']))
					{
						$config['upload_path']          = './uploads/w9/';
		                $config['allowed_types']        = 'pdf|docx|doc|docs';
		                $config['encrypt_name']			= TRUE;

		                $this->load->library('upload', $config);

		                if($this->upload->do_upload('w9_form'))
		                {
		                	$filedata = $this->upload->data();
		                	$data['w9_form'] = $filedata['orig_name'];
		                	$data['w9_form_file'] = $filedata['file_name'];
		                }
		                else
		                {
		                	return $this->template->set_message("<p>".strip_tags($this->upload->display_errors())."</p>");
		                }
					}

					$response = $this->mduser->edit_user($filters, $data);
					if($response)
					{
						if($w9_form_file!="" && isset($data['w9_form']))
						{
							if(file_exists("./uploads/w9/".$w9_form_file))
							{
								unlink("./uploads/w9/".$w9_form_file);
							}
						}
					}
					$this->template->evaluate($response, 2);
				}else
				{
					$this->template->set_message(validation_errors("<p>","</p>"));
				}
			}
		}
		$this->template->response(false);
	}
	public function edit_notary_view($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$this->load->model("mduser");
			$filters = array(
				"u_id" => $id
			);
			$select = "u.id,u.w9_form,u.w9_form_file,u.eo_insurance,u.commission";
			$result = $this->mduser->get_user($filters,$select);
			if(!empty($result))
			{
				foreach ($result as $key => $value) {
					if($key=="id")
					{
						$value = encrypt($value);
					}
					$this->data[$key] = $value;
				}
			}
			$this->template->load("common","profile/content/edit-notary-info",$this->data,FALSE,TRUE);
		}
	}
	public function notary_view($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$this->load->model("mduser");
			$filters = array(
				"u_id" => $id
			);
			$select = "u.id,u.w9_form,u.w9_form_file,u.eo_insurance,u.commission";
			$result = $this->mduser->get_user($filters,$select);
			if(!empty($result))
			{
				foreach ($result as $key => $value) {
					if($key=="id")
					{
						$value = encrypt($value);
					}
					$this->data[$key] = $value;
				}
			}
			$this->template->load("common","profile/content/notary-info-content",$this->data,FALSE,TRUE);
		}
	}

	// USER ACCOUNTS
	public function edit_username_view($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$this->load->model("mduser");
			$filters = array(
				"u_id" => $id
			);
			$select = "u.id,u.username";
			$result = $this->mduser->get_user($filters,$select);
			if(!empty($result))
			{
				foreach ($result as $key => $value) {
					if($key=="id")
					{
						$value = encrypt($value);
					}
					$this->data[$key] = $value;
				}
			}
			$this->template->load("common","profile/content/edit-username-info",$this->data,FALSE,TRUE);
		}
	}
	public function account_view($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$this->load->model("mduser");
			$filters = array(
				"u_id" => $id
			);
			$select = "u.id,u.username";
			$result = $this->mduser->get_user($filters,$select);
			if(!empty($result))
			{
				foreach ($result as $key => $value) {
					if($key=="id")
					{
						$value = encrypt($value);
					}
					$this->data[$key] = $value;
				}
			}
			$this->template->load("common","profile/content/account-info-content",$this->data,FALSE,TRUE);
		}
	}
	public function update_username_info($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$this->load->model("mduser");
			// load library
			$this->load->library("form_validation");

			$original_username = $this->input->post("original_username");
			$username = $this->input->post("username");

			if($username != $original_username) {
			   $is_unique =  '|is_unique[users.username]';
			}else
			{
			   $is_unique =  '';
			}

			$this->form_validation->set_rules("username","Username","required|min_length[4]".$is_unique,array("is_unique" => "The %s already exists. Please choose another one."));
			if($this->form_validation->run())
			{
				$filters = array(
					"id" => $id
				);
				$data = array(
					"username" => $this->input->post("username")
				);
				$response = $this->mduser->edit_user($filters, $data);
				$this->template->evaluate($response, 2);
			}else
			{
				$this->template->set_message(validation_errors("<p>","</p>"));
			}
		}
		$this->template->response(false);
	}
	public function change_password_view($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$this->data['id'] = $enc_id;

			$this->template->load("common","profile/content/change-password",$this->data,FALSE,TRUE);
		}
	}
	public function update_basic_info($enc_id="")
	{
		$id = decrypt($enc_id);
		if(empty($id))
		{
			$id = user_id();
		}
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$this->load->model("mduser");
			// load library
			$this->load->library("form_validation");
			$user_info      = get_user_info($id);
			if($user_info['username']!=$this->input->post("username"))
			{
				$this->form_validation->set_rules("username","Username","required|valid_email|callback_is_email_check",array("is_email_check" => "The %s is already exists. Please choose another one."));
			}
			$this->form_validation->set_rules("firstname","Firstname","required");
			$this->form_validation->set_rules("lastname","Lastname","required");
			if($this->form_validation->run())
			{
				$data = array(
							"username" => $this->input->post("username"),
							"firstname" => $this->input->post("firstname"),
							"lastname" => $this->input->post("lastname")
						);
				$response = $this->mduser->update_basic($id, $data);
				$this->template->evaluate($response, 2);
			}else
			{
				$this->template->set_message(validation_errors("<p>","</p>"));
			}
		}
		$this->template->response(false,true);
	}
	public function change_password($enc_id="")
	{
		$id = decrypt($enc_id);
		if(empty($id))
		{
			$id = user_id();
		}
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$this->load->model("mduser");
			// load library
			$this->load->library("form_validation");

			$this->form_validation->set_rules("new_password","New Password","required|min_length[8]");
			if($this->form_validation->run())
			{
				$response = $this->mduser->change_password($id, $this->input->post("new_password"));
				$this->template->evaluate($response, 2);
			}else
			{
				$this->template->set_message(validation_errors("<p>","</p>"));
			}
		}
		$this->template->response(false,true);
	}
	public function details($user_id="")
	{
		$userID 	= decrypt($user_id);
		if(empty($userID))
		{
			echo "not found";exit;
		}
		$this->data['active_tab'] = "profile";
		if(isset($_GET['active_tab']) && $_GET['active_tab']=="savings")
		{
			$this->data['active_tab'] = "savings";
		}
		$this->load->model("mduser");
		$userinfo = $this->mduser->get_user_info(array("user_id"=>$userID));
		$this->data['fullname']				= $userinfo['firstname']." ".$userinfo['lastname'];
		$this->data['user_id']		= $user_id;
		$this->data['id']			= $userID; //user decryted id
		$this->data['profile_widget']		= $this->template->load("common","profile/includes/inc-profile-widget",$this->data,TRUE,TRUE);
		$this->data['profile_content']		= $this->template->load("common","profile/includes/inc-profile-content",$this->data,TRUE,TRUE);
		$this->data['my_savings'] 			= $this->_savings($userID);
		$this->template->load("common","profile/modals/view-profile",$this->data,FALSE,TRUE);
	}
	/*call back email*/
	public function is_email_check($email)
    {
    	$response = true;
    	$company_id = $this->input->post("company_id");
		$decrypt_company_id = decrypt($company_id);
		if(empty($decrypt_company_id))
		{
			$decrypt_company_id = get_company_id();
		}
        $this->db->where("email",$email);
         $this->db->where("company_id",$decrypt_company_id);
        $query = $this->db->get(USERS)->num_rows();
        if($query>0)
        {
        	$response = false;
        }
        return $response;
    }
}
