<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Savings extends CI_Controller 
{
	var $data = array();
	public function __construct()
	{
		parent::__construct(); 
		$this->template->check_if_not_logged_in();
		$this->load->helper("form");
		$this->load->library("form_validation");
		$this->load->model("mdsavings");
	}
	public function add($employee="")
	{
		if(!is_admin())
		{
			return redirect(base_url());
		}
		$this->data['employee_id'] = $employee;

		if($this->input->post())
		{
			$user_id = decrypt($employee);
			if(empty($user_id)){return false;}

			$this->form_validation->set_rules("date","Date","required");
			$this->form_validation->set_rules("value","Amount","required|numeric");
			$this->form_validation->set_rules("type","Type","required"); 
			if($this->form_validation->run())
			{
				$date 		= $this->input->post("date");
				$amount 	= $this->input->post("value");
				$type 		= $this->input->post("type");
				$notes 		= $this->input->post("notes");
				$company	= get_company_id();

				$raw_data = array(
								"date_added" 	=> date("Y-m-d H:i:s",strtotime($date)),
								"value"	=> $amount,
								"type"	=> $type,
								"notes" => $notes,
								"company_id" => $company,
								"user_id" => $user_id,
								"addedby" => user_id()
							);

				$response = $this->mdsavings->add($raw_data);
				if($response)
				{
					$this->template->set_error(0);
					$this->template->set_message("Transaction successfully added.");
				}

			}
			else
			{
				$this->template->set_message(validation_errors("<span>","</span>"));
			}

			return $this->template->response(false,true);
		}

		$this->template->load("common","profile/modals/add-savings",$this->data,FALSE,TRUE);
	}
	public function delete($employee="",$savings_id="")
	{
		if(!is_admin())
		{
			return redirect(base_url());
		}
		if(!empty($employee) && !empty($savings_id))
		{
			$user_id = decrypt($employee);

			$response = $this->mdsavings->delete($user_id,$savings_id);
			if($response)
			{
				$this->template->set_error(0);
				$this->template->set_message("Transaction successfully removed.");
			}
		}
		$this->template->response(false,true);
	}
}