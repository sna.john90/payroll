<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scripts extends CI_Controller 
{
	var $data = array();
	public function __construct()
	{
		parent::__construct();
		// set timezone
		$this->template->set_timezone();
		//$this->template->check_if_not_logged_in();

	}
	public function index()
	{
		show_404();
	} 
	private function get_latlang($lat,$lang)
	{
		$address = "";
		if($lat!="" AND $lang!="")
		{
			$url = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCnjmsF1r9djkmO2S3fLHh9Ysbc7CIvjog&latlng={$lat},{$lang}";
			$data = get_curl_json($url);
			if(isset($data->results[0]->formatted_address))
			{
				$address = $data->results[0]->formatted_address;
			}
		}
		return $address;
	}
	// run in cli
	//   php  <path index.php> <controller> <method> params
	// example :
	//   php index.php scripts addnotaries "hello"
	public function addnotaries()
	{
		//disable access from any browser client
		if(isset($_SERVER['HTTP_USER_AGENT']) AND $_SERVER['HTTP_USER_AGENT']!="")
		{
			echo "not found.";
			exit;
		}
		date_default_timezone_set("America/New_York");
		$notaries = array();
		$states = array(
			"AL",
			"AK",
			"AZ",
			"AR",
			"CA",
			"CO",
			"CT",
			"DE",
			"FL",
			"GA",
			"HI",
			"ID",
			"IL",
			"IN",
			"IA",
			"KS",
			"KY",
			"LA",
			"ME",
			"MD",
			"MA",
			"MI",
			"MN",
			"MS",
			"MO",
			"MT",
			"NE",
			"NV",
			"NH",
			"NJ",
			"NM",
			"NY",
			"NC",
			"ND",
			"OH",
			"OK",
			"OR",
			"PA",
			"RI",
			"SC",
			"SD",
			"TN",
			"TX",
			"UT",
			"VT",
			"VA",
			"WA",
			"WV",
			"WI",
			"WY");
		foreach($states as $value)
		{
			$data 			=  file_get_contents(FCPATH."uploads/notary/{$value}.json");
			$json_encode 	= json_decode($data,TRUE);
			if(isset($json_encode['Results']) AND !empty($json_encode['Results']))
			{
				foreach($json_encode['Results'] as $datares)
				{
					$notaries[] = array(
									"name"				=> $datares['FullName'],
									"description"		=> $datares['Description'],
									"username"			=> $datares['DefaultEmail'],	 		
									"email"				=> $datares['DefaultEmail'],
									"password"			=> password_hash("notary2016", PASSWORD_BCRYPT),
									"created_at"		=> date("Y-m-d H:i:s"),		
									"address"			=> $this->get_latlang($datares['Latitude'],$datares['Longitude']),	
									"city"				=> $datares['City'],", ".$datares['State'],
									"longitude"			=> $datares['Longitude'],	
									"latitude"			=> $datares['Latitude'],	
									"state"				=> $datares['State'], 
									"home_phone"		=> $datares['DefaultPhone'],		
									"office_phone"		=> $datares['DefaultPhone'],		
									"cell_phone"		=> $datares['DefaultPhone'],  	
									"status"			=> 1,	
									"auto"				=> 1
								); 
				}
				if(!empty($notaries))
				{
					$this->load->model("mdscripts");
					$response = $this->mdscripts->import_data($notaries);
					if($response['total'] > 0 || $response['duplicate'] > 0)
					{	
						echo "<br/>Successfully saved<br/>";
						echo "Total Saved: ".$response['total']."<br/>";
						echo "Total Duplicates: ".$response['duplicate'];
					}
				} 
			} 
		}
		
	}
	// public function import_data()
	// {
	// 	$csv = array_map('str_getcsv', file(FCPATH.'uploads/states/wyoming.csv'));
		
	// 	$fields = $csv[0];
	// 	$data 	= array();
	// 	foreach ($csv as $key => $value) 
	// 	{
	// 		$temp = array(

	// 				);
	// 		if($key!=0)
	// 		{
	// 			foreach ($value as $skey => $svalue) 
	// 			{
	// 				if($skey>6)
	// 				{
	// 					if($fields[$skey]=="city")
	// 					{
	// 						$svalue = str_replace("- ","",$svalue);
	// 					}
	// 					if($fields[$skey]=="address")
	// 					{
	// 						$svalue = str_replace("Available Upon Request","",$svalue);
	// 						$svalue = str_replace("Ask for address please. ","",$svalue);
	// 					}

	// 					$svalue = preg_replace('/[\x00-\x08\x0B\x0C\x0E-\x1F\x80-\x9F]/u', ' ', $svalue);
	// 					$svalue = preg_replace('!\s+!', ' ', $svalue);
	// 					$key_value = preg_replace('/[\x00-\x08\x0B\x0C\x0E-\x1F\x80-\x9F]/u', ' ', $fields[$skey]);
	// 					$key_value = str_replace("\xA0", ' ', $key_value);
	// 					$key_value = preg_replace('/[[:cntrl:]]+/','',$key_value);
	// 					$key_value = trim($key_value);

	// 					$temp[$key_value] = (trim($svalue)!="null")? trim($svalue) : "";
	// 				}
	// 			}

	// 			if(isset($temp['email']))
	// 			{
	// 				$temp['username'] 	= $temp['email'];
	// 			}else
	// 			{
	// 				$temp['username'] 	= str_replace(" ", ".",strtolower($temp['name']));
	// 			}
				
	// 			$temp['created_at']	= date("Y-m-d H:i:s");
	// 			$temp['status']		= 1;
	// 			$temp['state'] 		= "Wyoming";
	// 			$temp['auto']		= 1;
	// 			$temp['created_at'] = date("Y-m-d H:i:s");
	// 			$temp['updated_at'] = date("Y-m-d H:i:s");

	// 			// get latitude and longitude
	// 			if(isset($temp['address']) && $temp['address']!="")
	// 			{
	// 				$url = "https://maps.google.com/maps/api/geocode/json?address=".urlencode($temp['address'])."&key=AIzaSyC5l9KAo1_hm_GA8N6aUV1fqJXcn2rHwS4&region=US";
	// 			}else
	// 			{
	// 				$url = "https://maps.google.com/maps/api/geocode/json?address=".urlencode($temp['city'])."&key=AIzaSyC5l9KAo1_hm_GA8N6aUV1fqJXcn2rHwS4&region=US";
	// 			}
			
	// 			$location = get_curl_json($url);
	// 			echo $url;
	// 			echo $temp['name'];
	// 			var_dump($location);
	// 			if(isset($location->results))
	// 			{
	// 				foreach ($location->results as $key2 => $value2) {
	// 					$temp["latitude"] = $value2->geometry->location->lat;
	// 					$temp["longitude"] = $value2->geometry->location->lng;
	// 					break;
	// 				}
	// 			}

	// 			$data[] = $temp;
	// 		}
	// 	}
	// 	print_me($data);
	// 	echo "Total Data: ".count($data);

	// 	if(!empty($data))
	// 	{
	// 		$this->load->model("mdscripts");
	// 		$response = $this->mdscripts->import_data($data);
	// 		if($response['total'] > 0 || $response['duplicate'] > 0)
	// 		{	
	// 			echo "<br/>Successfully saved<br/>";
	// 			echo "Total Saved: ".$response['total']."<br/>";
	// 			echo "Total Duplicates: ".$response['duplicate'];
	// 		}
	// 	}
	// }

	// public function show_data()
	// {
	// 	$csv = array_map('str_getcsv', file(FCPATH.'uploads/states/wyoming.csv'));
		
	// 	$fields = $csv[0];
	// 	$data 	= array();
	// 	foreach ($csv as $key => $value) 
	// 	{
	// 		$temp = array(

	// 				);
	// 		if($key!=0)
	// 		{
	// 			foreach ($value as $skey => $svalue) 
	// 			{
	// 				if($skey>6)
	// 				{
	// 					if($fields[$skey]=="city")
	// 					{
	// 						$svalue = str_replace("- ","",$svalue);
	// 					}
	// 					if($fields[$skey]=="address")
	// 					{
	// 						$svalue = str_replace("Available Upon Request","",$svalue);
	// 						$svalue = str_replace("Ask for address please. ","",$svalue);
	// 					}

	// 					$svalue = preg_replace('/[\x00-\x08\x0B\x0C\x0E-\x1F\x80-\x9F]/u', ' ', $svalue);
	// 					$svalue = preg_replace('!\s+!', ' ', $svalue);
	// 					$key_value = preg_replace('/[\x00-\x08\x0B\x0C\x0E-\x1F\x80-\x9F]/u', ' ', $fields[$skey]);
	// 					$key_value = str_replace("\xA0", ' ', $key_value);
	// 					$key_value = preg_replace('/[[:cntrl:]]+/','',$key_value);
	// 					$key_value = trim($key_value);

	// 					$temp[$key_value] = (trim($svalue)!="null")? trim($svalue) : "";
	// 				}
	// 			}

	// 			if(isset($temp['email']))
	// 			{
	// 				$temp['username'] 	= $temp['email'];
	// 			}else
	// 			{
	// 				$temp['username'] 	= str_replace(" ", ".",strtolower($temp['name']));
	// 			}
				
	// 			$temp['created_at']	= date("Y-m-d H:i:s");
	// 			$temp['status']		= 1;
	// 			$temp['state'] 		= "Wyoming";
	// 			$temp['auto']		= 1;
	// 			$temp['created_at'] = date("Y-m-d H:i:s");
	// 			$temp['updated_at'] = date("Y-m-d H:i:s");

	// 			$data[] = $temp;
	// 		}
	// 	}
	// 	print_me($data);
	// }

	// update tracking
	public function update_tracking_information()
	{
		if($this->input->is_ajax_request())
		{
			session_write_close();
			
			$this->load->model("mdschedule");
			$this->load->library("track");
			$accessKey = "CqCePr9Jf1H04it0";
			$password = "jf3n6phXhZ7Ha8sgTq6UYYEFv";
			$acctNum = '510087526';
			$meterNum = '118740004';

			$this->track->initialize($accessKey, $password, $acctNum, $meterNum);

			$filters = array(
				"notary_status_id" 			=> 3,
				"tracking_number_not_null" 	=> 1,
				"is_delivered"				=> 0
			);
			if(is_loan_officer() || is_lender_admin() || is_processor() || is_underwriter() || is_guest())
			{
				$filters["lender_id"] = get_company_id();
			}
			$select ="sn.id,tracking_number,shipment_type";

			$result = $this->mdschedule->listing_schedule_notary($filters,$select);

			$data_to_insert = array();

			if(!empty($result))
			{
				foreach($result as $keys => $values)
				{
					if($values['shipment_type']=="FedEx")
					{
						try {
							$track = $this->track->getByTrackingId($values['tracking_number']);

							if(is_array($track->CompletedTrackDetails->TrackDetails))
							{
								foreach ($track->CompletedTrackDetails->TrackDetails as $track_key => $track_value) {
									if($track_value->Notification->Severity=="SUCCESS")
									{
										if(is_array($track_value->DatesOrTimes))
										{
											foreach ($track_value->DatesOrTimes as $key => $value)
											{
												if($value->Type=="SHIP")
												{
													$temp = array(
														"id" => $values['id'],
														"is_delivered" => 0,
														"estimated_delivery_date" => date("Y-m-d H:i:s",strtotime('+3 days',strtotime($value->DateOrTimestamp)))
													);
													$data_to_insert[] = $temp;
													break;
												}
												if($value->Type=="ACTUAL_DELIVERY" || $value->Type=="ESTIMATED_DELIVERY")
												{
													$temp = array(
														"id" => $values['id'],
														"is_delivered" => 0,
														"estimated_delivery_date" => date("Y-m-d H:i:s",strtotime($value->DateOrTimestamp))
													);
													if($value->Type=="ACTUAL_DELIVERY")
													{
														$temp['is_delivered'] = 1;
													}
													$data_to_insert[] = $temp;
													break;
												}
											}
										}else
										{
											if($track_value->DatesOrTimes->Type=="ESTIMATED_DELIVERY" || $track_value->DatesOrTimes->Type=="ACTUAL_DELIVERY")
											{
												$temp = array(
													"id" => $values['id'],
													"is_delivered" => 0,
													"estimated_delivery_date" => date("Y-m-d H:i:s",strtotime($track_value->DatesOrTimes->DateOrTimestamp))
												);
												if($track_value->DatesOrTimes->Type=="ACTUAL_DELIVERY")
												{
													$temp['is_delivered'] = 1;
												}
												$data_to_insert[] = $temp;
											}
											if($track_value->DatesOrTimes->Type=="SHIP")
											{
												$temp = array(
													"id" => $values['id'],
													"is_delivered" => 0,
													"estimated_delivery_date" => date("Y-m-d H:i:s",strtotime('+3 days',strtotime($track_value->DatesOrTimes->DateOrTimestamp)))
												);
												$data_to_insert[] = $temp;
											}
										}
									}
									break;
								}
							}else
							{
								if(isset($track->CompletedTrackDetails->TrackDetails->Notification->Severity))
								{
									if($track->CompletedTrackDetails->TrackDetails->Notification->Severity=="SUCCESS")
									{
										if(isset($track->CompletedTrackDetails->TrackDetails->DatesOrTimes))
										{
											if(is_array($track->CompletedTrackDetails->TrackDetails->DatesOrTimes))
											{
												foreach ($track->CompletedTrackDetails->TrackDetails->DatesOrTimes as $key => $value)
												{
													if($value->Type=="SHIP")
													{
														$temp = array(
															"id" => $values['id'],
															"estimated_delivery_date" => date("Y-m-d H:i:s",strtotime('+3 days',strtotime($value->DateOrTimestamp)))
														);
														$data_to_insert[] = $temp;
														break;
													}
													if($value->Type=="ACTUAL_DELIVERY" || $value->Type=="ESTIMATED_DELIVERY")
													{
														$temp = array(
															"id" => $values['id'],
															"estimated_delivery_date" => date("Y-m-d H:i:s",strtotime($value->DateOrTimestamp))
														);
														if($value->Type=="ACTUAL_DELIVERY")
														{
															$temp['is_delivered'] = 1;
														}
														$data_to_insert[] = $temp;
														break;
													}
												}
											}else
											{
												if($track->CompletedTrackDetails->TrackDetails->DatesOrTimes->Type=="ESTIMATED_DELIVERY" || $track->CompletedTrackDetails->TrackDetails->DatesOrTimes->Type=="ACTUAL_DELIVERY")
												{
													$temp = array(
														"id" => $values['id'],
														"estimated_delivery_date" => date("Y-m-d H:i:s",strtotime($track->CompletedTrackDetails->TrackDetails->DatesOrTimes->DateOrTimestamp))
													);
													if($track->CompletedTrackDetails->TrackDetails->DatesOrTimes->Type=="ACTUAL_DELIVERY")
													{
														$temp['is_delivered'] = 1;
													}
													$data_to_insert[] = $temp;
												}
												if($track->CompletedTrackDetails->TrackDetails->DatesOrTimes->Type=="SHIP")
												{
													$temp = array(
														"id" => $values['id'],
														"estimated_delivery_date" => date("Y-m-d H:i:s",strtotime('+3 days',strtotime($track->CompletedTrackDetails->TrackDetails->DatesOrTimes->DateOrTimestamp)))
													);
													$data_to_insert[] = $temp;
												}
											}
										}
									}	    	
								}
							}
						}catch (Exception $e)
						{
							return $this->template->set_message($e);
						}
					}
				}
				if(!empty($data_to_insert))
				{
					$response = $this->mdschedule->update_schedule_notary_batch($filters,$data_to_insert,"id");
					$this->template->evaluate($response,2);
				}
			}
		}
		$this->template->response(false);
	}

	public function transfer_payment()
	{
		$this->load->model(array("mdschedule"));
		$filters = array(
			"request_notary_service" => 1
		);
		$result = $this->mdschedule->listing_schedule_notary($filters,"schedule_id,compensation_rate");
		$data_to_insert = array();
		if(!empty($result))
		{
			foreach($result as $key => $value) 
			{
				$temp = array(
					"schedule_id" => $value["schedule_id"],
					"name" => "Compensation Rate",
					"amount" => $value["compensation_rate"],
					"date_added" => date("Y-m-d H:i:s")
				);
				$data_to_insert[] = $temp;
			}
			print_me($data_to_insert);
			if(!empty($data_to_insert))
			{
				$response = $this->mdschedule->add_schedule_payment_batch($data_to_insert);
			}
		}
	}

	// public function testing()
	// {
	// 	$this->load->model("mduser");

	// 	$filters = array(
	// 		'user_group_id' => 5,
	// 	);
	// 	$select = "u.id,username,email,password";
	// 	$page = 1;
	// 	$result = $this->mduser->user_lists2($filters,$select,$page,20);
	// 	$data_to_update = array();
	// 	if(!empty($result))
	// 	{
	// 		foreach ($result['result'] as $key => $value) {
	// 			$temp = array(
	// 				"id" => $value['id'],
	// 				"username" => $value['email'],
	// 				"password" => password_hash("notary2016", PASSWORD_BCRYPT)
	// 			);
	// 			$data_to_update[] = $temp;
	// 		}
	// 		print_me($data_to_update);
	// 		echo "Page: ".$page."<br/>";
	// 		echo "Total Pages: ".$result['total_pages']."<br/>";
	// 		echo "Total Count: ".$result['total_count']."<br/>";
	// 		if(!empty($data_to_update))
	// 		{
	// 			$response = $this->mduser->update_user_batch($data_to_update,"id");
	// 			echo "Response: ".$response;
	// 		}
	// 	}
	// }
}