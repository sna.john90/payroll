<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller 
{
	var $data = array();

	public function __construct()
	{
		parent::__construct();
		$this->template->set_scripts(base_url('assets/js/settings.js'));
		
		$this->template->check_if_not_logged_in();
		$this->load->helper("form");
		$this->load->library("form_validation");
	}

	public function index()
	{	 
	}

	public function add_special_role_view()
	{
		if($this->input->is_ajax_request())
		{
			$this->data['company_id'] = encrypt(get_company_id());

			$this->template->load("common","settings/modals/add-user-special-role",$this->data,FALSE,TRUE);
		}
	}
	public function add_special_role()
	{
		$this->load->model("mduser");
		// load library
		$this->load->library("form_validation");
		if($this->input->is_ajax_request())
		{
			$this->form_validation->set_rules("user_id","User ID","required");
			$this->form_validation->set_rules("user_group_id","User Group ID","required");
			if($this->form_validation->run())
			{
				$data = array(
					"user_id" => decrypt($this->input->post("user_id")),
					"user_group_id" => $this->input->post("user_group_id"),
					"date_added" => date("Y-m-d H:i:s")
				);
				$response = $this->mduser->add_special_role($data);
				$this->template->evaluate($response,1);
			}else
			{
				$this->template->set_message(validation_errors("<p>","</p>"));
			}
		}
		$this->template->response(false);
	}
	public function remove_special_role($enc_id)
	{
		$this->load->model("mduser");
		
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$filters = array(
				"id" => $id
			);
			$response = $this->mduser->remove_special_role($filters);
			$this->template->evaluate($response,3);
		}
		$this->template->response(false);
	}

	public function user_roles_listing()
	{
		$this->load->model("mduser");

		if($this->input->is_ajax_request())
		{	
			$datatable = $this->input->get();

			$filters = array();

			if(is_lender_admin())
			{
				$filters['lending_company_id'] = get_company_id();
			}

			$column = array(
				"order_by_name",
				"order_by_special_roles",
				"order_by_date_added"
			);
			$filters[$column[$datatable["order"][0]["column"]]] = $datatable["order"][0]["dir"];
			$result = $this->mduser->user_special_role_listing($filters);
			if($result['totalCount']>0)
			{
				foreach ($result['result'] as $key => $value)
				{
					$value['user_id'] 				= encrypt($value['id']);
					$value['user_special_role_id']	= encrypt($value['user_special_role_id']);
					$value['special_role']			= $value['group_description'];
					$value['profile_pic']			= profile_picture($value['profile_pic']);
					
					$value["name"]					= '<img class="img-responsive pull-left pr15" src="'.$value['profile_pic'].'" width="48" /><a href="javascript:void(0);">'.$value['name'].'</a>';
					$value['date_added']            = date("F d, Y h:i A",strtotime($value['date_added']));

	                $value["actions"] = '<div class="btn-groupX btn-group-xs text-center"><a href="javascript:void(0)" class="btn btn-danger remove-role-btn ml5" data-toggle="tooltip" data-id="'.$value['user_special_role_id'].'" title="Delete"><i class="fa fa-trash-o "></i></a></div>';
				
					$response_data['data'][] = $value;
				}
			}

			$response_data['draw'] = $datatable['draw'];
			$response_data['recordsFiltered'] = $result['totalCount'];
			$response_data['recordsTotal'] = $result['totalCount'];
		}
		$this->template->array_to_json($response_data);
	}

	public function department()
	{
		if(!is_admin())
		{
			redirect(base_url("profile"),'refresh');
		}  
		$this->load->model("mdsettings");
		$lists = get_departments();
		$this->data['lists'] = $lists;
		$this->template->load("common","settings/index-department",$this->data); 
	}
	public function addDepartment()
	{
		if(!is_admin())
		{
			redirect(base_url("profile"),'refresh');
		}  
		if($this->input->post())
		{
			$this->load->model("mdsettings");
			$this->form_validation->set_rules("name","Name","required");
			$this->form_validation->set_rules("description","Description","required");
			$this->form_validation->set_rules("parent_id","Parent","required");
			if($this->form_validation->run())
			{
				$name 				= $this->input->post("name");
				$estimated_amount 	= $this->input->post("description");
				$parent 			= $this->input->post("parent_id");  
				$company_id 		= get_company_id();
				$user_id			= user_id();

				$raw_data = array(
								"name" 				=> $name,
								"description"	=> $estimated_amount,
								"parent_id"			=> $parent,
								"company_id"		=> $company_id 
							);

				$result = $this->mdsettings->add_department($raw_data);
				$this->template->evaluate($result,1); 
			}
			else 
			{
				$this->template->set_message(validation_errors("",""));
			}
			$this->template->response(false,true);exit;
		}
		$this->template->load("common","settings/modals/add-department",$this->data,FALSE,TRUE);
	}
	public function updateDepartment()
	{
		if(!is_admin())
		{
			redirect(base_url("profile"),'refresh');
		}  
		$this->load->model("mdsettings");

		$id = "";
		if(isset($_GET['id']))
		{
			$id = decrypt($_GET['id']);
		}
		if(!empty($id))
		{
			$result = $this->mdsettings->get_department(0,$id);
			$final_data = array(
								"name" => "",
								"description" => "",
								"parent_id" => 0
							);
		//	print_r($result);
			if(!empty($result))
			{
				$final_data = $result[0];
			}
			$this->data = $final_data;
			$this->data['user_id'] = $_GET['id'];
			$this->template->load("common","settings/modals/update-department",$this->data,FALSE,TRUE);
		}
	}
	public function doUpdateDepartment()
	{
		if(!is_admin())
		{
			redirect(base_url("profile"),'refresh');
		}  
		if($this->input->post())
		{
			$this->load->model("mdsettings");
			$this->form_validation->set_rules("name","Name","required");
			$this->form_validation->set_rules("description","Description","required");
			$this->form_validation->set_rules("parent_id","Parent","required");
			$this->form_validation->set_rules("id","ID","required");
			if($this->form_validation->run())
			{
				$id 				= $this->input->post("id");
				$dId 				= decrypt($id);
				if(empty($dId))
				{
					echo "not allowed";exit;
				}
				$name 				= $this->input->post("name");
				$description 		= $this->input->post("description");
				$parent 			= $this->input->post("parent_id");   

				$raw_data = array(
								"name" 				=> $name,
								"description"		=> $description,
								"parent_id"			=> $parent
							);

				$result = $this->mdsettings->update_department($raw_data,$dId);
				if($result)
				{
					$this->template->set_error(0);
					$this->template->set_message("Successfully updated.");
				}
			}
			else 
			{
				$this->template->set_message(validation_errors("",""));
			}
			$this->template->response(false,true);exit;
		}
	}
	public function deleteDepartment()
	{
		if(!is_admin())
		{
			redirect(base_url("profile"),'refresh');
		}  
		if(isset($_GET['id']))
		{
			$id = decrypt($_GET['id']);
			if(!empty($id))
			{
				$this->load->model("mdsettings");
				$response = $this->mdsettings->delete_department($id);
				if($response)
				{
					$this->template->set_error(0);
					$this->template->set_message("Successfully deleted.");
				}
			}
		}
		$this->template->response(false,true);
	}

	//payroll maintainables
	public function payroll()
	{
		if(!is_admin())
		{
			redirect(base_url("profile"),'refresh');
		}  
		$this->load->model("mdsettings");
		$lists = $this->mdsettings->get_payrollmnt();
		$this->data['lists'] = $lists;
		$this->template->load("common","settings/index-payrollmnt",$this->data); 
	}
}