<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	var $data = array();

	public function __construct()
	{
		parent::__construct();
		// set timezone
	//	$this->template->set_timezone();
		$this->template->check_if_not_logged_in();

		$this->load->model(array("mduser"));
	}

	public function index()
	{
		// redirect if super admin
		if(!is_admin())
		{
			redirect(base_url("profile"),'refresh');
		}
		// load js
		$this->template->set_styles(base_url('assets/lib/selectize/selectize.css'));
		$this->template->set_scripts(base_url('assets/lib/selectize/selectize.min.js'));
		$this->template->set_scripts(base_url('assets/lib/selectize/plugins/no_results.js'));
		$this->template->set_scripts(base_url('assets/lib/typeahead/typeahead.bundle.js'));
		$this->template->set_scripts(base_url('assets/lib/typeahead/handlebars.js'));

		$this->template->set_styles(base_url('assets/lib/jquery-nice-select/nice-select.css'));
		$this->template->set_scripts(base_url('assets/lib/jquery-nice-select/jquery.nice-select.min.js'));

		$this->template->set_scripts(base_url("assets/lib/jquery-input-mask/jquery.inputmask.bundle.min.js"));
		$this->template->set_scripts("http://maps.googleapis.com/maps/api/js?key=".$this->config->item("google_api_key")."&libraries=places");
		$this->template->set_scripts(base_url("assets/lib/jquery-geocomplete/jquery.geocomplete.min.js"));
		$this->template->set_scripts(base_url("assets/lib/pgenerator-master/pgenerator.jquery.js"));
		$this->template->set_styles(base_url("assets/lib/cropper/cropper.min.css"));
		$this->template->set_scripts(base_url("assets/lib/cropper/cropper.min.js"));

		if(is_lender_admin())
		{
			$this->load->helper("form");

			$this->template->set_scripts(base_url('assets/js/user2.js'));
		}else
		{
			$this->template->set_scripts(base_url('assets/js/user-view.js'));
		}

		$this->data['crop_modal'] = $this->template->load("common","modals/crop-picture-modal",$this->data,TRUE,TRUE);

		$this->load->model("mdcompany");
		$filters = array(
			"lcm_user_id" => user_id()
		);
		$this->data['companies'] = $this->mdcompany->company_members_list($filters);
		
		$this->data['company_id'] = encrypt(get_company_id());
		
		$this->template->load("common","users",$this->data);
	}

	public function users_lists($company_id,$search_key="")
	{
		$filters = array(
			"company_id" => decrypt($company_id),
			"search_name" => urldecode($search_key)
		);
		if(isset($_GET['user_group']))
		{
			$filters['user_group_id'] = (int)$_GET['user_group'];
		}
		$response = $this->mduser->user_lists($filters,$start=0,$limit=200);
		$data = array();
		if(!empty($response))
		{
			foreach ($response as $key => $value)
			{
				$value['profile_pic'] = profile_picture($value['profile_pic']);
				$value['id'] = encrypt($value['id']);
				$value['user_group_id'] = encrypt($value['user_group_id']);
				$data[] = $value;
			}
		}
		$this->template->set_error(0);
		$this->template->set_message("Users Lists");
		$this->template->set_response_data($data);
		$this->template->response(false);
	}

	public function add_existing_user()
	{
		if($this->input->is_ajax_request())
		{
			$this->load->library("form_validation");

			$this->form_validation->set_rules("id","User","required");
			$this->form_validation->set_rules("company_id","Company ID","required");

			if($this->form_validation->run())
			{
				$user_id  = decrypt($this->input->post("id"));
				$company_id = decrypt($this->input->post("company_id"));
				$data = array(
					"lending_company_id" => $company_id,
					"user_id" => $user_id,
					"date_added" => date("Y-m-d H:i:s")
				);
				$response = $this->mduser->add_lending_company_members($data);
				$this->template->evaluate($response,1);
			}else
			{
				$this->template->set_message(validation_errors("<p>","</p>"));
			}
		}
		$this->template->response(false);
	}

	public function users_only_lists($search_key="")
	{
		$filters = array(
			"user_group_id" => 1,
			"search_name" => urldecode($search_key),
			"order_by_name" => "ASC"
		);
		if(isset($_GET['user_group']))
		{
			$filters['user_group_id'] = (int)$_GET['user_group'];
		}
		$response = $this->mduser->user_only_lists($filters,$start=0,$limit=200);
		$data = array();
		if(!empty($response))
		{
			foreach ($response as $key => $value)
			{
				$value['profile_pic'] = profile_picture($value['profile_pic']);
				$value['id'] = encrypt($value['id']);
				$value['user_group_id'] = encrypt($value['user_group_id']);
				$data[] = $value;
			}
		}
		$this->template->set_error(0);
		$this->template->set_message("Users Only Lists");
		$this->template->set_response_data($data);
		$this->template->response(false);
	}

	public function get_company_users($enc_company_id)
	{
		$data = array();
		$company_id = decrypt($enc_company_id);
		if(!empty($company_id))
		{
			$user_ids= $this->mduser->user_company_only_lists($company_id);
			$filters = array(
				"wherein_user_group_id" => array(1,2,3,4,6,7,8),
				"search_name_and_email" => urldecode($_GET['search']),
				"order_by_name" => "ASC",
				"not_lending_company_id" => $company_id,
				"not_wherein_user_id" => $user_ids
			);
			$response = $this->mduser->user_company_lists($filters,$start=0,$limit=200);
			$data = array();
			if(!empty($response))
			{
				foreach ($response as $key => $value)
				{
					$value['profile_pic'] = profile_picture($value['profile_pic']);
					$value['id'] = encrypt($value['id']);
					$value['user_group_id'] = encrypt($value['user_group_id']);
					$data[] = $value;
				}
			}
			$this->template->set_error(0);
			$this->template->set_message("Company Users Lists");
			$this->template->set_response_data($data);
		}
		$this->template->response(false);
	}

	public function get_user_info($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$filters = array(
				"u_id" 			=> $id
			);
			$result = $this->mduser->get_user_and_company($filters);
			if(!empty($result))
			{
				$this->data['id'] = encrypt($result["id"]);
				$this->data['name'] = $result['name'];
				$this->data['description'] = $result['description'];
				$this->data['company_name'] = $result['company_name'];
				$this->data['email'] = $result['email'];
				$this->data['address'] = $result['address'];
				$this->data['cell_phone'] = $result['cell_phone'];
				$this->data['profile_pic'] = profile_picture($result['profile_pic']);
				$this->data['work_phone'] = $result['office_phone'];
			}
			$this->template->load("common","modals/user-details",$this->data,FALSE,TRUE);
		}
	}

	public function notary_lists($enc_schedule_id)
	{
		$schedule_id = decrypt($enc_schedule_id);
		$response_data = array(
			"data" => array(),
			"draw" => 1,
			"recordsFiltered" => 0,
			"recordsTotal" => 0
		);
		if(!empty($schedule_id) && $this->input->is_ajax_request())
		{	
			$datatable = $this->input->get();
			$start = $datatable['start'];
			$limit = $datatable['length'];
			$filters = array(
				"search_notary2" => $datatable['search']['value']
			);
			// if isset filters
			if(isset($_GET['filters']))
			{
				$temp = json_decode($this->input->get('filters'));
                foreach ($temp as $key => $value) {
                    $filters[$key] = $value;
                }
			}
			$filters['by_distance'] = array(
	        	"lat"   => $_GET['lat'],
	        	"long"  => $_GET['lng'],
	        	"distance" => $_GET['distance']
         	);
			$column = array(
				"order_by_favorite",
				"order_by_name",
				"order_by_status",
				"order_by_distance",
				"order_by_address"
			);
			$filters[$column[$datatable["order"][0]["column"]]] = $datatable["order"][0]["dir"];
			if(isset($filters['order_by_distance']))
			{
				$filters['order_by_distance'] = array(
					"has_distance" => ($_GET['lat']!=="" && $_GET['lng']!==""),
					"order" => $filters['order_by_distance']
				);
			}
			$filters['nf_user_id'] = user_id();
			
			$result = $this->mduser->notary_lists($filters,$start,$limit);
			if($result['totalCount']>0)
			{
				foreach ($result['result'] as $key => $value)
				{
					$value['id'] 					= encrypt($value['id']);
					$value['profile_pic']			= profile_picture($value['profile_pic']);

					if($value['notary_favorite_id']!=NULL)
					{
						$value['favorite'] = '<p class="text-center"><i class="favorite favorited fa fa-heart"></i></p>';
					}else
					{
						$value['favorite'] = '<p class="text-center"><i class="favorite fa fa-heart"></i></p>';
					}

					if($value['address']=="")
					{
						$value['address'] = $value['city'];
					}

					if($value['status']==2)
					{
						$value['certified']				= '<p class="text-center"><i class="certified certified-yes fa fa-certificate"></i></p>';
					}else
					{
						$value['certified']				= '<p class="text-center"><i class="certified fa fa-certificate"></i></p>';
					}

					$value['name']					= '<img class="img-responsive pull-left pr15" src="'.$value['profile_pic'].'" width="48" /><a href="javascript:void(0);" data-toggle="notary-info" data-id="'.$value['id'].'">'.$value['name'].'</a>';
					
					if(isset($value['distance']))
					{
						$value['locations']			= '<p class="text-center">'.number_format(round($value['distance'],1),1,'.',',').'</p>';
					}else
					{
						$value['locations']			= '<p class="text-center">---</p>';
					}
					
					$value['signings']				= '<p class="text-center">0</p>';
					
					$value['assign']				= '<a href="javascript:void(0)" class="btn btn-primary btn-sm assign-notary-btn" data-id="'.$value['id'].'">Assign</a>';

					$response_data['data'][] = $value;
				}
			}
			$response_data['draw'] = $datatable['draw'];
			$response_data['recordsFiltered'] = $result['totalCount'];
			$response_data['recordsTotal'] = $result['totalCount'];
		}
		$this->template->array_to_json($response_data);
	}
}