<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	var $data = array();

	public function __construct()
	{
		parent::__construct(); 
		$this->template->check_if_not_logged_in();
		// load helper
		$this->load->helper("form");
		$this->load->model(array("mdadmin"));
		
		$this->template->set_styles(base_url('assets/css/admin.css'));
	}

	public function index()
	{
		if(is_employee())
		{
			redirect(base_url("profile"),"refresh");
		}
	}

	public function status()
	{
		if(!is_admin())
		{
			redirect(base_url());
		}
		$this->template->set_scripts(base_url('assets/js/status.js'));
		// load bootstrap colorpicker
		$this->template->set_styles(base_url('assets/lib/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css'));
		$this->template->set_scripts(base_url('assets/lib/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js'));

		$this->template->load("common","admin/status",$this->data);
	}

	public function admin_status_listing()
	{
		$response_data = array(
			"data" => array(),
			"draw" => 1,
			"recordsFiltered" => 0,
			"recordsTotal" => 0
		);
		if($this->input->is_ajax_request())
		{	
			$datatable = $this->input->get();
			$start = $datatable['start'];
			$limit = $datatable['length'];
			$filters = array(
				"search_status_and_label" => $datatable['search']['value']
			);
			$column = array("order_by_status","order_by_label");
			$filters[$column[$datatable["order"][0]["column"]]] = $datatable["order"][0]["dir"];
			$result = $this->mdadmin->admin_status_listing($filters,$start,$limit);
			if($result['totalFiltered']>0)
			{
				foreach ($result['result'] as $key => $value) {
					$value['id'] = encrypt($value['id']);
					$value['color'] = '<span class="btn btn-default" style="background-color:'.$value['color'].';border-color:'.$value['color'].'"></span>';
					$value['action'] = '<div class="btn-groupX btn-group-xs"><a href="javascript:void(0)" class="btn btn-success edit-admin-status mr3" data-id="'.$value['id'].'" data-toggle="modal" title="Edit" data-target="#modalProfile"><i class="fa fa-pencil"></i></a><a href="javascript:void(0)" class="btn btn-danger remove-admin-status" data-id="'.$value['id'].'" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o "></i></a></div>';
					$response_data['data'][] = $value;
				}
			}
			$response_data['draw'] = $datatable['draw'];
			$response_data['recordsFiltered'] = $result['totalFiltered'];
			$response_data['recordsTotal'] = $result['totalCount'];
		}
		$this->template->array_to_json($response_data);
	}
	public function notary_status_listing()
	{
		$response_data = array(
			"data" => array(),
			"draw" => 1,
			"recordsFiltered" => 0,
			"recordsTotal" => 0
		);
		if($this->input->is_ajax_request())
		{	
			$datatable = $this->input->get();
			$start = $datatable['start'];
			$limit = $datatable['length'];
			$filters = array(
				"search_status_and_label" => $datatable['search']['value']
			);
			$column = array("order_by_status","order_by_label");
			$filters[$column[$datatable["order"][0]["column"]]] = $datatable["order"][0]["dir"];
			$result = $this->mdadmin->notary_status_listing($filters,$start,$limit);
			if($result['totalFiltered']>0)
			{
				foreach ($result['result'] as $key => $value) {
					$value['id'] = encrypt($value['id']);
					$value['color'] = '<span class="btn btn-default" style="background-color:'.$value['color'].';border-color:'.$value['color'].'"></span>';
					$value['action'] = '<div class="btn-groupX btn-group-xs"><a href="javascript:void(0)" class="btn btn-success edit-notary-status mr3" data-id="'.$value['id'].'" data-toggle="modal" title="Edit" data-target="#modalProfile"><i class="fa fa-pencil"></i></a><a href="javascript:void(0)" class="btn btn-danger remove-notary-status" data-id="'.$value['id'].'" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o "></i></a></div>';
					$response_data['data'][] = $value;
				}
			}
			$response_data['draw'] = $datatable['draw'];
			$response_data['recordsFiltered'] = $result['totalFiltered'];
			$response_data['recordsTotal'] = $result['totalCount'];
		}
		$this->template->array_to_json($response_data);
	}

	public function add_admin_status_view()
	{
		if($this->input->is_ajax_request())
		{
			$this->template->load("common","admin/modals/add_admin_status",$this->data,FALSE,TRUE);
		}
	}
	public function edit_admin_status_view($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$filters = array(
				"id" => $id
			);
			$result = $this->mdadmin->get_admin_status($filters);
			if(!empty($result))
			{
				$this->data['id'] 	= $enc_id;
				$this->data['status'] = $result['status'];
				$this->data['label'] = $result['label'];
				$this->data['color'] = $result['color'];
			}
			$this->template->load("common","admin/modals/edit_admin_status",$this->data,FALSE,TRUE);
		}
	}

	public function add_admin_status()
	{	
		// load library
		$this->load->library("form_validation");
		if($this->input->is_ajax_request())
		{
			$this->form_validation->set_rules("status","Status","required");
			$this->form_validation->set_rules("label","Label","required");
			$this->form_validation->set_rules("color","Color","required");
			if($this->form_validation->run())
			{
				$data = array(
					"status" => $this->input->post("status"),
					"label" => $this->input->post("label"),
					"color" => $this->input->post("color"),
					"border_color" => get_hex_color($this->input->post("color"),-0.8)
				);
				$response = $this->mdadmin->add_admin_status($data);
				$this->template->evaluate($response,1);
			}else
			{
				$this->template->set_message(validation_errors("<p>","</p>"));
			}
		}
		$this->template->response(false);
	}
	public function update_admin_status($enc_id)
	{	
		$id = decrypt($enc_id);
		// load library
		$this->load->library("form_validation");
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$original_status = $this->input->post("original_status");
			$status = $this->input->post("status");

			if($status != $original_status) {
			   $is_unique =  '|is_unique[admin_status.status]';
			}else
			{
			   $is_unique =  '';
			}

			$this->form_validation->set_rules("status","Status","required");
			$this->form_validation->set_rules("label","Label","required");
			$this->form_validation->set_rules("color","Color","required");
			if($this->form_validation->run())
			{
				$data = array(
					"status" => $this->input->post("status"),
					"label" => $this->input->post("label"),
					"color" => $this->input->post("color"),
					"border_color" => get_hex_color($this->input->post("color"),-0.8)
				);
				$response = $this->mdadmin->update_admin_status($data,array("id"=>$id));
				$this->template->evaluate($response,2);
			}else
			{
				$this->template->set_message(validation_errors("<p>","</p>"));
			}
		}
		$this->template->response(false);
	}
	public function remove_admin_status($enc_id)
	{	
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$response = $this->mdadmin->remove_admin_status(array("id"=>$id));
			$this->template->evaluate($response,3);
		}
		$this->template->response(false);
	}

	public function add_notary_status_view()
	{
		if($this->input->is_ajax_request())
		{
			$this->template->load("common","admin/modals/add_notary_status",$this->data,FALSE,TRUE);
		}
	}
	public function edit_notary_status_view($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$filters = array(
				"id" => $id
			);
			$result = $this->mdadmin->get_notary_status($filters);
			if(!empty($result))
			{
				$this->data['id'] 	= $enc_id;
				$this->data['status'] = $result['status'];
				$this->data['label'] = $result['label'];
				$this->data['color'] = $result['color'];
			}
			$this->template->load("common","admin/modals/edit_notary_status",$this->data,FALSE,TRUE);
		}
	}

	public function add_notary_status()
	{	
		// load library
		$this->load->library("form_validation");
		if($this->input->is_ajax_request())
		{
			$this->form_validation->set_rules("status","Status","required|is_unique[notary_status.status]",array("is_unique" => "%s must be unique."));
			$this->form_validation->set_rules("label","label","required");
			$this->form_validation->set_rules("color","Color","required");
			if($this->form_validation->run())
			{
				$data = array(
					"status" => $this->input->post("status"),
					"label" => $this->input->post("label"),
					"color" => $this->input->post("color"),
					"border_color" => get_hex_color($this->input->post("color"),-0.8)
				);
				$response = $this->mdadmin->add_notary_status($data);
				$this->template->evaluate($response,1);
			}else
			{
				$this->template->set_message(validation_errors("<p>","</p>"));
			}
		}
		$this->template->response(false);
	}
	public function update_notary_status($enc_id)
	{	
		$id = decrypt($enc_id);
		// load library
		$this->load->library("form_validation");
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$original_status = $this->input->post("original_status");
			$status = $this->input->post("status");

			if($status != $original_status) {
			   $is_unique =  '|is_unique[notary_status.status]';
			}else
			{
			   $is_unique =  '';
			}

			$this->form_validation->set_rules("status","Status","required".$is_unique,array("is_unique" => "%s must be unique."));
			$this->form_validation->set_rules("label","Label","required");
			$this->form_validation->set_rules("color","Color","required");
			if($this->form_validation->run())
			{
				$data = array(
					"status" => $this->input->post("status"),
					"label" => $this->input->post("label"),
					"color" => $this->input->post("color"),
					"border_color" => get_hex_color($this->input->post("color"),-0.8)
				);
				$response = $this->mdadmin->update_notary_status($data,array("id"=>$id));
				$this->template->evaluate($response,2);
			}else
			{
				$this->template->set_message(validation_errors("<p>","</p>"));
			}
		}
		$this->template->response(false);
	}
	public function remove_notary_status($enc_id)
	{	
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$response = $this->mdadmin->remove_notary_status(array("id"=>$id));
			$this->template->evaluate($response,3);
		}
		$this->template->response(false);
	}

}
