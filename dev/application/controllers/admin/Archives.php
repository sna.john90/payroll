<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Archives extends CI_Controller {

	var $data = array();

	public function __construct()
	{
		parent::__construct();
		$this->template->check_if_not_logged_in();
		$this->template->set_timezone();
		
		$this->load->helper(array("form"));
		// load models
		$this->load->model(array("mdschedule","mdadmin","mdcommon"));

		$this->template->set_styles(base_url('assets/lib/selectize/selectize.css'));
		$this->template->set_scripts(base_url('assets/lib/selectize/selectize.min.js'));
		$this->template->set_scripts(base_url('assets/lib/selectize/plugins/no_results.js'));

		$this->template->set_scripts(base_url("assets/lib/jquery-input-mask/jquery.inputmask.bundle.min.js"));
		$this->template->set_scripts("http://maps.googleapis.com/maps/api/js?key=".$this->config->item("google_api_key")."&libraries=places");
		$this->template->set_scripts(base_url("assets/lib/jquery-geocomplete/jquery.geocomplete.min.js"));

		$this->template->set_scripts(base_url("assets/lib/tmpl/tmpl.min.js"));
		$this->template->set_scripts(base_url("assets/lib/file-upload/jquery.ui.widget.js"));
		// The Iframe Transport is required for browsers without support for XHR file uploads
		$this->template->set_scripts(base_url("assets/lib/file-upload/jquery.iframe-transport.js"));
		// The basic File Upload plugin
		$this->template->set_scripts(base_url("assets/lib/file-upload/jquery.fileupload.js"));
		// The File Upload processing plugin
		$this->template->set_scripts(base_url("assets/lib/file-upload/jquery.fileupload-process.js"));
		// The File Upload validation plugin
		$this->template->set_scripts(base_url("assets/lib/file-upload/jquery.fileupload-validate.js"));
		// The File Upload user interface plugin
		$this->template->set_scripts(base_url("assets/lib/file-upload/jquery.fileupload-ui.js"));
	}

	public function schedules()
	{
		$this->load->model("mdcompany");

		$this->template->set_styles(base_url('assets/lib/jquery-nice-select/nice-select.css'));
		$this->template->set_scripts(base_url('assets/lib/jquery-nice-select/jquery.nice-select.min.js'));

		$this->template->set_scripts(base_url("assets/js/admin-archives.js"));
		$this->template->set_scripts(base_url("assets/js/notification.js"));

		// redirect if not admin
		if(!is_admin())
		{
			redirect(base_url()."past/signings",'refresh');
		}
		
		if(is_super_admin())
		{
			$filters = array(
				"order_by_name" => "ASC"
			);
			$this->data['companies'] = $this->mdcompany->company_list($filters);
		}else
		{
			$filters = array(
				"lcm_user_id" => user_id(),
				"order_by_name" => "ASC"
			);
			$this->data['companies'] = $this->mdcompany->company_members_list($filters);
		}

		$this->template->load("common","admin/archives",$this->data);
	}

	// datatable listing
	public function not_scheduled_listing()
	{
		if(is_admin())
		{
			// folder listing
			$folder_listing 		= $this->mdcommon->get_folder_listing();
			// admin listing
			$admin_status_listing 	= $this->mdadmin->get_admin_status_lists();
			// schedule status listing
			$schedule_status_listing 	= $this->mdcommon->schedule_status_listing();
		}

		$response_data = array(
			"data" => array(),
			"draw" => 1,
			"recordsFiltered" => 0,
			"recordsTotal" => 0
		);
		if($this->input->is_ajax_request())
		{	
			$datatable = $this->input->get();
			$start = $datatable['start'];
			$limit = $datatable['length'];
			$filters = array(
				"search_not_archive_schedule" => $datatable['search']['value'],
				"date_scheduled_timezone" => 1,
				"not_scheduled" => 1,
				"is_deleted" => 0
			);
			// if isset filters
			if(isset($_GET['filters']))
			{
				$temp = json_decode($this->input->get('filters'));
                foreach ($temp as $key => $value) {
                	if($key=="lender_id")
                	{
                		$value = decrypt($value);
                	}
                    $filters[$key] = $value;
                }
			}

			$filters['date_scheduled_past'] = 1;

			$column = array(
				"order_by_date_scheduled",
				"order_by_time",
				"order_by_borrower_name",
				"order_by_title_order_number",
				"order_by_folder_name",
				"order_by_schedule_status",
				"order_by_admin_status_importance"
			);
			$filters[$column[$datatable["order"][0]["column"]]] = $datatable["order"][0]["dir"];
			$result = $this->mdschedule->not_scheduled_listing($filters,$start,$limit);
			if($result['totalCount']>0)
			{
				foreach ($result['result'] as $key => $value)
				{
					$value['id'] 								= encrypt($value['id']);
					$value['due_today'] 						= false;
					$value['date_scheduled_label'] 				= date("F d, Y",strtotime($value['date_scheduled_timezone']));
					$value['time_label'] 						= date("h:i A",strtotime($value['date_scheduled_timezone']));

					$location 							= explode(", ", $value['property_location']);
					$value['property_state']            = $location[0];
					if($location[1]=="SC" || $location[1]=="GA" || $location[1]=="CT" || $location[1]=="MA")
					{
						$value['property_state'] 	    = "<strong>".$location[0]."</strong>";
					}

					$value['borrower']	 				= '<a href="javascript:void(0)" data-toggle="loan-detail" data-id="'.$value['id'].'">'.$value['borrower_name'].'</a><div class="font-small muted">'.$value['property_state'].'</div>';
					if($value['request_notary_service']==1)
					{
						$value['borrower']	 			= '<a class="font-bold" href="javascript:void(0)" tooltip title="Notary Service Requested" data-toggle="loan-detail" data-id="'.$value['id'].'">'.$value['borrower_name'].'</a><div class="font-small muted">'.$value['property_state'].'</div>';
					}

					$schedule_id               					= $value['id'];
					$loan_officer_id							= $value['loan_officer_id'];
					$admin_status_id  							= $value['admin_status_id'];
					$admin_status  								= $value['admin_status'];
					$admin_status_label  						= $value['admin_status_label'];
					$admin_status_color 						= $value['admin_status_color'];
					$admin_status_border_color  				= $value['admin_status_border_color'];
					$schedule_status_id  						= $value['schedule_status_id'];
					$schedule_status_name  						= $value['schedule_status_name'];
					$folder_id									= $value['folder_id'];
					$folder_name								= $value['folder_name'];
					$request_notary_service 					= $value['request_notary_service'];

					$is_scheduled								= true;

					// STATUS
					if(is_lender_admin() || is_underwriter() || is_admin() || ( (is_loan_officer() && $admin_status!="Docs Drawn") && (is_loan_officer() && user_id()==$loan_officer_id) ) )
					{
						$value['status'] = '<div class="btn-status-col dropdown" data-toggle="tooltip" title="';
						if($admin_status_id==0)
						{
							$value['status'] .= 'Select Status" style="width:160px;">';
							$value['status'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-default"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Status<span class="caret pull-right"></span></a>';
							$value['status'] .= '<ul class="dropdown-menu pull-right">';
						}else
						{
							$value['status'] .= $admin_status_label.'" style="width:160px;">';
							$value['status'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color:'.$admin_status_color.';border-color:'.$admin_status_border_color.';color:#fff;">'.$admin_status_label.'<span class="caret pull-right"></span></a>';
							$value['status'] .= '<ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)" class="select-admin-status" data-id="0" data-status="None" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:#555;"></span> &nbsp;None</a></li>';
						}
						foreach ($admin_status_listing as $status_key => $status_value) {
							if( ((($status_value['status']!="Rescheduled" && $is_scheduled==false) || $is_scheduled==true) && !is_loan_officer()) || (is_loan_officer() && $status_value['status']!="Docs Drawn" ) )
							{
								$value['status'] .= '<li><a href="javascript:void(0)" class="select-admin-status" data-id="'.$status_value['id'].'" data-status="'.$status_value['status'].'" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:'.$status_value['color'].';"></span> &nbsp;'.$status_value['label'].'</a></li>';
							}
						}
						$value['status'] .= '</ul></div>';
					}else
					{
						if($admin_status_id==0)
						{
							$value['status'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis btn-default" style="width:160px;text-align:left;">No status set</a>';
						}else
						{
							$value['status'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle" style="background-color:'.$admin_status_color.';width:160px;border-color:'.$admin_status_border_color.';color:#fff;text-align:left;">'.$admin_status_label.'</a>';
						}
					}

					// SCHEDULE STATUS
					if(!is_guest())
					{
						$value['schedule_status'] = '<div class="dropdown">';
						if($schedule_status_id==0)
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Status</a>';
						}else
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$schedule_status_name.'</a>';
						}
						$value['schedule_status'] .= '<ul class="dropdown-menu pull-right">';
						foreach ($schedule_status_listing as $status_key => $status_value) {
							$value['schedule_status'] .= '<li><a href="javascript:void(0)" class="select-schedule-status" data-id="'.$status_value['id'].'" data-schedule-id="'.$schedule_id.'">'.$status_value['name'].'</a></li>';
						}
						$value['schedule_status'] .= '</ul></div>';
					}else
					{
						$value['schedule_status'] = "";
						if($schedule_status_id==0)
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">None</a>';
						}else
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$schedule_status_name.'</a>';
						}
					}

					// FOLDERS
					if(!is_guest())
					{	
						$value['folder'] = '<div class="dropdown">';
						if($folder_id==0)
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Folder</a>';
						}else
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$folder_name.'</a>';
						}
						$value['folder'] .= '<ul class="dropdown-menu pull-right">';
						foreach ($folder_listing as $folder_key => $folder_value) {
							if($folder_value['name']=='Pre-Closing QC' && !is_underwriter())
							{
								continue;
							}
							$value['folder'] .= '<li><a href="javascript:void(0)" class="select-schedule-folder" data-id="'.$folder_value['id'].'" data-schedule-id="'.$schedule_id.'">'.$folder_value['name'].'</a></li>';
						}
						$value['folder'] .= '</ul></div>';
					}else
					{
						$value['folder'] = "";
						if($folder_id==0)
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">None</a>';
						}else
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$folder_name.'</a>';
						}
					}

					// SIGNING COMPANY
					if(is_lender_admin() || is_loan_officer() || is_admin())
					{
						$value['signing_company'] = '<div class="btn-status-col dropdown" data-toggle="tooltip" title="';
						if($request_notary_service==1)
						{
							$value['signing_company'] .= 'NotaryCloud" style="width:120px;">';
							$value['signing_company'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background:#38b53d;border-color:#2d9131;">NotaryCloud<span class="caret pull-right"></span></a>';
						}else
						{
							$value['signing_company'] .= 'Notary Assurance" style="width:120px;">';
							$value['signing_company'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background:#75caeb;border-color:#5fc1e8;">Notary Assurance<span class="caret pull-right"></span></a>';
						}

						$value['signing_company'] .= '<ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)" class="select-signing-company" data-name="0" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:#75caeb;"></span> &nbsp;Notary Assurance</a></li><li><a href="javascript:void(0)" class="select-signing-company" data-name="1" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:#38b53d;"></span> &nbsp;NotaryCloud</a></li></ul></div>';
					}else
					{
						if($request_notary_service==1)
						{
							$value['signing_company'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary" style="background:#38b53d;border-color:#2d9131;width:120px;text-align:left;">NotaryCloud</a>';
						}else
						{
							$value['signing_company'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary" style="background:#75caeb;border-color:#5fc1e8;width:120px;text-align:left;">Notary Assurance</a>';
						}
					}

					$value["actions"] 							= '<div class="btn-groupX btn-group-xs text-center"><a href="javascript:void(0)" class="btn btn-danger remove-schedule-btn" data-toggle="tooltip" data-id="'.$value['id'].'" title="Delete"><i class="fa fa-trash-o "></i></a></div>';

                    $value['comments_label'] 					= '<div class="text-center"><a href="javascript:void(0);" data-toggle="comment-details" data-schedule-id="'.$value['id'].'" class="text-center center-block font-md"><i class="fa fa-comments-o"></i> <span class="font-md">'.$value['count_comments'].'</span></a></div>';

                    $value['notes_label'] 						= '<div class="text-center"><a href="javascript:void(0);" data-toggle="notes-details" data-schedule-id="'.$value['id'].'" class="text-center center-block font-md"><i class="fa fa-sticky-note-o"></i> <span class="font-md">'.$value['count_notes'].'</span></a></div>';

					$response_data['data'][] = $value;
				}
			}
			$response_data['draw'] = $datatable['draw'];
			$response_data['recordsFiltered'] = $result['totalCount'];
			$response_data['recordsTotal'] = $result['totalCount'];
		}
		$this->template->array_to_json($response_data);
	}

	public function all_scheduled_listing()
	{
		if(is_admin())
		{
			// folder listing
			$folder_listing 		= $this->mdcommon->get_folder_listing();
			// admin listing
			$admin_status_listing 	= $this->mdadmin->get_admin_status_lists();
			// schedule status listing
			$schedule_status_listing 	= $this->mdcommon->schedule_status_listing();
		}

		$response_data = array(
			"data" => array(),
			"draw" => 1,
			"recordsFiltered" => 0,
			"recordsTotal" => 0
		);
		if($this->input->is_ajax_request())
		{	
			$datatable = $this->input->get();
			$start = $datatable['start'];
			$limit = $datatable['length'];
			$filters = array(
				"search_archive_schedule" => $datatable['search']['value'],
				"date_scheduled_timezone" => 1
			);
			// if isset filters
			if(isset($_GET['filters']))
			{
				$temp = json_decode($this->input->get('filters'));
                foreach ($temp as $key => $value) {
                	if($key=="lender_id")
                	{
                		$value = decrypt($value);
                	}
                    $filters[$key] = $value;
                }
			}

			$filters['date_scheduled_past'] = 1;

			$column = array(
				"order_by_date_scheduled",
				"order_by_time",
				"order_by_borrower_name",
				"order_by_title_order_number",
				"order_by_folder_name",
				"order_by_schedule_status",
				"order_by_admin_status_importance",
				"order_by_request_notary_service"
			);
			$filters[$column[$datatable["order"][0]["column"]]] = $datatable["order"][0]["dir"];
			$result = $this->mdschedule->scheduled_listing($filters,$start,$limit);
			if($result['totalCount']>0)
			{
				foreach ($result['result'] as $key => $value)
				{	
					$value['id'] 								= encrypt($value['id']);
					$value['due_today'] 						= false;
					$value['date_scheduled_label'] 				= date("F d, Y",strtotime($value['date_scheduled_timezone']));
					$value['time_label'] 						= date("h:i A",strtotime($value['date_scheduled_timezone']));

					$location 							= explode(", ", $value['property_location']);
					$value['property_state']            = $location[0];
					if($location[1]=="SC" || $location[1]=="GA" || $location[1]=="CT" || $location[1]=="MA")
					{
						$value['property_state'] 	    = "<strong>".$location[0]."</strong>";
					}

					$value['borrower']	 				= '<a href="javascript:void(0)" data-toggle="loan-detail" data-id="'.$value['id'].'">'.$value['borrower_name'].'</a><div class="font-small muted">'.$value['property_state'].'</div>';
					if($value['request_notary_service']==1)
					{
						$value['borrower']	 			= '<a class="font-bold" href="javascript:void(0)" tooltip title="Notary Service Requested" data-toggle="loan-detail" data-id="'.$value['id'].'">'.$value['borrower_name'].'</a><div class="font-small muted">'.$value['property_state'].'</div>';
					}

					$schedule_id               					= $value['id'];
					$loan_officer_id							= $value['loan_officer_id'];
					$admin_status_id  							= $value['admin_status_id'];
					$admin_status  								= $value['admin_status'];
					$admin_status_label  						= $value['admin_status_label'];
					$admin_status_color 						= $value['admin_status_color'];
					$admin_status_border_color  				= $value['admin_status_border_color'];
					$schedule_status_id  						= $value['schedule_status_id'];
					$schedule_status_name  						= $value['schedule_status_name'];
					$folder_id									= $value['folder_id'];
					$folder_name								= $value['folder_name'];
					$request_notary_service 					= $value['request_notary_service'];

					$is_scheduled								= true;

					// STATUS
					if(is_lender_admin() || is_underwriter() || is_admin() || ( (is_loan_officer() && $admin_status!="Docs Drawn") && (is_loan_officer() && user_id()==$loan_officer_id) ) )
					{
						$value['status'] = '<div class="btn-status-col dropdown" data-toggle="tooltip" title="';
						if($admin_status_id==0)
						{
							$value['status'] .= 'Select Status" style="width:160px;">';
							$value['status'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-default"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Status<span class="caret pull-right"></span></a>';
							$value['status'] .= '<ul class="dropdown-menu pull-right">';
						}else
						{
							$value['status'] .= $admin_status_label.'" style="width:160px;">';
							$value['status'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color:'.$admin_status_color.';border-color:'.$admin_status_border_color.';color:#fff;">'.$admin_status_label.'<span class="caret pull-right"></span></a>';
							$value['status'] .= '<ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)" class="select-admin-status" data-id="0" data-status="None" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:#555;"></span> &nbsp;None</a></li>';
						}
						foreach ($admin_status_listing as $status_key => $status_value) {
							if( ((($status_value['status']!="Rescheduled" && $is_scheduled==false) || $is_scheduled==true) && !is_loan_officer()) || (is_loan_officer() && $status_value['status']!="Docs Drawn" ) )
							{
								$value['status'] .= '<li><a href="javascript:void(0)" class="select-admin-status" data-id="'.$status_value['id'].'" data-status="'.$status_value['status'].'" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:'.$status_value['color'].';"></span> &nbsp;'.$status_value['label'].'</a></li>';
							}
						}
						$value['status'] .= '</ul></div>';
					}else
					{
						if($admin_status_id==0)
						{
							$value['status'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis btn-default" style="width:160px;text-align:left;">No status set</a>';
						}else
						{
							$value['status'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle" style="background-color:'.$admin_status_color.';width:160px;border-color:'.$admin_status_border_color.';color:#fff;text-align:left;">'.$admin_status_label.'</a>';
						}
					}

					// SCHEDULE STATUS
					if(!is_guest())
					{
						$value['schedule_status'] = '<div class="dropdown">';
						if($schedule_status_id==0)
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Status</a>';
						}else
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$schedule_status_name.'</a>';
						}
						$value['schedule_status'] .= '<ul class="dropdown-menu pull-right">';
						foreach ($schedule_status_listing as $status_key => $status_value) {
							$value['schedule_status'] .= '<li><a href="javascript:void(0)" class="select-schedule-status" data-id="'.$status_value['id'].'" data-schedule-id="'.$schedule_id.'">'.$status_value['name'].'</a></li>';
						}
						$value['schedule_status'] .= '</ul></div>';
					}else
					{
						$value['schedule_status'] = "";
						if($schedule_status_id==0)
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">None</a>';
						}else
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$schedule_status_name.'</a>';
						}
					}

					// FOLDERS
					if(!is_guest())
					{	
						$value['folder'] = '<div class="dropdown">';
						if($folder_id==0)
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Folder</a>';
						}else
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$folder_name.'</a>';
						}
						$value['folder'] .= '<ul class="dropdown-menu pull-right">';
						foreach ($folder_listing as $folder_key => $folder_value) {
							if($folder_value['name']=='Pre-Closing QC' && !is_underwriter())
							{
								continue;
							}
							$value['folder'] .= '<li><a href="javascript:void(0)" class="select-schedule-folder" data-id="'.$folder_value['id'].'" data-schedule-id="'.$schedule_id.'">'.$folder_value['name'].'</a></li>';
						}
						$value['folder'] .= '</ul></div>';
					}else
					{
						$value['folder'] = "";
						if($folder_id==0)
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">None</a>';
						}else
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$folder_name.'</a>';
						}
					}

					// SIGNING COMPANY
					if(is_lender_admin() || is_loan_officer() || is_admin())
					{
						$value['signing_company'] = '<div class="btn-status-col dropdown" data-toggle="tooltip" title="';
						if($request_notary_service==1)
						{
							$value['signing_company'] .= 'NotaryCloud" style="width:120px;">';
							$value['signing_company'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background:#38b53d;border-color:#2d9131;">NotaryCloud<span class="caret pull-right"></span></a>';
						}else
						{
							$value['signing_company'] .= 'Notary Assurance" style="width:120px;">';
							$value['signing_company'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background:#75caeb;border-color:#5fc1e8;">Notary Assurance<span class="caret pull-right"></span></a>';
						}

						$value['signing_company'] .= '<ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)" class="select-signing-company" data-name="0" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:#75caeb;"></span> &nbsp;Notary Assurance</a></li><li><a href="javascript:void(0)" class="select-signing-company" data-name="1" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:#38b53d;"></span> &nbsp;NotaryCloud</a></li></ul></div>';
					}else
					{
						if($request_notary_service==1)
						{
							$value['signing_company'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary" style="background:#38b53d;border-color:#2d9131;width:120px;text-align:left;">NotaryCloud</a>';
						}else
						{
							$value['signing_company'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary" style="background:#75caeb;border-color:#5fc1e8;width:120px;text-align:left;">Notary Assurance</a>';
						}
					}

					if($value['is_deleted']==0)
                    {
                    	$value["actions"] 						= '<div class="btn-groupX btn-group-xs text-center"><a href="javascript:void(0)" class="btn btn-danger remove-schedule-btn" data-toggle="tooltip" data-id="'.$value['id'].'" title="Delete"><i class="fa fa-trash-o "></i></a></div>';
                    }else
                    {
                    	$value["actions"] 						= '<div class="btn-groupX btn-group-xs text-center"><a href="javascript:void(0)" class="btn btn-danger permanently-delete-schedule-btn" style="margin-right:3px;" data-toggle="tooltip" data-id="'.$value['id'].'" title="Delete Forever"><i class="fa fa-trash-o "></i></a><a href="javascript:void(0)" class="btn btn-success undo-schedule-btn" data-toggle="tooltip" data-id="'.$value['id'].'" title="Restore"><i class="fa fa-undo "></i></a></div>';
                    }

                    $value['comments_label'] 					= '<div class="text-center"><a href="javascript:void(0);" data-toggle="comment-details" data-schedule-id="'.$value['id'].'" class="text-center center-block font-md"><i class="fa fa-comments-o"></i> <span class="font-md">'.$value['count_comments'].'</span></a></div>';

                    $value['notes_label'] 						= '<div class="text-center"><a href="javascript:void(0);" data-toggle="notes-details" data-schedule-id="'.$value['id'].'" class="text-center center-block font-md"><i class="fa fa-sticky-note-o"></i> <span class="font-md">'.$value['count_notes'].'</span></a></div>';

					$response_data['data'][] = $value;
				}
			}
			$response_data['draw'] = $datatable['draw'];
			$response_data['recordsFiltered'] = $result['totalCount'];
			$response_data['recordsTotal'] = $result['totalCount'];
		}
		$this->template->array_to_json($response_data);
	}

	public function completed_scheduled_listing()
	{
		if(is_admin())
		{
			// folder listing
			$folder_listing 		= $this->mdcommon->get_folder_listing();
			// admin listing
			$admin_status_listing 	= $this->mdadmin->get_admin_status_lists();
			// schedule status listing
			$schedule_status_listing 	= $this->mdcommon->schedule_status_listing();
		}

		$response_data = array(
			"data" => array(),
			"draw" => 1,
			"recordsFiltered" => 0,
			"recordsTotal" => 0
		);
		if($this->input->is_ajax_request())
		{	
			$datatable = $this->input->get();
			$start = $datatable['start'];
			$limit = $datatable['length'];
			$filters = array(
				"search_archive_schedule" => $datatable['search']['value'],
				"date_scheduled_timezone" => 1,
				"scheduled_completed" => 1,
				"is_deleted" => 0
			);
			// if isset filters
			if(isset($_GET['filters']))
			{
				$temp = json_decode($this->input->get('filters'));
                foreach ($temp as $key => $value) {
                	if($key=="lender_id")
                	{
                		$value = decrypt($value);
                	}
                    $filters[$key] = $value;
                }
			}

			$filters['date_scheduled_past'] = 1;

			$column = array(
				"order_by_date_scheduled",
				"order_by_time",
				"order_by_borrower_name",
				"order_by_title_order_number",
				"order_by_notary_name",
				"order_by_folder_name",
				"order_by_schedule_status",
				"order_by_admin_status_importance"
			);
			$filters[$column[$datatable["order"][0]["column"]]] = $datatable["order"][0]["dir"];
			$result = $this->mdschedule->scheduled_listing($filters,$start,$limit);
			if($result['totalCount']>0)
			{
				foreach ($result['result'] as $key => $value)
				{
					$value['id'] 								= encrypt($value['id']);
					$value['due_today'] 						= false;
					$value['notary_id']							= encrypt($value['notary_id']);
					$value['date_scheduled_label'] 				= date("F d, Y",strtotime($value['date_scheduled_timezone']));
					$value['time_label'] 						= date("h:i A",strtotime($value['date_scheduled_timezone']));

					$location 							= explode(", ", $value['property_location']);
					$value['property_state']            = $location[0];
					if($location[1]=="SC" || $location[1]=="GA" || $location[1]=="CT" || $location[1]=="MA")
					{
						$value['property_state'] 	    = "<strong>".$location[0]."</strong>";
					}

					$value['borrower']	 				= '<a href="javascript:void(0)" data-toggle="loan-detail" data-id="'.$value['id'].'">'.$value['borrower_name'].'</a><div class="font-small muted">'.$value['property_state'].'</div>';
					if($value['request_notary_service']==1)
					{
						$value['borrower']	 			= '<a class="font-bold" href="javascript:void(0)" tooltip title="Notary Service Requested" data-toggle="loan-detail" data-id="'.$value['id'].'">'.$value['borrower_name'].'</a><div class="font-small muted">'.$value['property_state'].'</div>';
					}

					if( $value['notary_name']=="" || ($value['notary_name']!="" && $value['request_notary_service']==0))
					{
						$value['notary_name'] 					= '<a href="javascript:void(0)">None</a>';
					}else
					{
						$value['notary_name'] 					= '<a href="javascript:void(0)" data-toggle="notary-info" data-id="'.$value['notary_id'].'">'.$value['notary_name'].'</a>';
					}

					$schedule_id               					= $value['id'];
					$loan_officer_id							= $value['loan_officer_id'];
					$admin_status_id  							= $value['admin_status_id'];
					$admin_status  								= $value['admin_status'];
					$admin_status_label  						= $value['admin_status_label'];
					$admin_status_color 						= $value['admin_status_color'];
					$admin_status_border_color  				= $value['admin_status_border_color'];
					$schedule_status_id  						= $value['schedule_status_id'];
					$schedule_status_name  						= $value['schedule_status_name'];
					$folder_id									= $value['folder_id'];
					$folder_name								= $value['folder_name'];
					$request_notary_service 					= $value['request_notary_service'];

					$is_scheduled								= true;

					// STATUS
					if(is_lender_admin() || is_underwriter() || is_admin() || ( (is_loan_officer() && $admin_status!="Docs Drawn") && (is_loan_officer() && user_id()==$loan_officer_id) ) )
					{
						$value['status'] = '<div class="btn-status-col dropdown" data-toggle="tooltip" title="';
						if($admin_status_id==0)
						{
							$value['status'] .= 'Select Status" style="width:160px;">';
							$value['status'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-default"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Status<span class="caret pull-right"></span></a>';
							$value['status'] .= '<ul class="dropdown-menu pull-right">';
						}else
						{
							$value['status'] .= $admin_status_label.'" style="width:160px;">';
							$value['status'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color:'.$admin_status_color.';border-color:'.$admin_status_border_color.';color:#fff;">'.$admin_status_label.'<span class="caret pull-right"></span></a>';
							$value['status'] .= '<ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)" class="select-admin-status" data-id="0" data-status="None" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:#555;"></span> &nbsp;None</a></li>';
						}
						foreach ($admin_status_listing as $status_key => $status_value) {
							if( ((($status_value['status']!="Rescheduled" && $is_scheduled==false) || $is_scheduled==true) && !is_loan_officer()) || (is_loan_officer() && $status_value['status']!="Docs Drawn" ) )
							{
								$value['status'] .= '<li><a href="javascript:void(0)" class="select-admin-status" data-id="'.$status_value['id'].'" data-status="'.$status_value['status'].'" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:'.$status_value['color'].';"></span> &nbsp;'.$status_value['label'].'</a></li>';
							}
						}
						$value['status'] .= '</ul></div>';
					}else
					{
						if($admin_status_id==0)
						{
							$value['status'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis btn-default" style="width:160px;text-align:left;">No status set</a>';
						}else
						{
							$value['status'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle" style="background-color:'.$admin_status_color.';width:160px;border-color:'.$admin_status_border_color.';color:#fff;text-align:left;">'.$admin_status_label.'</a>';
						}
					}

					// SCHEDULE STATUS
					if(!is_guest())
					{
						$value['schedule_status'] = '<div class="dropdown">';
						if($schedule_status_id==0)
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Status</a>';
						}else
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$schedule_status_name.'</a>';
						}
						$value['schedule_status'] .= '<ul class="dropdown-menu pull-right">';
						foreach ($schedule_status_listing as $status_key => $status_value) {
							$value['schedule_status'] .= '<li><a href="javascript:void(0)" class="select-schedule-status" data-id="'.$status_value['id'].'" data-schedule-id="'.$schedule_id.'">'.$status_value['name'].'</a></li>';
						}
						$value['schedule_status'] .= '</ul></div>';
					}else
					{
						$value['schedule_status'] = "";
						if($schedule_status_id==0)
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">None</a>';
						}else
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$schedule_status_name.'</a>';
						}
					}

					// FOLDERS
					if(!is_guest())
					{	
						$value['folder'] = '<div class="dropdown">';
						if($folder_id==0)
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Folder</a>';
						}else
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$folder_name.'</a>';
						}
						$value['folder'] .= '<ul class="dropdown-menu pull-right">';
						foreach ($folder_listing as $folder_key => $folder_value) {
							if($folder_value['name']=='Pre-Closing QC' && !is_underwriter())
							{
								continue;
							}
							$value['folder'] .= '<li><a href="javascript:void(0)" class="select-schedule-folder" data-id="'.$folder_value['id'].'" data-schedule-id="'.$schedule_id.'">'.$folder_value['name'].'</a></li>';
						}
						$value['folder'] .= '</ul></div>';
					}else
					{
						$value['folder'] = "";
						if($folder_id==0)
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">None</a>';
						}else
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$folder_name.'</a>';
						}
					}

					// SIGNING COMPANY
					if(is_lender_admin() || is_loan_officer() || is_admin())
					{
						$value['signing_company'] = '<div class="btn-status-col dropdown" data-toggle="tooltip" title="';
						if($request_notary_service==1)
						{
							$value['signing_company'] .= 'NotaryCloud" style="width:120px;">';
							$value['signing_company'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background:#38b53d;border-color:#2d9131;">NotaryCloud<span class="caret pull-right"></span></a>';
						}else
						{
							$value['signing_company'] .= 'Notary Assurance" style="width:120px;">';
							$value['signing_company'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background:#75caeb;border-color:#5fc1e8;">Notary Assurance<span class="caret pull-right"></span></a>';
						}

						$value['signing_company'] .= '<ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)" class="select-signing-company" data-name="0" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:#75caeb;"></span> &nbsp;Notary Assurance</a></li><li><a href="javascript:void(0)" class="select-signing-company" data-name="1" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:#38b53d;"></span> &nbsp;NotaryCloud</a></li></ul></div>';
					}else
					{
						if($request_notary_service==1)
						{
							$value['signing_company'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary" style="background:#38b53d;border-color:#2d9131;width:120px;text-align:left;">NotaryCloud</a>';
						}else
						{
							$value['signing_company'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary" style="background:#75caeb;border-color:#5fc1e8;width:120px;text-align:left;">Notary Assurance</a>';
						}
					}

					$value["actions"] 							= '<div class="btn-groupX btn-group-xs text-center"><a href="javascript:void(0)" class="btn btn-danger remove-schedule-btn" data-toggle="tooltip" data-id="'.$value['id'].'" title="Delete"><i class="fa fa-trash-o "></i></a></div>';

                    $value['comments_label'] 					= '<div class="text-center"><a href="javascript:void(0);" data-toggle="comment-details" data-schedule-id="'.$value['id'].'" class="text-center center-block font-md"><i class="fa fa-comments-o"></i> <span class="font-md">'.$value['count_comments'].'</span></a></div>';

                    $value['notes_label'] 						= '<div class="text-center"><a href="javascript:void(0);" data-toggle="notes-details" data-schedule-id="'.$value['id'].'" class="text-center center-block font-md"><i class="fa fa-sticky-note-o"></i> <span class="font-md">'.$value['count_notes'].'</span></a></div>';

					$response_data['data'][] = $value;
				}
			}
			$response_data['draw'] = $datatable['draw'];
			$response_data['recordsFiltered'] = $result['totalCount'];
			$response_data['recordsTotal'] = $result['totalCount'];
		}
		$this->template->array_to_json($response_data);
	}
	
	public function scheduled_listing()
	{
		if(is_admin())
		{
			// folder listing
			$folder_listing 		= $this->mdcommon->get_folder_listing();
			// admin listing
			$admin_status_listing 	= $this->mdadmin->get_admin_status_lists();
			// schedule status listing
			$schedule_status_listing 	= $this->mdcommon->schedule_status_listing();
		}

		$response_data = array(
			"data" => array(),
			"draw" => 1,
			"recordsFiltered" => 0,
			"recordsTotal" => 0
		);
		if($this->input->is_ajax_request())
		{	
			$datatable = $this->input->get();
			$start = $datatable['start'];
			$limit = $datatable['length'];
			$filters = array(
				"search_archive_schedule" => $datatable['search']['value'],
				"date_scheduled_timezone" => 1,
				"scheduled_notary_cloud" => 1,
				"is_deleted" => 0
			);
			// if isset filters
			if(isset($_GET['filters']))
			{
				$temp = json_decode($this->input->get('filters'));
                foreach ($temp as $key => $value) {
                	if($key=="lender_id")
                	{
                		$value = decrypt($value);
                	}
                    $filters[$key] = $value;
                }
			}

			$filters['date_scheduled_past'] = 1;

			$column = array(
				"order_by_date_scheduled",
				"order_by_time",
				"order_by_borrower_name",
				"order_by_title_order_number",
				"order_by_notary_name",
				"order_by_folder_name",
				"order_by_schedule_status",
				"order_by_admin_status_importance"
			);
			$filters[$column[$datatable["order"][0]["column"]]] = $datatable["order"][0]["dir"];
			$result = $this->mdschedule->scheduled_listing($filters,$start,$limit);
			if($result['totalCount']>0)
			{
				foreach ($result['result'] as $key => $value)
				{
					$value['id'] 								= encrypt($value['id']);
					$value['due_today'] 						= false;
					$value['notary_id']							= encrypt($value['notary_id']);
					$value['date_scheduled_label'] 				= date("F d, Y",strtotime($value['date_scheduled_timezone']));
					$value['time_label'] 						= date("h:i A",strtotime($value['date_scheduled_timezone']));

					$location 							= explode(", ", $value['property_location']);
					$value['property_state']            = $location[0];
					if($location[1]=="SC" || $location[1]=="GA" || $location[1]=="CT" || $location[1]=="MA")
					{
						$value['property_state'] 	    = "<strong>".$location[0]."</strong>";
					}

					$value['borrower']	 				= '<a href="javascript:void(0)" data-toggle="loan-detail" data-id="'.$value['id'].'">'.$value['borrower_name'].'</a><div class="font-small muted">'.$value['property_state'].'</div>';
					if($value['request_notary_service']==1)
					{
						$value['borrower']	 			= '<a class="font-bold" href="javascript:void(0)" tooltip title="Notary Service Requested" data-toggle="loan-detail" data-id="'.$value['id'].'">'.$value['borrower_name'].'</a><div class="font-small muted">'.$value['property_state'].'</div>';
					}

					if( $value['notary_name']=="" || ($value['notary_name']!="" && $value['request_notary_service']==0))
					{
						$value['notary_name'] 					= '<a href="javascript:void(0)">None</a>';
					}else
					{
						$value['notary_name'] 					= '<a href="javascript:void(0)" data-toggle="notary-info" data-id="'.$value['notary_id'].'">'.$value['notary_name'].'</a>';
					}

					$schedule_id               					= $value['id'];
					$loan_officer_id							= $value['loan_officer_id'];
					$admin_status_id  							= $value['admin_status_id'];
					$admin_status  								= $value['admin_status'];
					$admin_status_label  						= $value['admin_status_label'];
					$admin_status_color 						= $value['admin_status_color'];
					$admin_status_border_color  				= $value['admin_status_border_color'];
					$schedule_status_id  						= $value['schedule_status_id'];
					$schedule_status_name  						= $value['schedule_status_name'];
					$folder_id									= $value['folder_id'];
					$folder_name								= $value['folder_name'];
					$request_notary_service 					= $value['request_notary_service'];

					$is_scheduled								= true;

					// STATUS
					if(is_lender_admin() || is_underwriter() || is_admin() || ( (is_loan_officer() && $admin_status!="Docs Drawn") && (is_loan_officer() && user_id()==$loan_officer_id) ) )
					{
						$value['status'] = '<div class="btn-status-col dropdown" data-toggle="tooltip" title="';
						if($admin_status_id==0)
						{
							$value['status'] .= 'Select Status" style="width:160px;">';
							$value['status'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-default"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Status<span class="caret pull-right"></span></a>';
							$value['status'] .= '<ul class="dropdown-menu pull-right">';
						}else
						{
							$value['status'] .= $admin_status_label.'" style="width:160px;">';
							$value['status'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color:'.$admin_status_color.';border-color:'.$admin_status_border_color.';color:#fff;">'.$admin_status_label.'<span class="caret pull-right"></span></a>';
							$value['status'] .= '<ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)" class="select-admin-status" data-id="0" data-status="None" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:#555;"></span> &nbsp;None</a></li>';
						}
						foreach ($admin_status_listing as $status_key => $status_value) {
							if( ((($status_value['status']!="Rescheduled" && $is_scheduled==false) || $is_scheduled==true) && !is_loan_officer()) || (is_loan_officer() && $status_value['status']!="Docs Drawn" ) )
							{
								$value['status'] .= '<li><a href="javascript:void(0)" class="select-admin-status" data-id="'.$status_value['id'].'" data-status="'.$status_value['status'].'" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:'.$status_value['color'].';"></span> &nbsp;'.$status_value['label'].'</a></li>';
							}
						}
						$value['status'] .= '</ul></div>';
					}else
					{
						if($admin_status_id==0)
						{
							$value['status'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis btn-default" style="width:160px;text-align:left;">No status set</a>';
						}else
						{
							$value['status'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle" style="background-color:'.$admin_status_color.';width:160px;border-color:'.$admin_status_border_color.';color:#fff;text-align:left;">'.$admin_status_label.'</a>';
						}
					}

					// SCHEDULE STATUS
					if(!is_guest())
					{
						$value['schedule_status'] = '<div class="dropdown">';
						if($schedule_status_id==0)
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Status</a>';
						}else
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$schedule_status_name.'</a>';
						}
						$value['schedule_status'] .= '<ul class="dropdown-menu pull-right">';
						foreach ($schedule_status_listing as $status_key => $status_value) {
							$value['schedule_status'] .= '<li><a href="javascript:void(0)" class="select-schedule-status" data-id="'.$status_value['id'].'" data-schedule-id="'.$schedule_id.'">'.$status_value['name'].'</a></li>';
						}
						$value['schedule_status'] .= '</ul></div>';
					}else
					{
						$value['schedule_status'] = "";
						if($schedule_status_id==0)
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">None</a>';
						}else
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$schedule_status_name.'</a>';
						}
					}

					// FOLDERS
					if(!is_guest())
					{	
						$value['folder'] = '<div class="dropdown">';
						if($folder_id==0)
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Folder</a>';
						}else
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$folder_name.'</a>';
						}
						$value['folder'] .= '<ul class="dropdown-menu pull-right">';
						foreach ($folder_listing as $folder_key => $folder_value) {
							if($folder_value['name']=='Pre-Closing QC' && !is_underwriter())
							{
								continue;
							}
							$value['folder'] .= '<li><a href="javascript:void(0)" class="select-schedule-folder" data-id="'.$folder_value['id'].'" data-schedule-id="'.$schedule_id.'">'.$folder_value['name'].'</a></li>';
						}
						$value['folder'] .= '</ul></div>';
					}else
					{
						$value['folder'] = "";
						if($folder_id==0)
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">None</a>';
						}else
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$folder_name.'</a>';
						}
					}

					// SIGNING COMPANY
					if(is_lender_admin() || is_loan_officer() || is_admin())
					{
						$value['signing_company'] = '<div class="btn-status-col dropdown" data-toggle="tooltip" title="';
						if($request_notary_service==1)
						{
							$value['signing_company'] .= 'NotaryCloud" style="width:120px;">';
							$value['signing_company'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background:#38b53d;border-color:#2d9131;">NotaryCloud<span class="caret pull-right"></span></a>';
						}else
						{
							$value['signing_company'] .= 'Notary Assurance" style="width:120px;">';
							$value['signing_company'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background:#75caeb;border-color:#5fc1e8;">Notary Assurance<span class="caret pull-right"></span></a>';
						}

						$value['signing_company'] .= '<ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)" class="select-signing-company" data-name="0" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:#75caeb;"></span> &nbsp;Notary Assurance</a></li><li><a href="javascript:void(0)" class="select-signing-company" data-name="1" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:#38b53d;"></span> &nbsp;NotaryCloud</a></li></ul></div>';
					}else
					{
						if($request_notary_service==1)
						{
							$value['signing_company'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary" style="background:#38b53d;border-color:#2d9131;width:120px;text-align:left;">NotaryCloud</a>';
						}else
						{
							$value['signing_company'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary" style="background:#75caeb;border-color:#5fc1e8;width:120px;text-align:left;">Notary Assurance</a>';
						}
					}

					$value["actions"] 							= '<div class="btn-groupX btn-group-xs text-center"><a href="javascript:void(0)" class="btn btn-danger remove-schedule-btn" data-toggle="tooltip" data-id="'.$value['id'].'" title="Delete"><i class="fa fa-trash-o "></i></a></div>';

                    $value['comments_label'] 					= '<div class="text-center"><a href="javascript:void(0);" data-toggle="comment-details" data-schedule-id="'.$value['id'].'" class="text-center center-block font-md"><i class="fa fa-comments-o"></i> <span class="font-md">'.$value['count_comments'].'</span></a></div>';

                    $value['notes_label'] 						= '<div class="text-center"><a href="javascript:void(0);" data-toggle="notes-details" data-schedule-id="'.$value['id'].'" class="text-center center-block font-md"><i class="fa fa-sticky-note-o"></i> <span class="font-md">'.$value['count_notes'].'</span></a></div>';

					$response_data['data'][] = $value;
				}
			}
			$response_data['draw'] = $datatable['draw'];
			$response_data['recordsFiltered'] = $result['totalCount'];
			$response_data['recordsTotal'] = $result['totalCount'];
		}
		$this->template->array_to_json($response_data);
	}

	public function scheduled_notary_assurance_listing()
	{
		if(is_admin())
		{
			// folder listing
			$folder_listing 		= $this->mdcommon->get_folder_listing();
			// admin listing
			$admin_status_listing 	= $this->mdadmin->get_admin_status_lists();
			// schedule status listing
			$schedule_status_listing 	= $this->mdcommon->schedule_status_listing();
		}

		$response_data = array(
			"data" => array(),
			"draw" => 1,
			"recordsFiltered" => 0,
			"recordsTotal" => 0
		);
		if($this->input->is_ajax_request())
		{	
			$datatable = $this->input->get();
			$start = $datatable['start'];
			$limit = $datatable['length'];
			$filters = array(
				"search_archive_schedule" => $datatable['search']['value'],
				"date_scheduled_timezone" => 1,
				"scheduled_notary_assurance" => 1,
				"is_deleted" => 0
			);
			// if isset filters
			if(isset($_GET['filters']))
			{
				$temp = json_decode($this->input->get('filters'));
                foreach ($temp as $key => $value) {
                	if($key=="lender_id")
                	{
                		$value = decrypt($value);
                	}
                    $filters[$key] = $value;
                }
			}

			$filters['date_scheduled_past'] = 1;

			$column = array(
				"order_by_date_scheduled",
				"order_by_time",
				"order_by_borrower_name",
				"order_by_title_order_number",
				"order_by_folder_name",
				"order_by_schedule_status",
				"order_by_admin_status_importance"
			);
			$filters[$column[$datatable["order"][0]["column"]]] = $datatable["order"][0]["dir"];
			$result = $this->mdschedule->scheduled_listing($filters,$start,$limit);
			if($result['totalCount']>0)
			{
				foreach ($result['result'] as $key => $value)
				{
					$value['id'] 								= encrypt($value['id']);
					$value['due_today'] 						= false;
					$value['date_scheduled_label'] 				= date("F d, Y",strtotime($value['date_scheduled_timezone']));
					$value['time_label'] 						= date("h:i A",strtotime($value['date_scheduled_timezone']));

					$location 							= explode(", ", $value['property_location']);
					$value['property_state']            = $location[0];
					if($location[1]=="SC" || $location[1]=="GA" || $location[1]=="CT" || $location[1]=="MA")
					{
						$value['property_state'] 	    = "<strong>".$location[0]."</strong>";
					}

					$value['borrower']	 				= '<a href="javascript:void(0)" data-toggle="loan-detail" data-id="'.$value['id'].'">'.$value['borrower_name'].'</a><div class="font-small muted">'.$value['property_state'].'</div>';
					if($value['request_notary_service']==1)
					{
						$value['borrower']	 			= '<a class="font-bold" href="javascript:void(0)" tooltip title="Notary Service Requested" data-toggle="loan-detail" data-id="'.$value['id'].'">'.$value['borrower_name'].'</a><div class="font-small muted">'.$value['property_state'].'</div>';
					}

					$schedule_id               					= $value['id'];
					$loan_officer_id							= $value['loan_officer_id'];
					$admin_status_id  							= $value['admin_status_id'];
					$admin_status  								= $value['admin_status'];
					$admin_status_label  						= $value['admin_status_label'];
					$admin_status_color 						= $value['admin_status_color'];
					$admin_status_border_color  				= $value['admin_status_border_color'];
					$schedule_status_id  						= $value['schedule_status_id'];
					$schedule_status_name  						= $value['schedule_status_name'];
					$folder_id									= $value['folder_id'];
					$folder_name								= $value['folder_name'];
					$request_notary_service 					= $value['request_notary_service'];

					$is_scheduled								= true;

					// STATUS
					if(is_lender_admin() || is_underwriter() || is_admin() || ( (is_loan_officer() && $admin_status!="Docs Drawn") && (is_loan_officer() && user_id()==$loan_officer_id) ) )
					{
						$value['status'] = '<div class="btn-status-col dropdown" data-toggle="tooltip" title="';
						if($admin_status_id==0)
						{
							$value['status'] .= 'Select Status" style="width:160px;">';
							$value['status'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-default"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Status<span class="caret pull-right"></span></a>';
							$value['status'] .= '<ul class="dropdown-menu pull-right">';
						}else
						{
							$value['status'] .= $admin_status_label.'" style="width:160px;">';
							$value['status'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color:'.$admin_status_color.';border-color:'.$admin_status_border_color.';color:#fff;">'.$admin_status_label.'<span class="caret pull-right"></span></a>';
							$value['status'] .= '<ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)" class="select-admin-status" data-id="0" data-status="None" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:#555;"></span> &nbsp;None</a></li>';
						}
						foreach ($admin_status_listing as $status_key => $status_value) {
							if( ((($status_value['status']!="Rescheduled" && $is_scheduled==false) || $is_scheduled==true) && !is_loan_officer()) || (is_loan_officer() && $status_value['status']!="Docs Drawn" ) )
							{
								$value['status'] .= '<li><a href="javascript:void(0)" class="select-admin-status" data-id="'.$status_value['id'].'" data-status="'.$status_value['status'].'" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:'.$status_value['color'].';"></span> &nbsp;'.$status_value['label'].'</a></li>';
							}
						}
						$value['status'] .= '</ul></div>';
					}else
					{
						if($admin_status_id==0)
						{
							$value['status'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis btn-default" style="width:160px;text-align:left;">No status set</a>';
						}else
						{
							$value['status'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle" style="background-color:'.$admin_status_color.';width:160px;border-color:'.$admin_status_border_color.';color:#fff;text-align:left;">'.$admin_status_label.'</a>';
						}
					}

					// SCHEDULE STATUS
					if(!is_guest())
					{
						$value['schedule_status'] = '<div class="dropdown">';
						if($schedule_status_id==0)
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Status</a>';
						}else
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$schedule_status_name.'</a>';
						}
						$value['schedule_status'] .= '<ul class="dropdown-menu pull-right">';
						foreach ($schedule_status_listing as $status_key => $status_value) {
							$value['schedule_status'] .= '<li><a href="javascript:void(0)" class="select-schedule-status" data-id="'.$status_value['id'].'" data-schedule-id="'.$schedule_id.'">'.$status_value['name'].'</a></li>';
						}
						$value['schedule_status'] .= '</ul></div>';
					}else
					{
						$value['schedule_status'] = "";
						if($schedule_status_id==0)
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">None</a>';
						}else
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$schedule_status_name.'</a>';
						}
					}

					// FOLDERS
					if(!is_guest())
					{	
						$value['folder'] = '<div class="dropdown">';
						if($folder_id==0)
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Folder</a>';
						}else
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$folder_name.'</a>';
						}
						$value['folder'] .= '<ul class="dropdown-menu pull-right">';
						foreach ($folder_listing as $folder_key => $folder_value) {
							if($folder_value['name']=='Pre-Closing QC' && !is_underwriter())
							{
								continue;
							}
							$value['folder'] .= '<li><a href="javascript:void(0)" class="select-schedule-folder" data-id="'.$folder_value['id'].'" data-schedule-id="'.$schedule_id.'">'.$folder_value['name'].'</a></li>';
						}
						$value['folder'] .= '</ul></div>';
					}else
					{
						$value['folder'] = "";
						if($folder_id==0)
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">None</a>';
						}else
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$folder_name.'</a>';
						}
					}

					// SIGNING COMPANY
					if(is_lender_admin() || is_loan_officer() || is_admin())
					{
						$value['signing_company'] = '<div class="btn-status-col dropdown" data-toggle="tooltip" title="';
						if($request_notary_service==1)
						{
							$value['signing_company'] .= 'NotaryCloud" style="width:120px;">';
							$value['signing_company'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background:#38b53d;border-color:#2d9131;">NotaryCloud<span class="caret pull-right"></span></a>';
						}else
						{
							$value['signing_company'] .= 'Notary Assurance" style="width:120px;">';
							$value['signing_company'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background:#75caeb;border-color:#5fc1e8;">Notary Assurance<span class="caret pull-right"></span></a>';
						}

						$value['signing_company'] .= '<ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)" class="select-signing-company" data-name="0" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:#75caeb;"></span> &nbsp;Notary Assurance</a></li><li><a href="javascript:void(0)" class="select-signing-company" data-name="1" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:#38b53d;"></span> &nbsp;NotaryCloud</a></li></ul></div>';
					}else
					{
						if($request_notary_service==1)
						{
							$value['signing_company'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary" style="background:#38b53d;border-color:#2d9131;width:120px;text-align:left;">NotaryCloud</a>';
						}else
						{
							$value['signing_company'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary" style="background:#75caeb;border-color:#5fc1e8;width:120px;text-align:left;">Notary Assurance</a>';
						}
					}

					$value["actions"] 							= '<div class="btn-groupX btn-group-xs text-center"><a href="javascript:void(0)" class="btn btn-danger remove-schedule-btn" data-toggle="tooltip" data-id="'.$value['id'].'" title="Delete"><i class="fa fa-trash-o "></i></a></div>';

                    $value['comments_label'] 					= '<div class="text-center"><a href="javascript:void(0);" data-toggle="comment-details" data-schedule-id="'.$value['id'].'" class="text-center center-block font-md"><i class="fa fa-comments-o"></i> <span class="font-md">'.$value['count_comments'].'</span></a></div>';

                    $value['notes_label'] 						= '<div class="text-center"><a href="javascript:void(0);" data-toggle="notes-details" data-schedule-id="'.$value['id'].'" class="text-center center-block font-md"><i class="fa fa-sticky-note-o"></i> <span class="font-md">'.$value['count_notes'].'</span></a></div>';

					$response_data['data'][] = $value;
				}
			}
			$response_data['draw'] = $datatable['draw'];
			$response_data['recordsFiltered'] = $result['totalCount'];
			$response_data['recordsTotal'] = $result['totalCount'];
		}
		$this->template->array_to_json($response_data);
	}

	public function deleted_scheduled_listing()
	{
		if(is_admin())
		{
			// folder listing
			$folder_listing 		= $this->mdcommon->get_folder_listing();
			// admin listing
			$admin_status_listing 	= $this->mdadmin->get_admin_status_lists();
			// schedule status listing
			$schedule_status_listing 	= $this->mdcommon->schedule_status_listing();
		}
		
		$response_data = array(
			"data" => array(),
			"draw" => 1,
			"recordsFiltered" => 0,
			"recordsTotal" => 0
		);
		if($this->input->is_ajax_request())
		{	
			$datatable = $this->input->get();
			$start = $datatable['start'];
			$limit = $datatable['length'];
			$filters = array(
				"search_not_archive_schedule" => $datatable['search']['value'],
				"date_scheduled_timezone" => 1,
				"is_deleted" => 1
			);
			// if isset filters
			if(isset($_GET['filters']))
			{
				$temp = json_decode($this->input->get('filters'));
                foreach ($temp as $key => $value) {
                	if($key=="lender_id")
                	{
                		$value = decrypt($value);
                	}
                    $filters[$key] = $value;
                }
			}

			$filters['date_scheduled_past'] = 1;

			$column = array(
				"order_by_date_scheduled",
				"order_by_borrower_name",
				"order_by_title_order_number",
				"order_by_folder_name",
				"order_by_schedule_status",
				"order_by_admin_status_importance"
			);
			$filters[$column[$datatable["order"][0]["column"]]] = $datatable["order"][0]["dir"];
			$result = $this->mdschedule->not_scheduled_listing($filters,$start,$limit);
			if($result['totalCount']>0)
			{
				foreach ($result['result'] as $key => $value)
				{
					$value['id'] 								= encrypt($value['id']);
					$value['due_today'] 						= false;
					$value['date_scheduled_label'] 				= date("F d, Y",strtotime($value['date_scheduled_timezone']));
					$value['time_label'] 						= date("h:i A",strtotime($value['date_scheduled_timezone']));

					$location 							= explode(", ", $value['property_location']);
					$value['property_state']            = $location[0];
					if($location[1]=="SC" || $location[1]=="GA" || $location[1]=="CT" || $location[1]=="MA")
					{
						$value['property_state'] 	    = "<strong>".$location[0]."</strong>";
					}

					$value['borrower']	 				= '<a href="javascript:void(0)" data-toggle="loan-detail" data-id="'.$value['id'].'">'.$value['borrower_name'].'</a><div class="font-small muted">'.$value['property_state'].'</div>';
					if($value['request_notary_service']==1)
					{
						$value['borrower']	 			= '<a class="font-bold" href="javascript:void(0)" tooltip title="Notary Service Requested" data-toggle="loan-detail" data-id="'.$value['id'].'">'.$value['borrower_name'].'</a><div class="font-small muted">'.$value['property_state'].'</div>';
					}

					$schedule_id               					= $value['id'];
					$loan_officer_id							= $value['loan_officer_id'];
					$admin_status_id  							= $value['admin_status_id'];
					$admin_status  								= $value['admin_status'];
					$admin_status_label  						= $value['admin_status_label'];
					$admin_status_color 						= $value['admin_status_color'];
					$admin_status_border_color  				= $value['admin_status_border_color'];
					$schedule_status_id  						= $value['schedule_status_id'];
					$schedule_status_name  						= $value['schedule_status_name'];
					$folder_id									= $value['folder_id'];
					$folder_name								= $value['folder_name'];
					$request_notary_service 					= $value['request_notary_service'];

					$is_scheduled								= true;

					// STATUS
					if(is_lender_admin() || is_underwriter() || is_admin() || ( (is_loan_officer() && $admin_status!="Docs Drawn") && (is_loan_officer() && user_id()==$loan_officer_id) ) )
					{
						$value['status'] = '<div class="btn-status-col dropdown" data-toggle="tooltip" title="';
						if($admin_status_id==0)
						{
							$value['status'] .= 'Select Status" style="width:160px;">';
							$value['status'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-default"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Status<span class="caret pull-right"></span></a>';
							$value['status'] .= '<ul class="dropdown-menu pull-right">';
						}else
						{
							$value['status'] .= $admin_status_label.'" style="width:160px;">';
							$value['status'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color:'.$admin_status_color.';border-color:'.$admin_status_border_color.';color:#fff;">'.$admin_status_label.'<span class="caret pull-right"></span></a>';
							$value['status'] .= '<ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)" class="select-admin-status" data-id="0" data-status="None" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:#555;"></span> &nbsp;None</a></li>';
						}
						foreach ($admin_status_listing as $status_key => $status_value) {
							if( ((($status_value['status']!="Rescheduled" && $is_scheduled==false) || $is_scheduled==true) && !is_loan_officer()) || (is_loan_officer() && $status_value['status']!="Docs Drawn" ) )
							{
								$value['status'] .= '<li><a href="javascript:void(0)" class="select-admin-status" data-id="'.$status_value['id'].'" data-status="'.$status_value['status'].'" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:'.$status_value['color'].';"></span> &nbsp;'.$status_value['label'].'</a></li>';
							}
						}
						$value['status'] .= '</ul></div>';
					}else
					{
						if($admin_status_id==0)
						{
							$value['status'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis btn-default" style="width:160px;text-align:left;">No status set</a>';
						}else
						{
							$value['status'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle" style="background-color:'.$admin_status_color.';width:160px;border-color:'.$admin_status_border_color.';color:#fff;text-align:left;">'.$admin_status_label.'</a>';
						}
					}

					// SCHEDULE STATUS
					if(!is_guest())
					{
						$value['schedule_status'] = '<div class="dropdown">';
						if($schedule_status_id==0)
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Status</a>';
						}else
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$schedule_status_name.'</a>';
						}
						$value['schedule_status'] .= '<ul class="dropdown-menu pull-right">';
						foreach ($schedule_status_listing as $status_key => $status_value) {
							$value['schedule_status'] .= '<li><a href="javascript:void(0)" class="select-schedule-status" data-id="'.$status_value['id'].'" data-schedule-id="'.$schedule_id.'">'.$status_value['name'].'</a></li>';
						}
						$value['schedule_status'] .= '</ul></div>';
					}else
					{
						$value['schedule_status'] = "";
						if($schedule_status_id==0)
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">None</a>';
						}else
						{
							$value['schedule_status'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$schedule_status_name.'</a>';
						}
					}

					// FOLDERS
					if(!is_guest())
					{	
						$value['folder'] = '<div class="dropdown">';
						if($folder_id==0)
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select Folder</a>';
						}else
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$folder_name.'</a>';
						}
						$value['folder'] .= '<ul class="dropdown-menu pull-right">';
						foreach ($folder_listing as $folder_key => $folder_value) {
							if($folder_value['name']=='Pre-Closing QC' && !is_underwriter())
							{
								continue;
							}
							$value['folder'] .= '<li><a href="javascript:void(0)" class="select-schedule-folder" data-id="'.$folder_value['id'].'" data-schedule-id="'.$schedule_id.'">'.$folder_value['name'].'</a></li>';
						}
						$value['folder'] .= '</ul></div>';
					}else
					{
						$value['folder'] = "";
						if($folder_id==0)
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">None</a>';
						}else
						{
							$value['folder'] .= '<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$folder_name.'</a>';
						}
					}

					// SIGNING COMPANY
					if(is_lender_admin() || is_loan_officer() || is_admin())
					{
						$value['signing_company'] = '<div class="btn-status-col dropdown" data-toggle="tooltip" title="';
						if($request_notary_service==1)
						{
							$value['signing_company'] .= 'NotaryCloud" style="width:120px;">';
							$value['signing_company'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background:#38b53d;border-color:#2d9131;">NotaryCloud<span class="caret pull-right"></span></a>';
						}else
						{
							$value['signing_company'] .= 'Notary Assurance" style="width:120px;">';
							$value['signing_company'] .= '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background:#75caeb;border-color:#5fc1e8;">Notary Assurance<span class="caret pull-right"></span></a>';
						}

						$value['signing_company'] .= '<ul class="dropdown-menu pull-right"><li><a href="javascript:void(0)" class="select-signing-company" data-name="0" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:#75caeb;"></span> &nbsp;Notary Assurance</a></li><li><a href="javascript:void(0)" class="select-signing-company" data-name="1" data-schedule-id="'.$schedule_id.'"><span class="color-block fa fa-square" style="color:#38b53d;"></span> &nbsp;NotaryCloud</a></li></ul></div>';
					}else
					{
						if($request_notary_service==1)
						{
							$value['signing_company'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary" style="background:#38b53d;border-color:#2d9131;width:120px;text-align:left;">NotaryCloud</a>';
						}else
						{
							$value['signing_company'] = '<a href="javascript:void(0)" class="btn-status btn btn-xs ellipsis dropdown-toggle btn-primary" style="background:#75caeb;border-color:#5fc1e8;width:120px;text-align:left;">Notary Assurance</a>';
						}
					}

					$value["actions"] 							= '<div class="btn-groupX btn-group-xs text-center"><a href="javascript:void(0)" class="btn btn-danger permanently-delete-schedule-btn" style="margin-right:3px;" data-toggle="tooltip" data-id="'.$value['id'].'" title="Delete Forever"><i class="fa fa-trash-o "></i></a><a href="javascript:void(0)" class="btn btn-success undo-schedule-btn" data-toggle="tooltip" data-id="'.$value['id'].'" title="Restore"><i class="fa fa-undo "></i></a></div>';

                    $value['comments_label'] 					= '<div class="text-center"><a href="javascript:void(0);" data-toggle="comment-details" data-schedule-id="'.$value['id'].'" class="text-center center-block font-md"><i class="fa fa-comments-o"></i> <span class="font-md">'.$value['count_comments'].'</span></a></div>';

                    $value['notes_label'] 						= '<div class="text-center"><a href="javascript:void(0);" data-toggle="notes-details" data-schedule-id="'.$value['id'].'" class="text-center center-block font-md"><i class="fa fa-sticky-note-o"></i> <span class="font-md">'.$value['count_notes'].'</span></a></div>';

					$response_data['data'][] = $value;
				}
			}
			$response_data['draw'] = $datatable['draw'];
			$response_data['recordsFiltered'] = $result['totalCount'];
			$response_data['recordsTotal'] = $result['totalCount'];
		}
		$this->template->array_to_json($response_data);
	}

}