<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once("Admin.php");

class Company extends Admin {

	var $data = array();

	public function __construct()
	{
		parent::__construct();
		$this->template->check_if_not_logged_in();
		$this->load->helper(array("form"));
		$this->load->model(array("mdcompany"));

		// scripts
		$this->template->set_scripts(base_url('assets/js/company.js'));
	}

	public function index()
	{
		// redirect if not admin
		if(!is_super_admin())
		{
			redirect(base_url('profile'),"refresh");
		}
		// load js
		$this->template->set_scripts(base_url("assets/lib/jquery-input-mask/jquery.inputmask.bundle.min.js"));
		$this->template->set_scripts("http://maps.googleapis.com/maps/api/js?key=".$this->config->item("google_api_key")."&libraries=places");
		$this->template->set_scripts(base_url("assets/lib/jquery-geocomplete/jquery.geocomplete.min.js"));
		$this->template->set_styles(base_url("assets/lib/cropper/cropper.min.css"));
		$this->template->set_scripts(base_url("assets/lib/cropper/cropper.min.js"));

		$this->data['crop_modal'] = $this->template->load("common","modals/crop-picture-modal",$this->data,TRUE,TRUE);
		
		$this->template->load("common","admin/company",$this->data);
	}

	public function add_company_view()
	{
		if($this->input->is_ajax_request())
		{
			$this->template->load("common","admin/modals/add_company",$this->data,FALSE,TRUE);
		}
	}
	public function edit_company_view($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$filters = array(
				"id" => $id
			);
			if(!is_admin())
			{
				redirect(base_url("profile"),'refresh');
			}  
			if(!is_super_admin())
			{
				$company_id = get_company_id();
				if($id!=$company_id)
				{
					redirect(base_url("profile"),'refresh');
				}
			}
			$result = $this->mdcompany->get_company($filters);
			if(!empty($result))
			{
				$this->data['id'] 	= $enc_id;
				$this->data['name'] = $result['name'];
				$this->data['company_pic'] = company_picture($result['company_pic']);
				$this->data['address'] = $result['address'];
				$this->data['description'] = $result['description'];
				$this->data['email'] = $result['email'];
				$this->data['website'] = $result['website'];
				$this->data['telephone_number'] = $result['telephone_number'];
				$this->data['fax_number'] = $result['fax_number'];
			}
			$this->template->load("common","admin/modals/edit_company",$this->data,FALSE,TRUE);
		}
	}

	public function add_company()
	{
		if($this->input->is_ajax_request())
		{
			// load library
			$this->load->library("form_validation");

			$this->form_validation->set_rules("name","Company Name","required|is_unique[company.name]",array("is_unique" => "The %s already exists. Please input another one."));
			$this->form_validation->set_rules("description","Description","required");
			$this->form_validation->set_rules("address","Address","required");
			// $this->form_validation->set_rules("longitude","Longitude","required");
			// $this->form_validation->set_rules("latitude","Latitude","required");
			$this->form_validation->set_rules("email","Email","required|is_unique[company.email]",array("is_unique" => "The %s already exists. Please input another one."));
			$this->form_validation->set_rules("telephone_number","Telephone Number","required|min_length[6]");
			$this->form_validation->set_rules("fax_number","Fax Number","required");
			
			if($this->form_validation->run())
			{
				$data = array(
					"name" => ucwords($this->input->post("name")),
					"description" => ucfirst($this->input->post("description")),
					"address" => $this->input->post("address"),
					"longitude" => $this->input->post("longitude"),
					"latitude" => $this->input->post("latitude"),
					"email" => $this->input->post("email"),
					"website" => $this->input->post("website"),
					"telephone_number" => $this->input->post("telephone_number"),
					"fax_number" => $this->input->post("fax_number"),
					"date_added" => date("Y-m-d H:i:s")
				);
				// check if there is profile file upload
				$image_url = $this->input->post("company_picture");
				if(isset($image_url) && !empty($image_url))
				{
					$filename = random_string(40);
					//save image
		            $img_trimmed = substr(explode(";",$image_url)[1], 7);
		            $image_path = './uploads/company/'.$filename.'.png';
		            file_put_contents($image_path, base64_decode($img_trimmed));
		            if(file_exists($image_path))
            		{
            			$data['company_pic'] = $filename.".png";
            		}else{
            			return $this->template->set_message("<p>Could not create image file. Please try another one.</p>");
            		}
	            }
				$response = $this->mdcompany->add_company($data);
				if($response)
				{
					//add default user for the company
					$password = "admin2016!";
					$data = array(
						"firstname" 	=> ucwords($this->input->post("name")), 
						"lastname" 		=> "Admin", 
						"username" 		=> $this->input->post("email"),
						"password" 		=> $password,
						"email"			=> $this->input->post("email"),
						"company_id"	=> $response,
						"created_at" 	=> date("Y-m-d H:i:s"),
						"updated_at" 	=> date("Y-m-d H:i:s")
					);
					$this->load->model("mduser");
					$this->mduser->create_user($data,2);
				}
				$this->template->evaluate($response,1);
			}else
			{
				$this->template->set_message(validation_errors("<p>","</p>"));
			}
		}
		$this->template->response(false);
	}
	public function edit_company($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			// load library
			$this->load->library("form_validation");

			$original_company = $this->input->post("original_company");
			$name = $this->input->post("name");

			if($name != $original_company) {
			   $is_unique =  '|is_unique[company.name]';
			}else
			{
			   $is_unique =  '';
			}

			$this->form_validation->set_rules("name","Company Name","required".$is_unique,array("is_unique" => "The %s already exists. Please input another one."));
			$this->form_validation->set_rules("description","Description","required");
			$this->form_validation->set_rules("address","Address","required");
			// $this->form_validation->set_rules("longitude","Longitude","required");
			// $this->form_validation->set_rules("latitude","Latitude","required");
			$this->form_validation->set_rules("email","Email","required");
			$this->form_validation->set_rules("telephone_number","Telephone Number","required|min_length[6]");
			$this->form_validation->set_rules("fax_number","Fax Number","required");
			
			if($this->form_validation->run())
			{
				$filters = array(
					"id" => $id
				);
				$data = array(
					"name" => ucwords($name),
					"description" => ucfirst($this->input->post("description")),
					"address" => $this->input->post("address"),
					"longitude" => $this->input->post("longitude"),
					"latitude" => $this->input->post("latitude"),
					"email" => $this->input->post("email"),
					"website" => $this->input->post("website"),
					"telephone_number" => $this->input->post("telephone_number"),
					"fax_number" => $this->input->post("fax_number"),
					"date_added" => date("Y-m-d H:i:s")
				);
				// check if there is profile file upload
				$image_url = $this->input->post("company_picture");
				if(isset($image_url) && !empty($image_url))
				{
					$filename = random_string(40);
					//save image
		            $img_trimmed = substr(explode(";",$image_url)[1], 7);
		            $image_path = './uploads/company/'.$filename.'.png';
		            file_put_contents($image_path, base64_decode($img_trimmed));
		            if(file_exists($image_path))
            		{
            			$data['company_pic'] = $filename.".png";
            		}else{
            			return $this->template->set_message("<p>Could not create image file. Please try another one.</p>");
            		}
	            }
				$response = $this->mdcompany->edit_company($filters,$data);
				$this->template->evaluate($response,2);
			}else
			{
				$this->template->set_message(validation_errors("<p>","</p>"));
			}
		}
		$this->template->response(false);
	}
	public function remove_company($enc_id)
	{	
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$response = $this->mdcompany->remove_company(array("id"=>$id));
			$this->template->evaluate($response,3);
		}
		$this->template->response(false);
	}


	// get company details
	public function get_company_details($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			$filters = array(
				"id" 			=> $id
			);
			$result = $this->mdcompany->get_company($filters);
			if(!empty($result))
			{
				$this->data['id'] = encrypt($result["id"]);
				$this->data['name'] = $result['name'];
				$this->data['company_pic'] = company_picture($result['company_pic']);
				$this->data['description'] = $result['description'];
				$this->data['email'] = $result['email'];
				$this->data['website'] = $result['website'];
				$this->data['address'] = $result['address'];
				$this->data['telephone_number'] = $result['telephone_number'];
				$this->data['fax_number'] = $result['fax_number'];
			} 
			$this->template->load("common","modals/company-details",$this->data,FALSE,TRUE);
		}
	}


	// Listing for Datatables
	public function company_listing()
	{
		$response_data = array(
			"data" => array(),
			"draw" => 1,
			"recordsFiltered" => 0,
			"recordsTotal" => 0
		);
		if($this->input->is_ajax_request())
		{	
			$datatable = $this->input->get();
			$start = $datatable['start'];
			$limit = $datatable['length'];
			$filters = array(
				"search_company" => $datatable['search']['value']
			);
			// if isset filters
			if(isset($_GET['filters']))
			{
				$temp = json_decode($this->input->get('filters'));
                foreach ($temp as $key => $value) {
                    $filters[$key] = $value;
                }
			}
			$column = array("order_by_name","order_by_address","order_by_email","order_by_date_added");
			$filters[$column[$datatable["order"][0]["column"]]] = $datatable["order"][0]["dir"];
			$result = $this->mdcompany->company_listing($filters,$start,$limit); 
			if($result['totalFiltered']>0)
			{
				foreach ($result['result'] as $key => $value)
				{
					$value['id'] = encrypt($value['id']);
					$value['company_pic'] = company_picture($value['company_pic']);
					$value['company_name'] = '<img class="img-responsive pull-left pr15" src="'.$value['company_pic'].'" width="48"/><a class="font-thin" href="javascript:void(0)" data-toggle="company-info" data-id="'.$value['id'].'">'.$value['name'].'</a>';
					$value['address'] = $value['address'];
					$value['email'] = '<a href="mailto:'.$value['email'].'">'.$value['email']."</a>";
					$value['website'] = '<a href="'.$value['website'].' target="_blank">'.$value['website']."</a>";
					$value['date_added'] = date('F d, Y',strtotime($value['date_added']));
					$value['action'] = '<div class="btn-groupX btn-group-xs"><a href="javascript:void(0)" class="btn btn-success edit-company-btn mr3" data-toggle="tooltip" data-id="'.$value['id'].'" title="Edit"><i class="fa fa-pencil"></i></a><a href="javascript:void(0)" class="btn ml5 btn-danger remove-company-btn" data-toggle="tooltip" data-id="'.$value['id'].'" title="Delete"><i class="fa fa-trash-o "></i></a></div>';
					$response_data['data'][] = $value;
				}
			}
			$response_data['draw'] = $datatable['draw'];
			$response_data['recordsFiltered'] = $result['totalFiltered'];
			$response_data['recordsTotal'] = $result['totalCount'];
		}
		$this->template->array_to_json($response_data);
	}
}