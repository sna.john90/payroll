<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once("Admin.php");

class Users extends Admin {

	var $data = array();

	public function __construct()
	{
		parent::__construct();

		// load model
		$this->load->model(array("mduser","mdcompany"));

		$this->template->set_scripts(base_url('assets/js/users.js')."?_=".date("YmdHis"));
	}

	public function index()
	{ 
		// redirect if not admin
		if(!is_admin() && !is_super_admin())
		{
			redirect(base_url()."users",'refresh');
		} 
		// load js
		$this->template->set_styles(base_url('assets/lib/selectize/selectize.css'));
		$this->template->set_scripts(base_url('assets/lib/selectize/selectize.min.js'));
		$this->template->set_scripts(base_url('assets/lib/selectize/plugins/no_results.js'));
		$this->template->set_scripts(base_url('assets/lib/typeahead/typeahead.bundle.js'));
		$this->template->set_scripts(base_url('assets/lib/typeahead/handlebars.js'));

		$this->template->set_styles(base_url('assets/lib/jquery-nice-select/nice-select.css'));
		$this->template->set_scripts(base_url('assets/lib/jquery-nice-select/jquery.nice-select.min.js'));

		$this->template->set_scripts(base_url("assets/lib/jquery-input-mask/jquery.inputmask.bundle.min.js"));
		$this->template->set_scripts("http://maps.googleapis.com/maps/api/js?key=".$this->config->item("google_api_key")."&libraries=places");
		$this->template->set_scripts(base_url("assets/lib/jquery-geocomplete/jquery.geocomplete.min.js"));
		$this->template->set_scripts(base_url("assets/lib/pgenerator-master/pgenerator.jquery.js"));
		$this->template->set_styles(base_url("assets/lib/cropper/cropper.min.css"));
		$this->template->set_scripts(base_url("assets/lib/cropper/cropper.min.js"));
		
		if(is_super_admin())
		{
			$filters = array(
				"order_by_name" => "ASC"
			);
			$this->data['companies'] = $this->mdcompany->company_list($filters);
		}

		$this->data['crop_modal'] = $this->template->load("common","modals/crop-picture-modal",$this->data,TRUE,TRUE);

		$this->template->load("common","admin/users",$this->data);
	}

	public function add_users_view($company_id="")
	{
		$c_id = decrypt($company_id);
		if(empty($c_id) && is_admin())
		{
			$c_id = get_company_id();
		}
		if(!empty($c_id) && $this->input->is_ajax_request())
		{
			$this->data['company_id'] = encrypt($c_id);

			$this->template->load("common","admin/modals/add_users",$this->data,FALSE,TRUE);
		}
	}
	public function edit_users_view($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{ 
			$filters = array(
				"u_id" => $id
			);
			$select = "u.*,ur.id as user_group_ref_id,ur.user_group_id,ug.name as user_type,ug.description as user_type_description,empd.id as empd_ref_id, empd.basic_pay,empd.payment_type,empd.designation,empd.date_hired,empd.status as emp_status,empd.department_id,empd.gender_status";
			$result = $this->mduser->get_user($filters,$select);
			if($result)
			{
				$this->data = $result;
				$this->data['user_group_ref_id'] = encrypt($result['user_group_ref_id']);
				$this->data['id'] 				 = $enc_id;
				$this->data['company_id'] 		 = encrypt($result['company_id']);
				$this->data['empd_ref_id'] 		 = encrypt($result['empd_ref_id']); 
			} 
			$this->template->load("common","admin/modals/edit_users",$this->data,FALSE,TRUE);
		}
	}
	public function view($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{ 
			$filters = array(
				"u_id" => $id
			);
			$select = "u.*,ur.id as user_group_ref_id,ur.user_group_id,ug.name as user_type,ug.description as user_type_description,empd.id as empd_ref_id, empd.basic_pay,empd.payment_type,empd.designation,empd.date_hired,empd.status as emp_status,empd.department_id,empd.gender_status";
			$result = $this->mduser->get_user($filters,$select);
			if($result)
			{
				$this->data = $result; 
			} 
			$this->template->load("common","admin/modals/user_details",$this->data,FALSE,TRUE);
		}
	}

	// Add users
	public function add()
	{
		if($this->input->is_ajax_request())
		{
			if(!is_admin())
			{
				redirect(base_url("profile"),"refresh");
			}
			// load library
			$this->load->library("form_validation"); 
			$this->form_validation->set_rules("firstname","Name","required"); 
			$this->form_validation->set_rules("lastname","Lastname","required");
			$this->form_validation->set_rules("birthdate","Birth date","required");
			$this->form_validation->set_rules("gender","Gender","required");
			$this->form_validation->set_rules("phone","Phone","required");
			$this->form_validation->set_rules("address","Address","required"); 
			$this->form_validation->set_rules("user_group_id","User Group","integer|required");
			$this->form_validation->set_rules("company_id","Company ID","required");
			$this->form_validation->set_rules("rate","Rate","numeric|required");
			$this->form_validation->set_rules("employee_id","Employee ID","numeric|required");
			$this->form_validation->set_rules("date_hired","Date Hired","required"); 
			$this->form_validation->set_rules("department_id","Department","required");
			$this->form_validation->set_rules("designation","Designation","required");
			$this->form_validation->set_rules("payment_type","Payment Schedule","required");
			$this->form_validation->set_rules("gender_status","Gender Status","required");
			$this->form_validation->set_rules("status","Employment Status","required");
			$password = $this->input->post("password");
			$company_id = $this->input->post("company_id"); 
			$decrypt_company_id = decrypt($company_id);
			if(empty($decrypt_company_id))
			{
				$decrypt_company_id = get_company_id();
			}
			if($decrypt_company_id > 0)
			{
				$this->form_validation->set_rules("email","Email","required|valid_email|callback_is_email_check",array("is_email_check" => "The %s is already exists. Please choose another one."));
			}

			if(!empty($password))
			{
				$this->form_validation->set_rules("password","Password","required|min_length[6]");
			}

			if($this->form_validation->run())
			{  
				if(empty($password))
				{
					$password = "payroll1234";
				} 
				$data = array(
					"company_id" => $decrypt_company_id,
					"emp_id"  	 => $this->input->post("employee_id"),
					"firstname" => ucwords($this->input->post("firstname")),
					"middlename" => ucwords($this->input->post("middlename")),
					"lastname" => ucwords($this->input->post("lastname")), 
					"username" => $this->input->post("email"),
					"password" => $password,
					"email" => $this->input->post("email"),
					"birthdate" => $this->input->post("birthdate"),  
					"gender" => $this->input->post("gender"),   
					"address" => $this->input->post("address"),
					"phone" => $this->input->post("phone"), 
					"tin" => $this->input->post("tin"), 
					"sss" => $this->input->post("sss"), 
					"philhealth" => $this->input->post("philhealth"), 
					"pagibig" => $this->input->post("pagibig"), 
					"bankaccount" => $this->input->post("bank_account"), 
					"created_at" => date("Y-m-d H:i:s"),
					"updated_at" => date("Y-m-d H:i:s"),
					"added_by"	=> user_id()
				); 
				$response = $this->mduser->create_user($data, $this->input->post("user_group_id"),$decrypt_company_id,true); 
				if($response)
				{
					//save rate, hired, payment_type to employee_details table
					$data = array(
									"user_id" 		=> $response,
									"basic_pay"		=> $this->input->post("rate"),
									"department_id"	=> $this->input->post("department_id"),
									"payment_type"	=> $this->input->post("payment_type"),
									"designation"	=> $this->input->post("designation"),
									"date_hired"	=> $this->input->post("date_hired"),
									"status"		=> $this->input->post("status"),
									"gender_status"	=> $this->input->post("gender_status"),
									"added_by"		=> user_id()
								);
					$this->mduser->save_employee_details($data);
				}

				$this->template->evaluate($response,1);
			}else
			{
				$this->template->set_message(validation_errors("<p>","</p>"));
			}
		}
		$this->template->response(false);
	}
	// Edit users
	public function edit($enc_id)
	{
		$id = decrypt($enc_id);
		if(!empty($id) && $this->input->is_ajax_request())
		{
			if($this->input->is_ajax_request())
			{
				// load library
				$this->load->library("form_validation"); 
				$this->form_validation->set_rules("firstname","Name","required"); 
				$this->form_validation->set_rules("lastname","Lastname","required");
				$this->form_validation->set_rules("birthdate","Birth date","required");
				$this->form_validation->set_rules("gender","Gender","required");
				$this->form_validation->set_rules("phone","Phone","required");
				$this->form_validation->set_rules("address","Address","required"); 
				$this->form_validation->set_rules("user_group_id","User Group","integer|required");
				$this->form_validation->set_rules("company_id","Company ID","required");
				$this->form_validation->set_rules("rate","Rate","numeric|required");
				$this->form_validation->set_rules("employee_id","Employee ID","numeric|required");
				$this->form_validation->set_rules("date_hired","Date Hired","required"); 
				$this->form_validation->set_rules("department_id","Department","required");
				$this->form_validation->set_rules("designation","Designation","required");
				$this->form_validation->set_rules("payment_type","Payment Schedule","required");
				$this->form_validation->set_rules("gender_status","Gender Status","required");
				$this->form_validation->set_rules("status","Employment Status","required");
				$password = $this->input->post("password");

				$company_id = $this->input->post("company_id");
				$group_ref = $this->input->post("group_ref");
				$empd_ref = $this->input->post("empd_ref");

				$decrypt_company_id = decrypt($company_id);
				$decrypt_group_ref 	= decrypt($group_ref);
				$decrypt_emp_ref 	= decrypt($empd_ref);
				if(empty($decrypt_company_id) AND empty($decrypt_group_ref) AND empty($decrypt_emp_ref))
				{
					redirect(base_url("profile"),"refresh");
				}
				if($decrypt_company_id > 0)
				{
					$this->form_validation->set_rules("email","Email","required|valid_email|callback_is_email_check_existing",array("is_email_check_existing" => "The %s is already exists. Please choose another one."));
				}

				if(!empty($password))
				{
					$this->form_validation->set_rules("password","Password","required|min_length[6]");
				}

				if($this->form_validation->run())
				{   
					$data = array();
					$basic = array( 
						"emp_id"  	 => $this->input->post("employee_id"),
						"firstname" => ucwords($this->input->post("firstname")),
						"middlename" => ucwords($this->input->post("middlename")),
						"lastname" => ucwords($this->input->post("lastname")), 
						"username" => $this->input->post("email"),
						"password" => password_hash($password, PASSWORD_BCRYPT),
						"email" => $this->input->post("email"),
						"birthdate" => $this->input->post("birthdate"),  
						"gender" => $this->input->post("gender"),   
						"address" => $this->input->post("address"),
						"phone" => $this->input->post("phone"), 
						"tin" => $this->input->post("tin"), 
						"sss" => $this->input->post("sss"), 
						"philhealth" => $this->input->post("philhealth"), 
						"pagibig" => $this->input->post("pagibig"), 
						"bankaccount" => $this->input->post("bank_account"), 
						"updated_at" => date("Y-m-d H:i:s") 
					); 
					if(empty($password))
					{
						unset($basic['password']);
					} 
					$empdetails = array(
												"basic_pay"		=> $this->input->post("rate"),
												"department_id"	=> $this->input->post("department_id"),
												"payment_type"	=> $this->input->post("payment_type"),
												"designation"	=> $this->input->post("designation"),
												"date_hired"	=> $this->input->post("date_hired"),
												"status"		=> $this->input->post("status"),
												"gender_status"	=> $this->input->post("gender_status"),
												"empd_ref"  	=> $decrypt_emp_ref
											);

					$usergroup = array(
										"user_group_id" => $this->input->post("user_group_id"),
										"group_ref"		=> $decrypt_group_ref
									);


					$data['basic'] 		= $basic;
					$data['empdetails']	= $empdetails;
					$data['group']		= $usergroup;
					
					$response = $this->mduser->update_user($data,$id,$decrypt_company_id);  
					$this->template->evaluate($response,2);
				}else
				{
					$this->template->set_message(validation_errors("<p>","</p>"));
				}
			} 
		}
		$this->template->response(false);
	}
	public function remove($enc_id,$enc_company_id)
	{
		$id = decrypt($enc_id);
		$company_id = decrypt($enc_company_id);
		if(!empty($id) && !empty($company_id) && $this->input->is_ajax_request())
		{   
			$response = $this->mduser->remove($id,$company_id);
			$this->template->evaluate($response,3);
		}
		$this->template->response(false);
	}

	// users listing for datatables
	public function users_listing()
	{
		$response_data = array(
			"data" => array(),
			"draw" => 1,
			"recordsFiltered" => 0,
			"recordsTotal" => 0
		);
		if($this->input->is_ajax_request())
		{	
			$datatable = $this->input->get();
			$start = $datatable['start'];
			$limit = $datatable['length'];
			$filters = array(
				"search_user" => $datatable['search']['value']
			);
			// if isset filters
			if(isset($_GET['filters']))
			{
				$temp = json_decode($this->input->get('filters'));
                foreach ($temp as $key => $value) {
                	if($key=="company_id")
                	{
                		$value = decrypt($value);
                		if(empty($value))
                		{
                			$value = get_company_id();
                		}
                	}
                    $filters[$key] = $value;
                }
			}
			if(!is_super_admin())
			{
				//$filters['undisplay_comp_admin'] = 2;

			}

			$column = array("order_by_name","order_by_empid","order_by_department","order_by_designation","order_by_rate","order_by_date_hired","order_by_phone","order_by_status");
			$filters[$column[$datatable["order"][0]["column"]]] = $datatable["order"][0]["dir"]; 

			//print_r($filters); 
			$result = $this->mduser->users_listing($filters,$start,$limit);
			if($result['totalCount']>0)
			{
				foreach ($result['result'] as $key => $value)
				{
					$value['id'] = encrypt($value['id']);
					$value['company_id'] = encrypt($filters['company_id']);
					$value['profile_pic'] = profile_picture($value['profile_pic']);
					$value['name'] = '<div class="photo-box photo-box-xs pull-left mr10"><img class="img-responsive" src="'.$value['profile_pic'].'"/></div><a href="javascript:void(0)" class="mt5 center-block  g-view-profile mr5" data-toggle="tooltip"   data-uid="'.$value['id'].'" data-id="'.$value['id'].'" title="View Profile">'.$value['firstname']." ".$value['lastname'].'</a>';
					$value['employee_id'] = $value['emp_id'];
					$value['department'] = $value['dept_description'];
					$value['designation'] = $value['designation'];
					$value['rate'] 		 = number_format($value['basic_pay'],2);
					$value['date_hired'] = date("D  M d Y",strtotime($value['date_hired']));
					// $value['user_group'] = $value['user_type_description'];
					// $value['email'] = '<a href="mailto:'.$value['email'].'">'.$value['email']."</a>";
					$value['phone'] =  $value['phone'];
					$value['date_added'] = date('F d, Y',strtotime($value['created_at']));
					$value['status'] = $value['status']==1 ? '<button type="button" class="btn-success btn btn-xs"><i class="fa fa-check-circle-o"></i> Active</span>':'<span class="label label-danger"><i class="fa fa-times-circle-o"></i> Inactive</span>';
					$value['role'] = ucfirst($value['user_type']);
					$value['action'] = ""; 
					$delete = '';
					if($value['employee_id']!=0)
					{
						$delete = '<a href="javascript:void(0)" class="btn btn-danger remove-user-btn" data-toggle="tooltip" data-id="'.$value['id'].'" data-company-id="'.$value['company_id'].'" title="Delete"><i class="fa fa-trash-o "></i></a>';
						$value['action'] = '<div class="btn-groupX btn-group-xs"><a href="javascript:void(0)" class="hidden btn btn-warning g-view-profile mr5" data-toggle="tooltip" data-active-tab="savings" data-uid="'.$value['id'].'" data-id="'.$value['id'].'" title="View Savings"><i class="fa fa-dollar"></i></a><a href="javascript:void(0)" class="btn btn-info view-user-btn mr5 hidden" data-toggle="tooltip" data-id="'.$value['id'].'" title="View Details"><i class="fa fa-search-plus"></i></a><a href="javascript:void(0)" class="btn btn-success edit-user-btn mr5" data-toggle="tooltip" data-id="'.$value['id'].'" title="Edit"><i class="fa fa-pencil"></i></a>'.$delete.'</div>'; 
					}
					else
					{
						$value['name'] 			= $value['firstname']." ".$value['lastname'];
						$value['rate'] 		 	= "";
						$value['date_hired'] 	= "";
						$value['employee_id'] 	= "";
					}

						
					$response_data['data'][] = $value;
				}
			}
			$response_data['draw'] = $datatable['draw'];
			$response_data['recordsFiltered'] = $result['totalCount'];
			$response_data['recordsTotal'] = $result['totalCount'];
		}
		$this->template->array_to_json($response_data);
	}
	/*call back email*/
	public function is_email_check($email)
    {   
    	$response = true;
    	$company_id = $this->input->post("company_id"); 
		$decrypt_company_id = decrypt($company_id);
		if(empty($decrypt_company_id))
		{
			$decrypt_company_id = get_company_id();
		}
        $this->db->where("email",$email);
         $this->db->where("company_id",$decrypt_company_id);
        $query = $this->db->get(USERS)->num_rows(); 
        if($query>0)
        { 
        	$response = false;
        }
        return $response;
    }
    public function is_email_check_existing()
    {   
    	$email = $this->input->post("email");
    	$id = $this->input->post("id");
    	$company_id = $this->input->post("company_id");
    	$id = decrypt($id);
    	$company_id = decrypt($company_id);
    	if(empty($id))
    	{
    		return false;
    	}
    	if(empty($company_id))
    	{
    		return false;
    	}
    	$response = true;
        $this->db->where("email",$email);
         $this->db->where("company_id",$company_id);
        $query = $this->db->get(USERS)->row_array();  
        if(!empty($query))
        { 
        	if($query['id'] != $id)
        	{
        		return false;
        	} 
        }
        return $response;
    }
}
