<?php

if(!function_exists('encrypt'))
{
	function encrypt($string)
	{
		$ci = &get_instance();
		return $ci->my_encrypt->encode($string);
	}
}
if(!function_exists('decrypt'))
{
	function decrypt($string)
	{
		$ci = &get_instance();
		return $ci->my_encrypt->decode($string);
	}
}
if(!function_exists('is_admin'))
{
	function is_admin($user_id="")
	{
		$ci = &get_instance();
		return $ci->session->userdata('user_type') == 'admin' || $ci->session->userdata('user_type') == 'root' ||  $ci->session->userdata('user_type') == 'hr'  || $ci->session->userdata('user_type') == 'accountant';
	}
}
if(!function_exists('is_super_admin'))
{
	function is_super_admin($user_id="")
	{
		$ci = &get_instance();
		return $ci->session->userdata('user_type') == 'root';
	}
}

if(!function_exists('is_admin_alone'))
{
	function is_admin_alone($user_id="")
	{
		$ci = &get_instance();
		return $ci->session->userdata('user_type') == 'admin';
	}
}

if(!function_exists('get_timezone'))
{
	function get_timezone($standard_time="PST – Pacific Standard Time")
	{
		if($standard_time=="PST – Pacific Standard Time" || $standard_time=="Pacific Standard Time" || $standard_time=="PDT – Pacific Daylight Time" || $standard_time=="Pacific Daylight Time" || $standard_time=="PST – Pacific Standard Time" || $standard_time=="PDT – Pacific Daylight Time")
		{
			return "America/Los_Angeles";
		}else if($standard_time=="CST – Central Standard Time" || $standard_time=="Central Standard Time" || $standard_time=="CDT – Central Daylight Time" || $standard_time=="Central Daylight Time" || $standard_time=="CST – Central Standard Time" || $standard_time=="CDT – Central Daylight Time")
		{
			return "America/Chicago";
		}else if($standard_time=="EST – Eastern Standard Time" || $standard_time=="Eastern Standard Time" || $standard_time=="EDT – Eastern Daylight Time" || $standard_time=="Eastern Daylight Time" || $standard_time=="EST – Eastern Standard Time" || $standard_time=="EDT – Eastern Daylight Time")
		{
			return "America/New_York";
		}else if($standard_time=="MST – Mountain Standard Time" || $standard_time=="Mountain Standard Time" || $standard_time=="MST – Mountain Standard Time")
		{
			return "America/Denver";
		}
		else if($standard_time=="MDT - Mountain Daylight Time" || $standard_time=="Mountain Daylight Time" || $standard_time=="MDT – Mountain Daylight Time")
		{
			return "America/Phoenix";
		}
		else if($standard_time=="AKST – Alaska Standard Time" || $standard_time=="Alaska Standard Time" || $standard_time=="AKDT – Alaska Daylight Time" || $standard_time=="Alaska Daylight Time" || $standard_time=="AKST – Alaska Standard Time" || $standard_time=="AKDT – Alaska Daylight Time")
		{
			return "America/Anchorage";
		}
		else if($standard_time=="HAST – Hawaii-Aleutian Standard Time" || $standard_time=="Hawaii-Aleutian Standard Time" || $standard_time=="HAST – Hawaii-Aleutian Standard Time")
		{
			return "America/Adak";
		}
		else if($standard_time=="HADT – Hawaii-Aleutian Daylight Time" || $standard_time=="Hawaii-Aleutian Daylight Time" || $standard_time=="HADT – Hawaii-Aleutian Daylight Time")
		{
			return "Pacific/Honolulu";
		}
		return $standard_time;
	}
}
if(!function_exists('get_months'))
{
	function get_months($month="")
	{
		$months = array(
						"January","February","March","April","May","June","July","August","September","October","November","December"
					);
		if(!empty($month))
		{
			if($month > 0)
			{
				return $months[$month - 1];
			}
		}
		return $months;
	}
}
if(!function_exists('get_correct_timezone'))
{
	function get_correct_timezone($standard_time="PST – Pacific Standard Time")
	{
		if($standard_time=="PST – Pacific Standard Time" || $standard_time=="Pacific Standard Time" || $standard_time=="America/Los_Angeles")
		{
			return "PST – Pacific Standard Time";
		}else if($standard_time=="PDT – Pacific Daylight Time" || $standard_time=="Pacific Daylight Time")
		{
			return "PDT – Pacific Daylight Time";
		}else if($standard_time=="CST – Central Standard Time" || $standard_time=="Central Standard Time" || $standard_time=="America/Chicago")
		{
			return "CST – Central Standard Time";
		}else if($standard_time=="CDT – Central Daylight Time" || $standard_time=="Central Daylight Time")
		{
			return "CDT – Central Daylight Time";
		}else if($standard_time=="EST – Eastern Standard Time" || $standard_time=="Eastern Standard Time" || $standard_time=="America/New_York")
		{
			return "EST – Eastern Standard Time";
		}else if($standard_time=="EDT – Eastern Daylight Time" || $standard_time=="Eastern Daylight Time")
		{
			return "EDT – Eastern Daylight Time";
		}else if($standard_time=="MST – Mountain Standard Time" || $standard_time=="Mountain Standard Time" || $standard_time=="America/Denver")
		{
			return "MST – Mountain Standard Time";
		}
		else if($standard_time=="MDT – Mountain Daylight Time" || $standard_time=="Mountain Daylight Time" || $standard_time=="America/Phoenix")
		{
			return "MDT – Mountain Daylight Time";
		}
		else if($standard_time=="AKST – Alaska Standard Time" || $standard_time=="Alaska Standard Time" || $standard_time=="America/Anchorage")
		{
			return "AKST – Alaska Standard Time";
		}
		else if($standard_time=="AKDT – Alaska Daylight Time" || $standard_time=="Alaska Daylight Time")
		{
			return "AKDT – Alaska Daylight Time";
		}else if($standard_time=="HAST – Hawaii-Aleutian Standard Time" || $standard_time=="Hawaii-Aleutian Standard Time" || $standard_time=="America/Adak")
		{
			return "HAST – Hawaii-Aleutian Standard Time";
		}
		else if($standard_time=="HADT – Hawaii-Aleutian Daylight Time" || $standard_time=="Hawaii-Aleutian Daylight Time" || $standard_time=="Pacific/Honolulu")
		{
			return "HADT – Hawaii-Aleutian Daylight Time";
		}
		return $standard_time;
	}
}

if(!function_exists('get_standard_timezone'))
{
	function get_standard_timezone($timezone)
	{
		if($timezone=="America/Los_Angeles")
		{
			return "PST – Pacific Standard Time";
		}else if($timezone=="America/Chicago")
		{
			return "CST – Central Standard Time";
		}else if($timezone=="America/New_York")
		{
			return "EST – Eastern Standard Time";
		}else if($timezone=="America/Denver")
		{
			return "MST – Mountain Standard Time";
		}
		else if($timezone=="America/Phoenix")
		{
			return "MDT - Mountain Daylight Time";
		}
		else if($timezone=="America/Anchorage")
		{
			return "AKST – Alaska Standard Time";
		}
		else if($timezone=="America/Adak")
		{
			return "HAST – Hawaii-Aleutian Standard Time";
		}
		else if($timezone=="Pacific/Honolulu")
		{
			return "HADT – Hawaii-Aleutian Daylight Time";
		}
		return $timezone;
	}
}

if(!function_exists('get_timezone_offset'))
{
	function get_timezone_offset($remote_tz, $origin_tz = null) {
	    if($origin_tz === null) {
	        if(!is_string($origin_tz = date_default_timezone_get())) {
	            return false; // A UTC timestamp was returned -- bail out!
	        }
	    }
	    $origin_dtz = new DateTimeZone($origin_tz);
	    $remote_dtz = new DateTimeZone($remote_tz);
	    $origin_dt = new DateTime("now", $origin_dtz);
	    $remote_dt = new DateTime("now", $remote_dtz);
	    $offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
	    return $offset;
	}
}

if(!function_exists('convert_timezone'))
{
	function convert_timezone($time="",$toTz="",$fromTz="")
	{
		// timezone by php friendly values
	    $date = new DateTime($time, new DateTimeZone($fromTz));
	    $date->setTimezone(new DateTimeZone($toTz));
	    $time= $date->format('Y-m-d H:i:s');
	    return $time;
	}
}

if(!function_exists('convert_date_timezone'))
{
	function convert_date_timezone($time="",$toTz="",$fromTz="")
	{
		// timezone by php friendly values
	    $date = new DateTime($time, new DateTimeZone($fromTz));
	    $date->setTimezone(new DateTimeZone($toTz));
	    $time= $date->format('Y-m-d');
	    return $time;
	}
}

if(!function_exists('is_hr'))
{
	function is_hr($user_id="")
	{
		$ci = &get_instance();
		return $ci->session->userdata('user_type') == 'hr';
	}
}
if(!function_exists('is_accountant'))
{
	function is_accountant($user_id="")
	{
		$ci = &get_instance();
		return $ci->session->userdata('user_type') == 'accountant';
	}
}
if(!function_exists('is_employee'))
{
	function is_employee($user_id="")
	{
		$ci = &get_instance();
		return $ci->session->userdata('user_type') == 'employee';
	}
}

if(!function_exists('is_logged_in'))
{
	function is_logged_in()
	{
		$ci = &get_instance();
		return $ci->session->has_userdata("logged_in") && $ci->session->userdata("logged_in")==true;
	}
}

if(!function_exists('user_id'))
{
	function user_id()
	{
		$ci = &get_instance();
		return decrypt($ci->session->userdata("user_id"));
	}
}

if(!function_exists('encrypted_user_id'))
{
	function encrypted_user_id()
	{
		$ci = &get_instance();
		return $ci->session->userdata("user_id");
	}
}

if(!function_exists('profile_picture'))
{
	function profile_picture($filename="")
	{
		$profile_picture = base_url("assets/images/avatar-default.jpg");
		if($filename!="")
		{
			if(file_exists("./uploads/profile/".$filename))
			{
				$profile_picture = base_url("uploads/profile/".$filename);
			}
		}
		return $profile_picture;
	}
}

if(!function_exists('company_picture'))
{
	function company_picture($filename="",$company_id="")
	{
		$profile_picture = base_url("assets/images/avatar-company.png");
		if($filename!="")
		{
			if(file_exists("./uploads/company/".$filename))
			{
				$profile_picture = base_url("uploads/company/".$filename);
			}
		}
		else
		{
			$company_id = empty($company_id)? get_company_id() : $company_id;

			$ci =& get_instance();
			$ci->db->where("id",$company_id);
			$query = $ci->db->get(COMPANY)->row_array();
			if(!empty($query))
			{
				if($query['company_pic']!="")
				{
					$filename = $query['company_pic'];
					if(file_exists("./uploads/company/".$filename))
					{
						$profile_picture = base_url("uploads/company/".$filename);
					}
				}
			}
		}
		return $profile_picture;
	}
}

if(!function_exists('print_me'))
{
	function print_me($arr)
	{
		echo "<pre>";
		if(is_array($arr))
		{
			print_r($arr);
		}else
		{
			echo $arr;
		}
		echo "</pre>";
	}
}

if(!function_exists('random_string'))
{
	function random_string($length)
	{
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    $pass = array();
	    $alphaLength = strlen($alphabet) - 1;
	    for ($i = 0; $i < $length; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass);
	}
}

if(!function_exists('get_user_info'))
{
	function get_user_info($id="")
	{
		if($id=="")
		{
			$id = user_id();
		}
		$ci =& get_instance();
		$ci->load->database();

		$user = USERS;
        $user_group = USER_GROUPS;
        $user_ref = USER_GROUP_REFERENCE;
        $employee 		= EMP_DETAILS;
		$department 	= MNT_DEPT;

        $ci->db->select("u.*,ur.user_group_id,ug.name as user_type,ug.description as user_type_description");
        $ci->db->from("{$user} u");
        $ci->db->where("u.id",$id);
        $ci->db->join("{$user_ref} ur","ur.user_id=u.id","inner");
        $ci->db->join("{$user_group} ug","ug.id=ur.user_group_id");
        $response = $ci->db->get()->first_row('array');

        return $response;
	}
}
if(!function_exists('get_employee_details'))
{
	function get_employee_details($id="")
	{
		if($id=="")
		{
			$id = user_id();
		}
		$ci =& get_instance();
		$ci->load->database();

		$employee 		= EMP_DETAILS;
		$department 	= MNT_DEPT;
		$user 			= USERS;

        $ci->db->select("emp.*,dept.description,dept.name,u.firstname,u.lastname,u.emp_id");
        $ci->db->from("{$employee} emp");
        $ci->db->where("emp.user_id",$id);
        $ci->db->join("{$department} dept","dept.id=emp.department_id");
         $ci->db->join("{$user} u","u.id=emp.user_id");


        $response = $ci->db->get()->first_row('array');
        return $response;
	}
}

if(!function_exists('get_name'))
{
	function get_name($id="")
	{
		if($id=="")
		{
			$id = user_id();
		}
		$ci =& get_instance();
		$ci->load->database();
		$ci->db->where("id",$id);
        $ci->db->select("firstname,lastname");
        $response = $ci->db->get(USERS)->first_row('array');
        return $response['firstname']." ".$response['lastname'];
	}
}
if(!function_exists('get_company_name'))
{
	function get_company_name($id="")
	{
		if($id=="")
		{
			$id = get_company_id();
		}
		//$user 		= USERS;
		$company    = COMPANY;
		$ci =& get_instance();
		$ci->load->database();
		// $ci->db->where("u.id",$id);
  //       $ci->db->select("c.name");
  //       $ci->db->from("{$user} u");
  //       $ci->db->where("u.id",$id);
  //       $ci->db->join("{$company} c","c.id=u.company_id");
  //       $response = $ci->db->get()->row_array();
  //       print_r($response);
		$ci->db->where("id",$id);
		$response = $ci->db->get($company)->row_array(); 
        return $response['name'];
	}
}
if(!function_exists('get_company_details'))
{
	function get_company_details($id="")
	{
		if($id=="")
		{
			$id = get_company_id();
		}
		$user 		= USERS;
		$company    = COMPANY;
		$ci =& get_instance();
		$ci->load->database();
		$ci->db->where("id",$id);
		$response = $ci->db->get(COMPANY)->row_array();
        return $response;
	}
}
if(!function_exists('get_company_id'))
{
	function get_company_id($id="")
	{
		$response = 0;
		if($id=="")
		{
			$id = user_id();
		}
		$ci =& get_instance();
		$ci->load->database();

		$user = USERS;

        $ci->db->select("u.company_id");
        $ci->db->from("{$user} u");
        $ci->db->where("u.id",$id);
        $response = $ci->db->get()->first_row('array');
        if($response)
        {
        	$response = $response['company_id'];
        }
        return $response;
	}
}

if(!function_exists('get_file_icon'))
{
	function get_file_icon($filetype)
	{
		$file_icon = "fa-file-image-o";
		if($filetype=="application/pdf" || $filetype=="application/force-download" || $filetype=="application/x-download" || $filetype=="binary/octet-stream")
		{
			$file_icon = "fa-file-pdf-o";
		}else if($filetype=="text/rtf")
		{
			$file_icon = "fa-file-text-o";
		}else if($filetype=="application/msword" || $filetype=="application/vnd.ms-office" || $filetype=="application/vnd.openxmlformats-officedocument.wordprocessingml.document" || $filetype=="application/msword")
		{
			$file_icon = "fa-file-word-o";
		}
		return $file_icon;
	}
}

if(!function_exists('get_hex_color'))
{
	function get_hex_color($hex, $percent)
	{
		// Work out if hash given
		$hash = '';
		if (stristr($hex,'#')) {
			$hex = str_replace('#','',$hex);
			$hash = '#';
		}
		/// HEX TO RGB
		$rgb = array(hexdec(substr($hex,0,2)), hexdec(substr($hex,2,2)), hexdec(substr($hex,4,2)));
		//// CALCULATE
		for ($i=0; $i<3; $i++) {
			// See if brighter or darker
			if ($percent > 0) {
				// Lighter
				$rgb[$i] = round($rgb[$i] * $percent) + round(255 * (1-$percent));
			} else {
				// Darker
				$positivePercent = $percent - ($percent*2);
				$rgb[$i] = round($rgb[$i] * $positivePercent) + round(0 * (1-$positivePercent));
			}
			// In case rounding up causes us to go to 256
			if ($rgb[$i] > 255) {
				$rgb[$i] = 255;
			}
		}
		//// RBG to Hex
		$hex = '';
		for($i=0; $i < 3; $i++) {
			// Convert the decimal digit to hex
			$hexDigit = dechex($rgb[$i]);
			// Add a leading zero if necessary
			if(strlen($hexDigit) == 1) {
			$hexDigit = "0" . $hexDigit;
			}
			// Append to the hex string
			$hex .= $hexDigit;
		}
		return $hash.$hex;
	}
}

if(!function_exists('get_curl_json'))
{
	function get_curl_json($url)
	{
		$curl = curl_init($url);
		curl_setopt ($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_URL, $url);
		$response = curl_exec($curl);
		curl_close($curl);
		return json_decode($response,false);
	}
}

if(!function_exists('filter_link'))
{
	function filter_link($html)
	{
	    if($html=="")
	    {
	        $html = "<p></p>";
	    }
	    $dom = new DOMDocument();
	    $dom->loadHTML($html);

		//Evaluate Anchor tag in HTML
		$xpath = new DOMXPath($dom);
		$hrefs = $xpath->evaluate("/html/body//a");
		for ($i = 0; $i < $hrefs->length; $i++) {
		    $href = $hrefs->item($i);

		    //remove attribute
		    $href->removeAttribute('data-mce-href');
		}
		// save html
		$html = $dom->saveHTML();
		return $html;
	}
}

if(!function_exists('is_weekend'))
{
	function is_weekend()
	{
		$week_day = date("l");
		$time = (int)date("Gis");
	    return ($week_day=="Saturday" || $week_day=="Sunday" || $week_day=="Monday") && (($week_day=="Saturday" && $time >= 180000) || ($week_day=="Sunday" && $time >= 0 && $time <= 235959) || ($week_day=="Monday" && $time <= 180000));
	}
}

/*
*
* SPECIAL ROLES
*/

if(!function_exists('firstname_lastname'))
{
	function firstname_lastname($name)
	{
		$new_name = "";
		$new_name_arr = array();
		$new_name_arr = explode(", ",$name);
		foreach (array_reverse($new_name_arr) as $key => $value) {
			$new_name .= $value." ";
		}
	    return $new_name;
	}
}

if(!function_exists('get_companies'))
{
	function get_companies($user_id="")
	{
		$company_ids = array();
		$data = array();

		if($user_id=="")
		{
			$user_id = user_id();
		}
		$ci =& get_instance();
		$ci->load->database();

		$ci->db->distinct("lending_company_id");
		$ci->db->select("lending_company_id");
		$ci->db->where("user_id",$user_id);
		$data = $ci->db->get(COMPANY_MEMBERS)->result_array();
		if(!empty($data))
		{
			foreach ($data as $key => $value) {
				$company_ids[] = $value['lending_company_id'];
			}
		}

		return $company_ids;
	}

} 
if(!function_exists('get_user_types'))
{
	function get_user_types()
	{
		$ci =& get_instance();
		$ci->load->database();
		$ci->db->where("id !=",1);
		$data = $ci->db->get(USER_GROUPS)->result_array();
		return $data;
	}
}

// function get_expenses_category($category=0)
// {
// 	$response = array(
// 					array( "id" 	=> 1, "name"	=> "Office Supplies", "children" => array() ),
// 					array( "id" 	=> 2, "name"	=> "Office Rental", "children" => array() ),
// 					array( "id" 	=> 3, "name"	=> "Utilities", "children" => array(
// 																			array("id" => 4,"name" => "Internet"),
// 																			array("id" => 5,"name" => "Electricity"),
// 																	) ),
// 					array( "id" 	=> 6, "name"	=> "Transportation" , "children" => array() ),
// 					array( "id" 	=> 7, "name"	=> "Repair & Maintenance" , "children" => array() ),
// 					array( "id" 	=> 8, "name"	=> "Representation", "children" => array()  ),
// 					array( "id" 	=> 9, "name"	=> "Others", "children" => array()  )
// 				);
// 	if($category>0)
// 	{
// 		foreach($response as $value)
// 		{
// 			if($value['id']==$category)
// 			{
// 				return $value;
// 			}
// 			else if($value['children'])
// 			{
// 				if(!empty($value['children']))
// 				{
// 					foreach($value['children'] as $subvalue)
// 					{
// 						if($subvalue['id']==$category)
// 						{
// 							return $subvalue;
// 						}
// 					}
// 				}

// 			}
// 		}
// 	}
// 	return $response;
// }
if(!function_exists('get_payroll_number'))
{
	function get_payroll_number()
	{
		$company_id = get_company_id();
		$ci =& get_instance();
		$ci->load->database();
		$ci->db->select("payroll_number");
		$ci->db->from("payroll");
		$ci->db->where("company_id",$company_id);
		$ci->db->order_by("id","DESC");
		$ci->db->limit(1);
		$data = $ci->db->get()->row_array();

		$response = "P0001";
		if(!empty($data))
		{
			$pay_id = (int)str_replace("P","",$data['payroll_number']);
			$response = "P".str_pad($pay_id+1, 4, '0',STR_PAD_LEFT);
		}
		return $response;
	}
}
function gender_status_types()
{
	$data = array(
				"S" => "Single",
				"ME" => "Married"
			);
	$loop = $data;
	foreach($loop as $key=>$val)
	{
		for($i=1;$i<=4;$i++)
		{
			$data[$key.$i] = $val." w/ {$i} Dependent";
		}
	}
	return $data;
}
if(!function_exists('get_last_payperiod'))
{
	function get_last_payperiod($userid="")
	{
		$response = " - ";
		if(empty($userid))
		{
			return $response;
		}
		$company_id = get_company_id();
		$ci =& get_instance();
		$ci->load->database();
		$ci->db->select("*");
		$ci->db->from(EMP_PAYROLL);
		$ci->db->where("company_id",$company_id);
		$ci->db->where("user_id",$userid);
		$ci->db->group_by("payrun_id");
		$ci->db->order_by("payrun_id","DESC");
		$ci->db->limit(1);
		$data = $ci->db->get()->row_array();
		if(!empty($data))
		{
			$payrunid = $data["payrun_id"];
			$ci->db->select("period_from,period_to");
			$ci->db->from("payroll");
			$ci->db->where("id",$payrunid);
			$return = $ci->db->get()->row_array();
			if(!empty($return))
			{
				$response = $return['period_from']." - ".$return['period_to'];
			}

		}
		return $response;
	}
}
function format_date($date="",$format="m/d/Y")
{
	return date($format,strtotime($date));
}
function remove_number_format($number="")
{
	return floatval(str_replace(',', '', $number));
}
function get_expenses_category($category=0)
{
	$ci =& get_instance();
	$ci->load->model("mdexpenses");

	$final_data = array();

	$result = $ci->mdexpenses->get_categories();
	if(!empty($result))
	{
		foreach($result as $value)
		{
			$children = $ci->mdexpenses->get_categories($value['id']);
			$value['children'] = $children;
			$final_data[] = $value;
		}
	}
	if($category>0)
	{
		foreach($final_data as $value)
		{
			if($value['id']==$category)
			{
				return $value;
			}
			else if($value['children'])
			{
				if(!empty($value['children']))
				{
					foreach($value['children'] as $subvalue)
					{
						if($subvalue['id']==$category)
						{
							return $subvalue;
						}
					}
				}

			}
		}
	} 
	return $final_data;
}
if(!function_exists('get_departments'))
{
	function get_departments($category=0)
	{
		$ci =& get_instance();
		$ci->load->model("mdsettings");

		$final_data = array();

		$result = $ci->mdsettings->get_department();
		if(!empty($result))
		{
			foreach($result as $value)
			{
				$children = $ci->mdsettings->get_department($value['id']);
				$value['children'] = $children;
				$final_data[] = $value;
			}
		}
		if($category>0)
		{
			foreach($final_data as $value)
			{
				if($value['id']==$category)
				{
					return $value;
				}
				else if($value['children'])
				{
					if(!empty($value['children']))
					{
						foreach($value['children'] as $subvalue)
						{
							if($subvalue['id']==$category)
							{
								return $subvalue;
							}
						}
					}

				}
			}
		} 
		return $final_data;
	}
}