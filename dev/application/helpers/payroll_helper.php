<?php
if(!function_exists('payrun_personnel_count'))
{
	function payrun_personnel_count($payrun_id="")
	{
    $response = 0;
		$ci = &get_instance();
		if(!empty($payrun_id))
    {
        $ci->db->select("*");
        $ci->db->from(EMP_PRL);
        $ci->db->where("payrun_id",$payrun_id);
        $ci->db->group_by(array("user_id"));
        $result = $ci->db->get();
        $response = $result->num_rows();
    }
    return $response;
	}
}
if(!function_exists('payrun_total'))
{
	function payrun_total($payrun_id="",$type="cost")
	{
    $response = 0;
		$ci = &get_instance();
		if(!empty($payrun_id))
    {
      $ci->db->select("emp_prl.*,pmnt.type as pay_type", FALSE);
      $ci->db->from(EMP_PRL." emp_prl");
      $ci->db->join(PRL_MNT." pmnt", 'pmnt.name = emp_prl.mnt_di_id');
      $ci->db->where("emp_prl.payrun_id",$payrun_id);
      $result = $ci->db->get()->result_array();
        if(!empty($result))
        {
          $total_income     = 0;
          $total_deduction  = 0;
          foreach ($result as $key => $value) {
              if($value['pay_type']=="incentive")
              {
                $total_income = $total_income + $value['value'];
              }
              else
              {
                $total_deduction = $total_deduction + $value['value'];
              }
          }
          if($type=="cost")
          {
              $response = $total_income;
          }
          if($type=="pay")
          {
            $response = $total_income - $total_deduction;
          }
          if($type=="deduction")
          {
            $response = $total_deduction;
          }
        }
    }
    return $response;
	}
}
