<?php
/*
* TAX CALCULATOR
*
* BOR MATRIX:
* SSS MATRIX: https://www.sss.gov.ph/sss/appmanager/pages.jsp?page=scheduleofcontribution
* PHILHEALTH MATRIX: https://www.philhealth.gov.ph/partners/employers/contri_tbl.html
* PAG-IBIG/HDMF:
*     P1500 and below = 1%
*     Over = 2% (or P100)
*
*/

	function getCivilStatus($cstatus)
	{
		$status = "";
		if($cstatus =='S' || $cstatus =='ME')
		{
			$status	='S/ME';
		}
		else if($cstatus =='S1' || $cstatus =='ME1')
		{
			$status	='S1/ME1';
		}
		else if($cstatus =='S2' || $cstatus =='ME2')
		{
			$status	='S2/ME2';
		}
		else if($cstatus =='S3' || $cstatus =='ME3')
		{
			$status	='S3/ME3';
		}
		else if($cstatus =='S4' || $cstatus =='ME4')
		{
			$status	='S4/ME4';
		}
		return $status;
	}

	//get payment mode
	function getPeriod2($paymode="monthly")
	{
		$payModeVal = "";
		if($paymode =='monthly')
		{
			$payModeVal	='monthly';
		}
		else if($paymode =='semiMonthly')
		{
			$payModeVal	='semiMonthly';
		}
		return $payModeVal;
	}
	/*
	* Frequency of payment/payroll
	*/
	function getPeriod($period, $ee) {
		if($period == "monthly")
		{
			return $ee;
		}
		else if ($period == "semimonthly")
		{
			return ($ee/2);
		} else if ($period == "weekly")
		{
			return ($ee/4);
		}
	}
	/*
	* SSS Contributions
	* Contributions = 11% of mo. salary (not more than P16000)
	* Employee Share (EE) = 3.63%
	* Employer Share (ER) = 7.37%
	*/
	function getSSSContribution($basicPay="", $memStatus="employed", $period="monthly",$response="share")
	{
		$data = file_get_contents(FCPATH."assets/js/sss_table.json");
		$sssData = json_decode($data,TRUE);

		$period 	= str_replace("-","",$period);

		$sssEE 		= 0;
		$sssER 		= 0;
		$sssShare 	= 0;
		foreach($sssData as $sssKey=>$sssVal)
		{
					$dataTable		= $sssVal['membershipStatus'][0]['dataTable'];
					$bracketCount	= count($dataTable)-1;
					$minRate		= $dataTable[0]['salaryRangeEnd'];
					$maxRate		= $dataTable[$bracketCount]['salaryRangeStart'];

					if($memStatus === $sssVal['membershipStatus'][0]['status'])
					{
						$membershipType 	= $dataTable;
						foreach($membershipType as $sssSubVal)
						{
							$salaryRangeStart	= $sssSubVal['salaryRangeStart'];
							$salaryRangeEnd		= $sssSubVal['salaryRangeEnd'];

							if($basicPay <= $minRate && $minRate == $salaryRangeEnd)
							{
								$sssEE 		= $sssSubVal['ee'];
								$sssER 		= $sssSubVal['er'];
								$sssShare 	= getPeriod($period,$sssEE);
							}
							else if($basicPay >= $salaryRangeStart && $basicPay <= $salaryRangeEnd)
							{
								$sssEE 		= $sssSubVal['ee'];
								$sssER 		= $sssSubVal['er'];
								$sssShare 	= getPeriod($period,$sssEE);
							}
							else if($basicPay>=$salaryRangeEnd)
							{
								$sssEE 		= $sssSubVal['ee'];
								$sssER 		= $sssSubVal['er'];
								$sssShare 	= getPeriod($period,$sssEE);
							}
						}
					}
		}
		$data = array(
					"share" 	=> $sssShare,
					"company"	=> getPeriod($period,$sssER)
				);
		if($response=="all")
		{
			return $data;
		}
		else if(isset($data[$response]))
		{
			return $data[$response];
		}
	}

	/*
	* Withholding Tax
	*/
	function getWithholdingTax($grossIncome,$deductions,$cStatus, $period="monthly"){
		$period 	= str_replace("-","",$period); 

		$data = file_get_contents(FCPATH."assets/js/tax_table.json");
		$taxData = json_decode($data,TRUE);
		$cStatus = (empty($cStatus))? "S/ME" : $cStatus;
		$tax = 0;
		$diminish = 1250;
		$deductions = $deductions+$diminish;
		$taxableIncome = $grossIncome - $deductions;   
		foreach($taxData[0]['withholdingTax'] as $taxKey => $taxVal)
		{
			$taxPeriod	= $taxVal['period'];
			$taxTable 	= $taxVal['dataTable']; 
			if($period !== $taxPeriod)
			{
				continue 1;
			}
			foreach($taxTable as $key=>$val)
			{
				$compensationLevel = $val['compensationLevel'];
				if($cStatus === $val['status']  )
				{
					if($taxableIncome >= $compensationLevel)
					{
						$percent = $val['percent']/100;
						$excess	= $taxableIncome - $compensationLevel;
						$tax 	= ($excess * $percent) + $val['tax'];
					}
				}
			}
		} 
		return $tax;
	}

	/*
	* PAG-IBIG / HDMF  Contributions
	* Contributions = 1% of monthly compensation/salary if less than 1500, otherwise, 2%
	* Employer share = 2% of monthly compensation/salary
	* Max compensations = 5000
	*/
	function getPagibigContribution($monthlyCompensation)
	{ 

		$maxMonthlyCompensation 	= 5000.00;
		$bracket 	= 1500.00;
		if ($monthlyCompensation <= $bracket)
		{
			return $monthlyCompensation * 0.01;
		}
		else if ($monthlyCompensation > $bracket)
		{
			return $maxMonthlyCompensation * 0.02;
		}
	}

    /* PHILHEALTH / PHIC Contributions
    * Contribution or premium = 2.5% of salary rate
    * Employee Share (EE) = 50% of premium
    * Employer Share (ER) = 50% of premium
    * Max. Rate = 35,000
    *  @response = ee,er,premium,share,all
    */
	function getPhilhealthContribution($basicPay, $period="monthly",$response="ee"){
		$period 	= str_replace("-","",$period);

		$data = file_get_contents(FCPATH."assets/js/philhealth_table.json");
		$philhealthData = json_decode($data,TRUE);
		$philhealthPremium 	= 0;
		$philhealthEE 		= 0;
		$philhealthER 		= 0;
		$philHealthShare	= 0;
		foreach($philhealthData as $PhilhealthKey=>$philhealthVal)
		{
			$bracketCount	= count($philhealthVal['philhealth'])-1;
			$minRate		= $philhealthVal['philhealth'][0]['salaryRangeEnd'];
			$maxRate		= $philhealthVal['philhealth'][$bracketCount]['salaryRangeStart'];
			foreach($philhealthVal['philhealth'] as $key=>$val)
			{
				$salaryRangeStart 	=	$val['salaryRangeStart'];
				$salaryRangeEnd		=	$val['salaryRangeEnd'];

				if($basicPay >= $salaryRangeStart && $basicPay <= $salaryRangeEnd)
				{
					$philhealthPremium 	= $val['premium'];
					$philhealthEE		= $val['ee'];
					$philhealthER		= $val['er'];
				}
				else if($basicPay <= $minRate && $minRate == $salaryRangeEnd)
				{
					$philhealthPremium 	= $val['premium'];
					$philhealthEE		= $val['ee'];
					$philhealthER		= $val['er'];
				}
				else if($basicPay >= $maxRate)
				{
					$philhealthPremium 	= $val['premium'];
					$philhealthEE		= $val['ee'];
					$philhealthER		= $val['er'];
				}
			}
		}
		$philHealthShare = getPeriod($period,$philhealthEE);

		$return = array(
						"company"	=> getPeriod($period,$philhealthER),
						"share"		=> getPeriod($period,$philHealthShare)
					);
		if($response=="all")
		{
			return $return;
		}
		else if(isset($return[$response]))
		{
			return $return[$response];
		}
	}

	//get all sss,philhealth,pagibig,tax
	function get_employee_government_details($basic_pay="",$gross_income="",$period="monthly",$cStatus="S/ME",$totaldeductions=0)
	{
		$period 	= str_replace("-","",$period);

		$sss        = getSSSContribution($basic_pay,"employed",$period,"all");
      	$plh        = getPhilhealthContribution($basic_pay,$period,"all");
      	$pgb        = getPagibigContribution($basic_pay);
      	if($period=="semimonthly")
      	{ 
      		$pgb = $pgb/2; 
      	}
      	$totaldeductions = $sss['share'] + $plh['share'] + $pgb + $totaldeductions; 

      	$tax        = getWithholdingTax($gross_income,$totaldeductions,getCivilStatus($cStatus),$period);

      	



      	return array(
      				"sss"		 	=> $sss,
      				"philhealth" 	=> $plh,
      				"pagibig" 	 	=> $pgb,
      				"income_tax"	=> $tax
      			);

	}
	function get_netpay($gross_income=0,$deductions=0)
	{
		return $gross_income - $deductions;
	}

	function get_payroll_mnt($type="incentive",$additional=array(),$lists="",$include=false,$limit="")
	{
		$ci =& get_instance();
		$ci->load->database();

		$response = array();
		//incentives exclude
		$incentives = array("basic_pay");

		//deductions
		$deductions = array(
							"sss",
							"pagibig",
							"philhealth",
							"income_tax"
						);

		$company_id = get_company_id();
		$ci->db->where("(company_id = {$company_id} OR company_id = 0)",NULL,FALSE);
    if($limit!="all")
    {
      $ci->db->where("type",$type); //default deductions
    }
		if(!$include)
		{
			if($type=="incentive")
			{
				$ci->db->where_not_in("name",$incentives);
			}
			else
			{
				$ci->db->where_not_in("name",$deductions);
			}
		}

		if(!empty($additional))
		{
			foreach($additional as $key=>$value)
			{
				$ci->db->where($key,$value);
			}
		}
		$result = $ci->db->get(PRL_MNT)->result_array();
		$response = $result;

		if(!empty($lists))
		{
      $response = array();
      if (is_array($lists)) {
        foreach($result as $value)
  			{
  				$response[$value[$lists['key']]] = $value[$lists['value']];
  			}
      }
			else {
        foreach($result as $value)
  			{
  				$response[] = $value[$lists];
  			}
			}
		}
		return $response;
	}
  function types_default($type="incentives")
  {
    //incentives exclude
		$incentives = array("basic_pay");

		//deductions
		$deductions = array(
							"sss",
							"pagibig",
							"philhealth",
							"income_tax"
						);
    if ($type=="incentives") {
      return $incentives;
    }
    else {
      return $deductions;
    }
  }
 /*
  | @func        - is_payrun_emp_exists()
  | @description - checking payroll of the employee has been added
  | @params      - user id and payrun
  | @return - boolean
  */
function is_payrun_emp_exists($user_id="",$payrun_id="")
 {
   $response = false;
    $company_id = get_company_id();
    if(!empty($payrun_id))
    {
      $ci =& get_instance();
      $ci->load->database();
      $ci->db->select("count(*) as total");
      $ci->db->from(EMP_PRL);
      $ci->db->where("payrun_id",$payrun_id);
			if(!empty($user_id))
			{
					$ci->db->where("user_id",$user_id);
			}
      $ci->db->where("company_id",$company_id);

      $result = $ci->db->get()->row_array();
      if ($result['total']>0) {
        $response = true;
      }
    }
    return $response;
 }
 /* END OF is_payrun_emp_exists */
 /*
  | @func        - generate_default_payroll()
  | @description - generating default payroll
  | @params      - user id array, payrun id, paid(true or false)
  | @return - boolean
  */
function generate_default_payroll($user_id=array(),$payrun_id=0,$paid=false)
 {
 	$response = false;
   if(!empty($user_id) && $payrun_id!=0)
   {
     $ci =& get_instance();
     $ci->load->database();
     $db_data = array();
     $company_id = get_company_id();

     $status = ($paid)? 1 : 0;
     foreach ($user_id as $key => $id) {

      //  if(is_payrun_emp_exists($id,$payrun_id))
      //  {
      //    continue;
      //  }
       $emp_details = get_employee_details($id);
			 $basic_pay   = $emp_details['basic_pay'];
			 if($emp_details['payment_type']=="semi-monthly")
			 {
				 $basic_pay = $emp_details['basic_pay']/2;
			 }
       $db_data[] = array(
                   "company_id" 	=> $company_id,
                   "payrun_id" 	=> $payrun_id,
                   "user_id"		=> $id,
                   "mnt_di_id"		=> "basic_pay",
                   "value"			=> remove_number_format($basic_pay),
                   "company_share"	=> 0,
                   "status"		=> $status //draft
                 );
       $government_deductions = get_employee_government_details($emp_details['basic_pay'],$basic_pay,$emp_details['payment_type'],$emp_details['gender_status'],0);
        foreach($government_deductions as $key=>$value)
   			{
   				if(is_array($value))
   				{
            $db_data[] = array(
                        "company_id" 	=> $company_id,
                        "payrun_id" 	=> $payrun_id,
                        "user_id"		=> $id,
                        "mnt_di_id"		=> $key,
                        "value"			=> remove_number_format($value['share']),
                        "company_share"	=> remove_number_format($value['company']),
                        "status"		=> $status //draft
                      );
   				}
   				else
   				{
            $db_data[] = array(
                        "company_id" 	=> $company_id,
                        "payrun_id" 	=> $payrun_id,
                        "user_id"		=> $id,
                        "mnt_di_id"		=> $key,
                        "value"			=> remove_number_format($value),
                        "company_share"	=> 0,
                        "status"		=> $status //draft
                      );
   				}
   			}
     }
     $query = $ci->db->insert_batch(EMP_PRL,$db_data);
     if($query && $paid==true)
     {
     	$ci->db->where("company_id",$company_id);
     	$ci->db->where("id",$payrun_id);
     	$response = $ci->db->update(PRL,array(
     					"status" => 1
     				));
     }
     else
     {
     	$response = $query;
     }
   }
   return $response;
 }
 /* END OF generate_default_payroll */
