<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['success_added']		= '<p>Successfully added.</p>';
$lang['success_updated']    = '<p>Successfully updated.</p>';
$lang['success_deleted']    = '<p>Successfully deleted.</p>';
$lang['error_added']		= '<p>Request Denied. Cannot add data.</p>';
$lang['error_updated']    	= '<p>Request Denied. Cannot update data.</p>';
$lang['error_deleted']    	= '<p>Request Denied. Cannot delete data.</p>';