<?php

require_once("FedEx.php");

class Rate extends FedEx {

	private $request = array();

    /**
     *  Constructor requires the key, password,
     *  account number and password for the FedEx
     *  account to use the API. You may also override
     *  the default WSDL file.
     *
     *  @param string   // FedEx Key
     *  @param string   // FedEx account password
     *  @param string   // Account number
     *  @param string   // Meter number
     */
	public function __construct()
    {
        parent::__construct();
    }

    public function initialize($key, $passwd, $acct, $meter)
    {
        $this->wsdl_path = APPPATH."libraries/wsdl/RateService_v20.wsdl";

        parent::initialize($key, $passwd, $acct, $meter);
        // TODO: Set this in env()?
        $this->endPoint = 'https://wsbeta.fedex.com:443/web-services/track';
        
        $this->setCustomerTransactionId('Rate Available Services Request using PHP');
        $this->setVersion('crs',20,0,0);
    }

    /**
     *  Gets the tracking detials for the
     *  given tracking number and returns
     *  the FedEx request as an object.
     *
     *  @param string   // Tracking #
     *  @return SoapClient Object
     */
    public function setRequest($additional_request) {

    	$this->request['ReturnTransitAndCommit'] = true;

        $this->request['RequestedShipment']['DropoffType'] = $additional_request['DropoffType']; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
        $this->request['RequestedShipment']['ShipTimestamp'] = date('c');
        // Service Type and Packaging Type are not passed in the request
        $this->request['RequestedShipment']['Shipper'] = array(
            'Address' => $additional_request['ShipperAddress']
        );
        $this->request['RequestedShipment']['Recipient'] = array(
            'Address' => $additional_request['RecipientAddress']
        );
        $this->request['RequestedShipment']['ShippingChargesPayment'] = $additional_request['ShippingChargesPayment'];                                                     
        $this->request['RequestedShipment']['PackageCount'] = $additional_request['PackageCount'];
        $this->request['RequestedShipment']['RequestedPackageLineItems'] = $additional_request['PackageItems'];

    	$req = $this->buildRequest($this->request);

    	return $this->getSoapClient()->getRates($req);;

    }

}
