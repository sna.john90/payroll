<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Mdadmin Class.
 * 
 * @extends CI_Model
 */
class Mdadmin extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function add_admin_status($data=array())
	{
		if(!empty($data))
		{
			$this->db->insert(ADMIN_STATUS,$data);
			return $this->db->insert_id();
		}
		return FALSE;
	}
	public function update_admin_status($data=array(),$filters=false)
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fladmin',$filters);
        }
		if(!empty($data))
		{
			return $this->db->update(ADMIN_STATUS,$data);
		}
		return FALSE;
	}
	public function remove_admin_status($filters=false)
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fladmin',$filters);
        }
		return $this->db->delete(ADMIN_STATUS);
	}

	public function get_admin_status($filters=array())
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fladmin',$filters);
        }
        $response = $this->db->get(ADMIN_STATUS)->first_row('array');
		return $response;
	}

	public function add_notary_status($data=array())
	{
		if(!empty($data))
		{
			$this->db->insert(NOTARY_STATUS,$data);
			return $this->db->insert_id();
		}
		return FALSE;
	}

	public function update_notary_status($data=array(),$filters=false)
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fladmin',$filters);
        }
		if(!empty($data))
		{
			return $this->db->update(NOTARY_STATUS,$data);
		}
		return FALSE;
	}
	public function remove_notary_status($filters=false)
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fladmin',$filters);
        }
		return $this->db->delete(NOTARY_STATUS);
	}
	public function get_notary_status($filters=array())
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fladmin',$filters);
        }
        $response = $this->db->get(NOTARY_STATUS)->first_row('array');
		return $response;
	}
	public function admin_status_lists($filters=false,$select="*")
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fladmin',$filters);
        }
        $this->db->select($select,FALSE);
        $response = $this->db->get(ADMIN_STATUS)->result_array();
		return $response;
	}
	public function get_admin_status_lists()
	{
        $this->db->select("id,status,label,color,border_color");
        $this->db->order_by("label","ASC");
        $response = $this->db->get(ADMIN_STATUS)->result_array();
		return $response;
	}
	public function notary_status_lists($filters=false,$select="*")
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fladmin',$filters);
        }
        $this->db->select($select,FALSE);
        $response = $this->db->get(NOTARY_STATUS)->result_array();
		return $response;
	}

	// Listing for Datatables
	public function admin_status_listing($filters=false,$start=0,$limit=10)
	{
		$response = array();
		// get all records first
		$this->db->start_cache();
		$this->db->stop_cache();

		$response['totalCount'] = $this->db->get(ADMIN_STATUS)->num_rows();

		$this->db->flush_cache();

		// get records with filters
		$this->db->start_cache();
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fladmin',$filters);
        }
        $this->db->select();
        $this->db->from(ADMIN_STATUS);
        $this->db->stop_cache();

        $response['totalFiltered'] = $this->db->get()->num_rows();

        $this->db->limit($limit,$start);
        $response['limit'] = $limit;
        $response['start'] = $start;
        $response['result'] = $this->db->get()->result_array();

        $this->db->flush_cache();

		return $response;
	}
	public function notary_status_listing($filters=false,$start=0,$limit=10)
	{
		$response = array();
		// get all records first
		$this->db->start_cache();
		$this->db->stop_cache();

		$response['totalCount'] = $this->db->get(NOTARY_STATUS)->num_rows();

		$this->db->flush_cache();

		// get records with filters
		$this->db->start_cache();
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fladmin',$filters);
        }
        $this->db->select();
        $this->db->from(NOTARY_STATUS);
        $this->db->stop_cache();

        $response['totalFiltered'] = $this->db->get()->num_rows();

        $this->db->limit($limit,$start);
        $response['limit'] = $limit;
        $response['start'] = $start;
        $response['result'] = $this->db->get()->result_array();

        $this->db->flush_cache();

		return $response;
	}
}