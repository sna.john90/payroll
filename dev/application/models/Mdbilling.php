<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Mdbilling Class.
 * 
 * @extends CI_Model
 */
class Mdbilling extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function listing_by_signing($filters=false,$start=0,$limit=10)
	{
		$schedule = SCHEDULE;
		$schedule_notary = SCHEDULE_NOTARY;
		$borrower = BORROWERS;
		$users = USERS;
        $timezone = TIMEZONE;
        $payment = SCHEDULE_PAYMENT;

		$response = array();

		// get records with filters
		$this->db->start_cache();
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flbilling',$filters);
        }

        $offset = $this->session->userdata("timezone_offset");

        $this->db->select("( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ) as date_scheduled_timezone, COALESCE((SELECT SUM(amount) as total FROM ".$payment." sp WHERE sp.schedule_id = s.id),0) as total_amount");
        $this->db->select("s.id,s.timezone,s.title_order_number,s.property_location,b.name as borrower_name,sn.notary_id,u.name as notary_name,s.is_paid");
        $this->db->from("{$schedule} s");
        $this->db->join("{$borrower} b","b.id=s.borrower_id","left");
        $this->db->join("{$schedule_notary} sn","sn.schedule_id=s.id");
        $this->db->join("{$users} u","u.id=sn.notary_id","left");
        $this->db->join("{$timezone} tz","tz.name=s.timezone","left");
        $this->db->limit($limit,$start);

        $this->db->stop_cache();

        $response['limit'] = $limit;
        $response['start'] = $start;
        $response['result'] = $this->db->get()->result_array();
        $response['totalCount'] = $this->db->get()->num_rows();

        $this->db->flush_cache();

		return $response;
	}

}