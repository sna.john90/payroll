<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Mdborrower Class.
 * 
 * @extends CI_Model
 */
class Mdborrower extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function add($data=array())
	{
		$response = FALSE;
		if(!empty($data))
		{
			$this->db->insert(BORROWERS,$data);
			$response = $this->db->insert_id();
		}
		return $response;
	}
	public function check_add_borrower($where=array(),$data=array())
	{
		$borrower_id = 0;
		if(!empty($where))
		{
			$this->db->where($where);
		}
		$this->db->select("id");
		$response = $this->db->get(BORROWERS)->first_row("array");
		if(empty($response))
		{
			if(!empty($data))
			{
				$this->db->insert(BORROWERS,$data);
				$borrower_id = $this->db->insert_id();
			}
		}else{
			if(!empty($data))
			{
				$this->db->where("id",$response['id']);
				$this->db->update(BORROWERS,$data);
			}
			$borrower_id = $response['id'];
		}
		return $borrower_id;
	}
}