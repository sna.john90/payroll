<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Mdcommon Class.
 * 
 * @extends CI_Model
 */
class Mdcommon extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

    public function add_internal_note($data=array())
    {
        $response = FALSE;
        if(!empty($data))
        {
            $this->db->insert(INTERNAL_NOTES,$data);
            $response = $this->db->insert_id();
        }
        return $response;
    }

    public function update_internal_note($filters=false,$data=array())
    {
        $response = false;
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flcommon',$filters);
        }
        if(!empty($data))
        {
            $response = $this->db->update(INTERNAL_NOTES, $data);
        }
        return $response;
    }

    public function remove_internal_note($filters=false)
    {
        $response = FALSE;
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flcommon',$filters);
        }
        $response = $this->db->delete(INTERNAL_NOTES);
        return $response;
    }

	public function folder_listing($filters=false,$select="*")
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flcommon',$filters);
        }
        $this->db->select($select,FALSE);
        $response = $this->db->get(FOLDERS)->result_array();
        return $response;
	}

    public function schedule_status_listing($filters=false,$select="*")
    {
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flcommon',$filters);
        }
        $this->db->select($select,FALSE);
        $response = $this->db->get(SCHEDULE_STATUS)->result_array();
        return $response;
    }

    public function internal_notes_listing($filters=false,$select="*")
    {
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flcommon',$filters);
        }
        $this->db->select($select,FALSE);
        $response = $this->db->get(INTERNAL_NOTES)->result_array();
        return $response;
    }

    public function get_internal_note($filters=false,$select="*")
    {
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flcommon',$filters);
        }
        $this->db->select($select,FALSE);
        $response = $this->db->get(INTERNAL_NOTES)->first_row("array");
        return $response;
    }

    public function get_folder($filters=false,$select="*")
    {
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flcommon',$filters);
        }
        $this->db->select($select,FALSE);
        $response = $this->db->get(FOLDERS)->first_row("array");
        return $response;
    }

    public function get_schedule_status($filters=false,$select="*")
    {
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flcommon',$filters);
        }
        $this->db->select($select,FALSE);
        $response = $this->db->get(SCHEDULE_STATUS)->first_row("array");
        return $response;
    }

    public function get_notary_status($filters=false,$select="*")
    {
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flcommon',$filters);
        }
        $this->db->select($select,FALSE);
        $response = $this->db->get(NOTARY_STATUS)->first_row("array");
        return $response;
    }

	public function get_folder_listing($select="*")
	{
        $this->db->select("id,name");
        $this->db->order_by("name","ASC");
        $response = $this->db->get(FOLDERS)->result_array();
        return $response;
	}
}