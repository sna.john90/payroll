<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Mdcompany Class.
 * 
 * @extends CI_Model
 */
class Mdcompany extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function add_company($data)
	{
		if(!empty($data))
		{
			$this->db->insert(COMPANY,$data);
			return $this->db->insert_id();
		}
		return FALSE;
	}
	public function edit_company($filters=false, $data)
	{
		$response = false;
		$company = array();
		if($filters!=false)
        {
        	// get company
        	$this->db->select("company_pic");
			$this->db->where("id",$filters['id']);
			$company = $this->db->get(COMPANY)->first_row('array');

            $this->load->library('orm/Filters');
            $this->filters->detect('Flcompany',$filters);
        }
		if(!empty($data))
		{
			$response = $this->db->update(COMPANY,$data);

			if(!empty($company) && $response && isset($data['company_pic']))
			{
				if($company['company_pic']!="" && file_exists("./uploads/company/".$company['company_pic']))
				{
					unlink("./uploads/company/".$company['company_pic']);
				}
			}
		}
		return $response;
	}
	public function remove_company($filters=false)
	{
		$response = false;
		$company = array();
		if($filters!=false)
        {
        	// get company
        	$this->db->select("company_pic");
			$this->db->where("id",$filters['id']);
			$company = $this->db->get(COMPANY)->first_row('array');

            $this->load->library('orm/Filters');
            $this->filters->detect('Flcompany',$filters);
        }

		$response = $this->db->delete(COMPANY);

		if(!empty($company) && $response)
		{
			if($company['company_pic']!="" && file_exists("./uploads/company/".$company['company_pic']))
			{
				unlink("./uploads/company/".$company['company_pic']);
			}
		}
		return $response;
	}
	public function get_company($filters=false)
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flcompany',$filters);
        }
		$response =  $this->db->get(COMPANY)->first_row('array');
		return $response;
	}

	public function company_list($filters=false)
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flcompany',$filters);
        }
		$response =  $this->db->get(COMPANY)->result_array();
		return $response;
	}

	public function company_members_list($filters=false)
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flcompany',$filters);
        }
        $company = COMPANY;
        $company_members = COMPANY_MEMBERS;

        $this->db->distinct("cm.id");
        $this->db->select("cm.*");
        $this->db->from("{$company_members} lcm");
        $this->db->join("{$company} cm","cm.id=lcm.lending_company_id");

		$response =  $this->db->get()->result_array();
		return $response;
	}

	public function company_listing($filters=false,$start=0,$limit=10)
	{
		$response = array();
		// get all records first
		$this->db->start_cache();
		$this->db->stop_cache();

		$response['totalCount'] = $this->db->get(COMPANY)->num_rows();

		$this->db->flush_cache();

		// get records with filters
		$this->db->start_cache();
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flcompany',$filters);
        }
        $this->db->stop_cache();

        $response['totalFiltered'] = $this->db->get(COMPANY)->num_rows();

        $this->db->limit($limit,$start);
        $response['limit'] = $limit;
        $response['start'] = $start;
        $response['result'] = $this->db->get(COMPANY)->result_array();

        $this->db->flush_cache();

		return $response;
	}
}