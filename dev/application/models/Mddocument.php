<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Mddocument Class.
 * 
 * @extends CI_Model
 */
class Mddocument extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function add_schedule_documents($data=array())
	{
		$response = false;
		if(!empty($data))
		{
			$this->db->insert(SCHEDULE_DOCUMENTS,$data);
			$response = $this->db->insert_id();
		}
		return $response;
	}

	public function add_schedule_documents_batch($data=array())
	{
		$response = false;
		if(!empty($data))
		{
			$response = $this->db->insert_batch(SCHEDULE_DOCUMENTS,$data);
		}
		return $response;
	}

	public function is_documents_empty($schedule_id)
	{
		$response = false;
		$this->db->where("schedule_id",$schedule_id);
		$total = $this->db->get(SCHEDULE_DOCUMENTS)->num_rows();

		$response = $total==0;

		return $response;
	}

	public function update_schedule_document($filters=false,$data=array())
	{
		$response = false;
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fldocument',$filters);
        }
        if(!empty($data))
        {
        	$response = $this->db->update(SCHEDULE_DOCUMENTS,$data);
        }
        return $response;
	}

	public function update_schedule_document_batch($data=array(),$where)
	{
		$response = false;
        if(!empty($data))
        {
        	$response = $this->db->update_batch(SCHEDULE_DOCUMENTS,$data,$where);
        }
        return $response;
	}

	public function get_schedule_document($filters=false,$select="*")
	{
		$response = false;
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fldocument',$filters);
        }

        $schedule_document = SCHEDULE_DOCUMENTS;

        $this->db->select($select,FALSE);
        $this->db->from("{$schedule_document} sd");
        $response = $this->db->get()->first_row("array");
        return $response;
	}

	public function schedule_document_listing($filters=false,$select="*")
	{
		$response = false;
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fldocument',$filters);
        }
        $schedule_document = SCHEDULE_DOCUMENTS;

        $this->db->select($select,FALSE);
        $this->db->from("{$schedule_document} sd");
        $response = $this->db->get()->result_array();
        return $response;
	}

	public function schedule_default_document_listing($filters=false,$select="*")
	{
		$response = false;
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fldocument',$filters);
        }
        $schedule_default_document = SCHEDULE_DEFAULT_DOCUMENTS;

        $this->db->select($select,FALSE);
        $this->db->from("{$schedule_default_document} sdd");
        $response = $this->db->get()->result_array();
        return $response;
	}

	public function delete_schedule_document($filters=false)
	{
		$response = false;
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fldocument',$filters);
        }       
        $response = $this->db->delete(SCHEDULE_DOCUMENTS);
        return $response;
	}

	public function total_schedule_documents($filters=false)
	{
		$response = false;
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fldocument',$filters);
        }

        $schedule_document = SCHEDULE_DOCUMENTS;
        $this->db->from("{$schedule_document} sd");
        $response = $this->db->get()->num_rows();
        return $response;
	}

}