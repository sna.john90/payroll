<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Mdcompany Class.
 * 
 * @extends CI_Model
 */
class Mdexpenses extends CI_Model {
	var $company_id = 0;
	public function __construct() {
		parent::__construct();
		$this->company_id = get_company_id();
	}

	public function add($data)
	{
		if(!empty($data))
		{
			$this->db->insert(EXPENSE,$data);
			return $this->db->insert_id();
		}
		return FALSE;
	}
	public function update($data,$id)
	{
		if(!empty($data) && !empty($id))
		{
			$this->db->where("id",$id);
			$this->db->where("company_id",$this->company_id);
			return $this->db->update(EXPENSE,$data); 
		}
		return FALSE;
	}
	public function delete($id="")
	{
		$response = false;
		$company_id = get_company_id();
		if(!empty($id))
		{
			$this->db->where("id",$id);
			$this->db->where("company_id",$company_id);
			$response = $this->db->delete(EXPENSE);
		}
		return $response;
	}
	public function listing($filters=false,$start=0,$limit=5)
	{
		$expense = EXPENSE;
        $company_id = get_company_id();
		$response = array(); 
		// get records with filters
		$this->db->start_cache();
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flexpenses',$filters);
        }
        
        $this->db->select("exp.*,expc.name as category_name");
        $this->db->from("{$expense} exp"); 
        $this->db->where("exp.company_id",$company_id);
        $this->db->join("expenses_category expc","exp.category_id = expc.id");
        $this->db->order_by("exp.receipt_date","DESC");     
        $this->db->limit($limit,$start);
        $this->db->stop_cache();

        $response['limit'] = $limit;
        $response['start'] = $start;
        $response['result'] = $this->db->get()->result_array();
        $response['totalCount'] = $this->db->get()->num_rows();
        // echo $this->db->last_query();
        $this->db->flush_cache(); 
		return $response;
	}
	//NOTES
	/*
	--  parameters: expenses id
	--				notes
	*/
	public function add_note($expense_id="",$notes="")
	{
		$response = false;
		if(!empty($expense_id))
		{
			$data = array(
						"expense_id" => $expense_id,
						"notes"		 => $notes,
						"addedby"	 => user_id()
					);
			$response = $this->db->insert("expense_notes",$data);
		}
		return $response;
	}
	//get list of notes based on expenses id
	public function get_notes($expense_id="",$returnbolean=false)
	{
		$response = array();
		$user = USERS;
		if(!empty($expense_id))
		{
			$this->db->select("exp_note.*, u.firstname, u.lastname,u.profile_pic");
			$this->db->from("expense_notes exp_note");
			$this->db->join("{$user} u","u.id = exp_note.addedby","left");
			$this->db->where("exp_note.expense_id",$expense_id);
			$query = $this->db->get();
			if($returnbolean)
			{
				if($query->num_rows()>0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else {
				$response = $query->result_array();
			}
		}
		return $response;
	}

	//CATEGORIES
	public function get_categories($parentid=0,$id=0,$all=false)
	{
		$company_id = get_company_id();

		$this->db->where("company_id",$company_id);
		if(!$all)
		{
			if($id>0)
			{
				$this->db->where("id",$id);
			}
			else
			{
				if($parentid>0)
				{
					$this->db->where("parent",$parentid);
				}
				else 
				{
					$this->db->where("parent",0);
				}
			} 
		} 
		return $this->db->get("expenses_category")->result_array();
	}
	public function add_category($data=array())
	{
		$response = false;
		if(!empty($data))
		{
			$response = $this->db->insert("expenses_category",$data);
		}
		return $response;
	}
	public function update_category($data=array(),$id=0)
	{
		$response = false;
		if(!empty($data) && !empty($id))
		{
			$this->db->where("id",$id);
			$this->db->where("company_id",$this->company_id);
			$response = $this->db->update("expenses_category",$data);
		}
		return $response;
	}
	public function delete_category($id="")
	{
		$response = false;
		if(!empty($id))
		{
			$this->db->where("id",$id);
			$this->db->where("company_id",$this->company_id);
			$response = $this->db->delete("expenses_category");
		}
		return $response;
	}
	//<!--- END OF CATEGORIES -->


	public function personnel_expenses($employee_id="",$period_from="",$period_to)
	{
		$response = 0;
		if(!empty($employee_id) && !empty($period_from) && !empty($period_to))
		{
			$query = "SELECT * FROM view_total_incentives 
					WHERE 
						((period_from BETWEEN '{$period_from}' AND '{$period_to}') 
							OR (period_to BETWEEN '{$period_from}' AND '{$period_to}')) 
						AND user_id={$employee_id} AND company_id={$this->company_id}";
			//echo $query." <br />";
			$result = $this->db->query($query);
			if($result->num_rows() > 0)
			{
				$response = $result->row_array();
			}
		} 
		return $response;
	}

	//public function e
	public function get_expenses_total($filters=array(),$category_id="")
	{ 
		$response = 0;
		$this->db->select("sum(amount) as total",null,false);
		foreach($filters as $key=>$value)
		{
			$this->db->where($key,$value);
		}	
		$this->db->where("company_id",$this->company_id);
		$this->db->where("category_id",$category_id);

		$return = $this->db->get(EXPENSE);
		if($return->num_rows() > 0)
		{
			$response = $return->row()->total;
		}
		return $response;
	}

	public function get_expenses_total_view($view="view_total_incentives",$month="",$year="")
	{
		$total = ($view=="view_total_incentives")? "total_incentives" : "total";
		$response = 0;

		$query = "SELECT sum({$total}) as total FROM {$view} WHERE ((YEAR(period_from)='{$year}' 
					AND MONTH(period_to) = '{$month}') OR (YEAR(period_from)='{$year}' 
					AND MONTH(period_to) = '{$month}')) AND company_id = {$this->company_id}";

		$return = $this->db->query($query);
		if($return->num_rows() > 0)
		{
			$response = $return->row()->total;
		}
		return $response;
	}
}