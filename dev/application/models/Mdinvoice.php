<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Mdinvoice Class.
 * 
 * @extends CI_Model
 */
class Mdinvoice extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function add_schedule_invoices($data=array())
	{
		$response = false;
		if(!empty($data))
		{
			$this->db->insert(SCHEDULE_INVOICES,$data);
			$response = $this->db->insert_id();
		}
		return $response;
	}

	public function update_schedule_invoice($filters=false,$data=array())
	{
		$response = false;
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flinvoice',$filters);
        }
        if(!empty($data))
        {
        	$response = $this->db->update(SCHEDULE_INVOICES,$data);
        }
        return $response;
	}

	public function get_schedule_invoice($filters=false,$select="*")
	{
		$response = false;
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flinvoice',$filters);
        }

        $schedule_invoice = SCHEDULE_INVOICES;

        $this->db->select($select,FALSE);
        $this->db->from("{$schedule_invoice} si");
        $response = $this->db->get()->first_row("array");
        return $response;
	}

	public function schedule_invoice_listing($filters=false,$select="*")
	{
		$response = false;
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flinvoice',$filters);
        }
        $schedule_invoice = SCHEDULE_INVOICES;

        $this->db->select($select,FALSE);
        $this->db->from("{$schedule_invoice} si");
        $response = $this->db->get()->result_array();
        return $response;
	}

	public function delete_schedule_invoice($filters=false)
	{
		$response = false;
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flinvoice',$filters);
        }       
        $response = $this->db->delete(SCHEDULE_INVOICES);
        return $response;
	}

}