<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Mdnotary Class.
 * 
 * @extends CI_Model
 */
class Mdnotary extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function add($data=array(), $user_group=1)
	{
		$response = false;
		if(!empty($data))
		{
			// hash password
			if(isset($data['password']))
			{
				$data['password'] = $this->_hash_password($data['password']);
			}

			$this->db->insert(USERS,$data);
			$user_id = $this->db->insert_id();
			if($user_id)
			{
				// add user groups reference
				$data = array(
					"user_id" => $user_id,
					"user_group_id" => $user_group,
					"date_added" => date("Y-m-d H:i:s")
				);
				$response = $this->db->insert(USER_GROUP_REFERENCE,$data);
				if($response)
				{
					$response = $user_id;
				}
			}
		}
		return $response;
	}

	public function check_add_schedule_notary($data=array(),$schedule_id)
	{
		$schedule_notary_id = false;
		
		$this->db->where("schedule_id",$schedule_id);
		$this->db->select("id");
		$response = $this->db->get(SCHEDULE_NOTARY)->first_row("array");
		if(empty($response))
		{
			if(!empty($data))
			{
				$this->db->insert(SCHEDULE_NOTARY,$data);
				$schedule_notary_id = $this->db->insert_id();
			}
		}else{
			if(!empty($data))
			{
				$this->db->where("id",$response['id']);
				$this->db->update(SCHEDULE_NOTARY,$data);
			}
			$schedule_notary_id = $response['id'];
		}
		return $schedule_notary_id;
	}

	public function add_schedule_notary($data=array())
	{
		$response = false;
		if(!empty($data))
		{
			$this->db->insert(SCHEDULE_NOTARY,$data);
			$response = $this->db->insert_id();
		}
		return $response;
	}

	public function remove_schedule_notary($filters=array())
	{
		$response = false;
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flnotary',$filters);
        }
		$response = $this->db->delete(SCHEDULE_NOTARY);
		return $response;
	}

	public function check_if_the_same_notary($schedule_id, $notary_id)
	{
		$this->db->where("schedule_id",$schedule_id);
		$this->db->where("notary_id",$notary_id);
		$response = $this->db->get(SCHEDULE_NOTARY)->num_rows();
		return $response > 0;
	}

	public function check_add_favorite($id,$favorited)
	{
		$response = false;
		if($favorited==1)
		{
			$data = array(
				"notary_id" => $id,
				"user_id" => user_id(),
				"date_added" => date("Y-m-d H:i:s")
			);
			$response = $this->db->insert(NOTARY_FAVORITE,$data);
		}else{
			$data = array(
				"notary_id" => $id,
				"user_id" => user_id()
			);
			$this->db->where($data);
			$response = $this->db->delete(NOTARY_FAVORITE);
		}
		return $response;
	}

	public function get_notary_details($filters=false)
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flnotary',$filters);
        }
        $user = USERS;
        $user_group = USER_GROUPS;
        $user_ref = USER_GROUP_REFERENCE;
        $this->db->select("u.*,ug.id as user_group_id");
        $this->db->from("{$user} u");
        $this->db->join("{$user_ref} ur","ur.user_id=u.id","inner");
        $this->db->join("{$user_group} ug","ug.id=ur.user_group_id");
        $response = $this->db->get()->first_row('array');
        return $response;
	}

	public function get_schedule_notary($filters=false,$select="*")
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flnotary',$filters);
        }
        $this->db->select($select,FALSE);
        $response = $this->db->get(SCHEDULE_NOTARY)->first_row('array');
        return $response;
	}

	public function get_schedule_notary_info($filters=false,$select="*")
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flnotary',$filters);
        }
        $notary = SCHEDULE_NOTARY;
        $schedule = SCHEDULE;
        $this->db->select($select,FALSE);
        $this->db->from("{$notary} sn");
        $this->db->join("{$schedule} s","s.id=sn.schedule_id");
        $response = $this->db->get()->first_row('array');
        return $response;
	}

	public function get_schedule_notary_with_status($filters=false,$select="*")
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flnotary',$filters);
        }
        $notary = SCHEDULE_NOTARY;
        $status = NOTARY_STATUS;
        $this->db->select($select,FALSE);
        $this->db->from("{$notary} sn");
        $this->db->join("{$status} ns","ns.id=sn.notary_status_id","left");
        $response = $this->db->get()->first_row('array');
        return $response;
	}

	public function update_schedule_notary($filters=false,$data=array())
	{
		$response = false;
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flnotary',$filters);
        }
        if(!empty($data))
        {
        	$response = $this->db->update(SCHEDULE_NOTARY,$data);
        }
        return $response;
	}

	// notary listing
	public function notary_listing($filters=false,$start=0,$limit=10)
	{
		$user = USERS;
        $user_group = USER_GROUPS;
        $user_ref = USER_GROUP_REFERENCE;
        $notary_favorite = NOTARY_FAVORITE;

		$response = array();

		// get records with filters
		$this->db->start_cache();
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flnotary',$filters);
        }
        $this->db->distinct("u.id");
        $this->db->select("u.*,ur.user_group_id,ug.name as user_type,nf.id as notary_favorite_id");
        $this->db->from("{$user} u");
        $this->db->join("{$user_ref} ur","ur.user_id=u.id","inner");
        $this->db->join("{$user_group} ug","ug.id=ur.user_group_id");
        $this->db->join("{$notary_favorite} nf","nf.notary_id=u.id","left");
        $this->db->limit($limit,$start);

        $this->db->stop_cache();

        $response['limit'] = $limit;
        $response['start'] = $start;
        $response['result'] = $this->db->get()->result_array();
        $response['totalCount'] = $this->db->get()->num_rows();

        $this->db->flush_cache();

		return $response;
	}

	// schedule notary listing
	public function schedule_notary_listing($filters=false,$start=0,$limit=10)
	{
		$schedule 	= SCHEDULE;
		$schedule_notary 	= SCHEDULE_NOTARY;
		$users 		= USERS;
		$company 	= COMPANY;
		$borrower 	= BORROWERS;
		$notary_status 	= NOTARY_STATUS;
		$comments = SCHEDULE_COMMENTS;
		$timezone = TIMEZONE;

		$response = array();

		// get records with filters
		$this->db->start_cache();
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flnotary',$filters);
        }

        $offset = $this->session->userdata("timezone_offset");

        $this->db->select("s.id,s.date_scheduled,s.borrower_id,s.timezone,s.date_added,b.name as borrower,s.zipcode,s.street_address,s.unit_number,s.city,s.state,s.lender_id,lc.name as lender,s.loan_officer_id,u.name as loan_officer,sn.id as schedule_notary_id,sn.is_accepted,sn.notary_status_id,ns.status as notary_status,ns.label as notary_status_label,s.property_location,lc.company_pic, ( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ) as date_scheduled_timezone, CONCAT_WS(' ', s.street_address, s.unit_number, s.city, s.zipcode, s.state) as combined_address,COALESCE((SELECT COUNT(*) as count FROM ".$comments." sc WHERE sc.schedule_id = s.id),0) as count_comments");
        $this->db->from("{$schedule} s");
        $this->db->join("{$schedule_notary} sn","sn.schedule_id=s.id");
        $this->db->join("{$borrower} b","b.id=s.borrower_id","left");
        $this->db->join("{$company} lc","lc.id=s.lender_id","left");
        $this->db->join("{$users} u","u.id=s.loan_officer_id","left");
        $this->db->join("{$notary_status} ns","ns.id=sn.notary_status_id","left");
        $this->db->join("{$timezone} tz","tz.name=s.timezone","left");

        $this->db->limit($limit,$start);
        $this->db->stop_cache();

        $response['limit'] = $limit;
        $response['start'] = $start;
        $response['result'] = $this->db->get()->result_array();
        $response['totalCount'] = count($response['result']);

        $this->db->flush_cache();

		return $response;
	}

	public function get_schedule_info1($filters=false,$select="*")
	{
		$schedule 	= SCHEDULE;
		$schedule_notary 	= SCHEDULE_NOTARY;
		$users 		= USERS;
		$company 	= COMPANY;
		$borrower 	= BORROWERS;
		$notary_status 	= NOTARY_STATUS;
		$timezone = TIMEZONE;

		$response = array();

		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flnotary',$filters);
        }

        $this->db->select($select,FALSE);
        $this->db->from("{$schedule} s");
        $this->db->join("{$schedule_notary} sn","sn.schedule_id=s.id");
        $this->db->join("{$users} u3","u3.id=sn.notary_id","left");
        $this->db->join("{$borrower} b","b.id=s.borrower_id","left");
        $this->db->join("{$users} u1","u1.id=s.loan_officer_id","left");
        $this->db->join("{$users} u2","u2.id=s.underwriter_id","left");
        $this->db->join("{$timezone} tz","tz.name=s.timezone","left");
        $response = $this->db->get()->first_row("array");
       
        return $response;
	}

	public function get_schedule_info2($filters=false,$select="*")
	{
		$schedule 	= SCHEDULE;
		$schedule_notary 	= SCHEDULE_NOTARY;
		$users 		= USERS;
		$company 	= COMPANY;
		$borrower 	= BORROWERS;
		$notary_status 	= NOTARY_STATUS;
		$timezone = TIMEZONE;

		$response = array();

		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flnotary',$filters);
        }
        
        $this->db->select($select,FALSE);
        $this->db->from("{$schedule} s");
        $this->db->join("{$schedule_notary} sn","sn.schedule_id=s.id");
        $this->db->join("{$users} u","u.id=sn.notary_id","left");
        $this->db->join("{$borrower} b","b.id=s.borrower_id","left");
        $this->db->join("{$timezone} tz","tz.name=s.timezone","left");
        $response = $this->db->get()->first_row("array");
       
        return $response;
	}

	public function get_schedule_info3($filters=false,$select="*")
	{
		$schedule 	= SCHEDULE;
		$schedule_notary = SCHEDULE_NOTARY;
		$users 		= USERS;
		$timezone = TIMEZONE;

		$response = array();

		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flnotary',$filters);
        }
        
        $this->db->select($select,FALSE);
        $this->db->from("{$schedule} s");
        $this->db->join("{$schedule_notary} sn","sn.schedule_id=s.id");
        $this->db->join("{$users} u","u.id=sn.notary_id","left");
        $this->db->join("{$users} u2","u2.id=s.loan_officer_id","left");
        $this->db->join("{$timezone} tz","tz.name=s.timezone","left");
        $response = $this->db->get()->first_row("array");
       
        return $response;
	}

	public function get_schedule_info4($filters=false,$select="*")
	{
		$schedule 	= SCHEDULE;
		$schedule_notary = SCHEDULE_NOTARY;
		$users 		= USERS;
		$timezone = TIMEZONE;
		$borrower 	= BORROWERS;

		$response = array();

		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flnotary',$filters);
        }
        
        $this->db->select($select,FALSE);
        $this->db->from("{$schedule} s");
        $this->db->join("{$schedule_notary} sn","sn.schedule_id=s.id");
        $this->db->join("{$users} u","u.id=sn.notary_id","left");
        $this->db->join("{$users} u2","u2.id=s.loan_officer_id","left");
        $this->db->join("{$borrower} b","b.id=s.borrower_id","left");
        $this->db->join("{$timezone} tz","tz.name=s.timezone","left");
        $response = $this->db->get()->first_row("array");
       
        return $response;
	}

	public function get_schedule_notary_user_only($filters=false,$select="*")
	{
		$schedule_notary = SCHEDULE_NOTARY;
		$users 		= USERS;

		$response = array();

		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flnotary',$filters);
        }
        
        $this->db->select($select,FALSE);
        $this->db->from("{$schedule_notary} sn");
        $this->db->join("{$users} u","u.id=sn.notary_id","left");
        $response = $this->db->get()->first_row("array");
       
        return $response;
	}

	public function get_loan_officer_email($schedule_id)
	{
		$schedule = SCHEDULE;
		$users = USERS;
		$user_group_reference = USER_GROUP_REFERENCE;

		$response = false;
		$this->db->select("email,user_group_id");
		$this->db->where("s.id",$schedule_id);
		$this->db->from("{$schedule} s");
        $this->db->join("{$users} u","u.id=s.loan_officer_id","inner");
        $this->db->join("{$user_group_reference} ur","ur.user_id=u.id","inner");
        $result = $this->db->get()->first_row("array");
        if(!empty($result))
        {
        	$response = $result['email'];
        }
        return $response;
	}
	public function get_admin_emails($schedule_id)
	{
		$schedule = SCHEDULE;
		$users = USERS;
		$user_group_reference = USER_GROUP_REFERENCE;
		$company_members = COMPANY_MEMBERS;
        $company = COMPANY;

		$response = false;
		$this->db->select("email");
		$this->db->where("s.id",$schedule_id);
		$this->db->where("ur.user_group_id",1);
		$this->db->from("{$schedule} s");
		$this->db->join("{$company_members} lc","lc.lending_company_id=s.lender_id");
        $this->db->join("{$users} u","u.id=lc.user_id");
        $this->db->join("{$user_group_reference} ur","ur.user_id=u.id");
        $result = $this->db->get()->result_array();
        if(!empty($result))
        {
        	$response = $result;
        }
        return $response;
	}
	public function get_lender_admin_emails($schedule_id)
	{
		$schedule = SCHEDULE;
		$users = USERS;
		$user_group_reference = USER_GROUP_REFERENCE;
		$company_members = COMPANY_MEMBERS;
        $company = COMPANY;

		$response = false;
		$this->db->select("email");
		$this->db->where("s.id",$schedule_id);
		$this->db->where("ur.user_group_id",7);
		$this->db->from("{$schedule} s");
		$this->db->join("{$company_members} lc","lc.lending_company_id=s.lender_id");
        $this->db->join("{$users} u","u.id=lc.user_id");
        $this->db->join("{$user_group_reference} ur","ur.user_id=u.id");
        $result = $this->db->get()->result_array();
        if(!empty($result))
        {
        	$response = $result;
        }
        return $response;
	}

	public function get_closest_notary($schedule_id,$notary_id)
	{
		$response = false;
		$this->db->select("latitude,longitude");
		$this->db->where("id",$schedule_id);
		$schedule = $this->db->get(SCHEDULE)->first_row("array");
		if(!empty($schedule))
		{
			$users = USERS;
			$user_group_reference = USER_GROUP_REFERENCE;
			$notary_favorite = NOTARY_FAVORITE;

			$this->db->distinct("u.id");
			$this->db->select("u.id,name,IFNULL(nf.id,0) as favorite");
			$this->db->select("(((acos(sin((".$schedule['latitude']."*pi()/180)) * sin((u.latitude*pi()/180))+cos((".$schedule['latitude']."*pi()/180))*cos((u.latitude*pi()/180))*cos(((".$schedule['longitude']."-u.longitude)*pi()/180))))*180/pi())*60*1.1515) as distance");
			$this->db->where("ur.user_group_id",5);
			$this->db->where("u.id !=",$notary_id);
			$this->db->having("distance <=",30);
			$this->db->order_by("favorite",'DESC');
			$this->db->order_by("distance",'ASC');
			$this->db->from("{$users} u");
        	$this->db->join("{$user_group_reference} ur","ur.user_id=u.id");
        	$this->db->join("{$notary_favorite} nf","nf.notary_id=u.id","left");
        	$result = $this->db->get()->first_row("array");
        	if(!empty($result))
        	{
        		$response = $result['id'];
        	}
		}
		return $response;
	}

	private function _hash_password($password) {
		return password_hash($password, PASSWORD_BCRYPT);
	}
}