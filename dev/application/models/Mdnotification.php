<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Mdnotification Class.
 * 
 * @extends CI_Model
 */
class Mdnotification extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
	}

	public function listing($filters=false,$page=1,$limit=false)
	{
		$response = array(
            "result"    => array(),
            "more_data" => false
        );
		$this->db->start_cache();
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flnotification',$filters);
        }
        $notifications = NOTIFICATIONS;
        $users		   = USERS;
        $timezone      = TIMEZONE;
        $schedule      = SCHEDULE;
        $borrower     = BORROWERS;

        $offset = $this->session->userdata("timezone_offset");

        $this->db->select("not.*,s.id as notification_schedule_id,u.name,( FROM_UNIXTIME( UNIX_TIMESTAMP(not.date_added) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ) as date_added_timezone,b.name as borrower_name");
        $this->db->from("{$notifications} not");
        $this->db->join("{$users} u","u.id=not.user_id");
        $this->db->join("{$timezone} tz","tz.name=u.timezone");
        $this->db->join("{$schedule} s","s.id=not.schedule_id","left");
        $this->db->join("{$borrower} b","b.id=s.borrower_id","left");

        $total_count = $this->db->get()->num_rows();

        if($limit!=false)
        {
            $total_pages = (int)($total_count/$limit);
            if($page < $total_pages)
            {
                $response['more_data'] = true;
            }
            $start = ($limit*$page)-$limit;
            //limit
            $this->db->limit($limit,$start);
        }
        $this->db->stop_cache();
        $response['result'] = $this->db->get()->result_array();
        $this->db->flush_cache();
        return $response;
	}
}