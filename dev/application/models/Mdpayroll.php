<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Mdcompany Class.
 *
 * @extends CI_Model
 */
class Mdpayroll extends CI_Model {

	var $response = false;
	public function __construct() {
		parent::__construct();
	}

	public function create_payrun($data)
	{
		if(!empty($data))
		{
			$this->db->insert("payroll",$data);
			return $this->db->insert_id();
		}
		return FALSE;
	}

	/*
	| update_payrun_details()
	|   @params : id,value,field name
	|
	*/
	public function update_payrun_details($id="",$value="",$field="")
	{
		$company_id = get_company_id();
		if(!empty($id) && !empty($field))
		{
			$to_update = array(
								$field => $value
							);
			$this->db->where("company_id",$company_id);
			$this->db->where("id",$id);
			$this->response = $this->db->update("payroll",$to_update);
		}
		return $this->response;
	}
	//get_payrun_details
	// param :
	//		 payrun id
	//
	public function get_payrun_details($payrunid="")
	{
		$response = array();
		$company_id = get_company_id();
		if(!empty($payrunid))
		{
			if(!empty($company_id))
			{
				$this->db->where("company_id",$company_id);
			}
			$this->db->where("id",$payrunid);
			$response = $this->db->get("payroll")->row_array();
		}
		return $response;

	}

	//	employees()
	// @return : array
	// @params : company id
	public function employees()
	{
		$response = array();
		$company_id = get_company_id();
		if(!empty($company_id))
		{
			$user 				= USERS;
			$employee_details 	= EMP_DETAILS;
			$this->db->select("u.*,empd.basic_pay,empd.designation,empd.payment_type,empd.gender_status");
			$this->db->from("{$user} u");
			$this->db->join("{$employee_details} empd","empd.user_id = u.id");
			$this->db->where("u.company_id",$company_id);
			$this->db->where("u.emp_id !=",0);
			$response = $this->db->get()->result_array();
		}
		return $response;
	}

	//prev_employee()
	//  @desc : get the previous employee of the company
	//  @param : userid
	//  @response : previous id
	public function prev_employee($id="")
	{
		$response = 0;
		$company_id = get_company_id();
		if(!empty($id) && !empty($company_id))
		{
			$this->db->where("id <",$id);
			$this->db->where("company_id",$company_id);
			$this->db->where("emp_id !=",0);
			$this->db->limit(1);
			$this->db->order_by("id","DESC");
			$return  = $this->db->get(USERS)->row_array();
			if(!empty($return))
			{
				$response = $return['id'];
			}
		}
		return $response;
	}

	//next_employee()
	//  @desc : get the next employee of the company
	//  @param : userid
	//  @response : next id
	public function next_employee($id="")
	{
		$response = 0;
		$company_id = get_company_id();
		if(!empty($id) && !empty($company_id))
		{
			$this->db->where("id >",$id);
			$this->db->where("company_id",$company_id);
			$this->db->where("emp_id !=",0);
			$this->db->limit(1);
			$this->db->order_by("id","ASC");
			$return  = $this->db->get(USERS)->row_array();
			if(!empty($return))
			{
				$response = $return['id'];
			}
		}
		return $response;
	}

	public function update_payrun_empdetails($payrunid="",$data=array(),$employee="")
	{
		if(!empty($payrunid) && !empty($data) && $employee!="")
		{
			$this->db->where("payrun_id",$payrunid);
			$this->db->where("user_id",$employee);
			$delete = $this->db->delete(EMP_PRL);
			if($delete)
			{
				$this->response = $this->db->insert_batch(EMP_PRL,$data);
			}
		}
		return $this->response;
	}

	public function delete($id="")
	{
		$response = false;
		$company_id = get_company_id();
		if(!empty($id))
		{
			$this->db->where("id",$id);
			$this->db->where("company_id",$company_id);
			$response = $this->db->delete(EXPENSE);
		}
		return $response;
	}
	public function listings($filters=false,$start=0,$limit=5)
	{
		$expense = EXPENSE;
        $company_id = get_company_id();
		$response = array();
		// get records with filters
		$this->db->start_cache();
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flexpenses',$filters);
        }

        $this->db->select("");
        $this->db->from("{$expense} exp");
        $this->db->where("exp.company_id",$company_id);
        $this->db->order_by("exp.receipt_date","DESC");
        $this->db->limit($limit,$start);
        $this->db->stop_cache();

        $response['limit'] = $limit;
        $response['start'] = $start;
        $response['result'] = $this->db->get()->result_array();
        $response['totalCount'] = $this->db->get()->num_rows();
        // echo $this->db->last_query();
        $this->db->flush_cache();
		return $response;
	}
	//NOTES
	/*
	--  parameters: expenses id
	--				notes
	*/
	public function add_note($expense_id="",$notes="")
	{
		$response = false;
		if(!empty($expense_id))
		{
			$data = array(
						"expense_id" => $expense_id,
						"notes"		 => $notes,
						"addedby"	 => user_id()
					);
			$response = $this->db->insert("expense_notes",$data);
		}
		return $response;
	}
	 /*
	  | @func        - get_emp_payroll_details()
	  | @description - payroll deductions and incentives of an employee
	  | @params      - payrunid, userid
	  | @return - array
	  */
	 public function get_emp_payroll_details($payrun_id,$user_id)
	 {
	 		$this->response = array();
			if(!empty($payrun_id) && !empty($user_id))
			{
				  $this->db->select("emp_prl.*,pmnt.type as pay_type", FALSE);
					$this->db->from(EMP_PRL." emp_prl");
					$this->db->join(PRL_MNT." pmnt", 'pmnt.name = emp_prl.mnt_di_id');
					$this->db->where("emp_prl.payrun_id",$payrun_id);
					$this->db->where('emp_prl.user_id', $user_id);
					$this->response = $this->db->get()->result_array();
			}
			return $this->response;
	 }
	 /* END OF get_emp_payroll_details */
	  /*
	   | @func        - set_emp_payroll_paid()
	   | @description - set to paid and change the payrun to paid
	   | @params      - payrun id,user_ids
	   | @return - boolean
	   */
	  public function set_emp_payroll_paid($payrun_id="",$user_ids=array())
	  {
	  		if(!empty($payrun_id))
				{
						 $company_id = get_company_id();
						 $result = $this->set_payrun_paid($payrun_id);
						 if($result && !empty($user_ids))
						 {
							 $this->db->where('payrun_id',$payrun_id);
							 $this->db->where('company_id',$company_id);
							 $this->db->where_in('user_id',$user_ids);
							 $this->response = $this->db->update(EMP_PRL,array("status" => 1));
						 }
				}
				return $this->response;
	  }
	  /* END OF set_emp_payroll_paid */
		 /*
		  | @func        - set_payrun_paid()
		  | @description - description
		  | @params      - payrun id
		  | @return - boolean
		  */
		 public function set_payrun_paid($payrun_id="")
		 {
		 		if(!empty($payrun_id))
				{
					$company_id = get_company_id();
					$this->db->where('id',$payrun_id);
					$this->db->where('company_id',$company_id);
				  $this->response = $this->db->update(PRL,array("status" => 1));
				}
				return $this->response;
		 }
		 /* END OF set_payrun_paid */

		  /*
		   | @func        - listing()
		   | @description - description
		   | @params      - param1,param2
		   | @return - boolean
		   */
		  public function listing()
		  {
		  		$company_id = get_company_id();
					$this->db->where('company_id',$company_id);
					$this->db->order_by("period_from","DESC");
					return $this->db->get(PRL)->result_array();
		  }
		  /* END OF functionname */

			 /*
			  | @func        - delete_payrun()
			  | @description - delete payrun
			  | @params      - param1,param2
			  | @return - boolean
			  */
			 public function delete_payrun($payrun_id="")
			 {
			 		if(!empty($payrun_id))
					{
						$this->db->where("id",$payrun_id);
						$this->response = $this->db->delete(PRL);
					}
					return $this->response;
			 }
			 /* END OF delete_payrun */

			  /*
			   | @func        - get_notes()
			   | @description - description
			   | @params      - param1,param2
			   | @return - boolean
			   */
				 //get list of notes based on expenses id
			 	public function get_notes($payrun_id="",$returnbolean=false)
			 	{
			 		$response = array();
			 		$user = USERS;
			 		if(!empty($payrun_id))
			 		{
			 			$this->db->select("paynote.*, u.firstname, u.lastname,u.profile_pic");
			 			$this->db->from(PRL." paynote");
			 			$this->db->join("{$user} u","u.id = paynote.addedby","left");
			 			$this->db->where("paynote.id",$payrun_id);
			 			$query = $this->db->get(); 
			 			if($returnbolean)
			 			{
			 				if($query->num_rows()>0)
			 				{
			 					return true;
			 				}
			 				else
			 				{
			 					return false;
			 				}
			 			}
			 			else {
			 				$response = $query->result_array();
			 			}
			 		}
			 		return $response;
			 	}
			  /* END OF get_notes */
}
