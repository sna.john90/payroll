<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MdProfile extends CI_Model {

  public $variable;
  var $response = false;
  public function __construct()
  {
    parent::__construct();

  }
   /*
    | @func        - list_payroll()
    | @description - payroll listing
    | @params      - param1,param2
    | @return - boolean
    */
   public function list_payroll($id='')
   {
       $response = array();
       if(!empty($id))
       {
         $emp_prl = EMP_PRL;
         $this->db->select("emp_prl.*,prl.period_from,prl.period_to,payroll_date,
                    COALESCE((SELECT ROUND(SUM(value),2) FROM {$emp_prl} emp2 JOIN `payroll_maintainables` `prlmnt2` ON `prlmnt2`.`name` = `emp2`.`mnt_di_id` WHERE emp2.payrun_id = emp_prl.payrun_id AND prlmnt2.type='incentive' AND emp2.mnt_di_id!='basic_pay' AND emp2.user_id={$id}),0) as total_incentives,
                    COALESCE((SELECT ROUND(SUM(value),2) FROM {$emp_prl} emp2 JOIN `payroll_maintainables` `prlmnt2` ON `prlmnt2`.`name` = `emp2`.`mnt_di_id` WHERE emp2.payrun_id = emp_prl.payrun_id AND prlmnt2.type='deduction' AND emp2.user_id={$id}),0) as total_deductions",false);
         $this->db->from("{$emp_prl} emp_prl");
         $this->db->join(PRL." prl","prl.id = emp_prl.payrun_id");
         $this->db->join(PRL_MNT." prlmnt","prlmnt.name = emp_prl.mnt_di_id");
         $this->db->where("emp_prl.user_id",$id);
         $this->db->where("emp_prl.status",1); //paid
         $this->db->group_by(array("emp_prl.payrun_id","emp_prl.user_id"));
         $this->db->order_by("date(prl.period_from)","DESC");
         $return = $this->db->get()->result_array();
         if(!empty($return))
         {
            foreach($return as $key=>$value)
            {
               $temp = "";
               $temp = $value;

               $this->db->where("user_id",$id);
               $this->db->where("mnt_di_id","basic_pay");
               $this->db->where("payrun_id",$value['payrun_id']);
               $temp2 = $this->db->get(EMP_PRL)->row_array();
               if(!empty($temp2))
               {
                  $temp['basic_pay'] = $temp2['value'];
               }
               else
               {
                 $temp['basic_pay'] = 0;
               }
               $response[] = $temp;
            }
         }
       }
       return $response;
   }

    /*
     | @func        - payroll amount ()
     | @description - description
     | @params      - payrun id,user id
     | @return - boolean
     */
    public function payroll_details($payrun_id="",$user_id="",$type="incentive")
    {
        $response = array();
        if(!empty($payrun_id) && !empty($user_id))
        {
          $this->db->select("*,prlmnt.description",false);
          $this->db->from(EMP_PRL." emp_prl");
          $this->db->join(PRL_MNT." prlmnt","prlmnt.name = emp_prl.mnt_di_id");
          $this->db->where("payrun_id",$payrun_id);
          $this->db->where("user_id",$user_id);
          $this->db->where("prlmnt.type",$type);
          $response = $this->db->get()->result_array();
        }
        return $response;
    }
    /* END OF functionname */

   /* END OF list_payroll */

    /*
     | @func        - earning_total()
     | @description - description
     | @params      - term (alltime | year | month), user id
     | @return - number
     */
    public function earning_total($term="",$user_id="")
    {
        $response = 0;
        if(!empty($user_id))
        {
          $this->db->select("SUM(value) as total",false);
          $this->db->from(EMP_PRL." emp_prl");
          $this->db->join(PRL_MNT." prlmnt","prlmnt.name = emp_prl.mnt_di_id");
          $this->db->where("emp_prl.user_id",$user_id);
          $this->db->where("prlmnt.type","incentive");

          $response = $this->db->get()->row()->total;
        }
        return $response;
    }
    /* END OF earning_total */
}

/* End of file MdProfile.php */
/* Location: ./application/models/MdProfile.php */
