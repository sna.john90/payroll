<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Mdsavings Class.
 *
 * @extends CI_Model
 */
class Mdsavings extends CI_Model
{
	var $company_id = 0;
	var $response = false;
	public function __construct() {
		parent::__construct();
		$this->company_id = get_company_id();
	}
	public function get($id="")
	{

		$where_1 = "";
		if(!empty($id))
		{
			$where_1 = " AND ves.user_id={$id} ";
		}

		$query = "SELECT ves.*,pr.payroll_date FROM view_employee_savings ves
				  JOIN payroll pr ON ves.payrun_id=pr.id
				  WHERE ves.company_id={$this->company_id} {$where_1}";

		$query_2 = " UNION SELECT ves.*,ves.date_added as payroll_date FROM employee_savings  ves
					WHERE ves.company_id={$this->company_id} {$where_1} ORDER BY payroll_date DESC";
		return $this->db->query($query." ".$query_2)->result_array();
	}
	public function get_total($id,$filter="all")
	{
		$response = 0;

		$additional_where = "";
		$additional_where2 = "";
		if($filter=="month")
		{
			$additional_where = " AND (MONTH(payroll.payroll_date)='".date("m")."' AND YEAR(payroll.payroll_date)='".date("Y")."') ";
			$additional_where2 = " AND (MONTH(date_added)='".date("m")."' AND YEAR(date_added)='".date("Y")."') ";
		}
		else if($filter=="year")
		{
			$additional_where = " AND (YEAR(payroll.payroll_date)='".date("Y")."') ";
			$additional_where2 = " AND (YEAR(date_added)='".date("Y")."') ";
		}
		$additional = " AND ";

		$query = "SELECT sum(view_employee_savings.value) as total FROM view_employee_savings LEFT JOIN payroll ON view_employee_savings.payrun_id = payroll.id WHERE view_employee_savings.user_id={$id} AND view_employee_savings.company_id={$this->company_id} AND view_employee_savings.type='deduction' ".$additional_where;

		$return = $this->db->query($query);
		if($return->num_rows() > 0)
		{
			$response = $return->row()->total;
		}

		//get the manual table
		$query = "SELECT sum(value) as total FROM employee_savings
					WHERE user_id={$id} AND company_id={$this->company_id} AND type='manual-deposit' ".$additional_where2;
		$return = $this->db->query($query);
		if($return->num_rows() > 0)
		{
			$response = $response+$return->row()->total;
		}
		if($filter!="month")
		{
			$response = $response - $this->get_total_deduction($id,$filter);
		}

		return $response;
	}
	public function get_total_deduction($id,$filter="all")
	{
		$response = 0;

		$additional_where = "";
		$additional_where2 = "";
		if($filter=="month")
		{
		//	$additional_where = " AND (MONTH(payroll.payroll_date)='".date("m")."' AND YEAR(payroll.payroll_date)='".date("Y")."') ";
		//	$additional_where2 = " AND (MONTH(date_added)='".date("m")."' AND YEAR(date_added)='".date("Y")."') ";
		}
		else if($filter=="year")
		{
		//	$additional_where = " AND (YEAR(payroll.payroll_date)='".date("Y")."') ";
		//	$additional_where2 = " AND (YEAR(date_added)='".date("Y")."') ";
		}
		$additional = " AND ";

		$query = "SELECT sum(view_employee_savings.value) as total FROM view_employee_savings LEFT JOIN payroll ON view_employee_savings.payrun_id = payroll.id WHERE view_employee_savings.user_id={$id} AND view_employee_savings.company_id={$this->company_id} AND view_employee_savings.type='incentive' ".$additional_where;

		$return = $this->db->query($query);
		if($return->num_rows() > 0)
		{
			$response = $return->row()->total;
		}

		//get the manual table
		$query = "SELECT sum(value) as total FROM employee_savings
					WHERE user_id={$id} AND company_id={$this->company_id} AND type='manual-withdraw' ".$additional_where2;
		$return = $this->db->query($query);
		if($return->num_rows() > 0)
		{
			$response = $response+$return->row()->total;
		}

		return $response;
	}
	public function add($data=array())
	{
		if(!empty($data))
		{
			$this->response = $this->db->insert("employee_savings",$data);
		}
		return $this->response;
	}
	public function delete($user_id="",$savings_id="")
	{
		if(!empty($user_id) && !empty($savings_id))
		{
			$this->db->where("user_id",$user_id);
			$this->db->where("id",$savings_id);
			$this->db->where("company_id",$this->company_id);

			$this->response = $this->db->delete("employee_savings");
		}
		return $this->response;
	}
}
