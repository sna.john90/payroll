<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Mdschedule Class.
 * 
 * @extends CI_Model
 */
class Mdschedule extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function add($data=array())
	{
		$response = FALSE;
		if(!empty($data))
		{
			$this->db->insert(SCHEDULE,$data);
			$response = $this->db->insert_id();
		}
		return $response;
	}

    public function add_schedule_payment($data=array())
    {
        $response = FALSE;
        if(!empty($data))
        {
            $this->db->insert(SCHEDULE_PAYMENT,$data);
            $response = $this->db->insert_id();
        }
        return $response;
    }

    public function remove_schedule($filters=false)
    {
        $response = FALSE;
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $response = $this->db->delete(SCHEDULE);
        return $response;
    }

    public function remove_schedule_payment($filters=false)
    {
        $response = FALSE;
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $response = $this->db->delete(SCHEDULE_PAYMENT);
        return $response;
    }

    public function add_schedule_comments($data=array())
    {
        $response = FALSE;
        if(!empty($data))
        {
            $this->db->insert(SCHEDULE_COMMENTS,$data);
            $response = $this->db->insert_id();
        }
        return $response;
    }

    public function add_schedule_payment_batch($data=array())
    {
        $response = FALSE;
        if(!empty($data))
        {
            $response = $this->db->insert_batch(SCHEDULE_PAYMENT,$data);
        }
        return $response;
    }

    public function get_schedule($filters=false,$select="*")
    {
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $this->db->select($select,FALSE);
        $response = $this->db->get(SCHEDULE)->first_row("array");
        return  $response;
    }

    public function get_total_signings($filters=false)
    {
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $schedule = SCHEDULE;
        $timezone = TIMEZONE;
        $schedule_notary = SCHEDULE_NOTARY;

        $this->db->select("s.id");
        $this->db->from("{$schedule} s");
        $this->db->join("{$schedule_notary} sn","sn.schedule_id=s.id","left");
        $this->db->join("{$timezone} tz","tz.name=s.timezone","left");

        $response = $this->db->get()->num_rows();
        return  $response;
    }

    public function get_schedule_timezone($filters=false,$select="*")
    {
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $schedule = SCHEDULE;
        $timezone = TIMEZONE;

        $this->db->select($select,FALSE);
        $this->db->from("{$schedule} s");
        $this->db->join("{$timezone} tz","tz.name=s.timezone","left");

        $response = $this->db->get()->first_row("array");
        return  $response;
    }

    public function get_schedule_payment($filters=false,$select="*")
    {
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $this->db->select($select,FALSE);
        $response = $this->db->get(SCHEDULE_PAYMENT)->first_row("array");
        return  $response;
    }

    public function get_admin_status($filters=false,$select="*")
    {
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $this->db->select($select,FALSE);
        $response = $this->db->get(ADMIN_STATUS)->first_row("array");
        return  $response;
    }

    public function get_schedule_with_borrower($filters=false,$select="*")
    {
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $schedule = SCHEDULE;
        $borrower = BORROWERS;

        $this->db->select($select,FALSE);
        $this->db->from("{$schedule} s");
        $this->db->join("{$borrower} b","b.id=s.borrower_id");
        $response = $this->db->get()->first_row("array");
        return  $response;
    }

	public function get_schedule_info($filters=false,$select="*")
	{
		$schedule = SCHEDULE;
		$users = USERS;
		$borrower = BORROWERS;
		$admin_status = ADMIN_STATUS;
		$product = PRODUCT;
        $company = COMPANY;
        $members = COMPANY_MEMBERS;

		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $this->db->select($select,FALSE);
        $this->db->from("{$schedule} s");
        $this->db->join("{$borrower} b","b.id=s.borrower_id","left");
        $this->db->join("{$users} u","u.id=s.processor_id","left");
        $this->db->join("{$users} u2","u2.id=s.underwriter_id","left");
        $this->db->join("{$users} u3","u3.id=s.loan_officer_id","left");
        $this->db->join("{$members} lcm","lcm.user_id=u3.id","left");
        $this->db->join("{$company} lc","lc.id=lcm.lending_company_id","left");
        $this->db->join("{$product} p","p.id=s.product_id","left");
        $response = $this->db->get()->first_row("array");
        return  $response;
	}
    public function get_schedule_number($filters=false)
    {
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $schedule = SCHEDULE;
        $timezone = TIMEZONE;
        $schedule_notary = SCHEDULE_NOTARY;

        $offset = $this->session->userdata("timezone_offset");

        $this->db->distinct("s.id");
        // For underwriter delayed 6 hours
        if(is_underwriter())
        {
            $this->db->select("COUNT(s.id) as title, DATE_FORMAT( DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ), '%Y-%m-%d') as start");
        }else{
            $this->db->select("COUNT(s.id) as title, ( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d') ) as start");
        }

        $this->db->from("{$schedule} s");
        if(is_notary())
        {
            $this->db->join("{$schedule_notary} sn","sn.schedule_id=s.id");
        }
        $this->db->join("{$timezone} tz","tz.name=s.timezone","left");
        $response = $this->db->get()->result_array();
        return  $response;
    }

    public function total_month_schedule($filters=false,$currDate)
    {
        $response = 0;

        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }

        $schedule = SCHEDULE;
        $timezone = TIMEZONE;
        $schedule_notary = SCHEDULE_NOTARY;

        $offset = $this->session->userdata("timezone_offset");

        $this->db->distinct("s.id");
        $this->db->select("COUNT(s.id) as total");
        $this->db->where("( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m') ) =", date("Y-m",strtotime($currDate)));
        $this->db->from("{$schedule} s");
        if(is_notary())
        {
            $this->db->join("{$schedule_notary} sn","sn.schedule_id=s.id");
            $this->db->where("notary_id",user_id());
            $this->db->group_start();
                $this->db->where("is_accepted", 0);
                $this->db->or_where("is_accepted", 1);
            $this->db->group_end();
        }
        $this->db->join("{$timezone} tz","tz.name=s.timezone","left");
        $result = $this->db->get()->first_row("array");
        if(!empty($result))
        {
            $response = $result['total'];
        }
        return  $response;
    }

    public function get_schedule_delivery($params=array())
    {
        $schedule = SCHEDULE;
        $schedule_notary = SCHEDULE_NOTARY;

        $this->db->select("sn.is_delivered,sn.estimated_delivery_date");
        $this->db->where(" DATE_FORMAT(sn.estimated_delivery_date, '%Y-%m-%d') >=", $params['start']);
        $this->db->where(" DATE_FORMAT(sn.estimated_delivery_date, '%Y-%m-%d') <=", $params['end']);
        $this->db->where("s.is_deleted",0);

        $this->db->from("{$schedule_notary} sn");
        $this->db->join("{$schedule} s","s.id=sn.schedule_id");

        $response = $this->db->get()->result_array();
        return  $response;
    }

    public function is_notary_accepted($filters=array())
    {
        $response = false;
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $this->db->select("is_accepted");
        $result = $this->db->get(SCHEDULE_NOTARY)->first_row("array");
        if(!empty($result))
        {
            $response = $result['is_accepted'];
        }
        return  $response;
    }

    public function get_tracked_schedule_number($filters=false)
    {
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }

        $schedule = SCHEDULE;
        $schedule_notary = SCHEDULE_NOTARY;

        $this->db->distinct("sn.id");
        $this->db->select("COUNT(sn.id) as title, sn.estimated_delivery_date as start,sn.is_delivered,sn.estimated_delivery_date");

        $this->db->from("{$schedule_notary} sn");
        $this->db->join("{$schedule} s","s.id=sn.schedule_id");
        $response = $this->db->get()->result_array();
        return  $response;
    }

    public function schedule_comment_listing($filters=false,$select="*")
    {
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $comments = SCHEDULE_COMMENTS;
        $users = USERS;
        $this->db->select($select,FALSE);
        $this->db->from("{$comments} sc");
        $this->db->join("{$users} u","u.id=sc.user_id","left");
        $response = $this->db->get()->result_array();
        return  $response;
    }

    public function listing_schedule_payments($filters=false,$select="*")
    {
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $this->db->select($select,FALSE);
        $response = $this->db->get(SCHEDULE_PAYMENT)->result_array();
        return  $response;
    }

	public function update_schedule($filters=false,$data=array())
	{
		$response = false;
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        if(!empty($data))
        {
        	$response = $this->db->update(SCHEDULE, $data);
        }
        return $response;
	}

    public function update_schedule_payment($filters=false,$data=array())
    {
        $response = false;
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        if(!empty($data))
        {
            $response = $this->db->update(SCHEDULE_PAYMENT, $data);
        }
        return $response;
    }

    public function update_schedule_notary($filters=false,$data=array())
    {
        $response = false;
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        if(!empty($data))
        {
            $response = $this->db->update(SCHEDULE_NOTARY, $data);
        }
        return $response;
    }

    public function update_schedule_notary_batch($filters=false,$data=array(),$where="id")
    {
        if(!empty($data))
        {
            $response = $this->db->update_batch(SCHEDULE_NOTARY,$data,$where);
        }
        return $response;
    }

    public function remove_schedule_notary($filters=false)
    {
        $response = false;
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $response = $this->db->delete(SCHEDULE_NOTARY);
        return $response;
    }

    public function remove_schedule_documents($filters=false)
    {
        $response = false;
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $response = $this->db->delete(SCHEDULE_DOCUMENTS);
        return $response;
    }

	public function get_schedule_notary($filters=false,$select="*")
	{
		$response = false;
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $schedule_notary = SCHEDULE_NOTARY;
        $users = USERS;
        $notary_status = NOTARY_STATUS;

        $this->db->select($select,FALSE);
        $this->db->from("{$schedule_notary} sn");
        $this->db->join("{$users} u","u.id=sn.notary_id");
        $this->db->join("{$notary_status} ns","ns.id=sn.notary_status_id","left");
        $response = $this->db->get()->first_row("array");
        return $response;
	}

    public function listing_schedule_notary($filters=false,$select="*")
    {
        $response = false;
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $schedule_notary    = SCHEDULE_NOTARY;
        $schedule           = SCHEDULE;

        $this->db->select($select,FALSE);
        $this->db->from("{$schedule_notary} sn");
        $this->db->join("{$schedule} s","s.id=sn.schedule_id");
        $response = $this->db->get()->result_array();
        return $response;
    }

    public function get_schedule_notary_only($filters=false,$select="*")
    {
        $response = false;
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $this->db->select($select,FALSE);
        $response = $this->db->get(SCHEDULE_NOTARY)->first_row("array");
        return $response;
    }

    public function get_schedule_notary_only2($filters=false,$select="*")
    {
        $response = false;
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $schedule_notary    = SCHEDULE_NOTARY;
        $users = USERS;

        $this->db->select($select,FALSE);
        $this->db->from("{$schedule_notary} sn");
        $this->db->join("{$users} u","u.id=sn.notary_id","left");

        $response = $this->db->get()->first_row("array");
        return $response;
    }

    public function get_schedule_notary_with_schedule($filters=false,$select="*")
    {
        $response = false;
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $schedule_notary = SCHEDULE_NOTARY;
        $users = USERS;
        $schedule = SCHEDULE;
        $notary_status = NOTARY_STATUS;

        $this->db->select($select,FALSE);
        $this->db->from("{$schedule_notary} sn");
        $this->db->join("{$schedule} s","s.id=sn.schedule_id");
        $this->db->join("{$users} u","u.id=sn.notary_id");
        $this->db->join("{$notary_status} ns","ns.id=sn.notary_status_id","left");
        $response = $this->db->get()->first_row("array");
        return $response;
    }

    public function get_schedule_notary_info($filters=false,$select="*")
    {
        $response = false;
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }

        $schedule = SCHEDULE;
        $schedule_notary = SCHEDULE_NOTARY;
        $users = USERS;
        $borrower = BORROWERS;
        $company = COMPANY;
        $members = COMPANY_MEMBERS;
        $product = PRODUCT;

        $this->db->select($select,FALSE);
        $this->db->from("{$schedule_notary} sn");
        $this->db->join("{$schedule} s","s.id=sn.schedule_id");
        $this->db->join("{$borrower} b","b.id=s.borrower_id");
        $this->db->join("{$users} u","u.id=sn.notary_id");
        $this->db->join("{$users} u2","u2.id=s.loan_officer_id");
        $this->db->join("{$members} lcm","lcm.user_id=u2.id","left");
        $this->db->join("{$company} lc","lc.id=lcm.lending_company_id","left");
        $this->db->join("{$product} p","p.id=s.product_id","left");
        $response = $this->db->get()->first_row("array");
        return $response;
    }

	public function total_schedule_notary($filters=false)
	{
		$response = false;
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $this->db->select("id");
        $response = $this->db->get(SCHEDULE_NOTARY)->num_rows();
        return $response;
	}

    public function total_schedule_documents($filters=false)
    {
        $response = false;
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $this->db->select("id");
        $response = $this->db->get(SCHEDULE_DOCUMENTS)->num_rows();
        return $response;
    }

    public function is_closing_signed($filters=false)
    {
        $response = false;
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $this->db->select("id");
        $this->db->where("notary_status_id",3);
        $this->db->where("UNIX_TIMESTAMP(date_closed) >",0);
        $response = $this->db->get(SCHEDULE_NOTARY)->num_rows();
        return $response==1;
    }

	public function not_scheduled_listing($filters=false,$start=0,$limit=10)
	{
		$schedule = SCHEDULE;
		$schedule_notary = SCHEDULE_NOTARY;
		$borrower = BORROWERS;
		$admin_status = ADMIN_STATUS;
        $schedule_status = SCHEDULE_STATUS;
		$users = USERS;
        $folders = FOLDERS;
        $comments = SCHEDULE_COMMENTS;
        $timezone = TIMEZONE;
        $notes = INTERNAL_NOTES;

		$response = array();
		
		// get records with filters
		$this->db->start_cache();
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }

        $offset = $this->session->userdata("timezone_offset");

        $this->db->select("s.id,s.date_scheduled,s.timezone,s.property_location,s.title_order_number,s.processor_id,s.underwriter_id,s.admin_status_id,s.schedule_status_id,s.folder_id,s.loan_officer_id,s.request_notary_service,s.is_deleted,b.name as borrower_name,as.status as admin_status,as.label as admin_status_label,as.color as admin_status_color, as.border_color as admin_status_border_color, scs.name as schedule_status_name, f.name as folder_name, COALESCE((SELECT COUNT(*) as count FROM ".$comments." sc WHERE sc.schedule_id = s.id),0) as count_comments, COALESCE((SELECT COUNT(*) as count FROM ".$notes." no WHERE no.schedule_id = s.id),0) as count_notes, ( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ) as date_scheduled_timezone, sn.id as schedule_notary_id");
        $this->db->from("{$schedule} s");
        $this->db->join("{$borrower} b","b.id=s.borrower_id","left");
        $this->db->join("{$admin_status} as","as.id=s.admin_status_id","left");
        $this->db->join("{$schedule_status} scs","scs.id=s.schedule_status_id","left");
        $this->db->join("{$schedule_notary} sn","sn.schedule_id=s.id","left");
        $this->db->join("{$folders} f","f.id=s.folder_id","left");
        $this->db->join("{$timezone} tz","tz.name=s.timezone","left");
        
        if($limit!=-1)
        {
            $this->db->limit($limit,$start);
        }
        
        $this->db->stop_cache();
        $response['limit'] = $limit;
        $response['start'] = $start;
        $response['result'] = $this->db->get()->result_array();
        $response['totalCount'] = $this->db->get()->num_rows();
        //print_me($response);

        $this->db->flush_cache();

		return $response;
	}

	public function scheduled_listing($filters=false,$start=0,$limit=10)
	{
		$schedule = SCHEDULE;
		$schedule_notary = SCHEDULE_NOTARY;
		$borrower = BORROWERS;
		$admin_status = ADMIN_STATUS;
        $schedule_status = SCHEDULE_STATUS;
		$users = USERS;
        $folders = FOLDERS;
        $comments = SCHEDULE_COMMENTS;
        $timezone = TIMEZONE;
        $notes = INTERNAL_NOTES;

		$response = array();

		// get records with filters
		$this->db->start_cache();
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }

        $offset = $this->session->userdata("timezone_offset");

        $this->db->select("s.id,s.date_scheduled,s.timezone,s.property_location,s.title_order_number,s.processor_id,s.underwriter_id,s.admin_status_id,s.schedule_status_id,s.folder_id,s.loan_officer_id,s.request_notary_service,s.is_deleted, sn.id as schedule_notary_id,sn.notary_id,sn.estimated_delivery_date, sn.is_accepted, b.name as borrower_name,as.status as admin_status,as.label as admin_status_label,as.color as admin_status_color, as.border_color as admin_status_border_color, scs.name as schedule_status_name, f.name as folder_name, u.name as notary_name, COALESCE((SELECT COUNT(*) as count FROM ".$comments." sc WHERE sc.schedule_id = s.id),0) as count_comments, COALESCE((SELECT COUNT(*) as count FROM ".$notes." no WHERE no.schedule_id = s.id),0) as count_notes, ( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ) as date_scheduled_timezone");
        $this->db->from("{$schedule} s");
        $this->db->join("{$borrower} b","b.id=s.borrower_id","left");
        $this->db->join("{$admin_status} as","as.id=s.admin_status_id","left");
        $this->db->join("{$schedule_status} scs","scs.id=s.schedule_status_id","left");
        $this->db->join("{$schedule_notary} sn","sn.schedule_id=s.id","left");
        $this->db->join("{$users} u","u.id=sn.notary_id","left");
        $this->db->join("{$folders} f","f.id=s.folder_id","left");
        $this->db->join("{$timezone} tz","tz.name=s.timezone","left");

        if($limit!=-1)
        {
            $this->db->limit($limit,$start);
        }
        
        $this->db->stop_cache();
        $response['limit'] = $limit;
        $response['start'] = $start;
        $response['result'] = $this->db->get()->result_array();
        $response['totalCount'] = $this->db->get()->num_rows();

        $this->db->flush_cache();

		return $response;
	}

	public function schedule_listing($filters=false,$start=0,$limit=10)
	{
		$schedule = SCHEDULE;
		$borrower = BORROWERS;
        $schedule_notary = SCHEDULE_NOTARY;
		$admin_status = ADMIN_STATUS;
		$users = USERS;
        $folders = FOLDERS;
        $comments = SCHEDULE_COMMENTS;
        $timezone = TIMEZONE;

		$response = array();

		// get records with filters
		$this->db->start_cache();
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }

        $offset = $this->session->userdata("timezone_offset");

        $this->db->select("s.id,s.date_scheduled,s.timezone,s.property_location,s.loan_number,s.title_order_number,s.processor_id,s.underwriter_id,s.admin_status_id, s.folder_id,s.loan_officer_id,s.request_notary_service,s.is_deleted, b.name as borrower_name,u.name as processor_name, u2.name as underwriter_name, u3.name as notary_name, u4.name as loan_officer_name, as.status as admin_status,as.label as admin_status_label,as.color as admin_status_color, as.border_color as admin_status_border_color, f.name as folder_name,COALESCE((SELECT COUNT(*) as count FROM ".$comments." sc WHERE sc.schedule_id = s.id),0) as count_comments, sn.id as schedule_notary_id,sn.notary_id,sn.is_accepted,sn.estimated_delivery_date");

        // For underwriter delayed 6 hours
        if(is_underwriter())
        {
            $this->db->select("DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ) as date_scheduled_timezone");
        }else{
            $this->db->select("( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ) as date_scheduled_timezone");
        }

        $this->db->from("{$schedule} s");
        $this->db->join("{$borrower} b","b.id=s.borrower_id","left");
        $this->db->join("{$schedule_notary} sn","sn.schedule_id=s.id","left");
        $this->db->join("{$users} u","u.id=s.processor_id","left");
        $this->db->join("{$users} u2","u2.id=s.underwriter_id","left");
        $this->db->join("{$users} u3","u3.id=sn.notary_id","left");
        $this->db->join("{$users} u4","u4.id=s.loan_officer_id","left");
        $this->db->join("{$admin_status} as","as.id=s.admin_status_id","left");
        $this->db->join("{$folders} f","f.id=s.folder_id","left");
        $this->db->join("{$timezone} tz","tz.name=s.timezone","left");
        
        if($limit!=-1)
        {
            $this->db->limit($limit,$start);
        }

        $this->db->stop_cache();

        $response['limit'] = $limit;
        $response['start'] = $start;
        $response['result'] = $this->db->get()->result_array();
        $response['totalCount'] = $this->db->get()->num_rows();

        $this->db->flush_cache();

		return $response;
	}

    public function schedule_only_listing($filters=false,$select="*")
    {
        $this->db->start_cache();
        if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Flschedule',$filters);
        }
        $this->db->select($select,FALSE);
        $this->db->stop_cache();
        $response = $this->db->get(SCHEDULE)->result_array();
        $this->db->flush_cache();
        return $response;
    }
}