<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Mdduser Class.
 * 
 * @extends CI_Model
 */
class Mdscripts extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
	}
	public function import_data($data=array())
	{
		$response  = array(
			"total" => 0,
			"duplicate" => 0
		);
		if(!empty($data))
		{
			foreach ($data as $key => $value) 
			{
				$this->db->start_cache();
				$this->db->select("id")->from('users');
				$where = array(
					"name" => $value['name'],
					"state" => $value['state']
				);
				if(isset($value['email']))
				{	
					$where["email"] = $value['email'];
				}
				if(isset($value['city']))
				{	
					$where["city"] = $value['city'];
				}
				$this->db->where($where);
				$this->db->stop_cache();
				$num_rows = $this->db->get()->num_rows();
				$this->db->flush_cache();
				if($num_rows==0)
				{
					$query 	= $this->db->insert('users',$value);
					if($query)
					{
						$id 	= $this->db->insert_id();
						$temp 	= array(
							"user_id" 		=> $id,
							"user_group_id"	=> 5
						);
						$hello = $this->db->insert('user_group_reference',$temp);
						if($hello)
						{
							$response['total']++;
						}
					}
				}else
				{ 
					$response['duplicate']++;
				}
			}
		}
		return $response;
	}
}