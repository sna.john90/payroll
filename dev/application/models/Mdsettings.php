<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Mdsavings Class.
 * 
 * @extends CI_Model
 */
class Mdsettings extends CI_Model 
{
	var $company_id = 0;
	var $response = false;
	public function __construct() {
		parent::__construct();
		$this->company_id = get_company_id();
	}
	public function get_department($parentid=0,$id=0,$all=false)
	{
		if(!$all)
		{
			if($id>0)
			{
				$this->db->where("id",$id);
			}
			else
			{
				if($parentid>0)
				{
					$this->db->where("parent_id",$parentid);
				}
				else 
				{
					$this->db->where("parent_id",0);
				}
			} 
		} 
		$this->db->where("(company_id=0 OR company_id={$this->company_id})",null,false);
		$this->db->order_by("company_id","ASC");

		$response = $this->db->get("mnt_departments")->result_array();
	//	echo $this->db->last_query();
		return $response;
	} 
	public function add_department($data=array())
	{
		$response = false;
		if(!empty($data))
		{
			$response = $this->db->insert("mnt_departments",$data);
		}
		return $response;
	}
	public function update_department($data=array(),$id="")
	{
		if(!empty($data) && !empty($id))
		{
			$this->db->where("id",$id);
			$this->db->where("company_id",$this->company_id);

			$this->response = $this->db->update("mnt_departments",$data);
		}
		return $this->response;
	}
	public function delete_department($id)
	{
		$this->db->where("id",$id);
		$this->db->where("company_id",$this->company_id);
		return $this->db->delete("mnt_departments");
	}

	public function get_payrollmnt()
	{
		$this->db->where("(company_id=0 OR company_id={$this->company_id})",null,false);
		$this->db->order_by("company_id","DESC");
		return $this->db->get("payroll_maintainables")->result_array();
	}
}