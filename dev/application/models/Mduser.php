<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Mdduser Class.
 * 
 * @extends CI_Model
 */
class Mduser extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function create_user($data=array(), $user_type=1, $company_id="",$returnid=false)
	{
		if(!empty($data))
		{
			// hash password
			$data['password'] = $this->_hash_password($data['password']);

			$this->db->insert(USERS,$data);
			$user_id = $this->db->insert_id();
			if($user_id)
			{
				// add user groups reference
				$user_type = ($user_type==1)? 5 : $user_type;
				$data = array(
					"user_id" => $user_id,
					"user_group_id" => $user_type,
					"date_added" => date("Y-m-d H:i:s")
				);
				$response = $this->db->insert(USER_GROUP_REFERENCE,$data);
				if($returnid)
				{
					return $user_id;
				}
				return (bool)$response;
			}
		}
		return FALSE;
	}

	public function add_special_role($data=array())
	{
		if(!empty($data))
		{
			$this->db->insert(USER_SPECIAL_ROLES,$data);
			$response = $this->db->insert_id();
		}
		return $response;
	}

	public function add_lending_company_members($data=array())
	{
		if(!empty($data))
		{
			$this->db->insert(COMPANY_MEMBERS,$data);
			$response = $this->db->insert_id();
		}
		return $response;
	}

	public function remove_special_role($filters=false)
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fluser',$filters);
        }
		$response = $this->db->delete(USER_SPECIAL_ROLES);
		return $response;
	}

	public function edit_user($filters=false, $data=array())
	{
		$user = array();
		if($filters!=false)
        {
        	// get user
        	$this->db->select("profile_pic");
			$this->db->where("id",$filters['id']);
			$user = $this->db->get(USERS)->first_row('array');

            $this->load->library('orm/Filters');
            $this->filters->detect('Fluser',$filters);
        }
		if(!empty($data))
		{
			if(isset($data['password']))
			{
				$data['password'] = $this->_hash_password($data['password']);
			}
			$response = $this->db->update(USERS,$data);

			if(!empty($user) && $response && isset($data['profile_pic']))
			{
				if($user['profile_pic']!="" && file_exists("./uploads/profile/".$user['profile_pic']))
				{
					unlink("./uploads/profile/".$user['profile_pic']);
				}
			}
			return $response;
		}
		return FALSE;
	}

	public function update_user_batch($data=array(),$where)
	{
		if(!empty($data))
		{
			$response = $this->db->update_batch(USERS,$data,$where);
			return $response;
		}
		return FALSE;
	}
	public function update_user($data=array(),$id="",$company_id="")
	{
		$response = false;
		if(!empty($data) AND !empty($id) AND !empty($company_id))
		{
			//basic info
			$this->db->where("id",$id);
			$this->db->where("company_id",$company_id);
			$response = $this->db->update(USERS,$data['basic']);
			if($response)
			{
				//update emp details
				$empd_ref = $data['empdetails']['empd_ref'];
				unset($data['empdetails']['empd_ref']);
				$this->db->where("id",$empd_ref);
				$this->db->where("user_id",$id);
				$this->db->update(EMP_DETAILS,$data['empdetails']);

				//update group details
				$group_ref = $data['group']['group_ref'];
				unset($data['group']['group_ref']);
				$this->db->where("id",$group_ref);
				$this->db->where("user_id",$id);
				$this->db->update(USER_GROUP_REFERENCE,$data['group']);
			}
		}
		return $response;
	}
	public function edit_username_and_password($filters=false,$username,$password)
	{
		$data = array(
			"username" => $username,
			"password" => $this->_hash_password($password)
		);
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fluser',$filters);
        }
        if(!empty($data))
		{
			$response = $this->db->update(USERS,$data);
			return $response;
		}
		return FALSE;
	}

	public function check_user($filters=false)
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fluser',$filters);
        }
        $num_result = $this->db->get(USERS)->num_rows();
        return $num_result == 1;
	}

	public function reset_password($data, $code)
	{
		$data['password'] = $this->_hash_password($data['password']);
		$this->db->where("forgotten_password_code", $code);
		$response = $this->db->update(USERS,$data);
		return $response;
	}

	public function forgot_password($email)
	{
		$response = array();

        // All random stuffs
        $activation_code_part = "";
		if(function_exists("openssl_random_pseudo_bytes")) 
		{
			$activation_code_part = openssl_random_pseudo_bytes(128);
		}
		for($i = 0; $i < 1024 ; $i++) 
		{
			$activation_code_part = sha1($activation_code_part . mt_rand() . microtime(). $email);
		}
		// data to be save
		$data = array(
			'forgotten_password_code' => $activation_code_part,
		    'forgotten_password_time' => date("Y-m-d H:i:s")
		);

		$this->db->where("email", $email);
		$response['update'] = $this->db->update(USERS, $data);

		$response['data']   = $data;

		return $response;
	}

	public function edit_user_group_reference($filters=false, $data=array())
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fluser',$filters);
        }
		if(!empty($data))
		{
			$response = $this->db->update(USER_GROUP_REFERENCE,$data);
			return $response;
		}
		return FALSE;
	}
	public function remove_user($filters=false)
	{
		$user = array();
		if($filters!=false)
        {
        	// get user
        	$this->db->select("profile_pic");
			$this->db->where("id",$filters['id']);
			$user = $this->db->get(USERS)->first_row('array');

            $this->load->library('orm/Filters');
            $this->filters->detect('Fluser',$filters);
        }
		$response = $this->db->delete(USERS);
		if(!empty($user) && $response)
		{
			if($user['profile_pic']!="" && file_exists("./uploads/profile/".$user['profile_pic']))
			{
				unlink("./uploads/profile/".$user['profile_pic']);
			}
		}
		return $response;
	} 
	public function get_user_id($filters=false)
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fluser',$filters);
        }
        $this->db->select("id");
        return $this->db->get(USERS)->row('id');
	}

	public function get_user($filters=false,$select="u.*,ur.user_group_id,ug.name as user_type,ug.description as user_type_description")
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fluser',$filters);
        }
        $user = USERS;
        $user_group = USER_GROUPS;
        $user_ref = USER_GROUP_REFERENCE;
        $department = MNT_DEPT;
        $emp_details = EMP_DETAILS;


        $this->db->select($select,FALSE);
        $this->db->from("{$user} u");
        $this->db->join("{$user_ref} ur","ur.user_id=u.id","inner");
        $this->db->join("{$user_group} ug","ug.id=ur.user_group_id");
        $this->db->join("{$emp_details} empd","empd.user_id=u.id"); 
         $this->db->join("{$department} dept","dept.id=empd.department_id"); 
        $response = $this->db->get()->first_row('array');
        return $response;
	}

	public function get_user_only($filters=false,$select="*")
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fluser',$filters);
        }
        $response = $this->db->get(USERS)->first_row('array');
        return $response;
	}

	public function get_timezone_offset($filters=false)
	{
		$response = intval(-25200);
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fluser',$filters);
        }
        
        $this->db->select("gmt_offset");
        $response = $this->db->get(TIMEZONE)->first_row('array');
        if($response)
        {
        	$response = $response['gmt_offset'];
        }
        return $response;
	}

	public function get_user_info($filters=false,$select="u.*,ur.user_group_id,ug.name as user_type,ug.description as user_type_description,tz.gmt_offset as timezone_offset")
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fluser',$filters);
        }
        $user = USERS;
        $user_group = USER_GROUPS;
        $user_ref = USER_GROUP_REFERENCE;
        $timezone = TIMEZONE;

        $this->db->select($select,FALSE);
        $this->db->from("{$user} u");
        $this->db->join("{$user_ref} ur","ur.user_id=u.id","inner");
        $this->db->join("{$user_group} ug","ug.id=ur.user_group_id");
        $this->db->join("{$timezone} tz","tz.name=u.timezone","left");
        $response = $this->db->get()->first_row('array');

        return $response;
	}

	public function get_update_user_info($data=array(),$id,$select="*")
	{
		$response = false;

		$this->db->where("id",$id);
		$update = $this->db->update(USERS,$data);
		if($update)
		{
			$user = USERS;
	        $timezone = TIMEZONE;

	        $this->db->select($select);
	        $this->db->from("{$user} u");
	        $this->db->where("u.id",$id);
	        $this->db->join("{$timezone} tz","tz.name=u.timezone","left");
	        $response = $this->db->get()->first_row('array');
		}

        return $response;
	}

	public function get_user_and_company($filters=false,$select="u.*,ur.user_group_id,ug.name as user_type,ug.description as user_type_description,co.name as company_name")
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fluser',$filters);
        }
        $user = USERS;
        $user_group = USER_GROUPS;
        $user_ref = USER_GROUP_REFERENCE;
        $company_members = COMPANY_MEMBERS;
        $company = COMPANY;

        $this->db->select($select,FALSE);
        $this->db->from("{$user} u");
        $this->db->join("{$user_ref} ur","ur.user_id=u.id","inner");
        $this->db->join("{$user_group} ug","ug.id=ur.user_group_id");
        $this->db->join("{$company_members} lc","lc.user_id=u.id","left");
        $this->db->join("{$company} co","co.id=lc.lending_company_id","left");
        $response = $this->db->get()->first_row('array');
        return $response;
	}

	public function check_user_login($username, $password) {
		$this->db->select('password');
		$this->db->from(USERS);
		$this->db->where('username', $username);
		$this->db->where('deleted', 0);
		$this->db->where('status', 1);
		$result = $this->db->get()->row_array();
		if(!empty($result))
		{
			$hash = $result['password'];
			return $this->_verify_password_hash($password, $hash);
		}
		return false;
	}

	public function check_forgotten_password($code)
	{
		$this->db->select("id");
		$this->db->where("forgotten_password_code", $code);
		$result = $this->db->get(USERS)->num_rows();
		return $result == 1;
	}

	public function user_lists($filters=false,$start=0,$limit=10)
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fluser',$filters);
        }
        $user = USERS;
        $user_group = USER_GROUPS;
        $user_ref = USER_GROUP_REFERENCE;
        $company_member = COMPANY_MEMBERS;

        $this->db->select("u.*,ur.user_group_id,ug.name as user_type,ug.description as user_type_description");
        $this->db->from("{$user} u");
        $this->db->join("{$user_ref} ur","ur.user_id=u.id","inner");
        $this->db->join("{$user_group} ug","ug.id=ur.user_group_id");
        $this->db->join("{$company_member} lc","lc.user_id=u.id","inner");
        $response = $this->db->get()->result_array();
        return $response;
	}

	public function get_user_lists($where=array())
	{
        $user = USERS;
        $user_group = USER_GROUPS;
        $user_ref = USER_GROUP_REFERENCE;
        $company_member = COMPANY_MEMBERS;

        $this->db->select("u.*,ur.user_group_id,ug.name as user_type,ug.description as user_type_description");
        $this->db->from("{$user} u");
        $this->db->join("{$user_ref} ur","ur.user_id=u.id","inner");
        $this->db->join("{$user_group} ug","ug.id=ur.user_group_id");
        $this->db->join("{$company_member} lc","lc.user_id=u.id","inner");
        $this->db->where($where);
        $response = $this->db->get()->result_array();
        return $response;
	}


	public function user_only_lists($filters=false,$start=0,$limit=10)
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fluser',$filters);
        }
        $user = USERS;
        $user_group = USER_GROUPS;
        $user_ref = USER_GROUP_REFERENCE;

        $this->db->select("u.*,ur.user_group_id,ug.name as user_type,ug.description as user_type_description");
        $this->db->from("{$user} u");
        $this->db->join("{$user_ref} ur","ur.user_id=u.id","inner");
        $this->db->join("{$user_group} ug","ug.id=ur.user_group_id");
        $response = $this->db->get()->result_array();
        return $response;
	}

	public function user_company_lists($filters=false,$start=0,$limit=10)
	{
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fluser',$filters);
        }
        $user = USERS;
        $user_group = USER_GROUPS;
        $user_ref = USER_GROUP_REFERENCE;
        $company_members = COMPANY_MEMBERS;

        $this->db->distinct("u.id");
        $this->db->select("u.*,ur.user_group_id,ug.name as user_type,ug.description as user_type_description");
        $this->db->from("{$company_members} m");
        $this->db->join("{$user} u","u.id=m.user_id");
        $this->db->join("{$user_ref} ur","ur.user_id=u.id");
        $this->db->join("{$user_group} ug","ug.id=ur.user_group_id");
        $response = $this->db->get()->result_array();
        //echo $this->db->last_query();
        return $response;
	}

	public function user_company_only_lists($company_id)
	{
		$this->db->distinct("user_id");
		$this->db->select("user_id");
		$this->db->where("lending_company_id",$company_id);
		$response = $this->db->get(COMPANY_MEMBERS)->result_array();
        return $response;
	}

	public function total_lending_company_user_rows($where=array())
	{
		if(!empty($where))
		{
			$this->db->where($where);
		}
		$response = $this->db->get(COMPANY_MEMBERS)->num_rows();
		return $response;
	}

	public function user_lists2($filters=false,$select="*",$page=1,$limit=false)
	{
		$this->db->start_cache();
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fluser',$filters);
        }
        $user = USERS;
        $user_group = USER_GROUPS;
        $user_ref = USER_GROUP_REFERENCE;

        $this->db->select($select,FALSE);
        $this->db->from("{$user} u");
        $this->db->join("{$user_ref} ur","ur.user_id=u.id","inner");
        $this->db->join("{$user_group} ug","ug.id=ur.user_group_id");

        $total_count = $this->db->get()->num_rows();
        $response['total_count'] = $total_count;

        if($limit!=false)
        {
            $total_pages = (int)($total_count/$limit);
            $response['total_pages'] = $total_pages;
            $start = ($limit*$page)-$limit;
            //limit
            $this->db->limit($limit,$start);
        }
        $this->db->stop_cache();

        $response['result'] = $this->db->get()->result_array();
        $this->db->flush_cache();
        return $response;
	}


	// users listing
	public function users_listing($filters=false,$start=0,$limit=10)
	{
		$user = USERS;
        $user_group = USER_GROUPS;
        $user_ref = USER_GROUP_REFERENCE; 
        $department = MNT_DEPT;
        $emp_details = EMP_DETAILS;
        $company_id = get_company_id();
		$response = array(); 
		// get records with filters
		$this->db->start_cache();
		if($filters!=false)
        {
            $this->load->library('orm/Filters');
            $this->filters->detect('Fluser',$filters);
        }
        
        $this->db->select("u.*,ur.user_group_id,ug.name as user_type,ug.description as user_type_description,empd.basic_pay,empd.payment_type,empd.designation,empd.date_hired,empd.status as emp_status,dept.description as dept_description");
        $this->db->from("{$user} u");

        if(!is_super_admin())
        { 
        	$this->db->where("u.company_id",$company_id); 
        }
        $this->db->where("u.id !=",1); //superadmin
        $this->db->where("u.deleted",0); 
        $this->db->join("{$user_ref} ur","ur.user_id=u.id","inner");
        $this->db->join("{$user_group} ug","ug.id=ur.user_group_id"); 
        $this->db->join("{$emp_details} empd","empd.user_id=u.id","left"); 
         $this->db->join("{$department} dept","dept.id=empd.department_id","left"); 

        $this->db->limit($limit,$start);
        
        $this->db->stop_cache();

        $response['limit'] = $limit;
        $response['start'] = $start;
        $response['result'] = $this->db->get()->result_array();
        $response['totalCount'] = $this->db->get()->num_rows();
       //  echo $this->db->last_query();
        $this->db->flush_cache(); 
		return $response;
	}

	public function remove($user_id="",$company_id="")
	{
		$response = false;
		if(!empty($user_id) AND !empty($company_id))
		{
			$this->db->where("id",$user_id);
			if(!is_super_admin() AND is_admin_alone())
			{
				$this->db->where("company_id",get_company_id()); 
			}
			$this->db->where("company_id",$company_id); 
			$response = $this->db->update(USERS,array("deleted" => 1));
		}
		return $response;
	}

	public function change_password($id, $new_password)
	{
		$data = array(
			"password" => $this->_hash_password($new_password)
		);
		$this->db->where("id",$id);
		$response = $this->db->update(USERS,$data);
		return $response;
	}

	private function _hash_password($password) {
		return password_hash($password, PASSWORD_BCRYPT);
	}

	private function _verify_password_hash($password,$hash) {
		return password_verify($password, $hash);
	}

	private function _set_session($data)
	{
		$session_data = array(
			"user_id" => encrypt($data['id']),
			"username" => $data['username'],
			"email" => $data['email'],
			"user_type" => $data['user_type'],
			"logged_in" => (bool)true
		);
		$this->session->set_userdata($session_data);

		return TRUE;
	}

	/***EMPLOYEE DETAILS***/
	public function save_employee_details($data=array())
	{
		$response = false;
		if(!empty($data))
		{
			$response = $this->db->insert(EMP_DETAILS,$data);
		}
		return $response;
	}
	public function get_company_employees()
	{
		$company_id = get_company_id();
		$this->db->where("company_id",$company_id);
		$this->db->where("emp_id !=","0");
		return $this->db->get(USERS)->result_array();
	}
	public function update_basic($id="",$data=array())
	{
		$response = false;
		if(!empty($id) && !empty($data))
		{
			$company_id = get_company_id();
			$this->db->where("company_id",$company_id);
			$this->db->where("id",$id); 

			$response = $this->db->update("users",$data);

		}
		return $response;
	}

}