<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Fladmin Class.
 * 
 * @extends CI_Model
 */
class Fladmin extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function id($id)
	{
		$this->db->where("id",$id);
	}
	public function search_status_and_label($search_key="")
	{
		if($search_key!="" && $search_key!=" ")
		{
			$search = explode(" ",$search_key);
			foreach ($search as $key => $value) {
				$this->db->or_like("status",$value,"both");
				$this->db->or_like("label",$value,"both");
			}
		}
	}
	public function order_by_status($order='ASC')
	{
		$this->db->order_by("status",strtoupper($order));
	}
	public function order_by_label($order='ASC')
	{
		$this->db->order_by("label",strtoupper($order));
	}
}