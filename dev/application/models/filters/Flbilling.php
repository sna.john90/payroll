<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Flbilling Class.
 * 
 * @extends CI_Model
 */
class Flbilling extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function id($id)
	{
		$this->db->where("id",$id);
	}
	public function lender_id($id)
	{
		$this->db->where("lender_id",$id);
	}
	public function request_notary_service($val)
	{
		$this->db->where("request_notary_service",$val);
	}
	public function is_deleted($val)
	{
		$this->db->where("is_deleted",$val);
	}
	public function is_accepted($is_accepted)
	{
		$this->db->where("is_accepted", $is_accepted);
	}
	public function is_paid($is_paid)
	{
		$this->db->where("is_paid", $is_paid);
	}
	
	public function date_scheduled_today_up($val=1)
	{
		if($val==1)
		{
			$offset = $this->session->userdata("timezone_offset");
			$get_offset = "(UNIX_TIMESTAMP(UTC_TIMESTAMP()) + ".$offset.") - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)";
			$this->db->where("FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + {$get_offset}, '%Y-%m-%d') >=", date("Y-m-d"));
		}
	}
	public function date_scheduled_past($val=1)
	{
		if($val==1)
		{
			$offset = $this->session->userdata("timezone_offset");
			$get_offset = "(UNIX_TIMESTAMP(UTC_TIMESTAMP()) + ".$offset.") - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)";

			if(is_underwriter())
			{
				$this->db->where("( DATE_FORMAT( DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ), '%Y-%m-%d') ) <", date("Y-m-d"));
			}else
			{
				$this->db->where("FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + {$get_offset}, '%Y-%m-%d') <", date("Y-m-d"));
			}
		}
	}
	public function date_scheduled_timezone($val=1)
	{
		
	}
	public function order_by_date_scheduled($order='ASC')
	{
		$this->db->order_by("date_scheduled_timezone",strtoupper($order));
	}
	public function order_by_time($order='ASC')
	{
		$this->db->order_by("date_scheduled_timezone",strtoupper($order));
	}

}