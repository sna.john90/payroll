<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Flborrower Class.
 * 
 * @extends CI_Model
 */
class Flborrower extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function id($id)
	{
		$this->db->where("id",$id);
	}
}