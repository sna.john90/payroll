<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Flcommon Class.
 * 
 * @extends CI_Model
 */
class Flcommon extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function id($id)
	{
		$this->db->where("id",$id);
	}
	public function schedule_id($schedule_id)
	{
		$this->db->where("schedule_id",$schedule_id);
	}
	public function order_by_date_added($order="ASC")
	{
		$this->db->order_by("date_added",$order);
	}
	public function order_by_name($order="ASC")
	{
		$this->db->order_by("name",$order);
	}
}