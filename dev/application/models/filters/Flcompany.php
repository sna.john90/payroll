<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Flcompany Class.
 * 
 * @extends CI_Model
 */
class Flcompany extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function id($id)
	{
		$this->db->where("id",$id);
	}
	public function lcm_user_id($id)
	{
		$this->db->where("lcm.user_id",$id);
	}
	public function order_by_name($order='ASC')
	{
		$this->db->order_by("name",strtoupper($order));
	}
	public function order_by_address($order='ASC')
	{
		$this->db->order_by("address",strtoupper($order));
	}
	public function order_by_email($order='ASC')
	{
		$this->db->order_by("email",strtoupper($order));
	}
	public function order_by_date_added($order="ASC")
	{
		$this->db->order_by("date_added",strtoupper($order));
	}
	public function search_company($search_key="")
	{
		if($search_key!="" && $search_key!=" ")
		{
			$search = explode(" ",$search_key);
			foreach ($search as $key => $value) {
				$this->db->or_like("name",$value,"both");
				$this->db->or_like("address",$value,"both");
				$this->db->or_like("email",$value,"both");
			}
		}
	}
}