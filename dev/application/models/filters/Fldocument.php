<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Fldocument Class.
 * 
 * @extends CI_Model
 */
class Fldocument extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function id($id)
	{
		$this->db->where("id",$id);
	}
	public function schedule_id($id)
	{
		$this->db->where("schedule_id",$id);
	}
	public function is_template($is_template)
	{
		$this->db->where("is_template",$is_template);
	}
	public function sd_id($id)
	{
		$this->db->where("sd.id",$id);
	}
	public function order_by_is_template($order)
	{
		$this->db->order_by("is_template",$order);
	}
	public function order_by_date_uploaded($order)
	{
		$this->db->order_by("date_uploaded",$order);
	}
}