<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Flcommon Class.
 * 
 * @extends CI_Model
 */
class Flexpenses extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function id($id)
	{
		$this->db->where("exp.id",$id);
	}
	public function order_column($data=array())
	{
		$this->db->order_by("exp.{$data['field']}",$data['ordered']);
	} 
	public function month_year($monthyear="")
	{
		if(!empty($monthyear))
		{
			$monthyear_exp = explode("-",$monthyear);
			$this->db->where("YEAR(exp.receipt_date)",$monthyear_exp[0]);
			$this->db->where("MONTH(exp.receipt_date)",$monthyear_exp[1]);
		}
	}
	public function by_category($category="")
	{
		$this->db->where("exp.category_id",$category);
	}
}