<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Flinvoice Class.
 * 
 * @extends CI_Model
 */
class Flinvoice extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function id($id)
	{
		$this->db->where("id",$id);
	}
	public function schedule_id($id)
	{
		$this->db->where("schedule_id",$id);
	}
	public function si_id($id)
	{
		$this->db->where("si.id",$id);
	}
}