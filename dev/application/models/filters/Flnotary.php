<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Flnotary Class.
 * 
 * @extends CI_Model
 */
class Flnotary extends CI_Model {

	public function __construct() {
		parent::__construct();
	}


	public function id($id)
	{
		$this->db->where("id",$id);
	}
	public function s_id($id)
	{
		$this->db->where("s.id",$id);
	}
	public function schedule_id($id)
	{
		$this->db->where("schedule_id",$id);
	}
	public function is_deleted($val)
	{
		$this->db->where("is_deleted",$val);
	}
	public function sn_schedule_id($id)
	{
		$this->db->where("sn.schedule_id",$id);
	}
	public function request_notary_service($val)
	{
		$this->db->where("request_notary_service",$val);
	}
	public function u_id($id)
	{
		$this->db->where("u.id",$id);
	}
	public function nf_user_id($id)
	{
		$this->db->group_start();
			$this->db->where("nf.user_id",$id);
			$this->db->or_where("nf.user_id",NULL);
		$this->db->group_end();
	}
	public function user_group_id($id)
	{
		$this->db->where("ug.id",$id);
	}
	public function sn_notary_id($id)
	{
		$this->db->where("sn.notary_id",$id);
	}

	public function is_accepted_and_waiting($val=1)
	{
		if($val==1)
		{
			$this->db->group_start();
				$this->db->where("is_accepted", 0);
				$this->db->or_where("is_accepted", 1);
			$this->db->group_end();
		}
	}

	public function is_accepted($is_accepted)
	{
		$this->db->where("is_accepted", $is_accepted);
	}

	public function order_by_date_scheduled($order='ASC')
	{
		$this->db->order_by("date_scheduled",strtoupper($order));
	}
	public function order_by_borrower_name($order='ASC')
	{
		$this->db->order_by("b.name",strtoupper($order));
	}
	public function order_by_time($order='ASC')
	{
		$this->db->order_by("date_scheduled",strtoupper($order));
	}
	public function order_by_signing_location($order='ASC')
	{
		$this->db->order_by("combined_address",strtoupper($order));
	}
	public function order_by_property_state($order='ASC')
	{
		$this->db->order_by("s.property_location",strtoupper($order));
	}
	public function order_by_lender($order='ASC')
	{
		$this->db->order_by("lc.name",strtoupper($order));
	}
	public function order_by_loan_officer($order='ASC')
	{
		$this->db->order_by("u.name",strtoupper($order));
	}
	public function order_by_notary_status($order='ASC')
	{
		$this->db->order_by("ns.status",strtoupper($order));
	}
	public function order_by_favorite($order='ASC')
	{
		$this->db->order_by("nf.id",strtoupper($order));
	}
	public function order_by_name($order='ASC')
	{
		$this->db->order_by("u.name",strtoupper($order));
	}
	public function order_by_address($order='ASC')
	{
		$this->db->order_by("u.address",strtoupper($order));
	}
	public function order_by_email($order='ASC')
	{
		$this->db->order_by("email",strtoupper($order));
	}
	public function order_by_date_added($order='ASC')
	{
		$this->db->order_by("u.created_at",strtoupper($order));
	}
	public function date_scheduled_today_up($val=1)
	{
		if($val==1)
		{
			$offset = $this->session->userdata("timezone_offset");
			$get_offset = "(UNIX_TIMESTAMP(UTC_TIMESTAMP()) + ".$offset.") - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)";

			$this->db->where("FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + {$get_offset}, '%Y-%m-%d') >=", date("Y-m-d"));
		}
	}
	public function date_scheduled_within_48_hours($val=1)
	{
		if($val==1)
		{
			$offset = $this->session->userdata("timezone_offset");
			$get_offset = "(UNIX_TIMESTAMP(UTC_TIMESTAMP()) + ".$offset.") - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)";

			$this->db->group_start();

				$this->db->or_group_start();
				$this->db->where("FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + {$get_offset}, '%Y-%m-%d %H:%i') >=", date("Y-m-d H:i",strtotime("-48 hours",strtotime(date("Y-m-d H:i:s")))));
				$this->db->where("FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + {$get_offset}, '%Y-%m-%d %H:%i') <=", date("Y-m-d H:i",strtotime(date("Y-m-d H:i:s"))));
				$this->db->group_end();
				
				$this->db->or_group_start();
					$this->db->where("FROM_UNIXTIME( UNIX_TIMESTAMP(sn.date_closed), '%Y-%m-%d %H:%i:%s') >=", date("Y-m-d H:i:s",strtotime("-48 hours",strtotime(date("Y-m-d H:i:s")))));
					$this->db->where("FROM_UNIXTIME( UNIX_TIMESTAMP(sn.date_closed), '%Y-%m-%d %H:%i:%s') <=", date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s"))));
				$this->db->group_end();

			$this->db->group_end();
		}
	}

	public function scheduled_completed($val=1)
	{
		if($val==1)
		{
			$this->db->where("sn.notary_status_id",3);
			$this->db->where("request_notary_service",1);
		}
	}

	public function date_scheduled_past($val=1)
	{
		if($val==1)
		{
			$offset = $this->session->userdata("timezone_offset");
			$get_offset = "(UNIX_TIMESTAMP(UTC_TIMESTAMP()) + ".$offset.") - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)";

			$this->db->where("FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + {$get_offset}, '%Y-%m-%d') <", date("Y-m-d"));
		}
	}

	public function search_notary($search_key="")
	{
		if($search_key!="" && $search_key!=" ")
		{
			$offset = $this->session->userdata("timezone_offset");

			$this->db->group_start();
				$this->db->or_like("b.name",$search_key,"both");
				$this->db->or_like("s.property_location",$search_key,"both");
				$this->db->or_like("CONCAT_WS(' ', s.street_address, s.unit_number, s.city, s.zipcode, s.state)",$search_key,"both");
				$this->db->or_like("u.name",$search_key,"both");
				$this->db->or_like("lc.name",$search_key,"both");
				$this->db->or_like("IFNULL(ns.label,'None')",$search_key,"both");
				$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%M %d, %Y')",$search_key,"both");
				$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%h:%i %p')",$search_key,"both");
			$this->db->group_end();
		}
	} 

	public function search_notaries($search_key="")
	{
		if($search_key!="" && $search_key!=" ")
		{
			$this->db->group_start();
				$this->db->or_like("u.name",$search_key,"both");
				$this->db->or_like("u.email",$search_key,"both");
				$this->db->or_like("u.address",$search_key,"both");
			$this->db->group_end();
		}
	} 

	public function search_key($search_key="")
	{
		if($search_key!="" && $search_key!=" ")
		{
			$offset = $this->session->userdata("timezone_offset");

			$this->db->group_start();
				$this->db->or_like("b.name",$search_key,"both");
				$this->db->or_like("s.property_location",$search_key,"both");
				$this->db->or_like("CONCAT_WS(' ', s.street_address, s.unit_number, s.city, s.zipcode, s.state)",$search_key,"both");
				$this->db->or_like("u.name",$search_key,"both");
				$this->db->or_like("lc.name",$search_key,"both");
				$this->db->or_like("IFNULL(ns.label,'None')",$search_key,"both");
				$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%M %d, %Y')",$search_key,"both");
				$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%h:%i %p')",$search_key,"both");
			$this->db->group_end();
		}
	}
}