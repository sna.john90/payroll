<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Flnotification Class.
 * 
 * @extends CI_Model
 */
class Flnotification extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function company_id($company_id)
	{
		$this->db->where("company_id", $company_id);
	}
	public function schedule_id($schedule_id)
	{
		$this->db->where("schedule_id", $schedule_id);
	}

	public function order_by_date_added_timezone($order='ASC')
	{
		$this->db->order_by("date_added_timezone", $order);
	}
	public function order_by_not_id($order='ASC')
	{
		$this->db->order_by("not.id", $order);
	}

	public function search($search_key)
	{
		if($search_key!="")
		{
			$this->db->group_start();
				$this->db->or_like("u.name",$search_key,"both");
				$this->db->or_like("not.description",$search_key,"both");
				$this->db->or_like("b.name",$search_key,"both");
				// $search_keys = explode(" ", $search_key);
				// foreach ($search_keys as $key => $value) {
				// 	$this->db->or_like("not.description",$value,"both");
				// }
			$this->db->group_end();
		}
	}
}