<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Flschedule Class.
 * 
 * @extends CI_Model
 */
class Flschedule extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function id($id)
	{
		$this->db->where("id",$id);
	}
	public function schedule_id($id)
	{
		$this->db->where("schedule_id",$id);
	}
	public function notary_id($id)
	{
		$this->db->where("notary_id",$id);
	}
	public function notary_status_id($id)
	{
		$this->db->where("notary_status_id",$id);
	}
	public function s_id($id)
	{
		$this->db->where("s.id",$id);
	}
	public function lender_id($id)
	{
		if($id!=0)
		{
			$this->db->where("lender_id",$id);
		}
	}
	public function wherein_lender_id($ids=array())
	{
		if(!empty($ids))
		{
			$this->db->where_in("lender_id",$ids);
		}
	}
	public function is_delivered($value)
	{
		$this->db->where("is_delivered",$value);
	}
	public function s_processor_id($id)
	{
		$this->db->where("s.processor_id",$id);
	}
	public function s_underwriter_id($id)
	{
		$this->db->where("s.underwriter_id",$id);
	}
	public function s_loan_officer_id($id)
	{
		$this->db->where("s.loan_officer_id",$id);
	}
	public function request_notary_service($val)
	{
		$this->db->where("request_notary_service",$val);
	}
	public function is_deleted($val)
	{
		$this->db->where("is_deleted",$val);
	}
	public function date_scheduled_today_up($val=1)
	{
		if($val==1)
		{
			$offset = $this->session->userdata("timezone_offset");
			$get_offset = "(UNIX_TIMESTAMP(UTC_TIMESTAMP()) + ".$offset.") - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)";

			if(is_underwriter())
			{
				$this->db->where("( DATE_FORMAT( DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ), '%Y-%m-%d') ) >=", date("Y-m-d"));
			}else
			{
				$this->db->where("FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + {$get_offset}, '%Y-%m-%d') >=", date("Y-m-d"));
			}
		}
	}
	public function date_search_from($date_string)
	{
		if($date_string!="")
		{
			$offset = $this->session->userdata("timezone_offset");
			$get_offset = "(UNIX_TIMESTAMP(UTC_TIMESTAMP()) + ".$offset.") - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)";

			if(is_underwriter())
			{
				$this->db->where("( DATE_FORMAT( DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ), '%Y-%m-%d') ) >=", date("Y-m-d",strtotime($date_string)));
			}else
			{
				$this->db->where("FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + {$get_offset}, '%Y-%m-%d') >=", date("Y-m-d",strtotime($date_string)));
			}
		}
	}
	public function date_search_to($date_string)
	{
		if($date_string!="")
		{
			$offset = $this->session->userdata("timezone_offset");
			$get_offset = "(UNIX_TIMESTAMP(UTC_TIMESTAMP()) + ".$offset.") - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)";

			if(is_underwriter())
			{
				$this->db->where("( DATE_FORMAT( DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ), '%Y-%m-%d') ) <=", date("Y-m-d",strtotime($date_string)));
			}else
			{
				$this->db->where("FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + {$get_offset}, '%Y-%m-%d') <=", date("Y-m-d",strtotime($date_string)));
			}
		}
	}
	public function tracking_number_not_null($val=1)
	{
		if($val==1)
		{
			$this->db->where("tracking_number !=", "");
		}
	}
	public function date_scheduled_timezone($val=1)
	{
		
	}
	public function date_scheduled_within_48_hours($val=1)
	{
		if($val==1)
		{
			$offset = $this->session->userdata("timezone_offset");
			$get_offset = "(UNIX_TIMESTAMP(UTC_TIMESTAMP()) + ".$offset.") - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)";

			$this->db->group_start();
				if(is_underwriter())
				{
					$this->db->or_group_start();
						$this->db->where("( DATE_FORMAT( DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:s'), INTERVAL 6 HOUR ), '%Y-%m-%d %H:%i') ) >=", date("Y-m-d H:i",strtotime("-48 hours",strtotime(date("Y-m-d H:i:s")))));
						$this->db->where("( DATE_FORMAT( DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:s'), INTERVAL 6 HOUR ), '%Y-%m-%d %H:%i') ) <=", date("Y-m-d H:i",strtotime(date("Y-m-d H:i:s"))));
					$this->db->group_end();

					$this->db->or_group_start();
						$this->db->where("( DATE_FORMAT( DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_added), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ), '%Y-%m-%d %H:%i') ) >=", date("Y-m-d H:i:",strtotime("-48 hours",strtotime(date("Y-m-d H:i:s")))));
						$this->db->where("( DATE_FORMAT( DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_added), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ), '%Y-%m-%d %H:%i') ) <=", date("Y-m-d H:i:",strtotime(date("Y-m-d H:i:s"))));
					$this->db->group_end();

					$this->db->or_group_start();
						$this->db->where("( DATE_FORMAT( DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.last_update), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ), '%Y-%m-%d %H:%i') ) >=", date("Y-m-d H:i:",strtotime("-48 hours",strtotime(date("Y-m-d H:i:s")))));
						$this->db->where("( DATE_FORMAT( DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.last_update), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ), '%Y-%m-%d %H:%i') ) <=", date("Y-m-d H:i:",strtotime(date("Y-m-d H:i:s"))));
						$this->db->where("UNIX_TIMESTAMP(s.last_update) !=", 0);
					$this->db->group_end();

				}else
				{
					$this->db->or_group_start();
						$this->db->where("FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + {$get_offset}, '%Y-%m-%d %H:%i') >=", date("Y-m-d H:i",strtotime("-48 hours",strtotime(date("Y-m-d H:i:s")))));
						$this->db->where("FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + {$get_offset}, '%Y-%m-%d %H:%i') <=", date("Y-m-d H:i",strtotime(date("Y-m-d H:i:s"))));
					$this->db->group_end();
					
					$this->db->or_group_start();
						$this->db->where("FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_added), '%Y-%m-%d %H:%i:%s') >=", date("Y-m-d H:i:s",strtotime("-48 hours",strtotime(date("Y-m-d H:i:s")))));
						$this->db->where("FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_added), '%Y-%m-%d %H:%i:%s') <=", date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s"))));
					$this->db->group_end();

					$this->db->or_group_start();
						$this->db->where("FROM_UNIXTIME( UNIX_TIMESTAMP(s.last_update), '%Y-%m-%d %H:%i:%s') >=", date("Y-m-d H:i:s",strtotime("-48 hours",strtotime(date("Y-m-d H:i:s")))));
						$this->db->where("FROM_UNIXTIME( UNIX_TIMESTAMP(s.last_update), '%Y-%m-%d %H:%i:%s') <=", date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s"))));
						$this->db->where("UNIX_TIMESTAMP(s.last_update) !=", 0);
					$this->db->group_end();
				}
			$this->db->group_end();
		}
	}
	public function date_scheduled_past($val=1)
	{
		if($val==1)
		{
			$offset = $this->session->userdata("timezone_offset");
			$get_offset = "(UNIX_TIMESTAMP(UTC_TIMESTAMP()) + ".$offset.") - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)";

			if(is_underwriter())
			{
				$this->db->where("( DATE_FORMAT( DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ), '%Y-%m-%d') ) <", date("Y-m-d"));
			}else
			{
				$this->db->where("FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + {$get_offset}, '%Y-%m-%d') <", date("Y-m-d"));
			}
		}
	}
	public function group_by_date_scheduled_timezone($val=1)
	{
		if($val==1)
		{
			$this->db->group_by("DATE_FORMAT(start, '%Y-%m-%d')");
		}
	}
	public function group_by_estimated_delivery_date($val=1)
	{
		if($val==1)
		{
			$this->db->group_by("DATE_FORMAT(start, '%Y-%m-%d')");
		}
	}
	public function order_by_date_scheduled_timezone($order='ASC')
	{
		$this->db->order_by("start", $order);
	}
	public function order_by_estimated_delivery_date($order='ASC')
	{
		$this->db->order_by("start", $order);
	}
	public function date_scheduled_timezone_within($params=array())
	{
		if(!empty($params))
		{
			$offset = $this->session->userdata("timezone_offset");
        	$get_offset = "(UNIX_TIMESTAMP(UTC_TIMESTAMP()) + ".$offset.") - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)";

			$this->db->group_start();

				if(is_underwriter())
				{
					$this->db->where("( DATE_FORMAT( DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ), '%Y-%m-%d') ) >=", $params['start']);
					$this->db->where("( DATE_FORMAT( DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ), '%Y-%m-%d') ) <=", $params['end']);
				}else
				{
					$this->db->where("( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + {$get_offset}, '%Y-%m-%d') ) >=", $params['start']);
					$this->db->where("( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + {$get_offset}, '%Y-%m-%d') ) <=", $params['end']);
				}
			$this->db->group_end();
		}
	}
	public function date_estimated_delivery_within($params=array())
	{
		if(!empty($params))
		{
			$this->db->group_start();

				$this->db->where("( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + {$get_offset}, '%Y-%m-%d') ) >=", $params['start']);
				$this->db->where("( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + {$get_offset}, '%Y-%m-%d') ) <=", $params['end']);
				
			$this->db->group_end();
		}
	}
	public function estimated_delivery_date_within($params=array())
	{
		if(!empty($params))
		{
			$this->db->group_start();

				$this->db->where(" DATE_FORMAT(sn.estimated_delivery_date, '%Y-%m-%d') >=", $params['start']);
				$this->db->where(" DATE_FORMAT(sn.estimated_delivery_date, '%Y-%m-%d') <=", $params['end']);
				
			$this->db->group_end();
		}
	}
	public function date_scheduled_date_only($date)
	{
		$offset = $this->session->userdata("timezone_offset");
		$get_offset = "(UNIX_TIMESTAMP(UTC_TIMESTAMP()) + ".$offset.") - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)";
		
		if(is_underwriter())
		{
			$this->db->where("( DATE_FORMAT( DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ), '%Y-%m-%d') ) =", $date);
		}else
		{
			$this->db->where("( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d') ) =", $date);
		}
	}
	public function estimated_delivery_date_only($date)
	{
		$this->db->where(" DATE_FORMAT(sn.estimated_delivery_date, '%Y-%m-%d') =", $date);
	}
	public function has_download($val=1)
	{
		if($val==1)
		{
			$this->db->where("UNIX_TIMESTAMP(last_download) !=", 0);
		}
	}

	public function is_accepted_and_waiting($val=1)
	{
		if($val==1)
		{
			$this->db->group_start();
				$this->db->where("is_accepted", 0);
				$this->db->or_where("is_accepted", 1);
			$this->db->group_end();
		}
	}

	public function is_accepted($is_accepted)
	{
		$this->db->where("is_accepted", $is_accepted);
	}
	
	public function order_by_date_scheduled($order='ASC')
	{
		$this->db->order_by("date_scheduled_timezone",strtoupper($order));
	}
	public function order_by_date_added($order='ASC')
	{
		$this->db->order_by("date_added",strtoupper($order));
	}
	public function order_by_borrower_name($order='ASC')
	{
		$this->db->order_by("b.name",strtoupper($order));
	}
	public function order_by_property_state($order='ASC')
	{
		$this->db->order_by("property_location",strtoupper($order));
	}
	public function order_by_request_notary_service($order='ASC')
	{
		$this->db->order_by("request_notary_service",strtoupper($order));
	}
	public function order_by_loan_number($order='ASC')
	{
		$this->db->order_by("loan_number",strtoupper($order));
	}
	public function order_by_title_order_number($order='ASC')
	{
		$this->db->order_by("title_order_number",strtoupper($order));
	}
	public function order_by_processor($order='ASC')
	{
		$this->db->order_by("u.name",strtoupper($order));
	}
	public function order_by_underwriter($order='ASC')
	{
		$this->db->order_by("u2.name",strtoupper($order));
	}
	public function order_by_loan_officer($order='ASC')
	{
		$this->db->order_by("u3.name",strtoupper($order));
	}
	public function order_by_admin_status($order='ASC')
	{
		$this->db->order_by("as.status",strtoupper($order));
	}
	public function order_by_admin_status_importance($order='ASC')
	{
		$this->db->order_by("as.importance",strtoupper($order));
	}
	public function order_by_schedule_status($order='ASC')
	{
		$this->db->order_by("scs.name",strtoupper($order));
	}
	public function order_by_time($order='ASC')
	{
		$this->db->order_by("date_scheduled_timezone",strtoupper($order));
	}
	public function order_by_folder_name($order='ASC')
	{
		$this->db->order_by("f.name",strtoupper($order));
	}
	public function order_by_notary_name($order='ASC')
	{
		$this->db->order_by("u.name",strtoupper($order));
	}
	public function order_by_notary($order='ASC')
	{
		$this->db->order_by("u3.name",strtoupper($order));
	}
	public function search_schedule($search_key="")
	{
		if($search_key!="" && $search_key!=" ")
		{
			$offset = $this->session->userdata("timezone_offset");

			$this->db->group_start();
				$this->db->or_like("b.name",$search_key,"both");
				$this->db->or_like("property_location",$search_key,"both");
				$this->db->or_like("title_order_number",$search_key,"both");
				$this->db->or_like("loan_number",$search_key,"both");
				$this->db->or_like("IFNULL(f.name,'Select Folder')",$search_key,"both");
				$this->db->or_like("IFNULL(as.status,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(as.label,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(scs.name,'Select Status')",$search_key,"both");
				$this->db->or_like("IF(s.request_notary_service=1,'NotaryCloud','Notary Assurance')",$search_key,"both");

				$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%M %d, %Y')",$search_key,"both");
				$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%h:%i %p')",$search_key,"both");
			$this->db->group_end();
		}
	} 
	public function search_notary_schedule($search_key="")
	{
		if($search_key!="" && $search_key!=" ")
		{
			$offset = $this->session->userdata("timezone_offset");

			$this->db->group_start();
				$this->db->or_like("b.name",$search_key,"both");
				$this->db->or_like("property_location",$search_key,"both");
				$this->db->or_like("title_order_number",$search_key,"both");
				$this->db->or_like("loan_number",$search_key,"both");
				$this->db->or_like("IFNULL(f.name,'Select Folder')",$search_key,"both");
				$this->db->or_like("IFNULL(as.status,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(as.label,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(scs.name,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(u.name,'None')",$search_key,"both");
				$this->db->or_like("IF(s.request_notary_service=1,'NotaryCloud','Notary Assurance')",$search_key,"both");

				$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%M %d, %Y')",$search_key,"both");
				$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%h:%i %p')",$search_key,"both");
			$this->db->group_end();
		}
	} 
	public function search_archive_schedule($search_key="")
	{
		if($search_key!="" && $search_key!=" ")
		{
			$offset = $this->session->userdata("timezone_offset");

			$this->db->group_start();
				$this->db->or_like("b.name",$search_key,"both");
				$this->db->or_like("property_location",$search_key,"both");
				$this->db->or_like("title_order_number",$search_key,"both");
				$this->db->or_like("loan_number",$search_key,"both");
				$this->db->or_like("IFNULL(f.name,'None')",$search_key,"both");
				$this->db->or_like("IFNULL(as.status,'No status set')",$search_key,"both");
				$this->db->or_like("IFNULL(as.label,'No status set')",$search_key,"both");
				$this->db->or_like("IFNULL(u.name,'None')",$search_key,"both");
				$this->db->or_like("IF(s.request_notary_service=1,'NotaryCloud','Notary Assurance')",$search_key,"both");

				$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%M %d, %Y')",$search_key,"both");
				$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%h:%i %p')",$search_key,"both");
			$this->db->group_end();	
		}
	} 
	public function search_not_schedule($search_key="")
	{
		if($search_key!="" && $search_key!=" ")
		{
			$offset = $this->session->userdata("timezone_offset");

			$this->db->group_start();
				$this->db->or_like("b.name",$search_key,"both");
				$this->db->or_like("property_location",$search_key,"both");
				$this->db->or_like("title_order_number",$search_key,"both");
				$this->db->or_like("loan_number",$search_key,"both");
				$this->db->or_like("IFNULL(f.name,'Select Folder')",$search_key,"both");
				$this->db->or_like("IFNULL(as.status,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(as.label,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(scs.name,'Select Status')",$search_key,"both");
				$this->db->or_like("IF(s.request_notary_service=1,'NotaryCloud','Notary Assurance')",$search_key,"both");

				$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%M %d, %Y')",$search_key,"both");
				$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%h:%i %p')",$search_key,"both");
			$this->db->group_end();
		}
	} 
	public function search_not_archive_schedule($search_key="")
	{
		if($search_key!="" && $search_key!=" ")
		{
			$offset = $this->session->userdata("timezone_offset");

			$this->db->group_start();
				$this->db->or_like("b.name",$search_key,"both");
				$this->db->or_like("property_location",$search_key,"both");
				$this->db->or_like("title_order_number",$search_key,"both");
				$this->db->or_like("loan_number",$search_key,"both");
				$this->db->or_like("IFNULL(f.name,'None')",$search_key,"both");
				$this->db->or_like("IFNULL(as.status,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(as.label,'Select Status')",$search_key,"both");
				$this->db->or_like("IF(s.request_notary_service=1,'NotaryCloud','Notary Assurance')",$search_key,"both");

				$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%M %d, %Y')",$search_key,"both");
				$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%h:%i %p')",$search_key,"both");
			$this->db->group_end();
		}
	} 

	public function search_loan_schedule($search_key="")
	{
		if($search_key!="" && $search_key!=" ")
		{
			$offset = $this->session->userdata("timezone_offset");

			$this->db->group_start();
				$this->db->or_like("b.name",$search_key,"both");
				$this->db->or_like("property_location",$search_key,"both");
				$this->db->or_like("title_order_number",$search_key,"both");
				$this->db->or_like("loan_number",$search_key,"both");
				$this->db->or_like("IFNULL(u.name,'None')",$search_key,"both");
				$this->db->or_like("IFNULL(u2.name,'None')",$search_key,"both");
				$this->db->or_like("IFNULL(u3.name,'None')",$search_key,"both");
				$this->db->or_like("IFNULL(f.name,'None')",$search_key,"both");
				$this->db->or_like("IFNULL(f.name,'Select Folder')",$search_key,"both");
				$this->db->or_like("IFNULL(as.status,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(as.label,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(as.status,'No status set')",$search_key,"both");
				$this->db->or_like("IFNULL(as.label,'No status set')",$search_key,"both");
				$this->db->or_like("IF(s.request_notary_service=1,'NotaryCloud','Notary Assurance')",$search_key,"both");

				if(is_underwriter())
				{ 
					$this->db->or_like("DATE_FORMAT(DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ),'%M %d, %Y')",$search_key,"both");
					$this->db->or_like("DATE_FORMAT(DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ),'%h:%i %p')",$search_key,"both");
				}else 
				{
					$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%M %d, %Y')",$search_key,"both");
					$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%h:%i %p')",$search_key,"both");
				}
			$this->db->group_end();
		}
	} 

	public function search_key($search_key="")
	{
		if(isset($search_key) && !empty($search_key))
		{
			$offset = $this->session->userdata("timezone_offset");

			$this->db->group_start();
				$this->db->or_like("b.name",$search_key,"both");
				$this->db->or_like("property_location",$search_key,"both");
				$this->db->or_like("title_order_number",$search_key,"both");
				$this->db->or_like("loan_number",$search_key,"both");
				$this->db->or_like("IFNULL(f.name,'Select Folder')",$search_key,"both");
				$this->db->or_like("IFNULL(f.name,'None')",$search_key,"both");
				$this->db->or_like("IFNULL(as.status,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(as.label,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(as.status,'None')",$search_key,"both");
				$this->db->or_like("IFNULL(as.label,'None')",$search_key,"both");
				$this->db->or_like("IFNULL(scs.name,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(scs.name,'None')",$search_key,"both");
				$this->db->or_like("IFNULL(u.name,'None')",$search_key,"both");
				$this->db->or_like("IF(s.request_notary_service=1,'NotaryCloud','Notary Assurance')",$search_key,"both");

				if(is_underwriter())
				{ 
					$this->db->or_like("DATE_FORMAT(DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ),'%M %d, %Y')",$search_key,"both");
					$this->db->or_like("DATE_FORMAT(DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ),'%h:%i %p')",$search_key,"both");

				}else 
				{
					$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%M %d, %Y')",$search_key,"both");
					$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%h:%i %p')",$search_key,"both");
				}
			$this->db->group_end();
		}
	} 

	public function search_admin_track_key($search_key="")
	{
		if(isset($search_key) && !empty($search_key))
		{
			$offset = $this->session->userdata("timezone_offset");

			$this->db->group_start();
				$this->db->or_like("b.name",$search_key,"both");
				$this->db->or_like("property_location",$search_key,"both");
				$this->db->or_like("title_order_number",$search_key,"both");
				$this->db->or_like("loan_number",$search_key,"both");
				$this->db->or_like("IFNULL(f.name,'Select Folder')",$search_key,"both");
				$this->db->or_like("IFNULL(f.name,'None')",$search_key,"both");
				$this->db->or_like("IFNULL(as.status,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(as.label,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(as.status,'None')",$search_key,"both");
				$this->db->or_like("IFNULL(as.label,'None')",$search_key,"both");
				$this->db->or_like("IFNULL(scs.name,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(scs.name,'None')",$search_key,"both");
				$this->db->or_like("IFNULL(u.name,'None')",$search_key,"both");
				$this->db->or_like("IF(s.request_notary_service=1,'NotaryCloud','Notary Assurance')",$search_key,"both");

				$this->db->or_like("DATE_FORMAT(sn.estimated_delivery_date,'%h:%i %p')",$search_key,"both");
			$this->db->group_end();
		}
	} 

	public function search_admin_key($search_key="")
	{
		if(isset($search_key) && !empty($search_key))
		{
			$offset = $this->session->userdata("timezone_offset");

			$this->db->group_start();
				$this->db->or_like("b.name",$search_key,"both");
				$this->db->or_like("property_location",$search_key,"both");
				$this->db->or_like("title_order_number",$search_key,"both");
				$this->db->or_like("loan_number",$search_key,"both");
				$this->db->or_like("u.name",$search_key,"both");
				$this->db->or_like("u2.name",$search_key,"both");
				$this->db->or_like("u3.name",$search_key,"both");
				$this->db->or_like("IFNULL(f.name,'None')",$search_key,"both");
				$this->db->or_like("IFNULL(as.status,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(as.label,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(as.status,'No status set')",$search_key,"both");
				$this->db->or_like("IFNULL(as.label,'No status set')",$search_key,"both");
				$this->db->or_like("IF(s.request_notary_service=1,'NotaryCloud','Notary Assurance')",$search_key,"both");

				$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%M %d, %Y')",$search_key,"both");
				$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%h:%i %p')",$search_key,"both");
			$this->db->group_end();
		}
	} 

	public function search_calendar_key($search_key="")
	{
		if(isset($search_key) && !empty($search_key))
		{
			$offset = $this->session->userdata("timezone_offset");

			$this->db->group_start();
				$this->db->or_like("b.name",$search_key,"both");
				$this->db->or_like("property_location",$search_key,"both");
				$this->db->or_like("title_order_number",$search_key,"both");
				$this->db->or_like("loan_number",$search_key,"both");
				$this->db->or_like("u.name",$search_key,"both");
				$this->db->or_like("u2.name",$search_key,"both");
				$this->db->or_like("u3.name",$search_key,"both");
				$this->db->or_like("IFNULL(f.name,'None')",$search_key,"both");
				$this->db->or_like("IFNULL(as.status,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(as.label,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(as.status,'No status set')",$search_key,"both");
				$this->db->or_like("IFNULL(as.label,'No status set')",$search_key,"both");
				$this->db->or_like("IF(s.request_notary_service=1,'NotaryCloud','Notary Assurance')",$search_key,"both");

				if(is_underwriter())
				{ 
					$this->db->or_like("DATE_FORMAT(DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ),'%M %d, %Y')",$search_key,"both");
					$this->db->or_like("DATE_FORMAT(DATE_SUB( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s'), INTERVAL 6 HOUR ),'%h:%i %p')",$search_key,"both");

				}else 
				{
					$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%M %d, %Y')",$search_key,"both");
					$this->db->or_like("DATE_FORMAT(( FROM_UNIXTIME( UNIX_TIMESTAMP(s.date_scheduled) + ((UNIX_TIMESTAMP(UTC_TIMESTAMP()) + {$offset}) - (UNIX_TIMESTAMP(UTC_TIMESTAMP()) + tz.gmt_offset)), '%Y-%m-%d %H:%i:%s') ),'%h:%i %p')",$search_key,"both");
				}
			$this->db->group_end();
		}
	} 

	public function search_track_calendar_key($search_key="")
	{
		if(isset($search_key) && !empty($search_key))
		{
			$this->db->group_start();
				$this->db->or_like("b.name",$search_key,"both");
				$this->db->or_like("property_location",$search_key,"both");
				$this->db->or_like("title_order_number",$search_key,"both");
				$this->db->or_like("loan_number",$search_key,"both");
				$this->db->or_like("u.name",$search_key,"both");
				$this->db->or_like("u2.name",$search_key,"both");
				$this->db->or_like("u3.name",$search_key,"both");
				$this->db->or_like("IFNULL(f.name,'None')",$search_key,"both");
				$this->db->or_like("IFNULL(as.status,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(as.label,'Select Status')",$search_key,"both");
				$this->db->or_like("IFNULL(as.status,'No status set')",$search_key,"both");
				$this->db->or_like("IFNULL(as.label,'No status set')",$search_key,"both");
				$this->db->or_like("IF(s.request_notary_service=1,'NotaryCloud','Notary Assurance')",$search_key,"both");

				$this->db->or_like("DATE_FORMAT(sn.estimated_delivery_date,'%h:%i %p')",$search_key,"both");
			$this->db->group_end();
		}
	} 

	public function not_scheduled($val=1)
	{
		if($val==1)
		{
			$this->db->group_start();

				$this->db->group_start();
					$this->db->where("sn.id",NULL);
					$this->db->where("request_notary_service",1);
				$this->db->group_end();

				$this->db->or_group_start();
					$this->db->where("sn.id !=",NULL);
					$this->db->where("request_notary_service",1);
					$this->db->where("is_accepted",0);
					$this->db->or_where("is_accepted",2);
				$this->db->group_end();

			$this->db->group_end();
		}
	}
	public function scheduled_notary_cloud($val=1)
	{
		if($val==1)
		{
			$this->db->group_start();
				$this->db->where("sn.id !=",NULL);
				$this->db->where("request_notary_service",1);
				$this->db->group_start();
					$this->db->where("is_accepted",1);
				$this->db->group_end();
			$this->db->group_end();
		}
	}
	public function scheduled_notary_assurance($val=1)
	{
		if($val==1)
		{
			$this->db->group_start();
				$this->db->where("request_notary_service",0);
			$this->db->group_end();
		}
	}
	public function scheduled_completed($val=1)
	{
		if($val==1)
		{
			$this->db->where("sn.notary_status_id",3);
			$this->db->where("request_notary_service",1);
		}
	}
}