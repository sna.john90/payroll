<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Fluser Class.
 * 
 * @extends CI_Model
 */
class Fluser extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function id($id)
	{
		$this->db->where("id",$id);
	}
	public function name($name)
	{
		$this->db->where("name",$name);
	}
	public function u_id($id)
	{
		$this->db->where("u.id",$id);
	}
	public function user_id($id)
	{
		$this->db->where("user_id",$id);
	}
	public function not_wherein_user_id($user_ids=array())
	{
		if(!empty($user_ids))
		{
			$this->db->group_start();
				foreach ($user_ids as $key => $value) {
					$this->db->where("m.user_id != ",$value['user_id']);
				}
			$this->db->group_end();
		}
	}
	public function email($email)
	{
		$this->db->where("email",$email);
	}
	public function nf_user_id($id)
	{
		$this->db->group_start();
			$this->db->where("nf.user_id",$id);
			$this->db->or_where("nf.user_id",NULL);
		$this->db->group_end();
	}
	public function user_group_id($id)
	{
		$this->db->where("ug.id",$id);
	}
	public function wherein_user_group_id($ids=array())
	{
		$this->db->where_in("ug.id",$ids);
	}
	public function company_id($id)
	{
		$this->db->where("u.company_id",$id);
	}
	public function not_user_id($val=1)
	{
		if($val == 1)
		{
			//$this->db->where("u.id !=",user_id());
		}
	}
	public function username($username)
	{
		$this->db->where("username",$username);
	}
	public function status($status)
	{
		$this->db->where("status",$status);
	}
	public function is_active($status)
	{
		$this->db->group_start();
			$this->db->where("status", 1);
			$this->db->or_where("status", 2);
		$this->db->group_end();
	}
	public function order_by_username($order='ASC')
	{
		$this->db->order_by("username",strtoupper($order));
	}
	public function order_by_name($order='ASC')
	{
		$this->db->order_by("u.firstname",strtoupper($order));
	}
	public function order_by_email($order='ASC')
	{
		$this->db->order_by("email",strtoupper($order));
	}
	public function order_by_user_group($order='ASC')
	{
		$this->db->order_by("ug.name",strtoupper($order));
	}
	public function order_by_date_added($order='ASC')
	{
		$this->db->order_by("u.created_at",strtoupper($order));
	}
	public function order_by_status($order='ASC')
	{
		$this->db->order_by("u.status",strtoupper($order));
	}
	public function order_by_address($order='ASC')
	{
		$this->db->order_by("u.address",strtoupper($order));
	}
	public function order_by_reviews($order='ASC')
	{
		
	}
	public function order_by_favorite($order='ASC')
	{
		$this->db->order_by("nf.id",strtoupper($order));
	}
	public function order_by_distance($arr=array())
	{
		if($arr['has_distance'])
		{
			$this->db->order_by("distance",strtoupper($arr['order']));
		}
	}
	public function order_by_empid($order="ASC")
	{
		$this->db->order_by("u.emp_id",strtoupper($order));
	}
	public function order_by_department($order="ASC")
	{
		$this->db->order_by("dept.id",strtoupper($order));
	}
	public function order_by_designation($order="ASC")
	{
		$this->db->order_by("empd.designation",strtoupper($order));
	}
	public function order_by_date_hired($order="ASC")
	{
		$this->db->order_by("empd.date_hired",strtoupper($order));
	}
	public function order_by_rate($order="ASC")
	{
		$this->db->order_by("empd.basic_pay",strtoupper($order));
	}
	public function order_by_phone($order="ASC")
	{
		$this->db->order_by("u.phone",strtoupper($order));
	}

	public function search_user($search_key="")
	{
		if($search_key!="" && $search_key!=" ")
		{
			$this->db->group_start();
				$this->db->or_like("u.name",$search_key,"both");
				$this->db->or_like("u.email",$search_key,"both");
				$this->db->or_like("ug.description",$search_key,"both");
			$this->db->group_end();
		}
	} 
	public function search_name($search_key="")
	{
		if($search_key!="" && $search_key!=" ")
		{
			$this->db->group_start();
			$search = explode(" ",$search_key);
			foreach ($search as $key => $value) {
				$this->db->or_like("u.name",$value,"both");
			}
			$this->db->group_end();
		}
	} 
	public function search_name_and_email($search_key="")
	{
		if($search_key!="" && $search_key!=" ")
		{
			$this->db->group_start();
				$this->db->or_like("u.name",$search_key,"both");
				$this->db->or_like("u.email",$search_key,"both");
			$this->db->group_end();
		}
	} 
	public function search_notary($search_key="")
	{
		if($search_key!="" && $search_key!=" ")
		{
			$this->db->group_start();
			$search = explode(" ",$search_key);
			foreach ($search as $key => $value) {
				$this->db->or_like("u.name",$value,"both");
				$this->db->or_like("email",$value,"both");
				$this->db->or_like("u.address",$value,"both");
			}
			$this->db->group_end();
		}
	} 
	public function search_notary2($search_key="")
	{
		if($search_key!="" && $search_key!=" ")
		{
			$this->db->group_start();
			$search = explode(" ",$search_key);
			foreach ($search as $key => $value) {
				$this->db->or_like("u.name",$value,"both");
				$this->db->or_like("u.address",$value,"both");
			}
			$this->db->group_end();
		}
	} 
	/*
		get the nearby users base on the given location and what distance
	 	location = array(
					  lat =>,
					  long => ,
					  distance = 30 //in miles
					)
	*/
	public function by_distance($location=array())
	{
		if(!empty($location))
		{
			if($location['lat']!=="" && $location['long']!=="")
			{
				//$this->db->select("( 3956 * 2 * ASIN(SQRT(POWER(SIN((".$location['lat']."- u.latitude) * pi()/180 / 2), 2) +COS(".$location['lat']." * pi()/180) *COS(u.latitude * pi()/180) *POWER(SIN((".$location['long']." - u.longitude) * pi()/180 / 2), 2) )) ) as distance");
				$this->db->select("( 3959 * acos( cos( radians(".$location['lat'].") ) * cos( radians( u.latitude ) ) * cos( radians( u.longitude ) - radians(".$location['long'].") ) + sin( radians(".$location['lat'].") ) * sin( radians( u.latitude ) ) ) ) as distance");
				if($location['distance']!=="")
				{
					$this->db->having("distance <={$location['distance']}");
				}
			}
		}
	}

	public function undisplay_comp_admin()
	{
		$this->db->where("ur.user_group_id !=",2);
	}
}