    <footer>
       <div class="container-fluid"> 
       		<div class="row font-xs p10">
				<div class="col-sm-6">
				© <?php echo date('Y'); ?> SmartPayroll. 
				</div>
				<div class="col-sm-6 text-right">
					All Rights Reserved.
				</div>
			</div>
       </div>      
    </footer><!-- END: footer --> 
    <!-- Include all js files --> 
    <script src="{asset_url}lib/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="{asset_url}lib/bootstrap-datepicker/css/bootstrap-datetimepicker.min.css" />

    <script src="{asset_url}lib/jquery-input-mask/jquery.inputmask.bundle.min.js"></script> 

    <script src="{asset_url}lib/datatables/js/jquery.dataTables.min.js"></script>
    <script src="{asset_url}lib/datatables/js/dataTables.bootstrap.min.js"></script>
    <script src="{asset_url}lib/bootstrap-select/js/bootstrap-select.min.js"></script>
    <script src="{asset_url}lib/jquery.form.min.js"></script>
    <script src="{asset_url}lib/jquery-confirm/jquery.confirm.min.js"></script> 

    <link rel="stylesheet" type="text/css" href="{asset_url}lib/preloader/preloader.css" />
    <script src="{asset_url}lib/preloader/preloader.js"></script>

    

    <!-- include other js files -->
    <script src="{asset_url}lib/common.js"></script>
    <script src="{asset_url}lib/common2.js?_=1"></script>

