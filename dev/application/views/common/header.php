<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SmartPayroll</title>

    <!-- CSS -->
    <link href="{asset_url}lib/fonts/Source-Sans-Pro/fonts.css" rel="stylesheet" >
    <link href="{asset_url}lib/bootstrap/css/bootstrap.min.css" rel="stylesheet"  media="all">
    <link href="{asset_url}lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="{asset_url}lib/datatables/css/dataTables.bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <link href="{asset_url}lib/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" media="all">
    <link href="{css_url}bootstrapjd.css" rel="stylesheet" media="all">
    <link href="{css_url}style.css?_=333" rel="stylesheet" media="all">
     <link href="{css_url}dev-style.css" rel="stylesheet" media="all">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

     <!-- load impt js files -->
    <script src="{asset_url}lib/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{asset_url}lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="{asset_url}lib/moment.js"></script>
    <script src="{asset_url}lib/moment-timezone-with-data.js"></script>

    <script type="text/javascript">
        var base_url = "{base_url}";
        var csrf_token = "{csrf_token}";
        var csrf_hash = "{csrf_hash}";
    </script>
  </head>
  <body> 
    