<!DOCTYPE html>
<html lang="en">
	{header}
	{styles}
	<body class="">
		{navbar}
		<div id="main"> 
			<div id="side-dock" class="animeFast"></div><!-- END: #side-dock -->
		    <div id="page-content" class="pt30 animeFast">
		     	{content}
		    </div><!-- END: #page-content --> 
			
		</div><!-- END: #main -->
		{footer}
		{scripts}
		<div class="for-printing"></div>
		<style type="text/css">
			.for-printing{
				display: none;
			}
			@media print {
				.for-printing{
					display: block;
				}
				#main{
					display: none;
				}
				.navbar{
					display: none;
				}
			}
		</style>
	</body>
</html>