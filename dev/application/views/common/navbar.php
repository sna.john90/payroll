<nav class="navbar navbar-nox navbar-fixed-top">
      <div class="navbar-wrap container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">MENU</span>
            <span class="fa fa-bars"></span>
          </button>
          <img src="<?php echo company_picture();?>" class="pull-left img-responsive company-pic"   />
          <?php if(is_super_admin()): ?>
                  <a class="navbar-brand" href="#">SmartPayroll</a>
          <?php else: ?> 
                  <a class="navbar-brand" href="#"><?php echo get_company_name(); ?></a>
          <?php endif;?>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="searchbox mt10">
              <form class="form">
                  <div class="input-group input-group-sm">
                    <input type="text" class="search-input animeFast" placeholder="Search...">
                    <a href="#" class=""><i class="fa fa-search"></i></a>
                  </div>
              </form>
            </div>
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="{base_url}">Home</a></li> 
            
              <?php if(is_super_admin()): ?>
                      <li> 
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Management</a>
                         <ul class="dropdown-menu">
                            <li><a href="{base_url}admin/company">Companies</a></li> 
                            <li><a href="{base_url}admin/users" >Users</a></li> 
                          </ul>
                      </li> 
              <?php elseif(is_admin_alone()): ?>
                      <li> 
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Payroll</a>
                         <ul class="dropdown-menu">
                            <li><a href="{base_url}payroll">Pay Runs</a></li> 
                            <li><a href="{base_url}admin/employees" >Employees</a></li> 
                            <!-- <li role="separator" class="divider"></li> -->
                            <!-- <li><a href="{base_url}payroll/settings"><i class="fa fa-cog"></i> Payroll Settings</a></li>   -->
                          </ul>
                      </li>
                      <li> 
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Expenses</a>
                         <ul class="dropdown-menu">
                            <li><a href="{base_url}expenses">All Expenses</a></li>
                            <!-- <li><a href="javascript:void(0)" class="add-expense-btn">Enter New Expense</a></li>  -->
                            <li role="separator" class="divider"></li>
                            <li><a href="{base_url}expenses/personnel">Salaries and Wages</a></li>
                            <li><a href="{base_url}expenses/monthly">Monthly Summary</a></li>  
                            <li><a href="{base_url}expenses/operating">Operating Summary</a></li>
                            <li role="separator" class="divider"></li>
                            <li class="dropdown-header">SETTINGS</li>
                            <li><a href="{base_url}expenses/categories">Categories</a></li>
                          </ul>
                      </li>
                       <li> 
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Management</a>
                         <ul class="dropdown-menu">    
                              <li class="dropdown-header">SETTINGS</li>
                             <li><a href="{base_url}settings/department" >Department</a></li> 
                             <li><a href="javascript:;" class="edit-company-btn" data-id="{gcompany_id_encrypted}" >Company</a></li>  
                             <li><a href="{base_url}settings/payroll" >Payroll</a></li>  
                          </ul>
                      </li>
                     
              <?php elseif(is_employee()): ?>
              <?php elseif(is_hr()): ?>
              <?php elseif(is_accountant()): ?>
              <?php endif;?>
              <!-- <ul class="dropdown-menu">
                <li class="dropdown-header">FRONTEND PAGES</li>
                <li><a href="#" class="hasPage" data-target="pa
                <li class="dropdown-header">ADMIN PAGES</li><a hrefge-profile.php">Profile</a></li>
                <li><a href="#" class="hasPage" data-target="page-payslip.php">Payslip</a></li>
                <li><a href="#" class="hasPage" data-target="page-login.php">Login Page</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#" class="hasPage" data-target="page-payroll-summary.php">Payroll Summary</a></li>
                <li><a href="#" class="hasPage" data-target="page-expense-personnel.php">Expense Personnel</a></li>
                <li><a href="#" class="hasPage" data-target="page-expense-operating.php">Expense Operating</a></li>
                <li><a href="#" class="hasPage" data-target="page-expense-summary.php">Expense Summary</a></li>
                <li><a href="#" class="hasPage" data-target="page-expense-summary-monthly.php">Expense Summary (Monthly)</a></li>
                <li><a href="#" class="hasPage" data-target="page-employee-list.php">Employee List</a></li>
                <li><a href="#" class="hasPage" data-target="page-salary.php">Employee Salary</a></li>
                <li><a href="#" data-toggle="modal" data-target="#modalAddEmployee">Add Employee</a></li>
                <li><a href="#" class="hasPage" data-target="page-maintainables.php">Maintainables</a></li>
              </ul> -->
            </li>
            <li class="profile-nav dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <span class="avatar-nav-img"><img class="img-responsive" src="<?php echo profile_picture();?>"></span> 
              <span class="avatar-nav-label"><?php echo get_name(); ?></span> <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li><a href="{base_url}profile"><i class="fa fa-user"></i> Profile</a></li> 
                <li role="separator" class="divider"></li>
                <li><a href="{base_url}auth/logout"><i class="fa fa-sign-out"></i> Logout</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav><!--END: .navbar -->