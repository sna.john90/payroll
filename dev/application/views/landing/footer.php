  </div> <!-- pagecontent -->

  </div><!-- END: #main -->
    <footer>
       <div class="container-fluid"> 
          <div class="row font-xs p10">
        <div class="col-sm-6">
        © <?php echo date('Y'); ?> SmartPayroll. 
        </div>
        <div class="col-sm-6 text-right">
          All Rights Reserved.
        </div>
      </div>
       </div>      
    </footer><!-- END: footer -->
<?php 

  //include('includes/inc-modal.php'); 

?> 
    
    <!-- Include all js files -->
    <script src="{asset_url}lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="{asset_url}lib/datatables/js/jquery.dataTables.min.js"></script>
    <script src="{asset_url}lib/datatables/js/dataTables.bootstrap.min.js"></script>
    <script src="{asset_url}lib/jquery.form.min.js"></script>
    <!-- include other js files -->
    <script src="{asset_url}lib/common.js"></script>
  </body>
</html>