<!DOCTYPE html>
<html lang="en">
	{header}
	{styles}
	<body class=""> 
		{content}
		{footer}
		{scripts}
	</body>
</html>
