<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="javascript:void(0)">
        <span class="brand-text">NOTARY<span class="text-success">CLOUD</span></span>
        <img class="img-responsive hidden" src="assets/logo-text.png">
      </a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#contact" class="">Contact Us</a></li>
        <li><a href="" class="" data-toggle="modal" data-target="#loginModal">Login</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>