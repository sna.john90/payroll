  <div class="container">
    <div class="panel panel-nox fullH">
    <div class="panel-heading clearfix">
        <div class="filter-option pull-right">
              <ul class="list-inline">
                   <li>
                      <a href="javascript:void(0)" id="add-company-btn" class="btn btn-success btn-sm"><i class="fa fa-plus-circle"></i> Add New</a>
                  </li>
              </ul>
          </div>
         <h4 class="panel-title pt10"><i class="fa fa-list-alt"></i> Companies</h4>
    </div>
    <div class="panel-body"> 
        <div class="table-responsive" data-column-defs='[{"sortable": false, "targets": [3,4]}]'> 
          <table id="companyTable" class="table table-hover"> 
            <thead class="threadcolor"> 
              <tr> 
                <th>Company Name</th>
                <th>Address</th>
                <th>Email</th>
                 <th>Website</th>
                <th>Date Added</th>
                <th>Action</th>
              </tr> 
            </thead> 
            <tbody></tbody> 
          </table>
        </div> 
          
    </div>
  </div>
</div>

 <script type="text/javascript">
      $(document).ready(function() {

      //data tables
      var smartDataTable  = $('.dataTable').DataTable({
        scrollCollapse: true,
        //scrollY: '90vh',
        "language": {
          "lengthMenu": "SHOW _MENU_",
          "paginate": {
            "previous": "&lt;",
            "next": "&gt;"
          }
       },
        dom:"<'row data-table-header'<'col-xs-8'f><'col-xs-4'l>>" +
          "<'row'<'col-sm-12'tr>>" +
          "<'row data-table-footer'<'col-sm-4 font-xs'i><'col-sm-8'p>>"
       });
       /* dom:"<'row'<'col-sm-12'tr>>" +
          "<'row data-table-footer'<'col-sm-4 font-xs'i><'col-sm-8'p>>"
       });*/
      
       
    });
</script>

{crop_modal}