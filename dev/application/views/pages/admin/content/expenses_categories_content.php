<div class="table-responsive">
			        <table class="table table-hover">
			          <thead>
			            <tr>
			              <th>Name</th>
			              <th class="text-right">Action</th>
			            </tr>
			          </thead>
			          <tbody>
			            <tr>
			              	<td>Office Supply</td>
		                    <td class="text-right">
			                    <div class="btn-group btn-group-xs">
			                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalAddCategory"><i class="fa fa-pencil"></i></a>
			                        <a href="" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
			                    </div>
		                    </td>
		                 </tr>
		                 <tr>   
		                    <td>Office Rental</td>
		                    <td class="text-right">
			                    <div class="btn-group btn-group-xs">
			                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalAddCategory"><i class="fa fa-pencil"></i></a>
			                        <a href="" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
			                    </div>
		                    </td>
		                 </tr>
		                 <tr>   
		                    <td>Utilities</td>
		                    <td class="text-right">
			                    <div class="btn-group btn-group-xs">
			                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalAddCategory"><i class="fa fa-pencil"></i></a>
			                        <a href="" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
			                    </div>
		                    </td>
		                 </tr>
		                 <tr class="list-level-2">   
		                    <td>Internet</td>
		                    <td class="text-right">
			                    <div class="btn-group btn-group-xs">
			                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalAddCategory"><i class="fa fa-pencil"></i></a>
			                        <a href="" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
			                    </div>
		                    </td>
		                 </tr>
		                 <tr class="list-level-2">   
		                    <td>Electricity</td>
		                    <td class="text-right">
			                    <div class="btn-group btn-group-xs">
			                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalAddCategory"><i class="fa fa-pencil"></i></a>
			                        <a href="" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
			                    </div>
		                    </td>
		                 </tr>
		                 <tr>  
		                    <td>Repair &amp; Maintenance</td>
		                    <td class="text-right">
			                    <div class="btn-group btn-group-xs">
			                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalAddCategory"><i class="fa fa-pencil"></i></a>
			                        <a href="" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
			                    </div>
		                    </td>
		                </tr>
		                 <tr> 
		                    <td>Representation</td>
		                    <td class="text-right">
			                    <div class="btn-group btn-group-xs">
			                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalAddCategory"><i class="fa fa-pencil"></i></a>
			                        <a href="" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
			                    </div>
		                    </td>
		                 </tr>
		                 <tr> 
		                    <td>Others</td>
		                    <td class="text-right">
			                    <div class="btn-group btn-group-xs">
			                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalAddCategory"><i class="fa fa-pencil"></i></a>
			                        <a href="" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
			                    </div>
		                    </td>
			            </tr>
			          </tbody>
			        </table>

			      </div>