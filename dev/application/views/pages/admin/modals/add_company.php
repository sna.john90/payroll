<button type="button" class="close close-tr text-dark" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
<?php echo form_open(base_url()."admin/company/add_company", array("id" => "add-company-form","onsubmit" => "return addCompanyEditableContent()","class"=>"form-horizontal")); ?>
  <div class="modal-header bg-graylite clearfix">
    <div class="table-grid">
      <div class="table-grid-cell" style="width:150px">
        <div class="btn-options-wrap img-avatar">
          <img class="img-responsive pull-left" src="{asset_url}images/avatar-company.png" width="150" cropper-canvas-image/>
          <ul class="btn-options btn-options-boxed list-inline pull-right">
            <li><a href="javascript:void(0)" id="add-company-picture"><i class="fa fa-camera"></i></a></li>
          </ul>
        </div>
      </div> 
      <div class="table-grid-cell vcenter pl15">
        <div class="btn-options-wrap">
          <input type="hidden" name="name" id="name-value">
          <h3 id="name-editable" class="font-thin mt0 mb0 pop-info p5" data-placement="top" data-content="Please specify company name here.">
            <div class="contenteditable-wrap">
              <div contenteditable="true">Company Name</div>
            </div>
          </h3>
        </div>
        <div class="contenteditable-wrap mt15">
         <input type="hidden" name="description" id="description-value">
         <div id="description-editable" class="slogan btn-options-wrap" contenteditable="true">Write awesome company description here ...</div>
        </div>
      </div>
    </div>  
  </div>
  <div class="modal-body">
    <input type="file" cropper data-reset="false" data-upload="false" class="hidden">
    <!-- Coordinates for Files -->
    <input type="hidden" id="image_url" name="company_picture">
    <div class="">
      <h4 class="mb0">Company Information</h4>
      <small>Add company information here.</small>
      <div class="form-modern mt30 pr15">
        <div id="address-details" class="form-group">
          <label class="col-sm-3 control-label text-label">Company Address</label>
          <input type="hidden" data-geo="lat" name="latitude">
          <input type="hidden" data-geo="lng" name="longitude">
          <div class="input-group col-sm-9">
            <input type="text" class="form-control   text-label" name="address">
            <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label text-label">Email Address</label>
          <div class="input-group col-sm-9">
            <input type="email" class="form-control" name="email">
            <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label text-label">Website</label>
          <div class="input-group col-sm-9">
            <input type="text" class="form-control text-label" name="website">
            <div class="input-group-addon"><i class="fa fa-globe"></i></div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label text-label">Telephone Number</label>
          <div class="input-group col-sm-9">
            <input type="text" class="form-control phone" name="telephone_number">
            <div class="input-group-addon"><i class="fa fa-phone"></i></div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label text-label">Fax Number</label>
          <div class="input-group col-sm-9">
            <input type="text" class="form-control phone" name="fax_number">
            <div class="input-group-addon"><i class="fa fa-fax"></i></div>
          </div>
        </div>
        <div class="form-group">
          <div class="text-right clearfix">
            <button class="btn btn-default btn-sm" data-dismiss="modal"><i class="fa fa-times-circle"></i> Cancel</button>
            <button class="btn btn-primary btn-sm submit-btn"><i class="fa fa-plus-circle"></i> Add</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
<style type="text/css">
  .popover.top {
   /* left: 0px !important;*/
  }
</style>
<script type="text/javascript">
  $(document).ready(function(){
    $(".address-geo").geocomplete({
      details: "#address-details",
      detailsAttribute: "data-geo"
    });
    // $(".address-geo").trigger("geocode");
    $("input.phone").inputmask("999-999-9999");
    var form = $('#add-company-form');
    // submit admin status form
    $('#add-company-form').ajaxForm({
      dataType: 'json',
      beforeSend:function(){
        form.find('.submit-btn').html('<i class="fa fa-plus-circle"></i> Adding ... ');
        form.find('.submit-btn').prop("disabled",true);
      },
      success:function(response){
        form.find('.submit-btn').html('<i class="fa fa-plus-circle"></i> Add');
        form.find('.submit-btn').prop("disabled",false);
        me_message_v2(response);
        if(response.error==0)
        {
          // reload datatable users
          $("#companyTable").DataTable().ajax.reload(null,false); 

          $("body #modalCompany").modal("hide");
          $("body #modalCompany").on('hidden.bs.modal',function () {
            $(this).data('bs.modal', null);
          });
        }
      }
    });
    $(".pop-info").on("blur",function(){
      $(this).popover("hide");
    });
    $(".pop-info").on("focus keyup keydown",function(){
      if($(this).text()!="Company Name")
      {
        $(this).popover("hide");
      }else
      {
        $(this).popover("show");
      }
    });
    $("*[contenteditable=true]").on("focus",function(){
     $(this).selectText();
    });
    // add company picture
    $("#add-company-picture").click(function(){
      form.find("input[cropper]").click();
    });
    form.on("change","input[cropper]",function(){
      var reset = $(this).data("reset");
      var upload = $(this).data("upload");
      if($(this).val().length!=0)
      {
        previewFile(this, reset);
      }
      $(this).replaceWith('<input type="file" cropper data-reset="false" data-upload="false" class="hidden">');
      form.find("input[cropper]").data("reset",reset);
      form.find("input[cropper]").data("upload",upload);
    });
  });
  function addCompanyEditableContent()
  {
    var name        = document.getElementById("name-editable").innerText;
    var description = document.getElementById("description-editable").innerText;
    if(name=="Company Name")
    {
      $(".pop-info").focus();
      response_message({'error':1,'message':'<p>Please specify company name.</p>'});
      return false;
    }
    document.getElementById("name-value").value =  name;
    document.getElementById("description-value").value =  description;
    return true;
  }
</script>


