<?php echo form_open(base_url()."admin/users/add",array("id" => "add-user-form")); ?>
<div class="modal-header bg-graylite">
        <button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="panel-title">Add Employee</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="company_id" value="{company_id}">
        <div class="form-modern row mb15">
            <div class="col-sm-6">
                <h4 class="font-normal"><i class="fa fa-user-circle-o"></i> Profile</h4>
                <ul class="list-table vmiddle list-unstyled">
                  <li>
                    <div class="list-table-label">First Name:</div>
                    <div class="list-table-content"> <input class="form-control" name="firstname"  ></div>
                  </li>
                  <li>
                    <div class="list-table-label">Last Name:</div>
                    <div class="list-table-content"> <input class="form-control" name="lastname"  ></div>
                  </li>
                  <li>
                    <div class="list-table-label">Middle Name:</div>
                    <div class="list-table-content"> <input class="form-control" name="middlename"  ></div>
                  </li>
                  <li>
                    <div class="list-table-label">Birth Date:</div>
                    <div class="list-table-content text-success"> <input class="form-control date" name="birthdate"  ></div>
                  </li>
                  <li>
                    <div class="list-table-label">Address:</div>
                    <div class="list-table-content text-success"> <input class="form-control" name="address"  ></div>
                  </li>
                  <li>
                    <div class="list-table-label">Gender:</div>
                    <div class="list-table-content text-success">
                      <select class="form-control" name="gender">
                          <option>Male</option>
                          <option>Female</option>
                      </select>
                    </div>
                  </li>

                </ul>
            </div>
            <div class="col-sm-6">
              <h4 class="font-normal">&nbsp;</h4>
              <ul class="list-table vmiddle list-unstyled">
                <li>
                    <div class="list-table-label">Phone:</div>
                    <div class="list-table-content text-success"> <input class="form-control" name="phone"  ></div>
                  </li>
                   <li>
                      <div class="list-table-label">TIN:</div>
                      <div class="list-table-content text-success"> <input class="form-control" name="tin"  ></div>
                  </li>
                  <li>
                      <div class="list-table-label">SSS #:</div>
                      <div class="list-table-content text-success"> <input class="form-control" name="sss"  ></div>
                  </li>
                  <li>
                      <div class="list-table-label">PHILHEALTH #:</div>
                      <div class="list-table-content text-success"> <input class="form-control" name="philhealth"  ></div>
                  </li>
                  <li>
                      <div class="list-table-label">PAG-IBIG #:</div>
                      <div class="list-table-content text-success"> <input class="form-control" name="pagibig"  ></div>
                  </li>
                </ul>
            </div>
        </div>

        <div class="section-divider"></div>

        <div class="form-modern row mb30">
            <div class="col-sm-6">
              <h4 class="font-normal"><i class="fa fa-bank"></i> Pay Details</h4>
                <ul class="list-table vmiddle list-unstyled">

                    <li>
                      <div class="list-table-label">Basic Pay:</div>
                      <div class="list-table-content"> <input class="form-control" name="rate"  ></div>
                    </li>

                    <li>
                      <div class="list-table-label">Department:</div>
                      <div class="list-table-content">
                        <select class="form-control" name="department_id">
                            <?php foreach(get_departments() as $key=>$value): ?>
                                <option value="<?php echo $value['id'];?>"><?php echo $value['description'];?></option>
                            <?php endforeach;?>
                        </select>
                      </div>
                    </li>
                    <li>
                      <div class="list-table-label">Status:</div>
                      <div class="list-table-content">
                        <select id="status" name="gender_status" class="form-control" required="">
                          <option value="" selected="selected"></option>
                          <?php foreach(gender_status_types() as $key=>$val): ?>
                              <option value="<?php echo $key;?>"  ><?php echo $val; ?></option>
                          <?php endforeach;?>
                        </select>
                        </div>
                    </li>
                    <li>
                      <div class="list-table-label">Payment Schedule:</div>
                      <div class="list-table-content">
                        <select class="form-control"  name="payment_type" >
                          <option class="hourly">Hourly</option>
                          <option class="contractual">Contractual</option>
                          <option class="daily">Daily</option>
                          <option class="semi-monthly">Semi-Monthly</option>
                          <option class="monthly">Monthly</option>
                        </select>
                        </div>
                    </li>
                    <li>
                      <div class="list-table-label">Bank Account #</div>
                      <div class="list-table-content"> <input class="form-control" name="bank_account"  ></div>
                    </li>

                  </ul>
            </div>

            <div class="col-sm-6">
                <h4 class="font-normal"><i class="fa fa-address-book-o"></i> Account Details</h4>
                <ul class="list-table vmiddle list-unstyled">
                  <li>
                      <div class="list-table-label">Employee ID #:</div>
                      <div class="list-table-content"> <input class="form-control" name="employee_id"  ></div>
                    </li>
                  <li>
                      <div class="list-table-label">Date Hired:</div>
                      <div class="list-table-content"> <input class="form-control date"   name="date_hired" > </div>
                    </li>
                  <li>
                      <div class="list-table-label">Designation:</div>
                      <div class="list-table-content"> <input type="text" class="form-control"   name="designation" ></div>
                    </li>
                  <li>
                      <div class="list-table-label">Employment Status:</div>
                      <div class="list-table-content">
                        <select class="form-control"  name="status" >
                          <option class="permanent">Permanent</option>
                          <option class="temporary">Temporary</option>
                          <option class="probation">Probation</option>
                          <option class="inactive">Inactive</option>
                        </select>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="section-divider"></div>

        <div class="form-modern row mb30">

            <div class="col-sm-6">
                <h4 class="font-normal"><i class="fa fa-key"></i> Login Information</h4>
                <ul class="list-table vmiddle list-unstyled">

                  <li>
                    <div class="list-table-label">Email:</div>
                    <div class="list-table-content"> <input class="form-control" name="email" placeholder="email@domain.com"  ></div>
                  </li>
                   <li>
                    <div class="list-table-label">Password: <span class="fa fa-question" data-toggle="tooltip" data-placement="top" data-container="body" title="(leave it blank if you want to use the default password. Default password is payroll1234)"></span></div>
                    <div class="list-table-content"> <input type="text" class="form-control" name="password"  ></div>
                  </li>
                  <li>
                    <div class="list-table-label">User type:</div>
                    <div class="list-table-content">
                      <select class="form-control"  name="user_group_id" >
                        <option value="2">Admin</option>
                        <option value="3">HR</option>
                        <option value="4">Accountant</option>
                        <option value="5" selected>Employee</option> 
                      </select>
                      </div>
                  </li>
                </ul>
            </div>
        </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary submit-btn"  >Save changes</button>
      </div>
</form>
<script type="text/javascript">
    $(document).ready(function(){
       $('[data-toggle="tooltip"]').tooltip();
       $('.date').datetimepicker({
        format: 'YYYY-MM-DD'
      });
       var form = $('#add-user-form');
    // submit admin status form
    $('#add-user-form').ajaxForm({
        dataType: 'json',
        beforeSend:function(){
          me_message_v2({error:2,message:"Saving employee details ..."});
          form.find('.submit-btn').prop("disabled",true);
        },
        success:function(response){
          form.find('.submit-btn').prop("disabled",false);
         me_message_v2(response);
          if(response.error==0)
          {
            // reload datatable users
            $("#userTable").DataTable().ajax.reload(null,false);

            $("body #modalAddUser1").modal("hide");
            $("body #modalAddUser1").on('hidden.bs.modal',function () {
              $(this).data('bs.modal', null);
            });
          }
        }
      });
    });
</script>
