<style type="text/css">
  .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    /*background: none!important;*/
    opacity: 1;
}
</style> 
<div class="modal-header bg-graylite">
        <button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="panel-title">Employee #: {emp_id}  - {firstname} {lastname} </h4>
      </div>
      <div class="modal-body details">  
        <div class="form-modern row mb30">
            <div class="col-sm-6">
                <h4 class="font-normal"><i class="fa fa-user-circle-o"></i> Profile</h4>
                <ul class="list-table vmiddle list-unstyled">
                  <li>
                    <div class="list-table-label">First Name:</div>
                    <div class="list-table-content"> <input class="form-control" name="firstname" value="{firstname}" ></div>
                  </li>
                  <li>
                    <div class="list-table-label">Last Name:</div>
                    <div class="list-table-content"> <input class="form-control" name="lastname" value="{lastname}" ></div>
                  </li>
                  <li>
                    <div class="list-table-label">Middle Name:</div>
                    <div class="list-table-content"> <input class="form-control" name="middlename" value="{middlename}" ></div>
                  </li>
                  <li>
                    <div class="list-table-label">Birth Date:</div>
                    <div class="list-table-content text-success"> <input class="form-control date" name="birthdate"  value="{birthdate}"></div>
                  </li>
                  <li>
                    <div class="list-table-label">Address:</div>
                    <div class="list-table-content text-success"> <input class="form-control" name="address"  value="{address}"></div>
                  </li>
                  <li>
                    <div class="list-table-label">Gender:</div>
                    <div class="list-table-content text-success">
                      <select class="form-control" name="gender">
                          <option <?php echo ($gender=="Male")? "selected" : "";?>>Male</option>
                          <option <?php echo ($gender=="Female")? "selected" : "";?>>Female</option>
                      </select>
                    </div>
                  </li>
                </ul>
            </div> 
            <div class="col-sm-6">
                <h4 class="font-normal">&nbsp;</h4>
                <ul class="list-table vmiddle list-unstyled">
                   <li>
                    <div class="list-table-label">Phone:</div>
                    <div class="list-table-content text-success"> <input class="form-control" name="phone"  value="{phone}"></div>
                  </li>
                   <li>
                      <div class="list-table-label">TIN:</div>
                      <div class="list-table-content text-success"> <input class="form-control" name="tin" value="{tin}" ></div>
                  </li>
                  <li>
                      <div class="list-table-label">SSS:</div>
                      <div class="list-table-content text-success"> <input class="form-control" name="sss"  value="{sss}" ></div>
                  </li>
                  <li>
                      <div class="list-table-label">PHILHEALTH:</div>
                      <div class="list-table-content text-success"> <input class="form-control" name="philhealth"  value="{philhealth}"></div>
                  </li>
                  <li>
                      <div class="list-table-label">PAG-IBIG:</div>
                      <div class="list-table-content text-success"> <input class="form-control" name="pagibig"  value="{pagibig}"></div>
                  </li>
                </ul>
            </div> 
        </div>

        <div class="section-divider"></div>

        <div class="form-modern row mb15">
            <div class="col-sm-6">
              <h4 class="font-normal"><i class="fa fa-bank"></i> Pay Details</h4>
              <ul class="list-table vmiddle list-unstyled">
                  <li>
                    <div class="list-table-label">Basic Pay</div>
                    <div class="list-table-content"> <input class="form-control" name="rate" value="{basic_pay}" ></div>
                  </li>
                  <li>
                    <div class="list-table-label">Department:</div>
                    <div class="list-table-content"> 
                      <select class="form-control" name="department_id">
                          <?php foreach(get_departments() as $key=>$value): ?>
                              <option value="<?php echo $value['id'];?>" <?php echo ($value['id']==$department_id)? "selected" : "";?> ><?php echo $value['description'];?></option>
                          <?php endforeach;?>
                      </select>
                    </div>
                  </li>
                  <li>
                    <div class="list-table-label">Gender Status:</div>
                    <div class="list-table-content data-container">{gender_status}
                      </div> 
                  </li> 
                  <li>
                    <div class="list-table-label">Payment Schedule:</div>
                    <div class="list-table-content">
                    <?php 
                        $payment_type = array(
                                          "hourly"          => "Hourly",
                                          "contractual"     => "Contractual",
                                          "daily"           => "Daily",
                                          "semi-monthly"        => "Semi-Monthly",
                                          "monthly"         => "Monthly"
                                        );
                    ?>
                      <select class="form-control"  name="payment_type" >
                          <?php foreach($payment_type as $key=>$value): ?>
                                  <option value="<?php echo $key; ?>" <?php echo ($key==$payment_type)? "selected" : "";?>><?php echo $value; ?></option>
                          <?php endforeach;?>
                      </select> 
                      </div>
                  </li> 
                  <li>
                    <div class="list-table-label">Bank Account #</div>
                    <div class="list-table-content"> <input class="form-control" name="bank_account"  value="{bankaccount}"></div>
                  </li>
                  
                </ul>
            </div>
            <div class="col-sm-6">
              <h4 class="font-normal"><i class="fa fa-address-book-o"></i> Account Details</h4>
              <ul class="list-table vmiddle list-unstyled">
                  <li>
                    <div class="list-table-label">Company ID #:</div>
                    <div class="list-table-content"> <input class="form-control" name="employee_id" value="{emp_id}" ></div>
                  </li>
                  <li>
                    <div class="list-table-label">Date Hired:</div>
                    <div class="list-table-content"> <input class="form-control date"   name="date_hired" value="{date_hired}"> </div>
                  </li> 
                  <li>
                    <div class="list-table-label">Designation:</div>
                    <div class="list-table-content"> <input type="text" class="form-control"   name="designation" value="{designation}"></div>
                  </li> 
                  <li>
                    <div class="list-table-label">Employment Status:</div>
                    <div class="list-table-content">
                      <select class="form-control"  name="status" >
                        <option value="permanent" <?php echo ($emp_status=="permanent")? "selected" : "";?>>Permanent</option>
                        <option value="temporary" <?php echo ($emp_status=="temporary")? "selected" : "";?>>Temporary</option>
                        <option value="probation" <?php echo ($emp_status=="probation")? "selected" : "";?>>Probation</option> 
                      </select> 
                      </div>
                  </li> 
                </ul>
            </div>
        </div>  

        <div class="section-divider"></div>

        <div class="form-modern row mb15">
            <div class="col-sm-6">
              <h4 class="font-normal"><i class="fa fa-key"></i> Login Information</h4>
                <ul class="list-table vmiddle list-unstyled">
                  <li>
                    <div class="list-table-label">Email:</div>
                    <div class="list-table-content"> <input class="form-control" name="email" placeholder="email@domain.com" value="{email}" ></div>
                  </li>
                   <li>
                    <div class="list-table-label">Password: <span class="fa fa-question" data-toggle="tooltip" data-placement="top" data-container="body" title="(leave it blank if you want to use the default password. Default password is payroll1234)"></span></div>
                    <div class="list-table-content"> <input type="text" class="form-control" name="password"  ></div>
                  </li>
                  <li>
                    <div class="list-table-label">User type:</div>
                    <div class="list-table-content">
                      <select class="form-control"  name="user_group_id" >
                          <?php foreach(get_user_types() as $key=>$value): ?>
                              <option value="<?php echo $value['id'];?>" <?php echo ($value['id']==$user_group_id)? "selected" : "";?> ><?php echo $value['description'];?></option>
                          <?php endforeach;?>
                      </select> 
                      </div>
                  </li> 
                </ul>
            </div>
            <div class="col-sm-6">
            </div>
        </div> 

      </div> 
</form> 
<script type="text/javascript">
    $(document).ready(function(){
      $('input,select,textarea').attr("readonly",true);
      $('input,select,textarea').prop("disabled",true);
    });_

</script>