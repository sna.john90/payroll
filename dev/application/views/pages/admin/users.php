
<div class="container-fluid">
  <div class="panel panel-nox fullH">
    <div class="panel-heading clearfix">
        <div class="filter-option pull-right">
            <ul class="list-inline">
                 <li>
                    <a href="javascript:void(0)" id="add-user-btn" class="btn btn-success btn-sm"><i class="fa fa-plus-circle"></i> Add Employee</a>
                </li>
                <!-- <li>
                    <div class="btn-group btn-group-sm">
                        <a href="" class="btn btn-default active"><i class="fa fa-bars"></i></a>
                        <a href="" class="btn btn-default"><i class="fa fa-th"></i></a>
                    </div>
                </li> -->
            </ul>
        </div>
        <h3 class="m0 mb15">Employee</h3>
        <?php if(is_super_admin()): ?>
               <div class="form-group pull-left">
                  <select id="company-select" style="display:none;">
                    <?php if(!empty($companies)): ?>
                      <?php foreach($companies as $key => $value): ?>
                        <option value="<?php echo encrypt($value['id']); ?>" <?php if($value['is_current']==1):?>selected<?php endif; ?>><?php echo $value['name']?></option>
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </select>
                </div>
        <?php endif;?>
    </div>
    <div class="panel-body">
        
        <div class="table-responsive"> 
        <table id="userTable" class="table vmiddle table-hover"> 
           <thead>
                <tr>
                    <th>Name</th>
                    <th>ID#</th>
                    <th>Department</th>
                    <th>Designation</th>
                    <th>Rate</th>
                    <th>Date Hired</th>
                    <th data-orderable="false">Phone</th>
                    <th>Status</th>
                     <th>Role</th>
                    <th>Action</th>
                </tr>
            </thead>
             <tfoot>
                <tr>
                    <th>Name</th>
                    <th>ID#</th>
                    <th>Department</th>
                    <th>Designation</th>
                    <th>Rate</th>
                    <th>Date Hired</th>
                    <th>Phone</th>
                     <th>Status</th>
                   <th>Role</th>
                    <th>Action</th>
                </tr>
            </tfoot>
           <tbody></tbody> 
        </table>
      </div>
        
    </div>

    <div class="panel-body">
        <div class="filter-option pull-right">
            <ul class="list-inline">
                 <li>
                    <a href="" class="btn btn-success btn-sm"><i class="fa fa-plus-circle"></i> Add Employee</a>
                </li>
            </ul>
        </div>
    </div>
  </div>
</div>    
<style type="text/css">
    #userTable td{
        padding: 6px 4px;
        font-size: 13px;
    }
</style>
{crop_modal}