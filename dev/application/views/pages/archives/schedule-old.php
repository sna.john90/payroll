<?php if(is_lender_admin()): ?>
  <div class="container-fluid">
    <div class="row clearfix mb50">
      <div id="sidebar-main-container" class="col-xs-12 col-sm-3">
        <h5 class="mt30 mb0 p15 pl0">Activity Logs</h5>
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search activity logs" id="search-activity-logs">
        </div>
        <div id="act-logs-cont" class="col-xs-12 pl0 pr0">
          <div id="activity-scroll-container">
            <div id="activity-logs-container" class="pr10" style="height:auto;">
              <p class="text-center font-xlg"><span class="fa fa-spinner fa-spin"></span></p>
            </div>
          </div>          
        </div>
      </div>
      <div class="col-xs-12 col-sm-9">
        <div class="page-header text-right clearfix" style="margin-top:30px;">
          <?php if(!is_guest()): ?>
            <h1 class="page-title mt10 pull-left">Past Signings</h1>
          <?php else: ?>
            <h1 class="page-title mt10 pull-left">All Past Signings</h1>
          <?php endif; ?>
          <ul class="list-inline pull-right mrneg5">
            <li><a href="{base_url}signings" class="btn btn-success btn-sm">CURRENT</a></li>
            <li><a href="javascript:void(0)" class="btn btn-success btn-sm active">Past</a></li>
          </ul>  
        </div> 
        <input type="hidden" id="lender-id" value="{company_id}">
        <div class="page-content pt0 mtneg20">

          <div id="schedule-tables-container" class="clearfix mb50" data-today="<?php echo date('m-d-Y',(strtotime ( '-1 day' , strtotime ( date("Y-m-d") ) ) )); ?>">
            <!-- IF NOT GUEST USER -->
            <?php if(!is_guest()): ?>
              <!-- CONTENT -->
              <?php $user = get_user_info(); ?>
              <ul class="nav nav-tabs nav-table" role="tablist">
                <li role="presentation" data-tab="all-signings" class="schedule-tab active"><a href="#tab-all-signings" aria-controls="tab-all-signings" role="tab" data-toggle="tab" class="titlelink">All Past Signings</a></li>
                <li role="presentation" data-tab="my-signings" class="schedule-tab"><a href="#tab-my-signings" aria-controls="tab-my-signings" role="tab" data-toggle="tab" class="titlelink"><?php echo $user['name'];?>'s Past Signings</a></li>
                <li role="presentation" data-tab="completed-signings" class="schedule-tab"><a href="#tab-completed-signings" aria-controls="tab-completed-signings" role="tab" data-toggle="tab" class="titlelink">Completed Signings</a></li>
                <li role="presentation" data-tab="deleted-signings" class="schedule-tab"><a href="#tab-deleted-signings" aria-controls="tab-deleted-signings" role="tab" data-toggle="tab" class="titlelink">Deleted Signings</a></li>
              </ul>
              <!-- Tab panes -->
              <div class="tab-content">
                <!-- all past signings content -->
                <div role="tabpanel" class="tab-pane active" id="tab-all-signings">
                  <div class="table-responsive mt20"> 
                    <table id="allScheduleTable" class="table table-hover" style="margin-bottom:200px !important;">
                      <thead class="threadcolor"> 
                        <tr>
                          <th class="date-label width-100">Schedule</th>
                          <th class="date-label width-80" id="time-label">Time</th>
                          <th class="single-row width-150">Borrower</th>
                          <th class="single-row width-100">Title Order #</th>
                          <th class="single-row width-150"><span data-toggle="tooltip" title="Loan Officer">Loan Officer</span></th>
                          <th class="single-row width-150"><span data-toggle="tooltip" title="Processor">Processor</span></th>
                          <th class="single-row width-150"><span data-toggle="tooltip" title="Underwriter">Underwriter</span></th>
                          <th class="single-row width-150">Folder</th>
                          <th class="status-text single-row">Status</th>
                          <th class="status-text single-row">Signing Company</th>
                          <th class="comment-text text-center"><i class="fa fa-comments-o" data-toggle="tooltip" title="Comments"></i></th>
                          <th class="action-text text-center"><span data-toggle="tooltip" title="Actions"><i class="fa fa-cog"></i></span></th>
                        </tr> 
                      </thead> 
                      <tbody></tbody> 
                    </table>
                  </div> 
                </div>
                <!-- my past signings content -->
                <div role="tabpanel" class="tab-pane" id="tab-my-signings">
                  <div class="table-responsive mt20"> 
                    <table id="myScheduleTable" class="table table-hover" width="100%" style="margin-bottom:200px !important;">
                      <thead class="threadcolor"> 
                        <tr>
                          <th class="date-label width-100">Schedule</th>
                          <th class="date-label width-80" id="time-label">Time</th>
                          <th class="single-row width-150">Borrower</th>
                          <th class="single-row width-100">Title Order #</th>
                          <th class="single-row width-150"><span data-toggle="tooltip" title="Loan Officer">Loan Officer</span></th>
                          <th class="single-row width-150"><span data-toggle="tooltip" title="Processor">Processor</span></th>
                          <th class="single-row width-150"><span data-toggle="tooltip" title="Underwriter">Underwriter</span></th>
                          <th class="single-row width-150">Folder</th>
                          <th class="status-text single-row">Status</th>
                          <th class="status-text single-row">Signing Company</th>
                          <th class="comment-text text-center"><i class="fa fa-comments-o" data-toggle="tooltip" title="Comments"></i></th>
                          <th class="action-text text-center"><span data-toggle="tooltip" title="Actions"><i class="fa fa-cog"></i></span></th>
                        </tr> 
                      </thead>
                      <tbody></tbody> 
                    </table>
                  </div> 
                </div>
                <!-- all completed signings content -->
                <div role="tabpanel" class="tab-pane" id="tab-completed-signings">
                  <div class="table-responsive mt20"> 
                    <table id="completedScheduleTable" class="table table-hover" style="margin-bottom:200px !important;">
                      <thead class="threadcolor"> 
                        <tr>
                          <th class="date-label width-100">Schedule</th>
                          <th class="date-label width-80" id="time-label">Time</th>
                          <th class="single-row width-150">Borrower</th>
                          <th class="single-row width-100">Title Order #</th>
                          <th class="single-row width-150"><span data-toggle="tooltip" title="Loan Officer">Loan Officer</span></th>
                          <th class="single-row width-150"><span data-toggle="tooltip" title="Processor">Processor</span></th>
                          <th class="single-row width-150"><span data-toggle="tooltip" title="Underwriter">Underwriter</span></th>
                          <th class="single-row width-150">Folder</th>
                          <th class="status-text single-row">Status</th>
                          <th class="status-text single-row">Signing Company</th>
                          <th class="comment-text text-center"><i class="fa fa-comments-o" data-toggle="tooltip" title="Comments"></i></th>
                          <th class="action-text-2 text-center"><span data-toggle="tooltip" title="Actions"><i class="fa fa-cog"></i></span></th>
                        </tr> 
                      </thead> 
                      <tbody></tbody> 
                    </table>
                  </div> 
                </div>
                <!-- all deleted signings content -->
                <div role="tabpanel" class="tab-pane" id="tab-deleted-signings">
                  <div class="table-responsive mt20"> 
                    <table id="deletedScheduleTable" class="table table-hover" style="margin-bottom:200px !important;">
                      <thead class="threadcolor"> 
                        <tr>
                          <th class="date-label width-100">Schedule</th>
                          <th class="date-label width-80" id="time-label">Time</th>
                          <th class="single-row width-150">Borrower</th>
                          <th class="single-row width-100">Title Order #</th>
                          <th class="single-row width-150"><span data-toggle="tooltip" title="Loan Officer">Loan Officer</span></th>
                          <th class="single-row width-150"><span data-toggle="tooltip" title="Processor">Processor</span></th>
                          <th class="single-row width-150"><span data-toggle="tooltip" title="Underwriter">Underwriter</span></th>
                          <th class="single-row width-150">Folder</th>
                          <th class="status-text single-row">Status</th>
                          <th class="status-text single-row">Signing Company</th>
                          <th class="comment-text text-center"><i class="fa fa-comments-o" data-toggle="tooltip" title="Comments"></i></th>
                          <th class="action-text-2 text-center"><span data-toggle="tooltip" title="Actions"><i class="fa fa-cog"></i></span></th>
                        </tr> 
                      </thead> 
                      <tbody></tbody> 
                    </table>
                  </div> 
                </div>
              </div>
              <!-- END CONTENT -->
            <?php else: ?>
              <div class="table-responsive mt10"> 
                <table id="allScheduleTable" class="table table-hover" style="margin-bottom:200px !important;">
                  <thead class="threadcolor"> 
                    <tr>
                      <th class="date-label width-100">Schedule</th>
                      <th class="date-label width-80" id="time-label">Time</th>
                      <th class="single-row width-150">Borrower</th>
                      <th class="single-row width-100">Title Order #</th>
                      <th class="single-row width-150"><span data-toggle="tooltip" title="Loan Officer">Loan Officer</span></th>
                      <th class="single-row width-150"><span data-toggle="tooltip" title="Processor">Processor</span></th>
                      <th class="single-row width-150"><span data-toggle="tooltip" title="Underwriter">Underwriter</span></th>
                      <th class="single-row width-150">Folder</th>
                      <th class="status-text single-row">Status</th>
                      <th class="status-text single-row">Signing Company</th>
                      <th class="comment-text text-center"><i class="fa fa-comments-o" data-toggle="tooltip" title="Comments"></i></th>
                    </tr> 
                  </thead> 
                  <tbody></tbody> 
                </table>
              </div> 
            <?php endif; ?>
          </div>
          
        </div>
      </div>
    </div>
  </div>
<?php else: ?>
  <div class="page-header container text-right clearfix" style="margin-top:30px;">
    <?php if(!is_guest()): ?>
      <h1 class="page-title mt10 pull-left mlneg15">Past Signings</h1>
    <?php else: ?>
      <h1 class="page-title mt10 pull-left mlneg15">All Past Signings</h1>
    <?php endif; ?>
    <ul class="list-inline pull-right mrneg20">
      <li><a href="{base_url}signings" class="btn btn-success btn-sm">CURRENT</a></li>
      <li><a href="javascript:void(0)" class="btn btn-success btn-sm active">Past</a></li>
    </ul>  
  </div> 
  <input type="hidden" id="lender-id" value="{company_id}">
  <div class="page-content pt0 mtneg20">
    <div class="container">
      <div id="schedule-tables-container" class="row clearfix mb50" data-today="<?php echo date('m-d-Y',(strtotime ( '-1 day' , strtotime ( date("Y-m-d") ) ) )); ?>">

        <!-- IF NOT GUEST USER -->
        <?php if(!is_guest()): ?>
          <!-- CONTENT -->
          <?php $user = get_user_info(); ?>
          <ul class="nav nav-tabs nav-table" role="tablist">
            <li role="presentation" data-tab="all-signings" class="schedule-tab active"><a href="#tab-all-signings" aria-controls="tab-all-signings" role="tab" data-toggle="tab" class="titlelink">All Past Signings</a></li>
            <?php if(!is_lender_admin()): ?>
              <li role="presentation" data-tab="my-signings" class="schedule-tab"><a href="#tab-my-signings" aria-controls="tab-my-signings" role="tab" data-toggle="tab" class="titlelink"><?php echo $user['name'];?>'s Past Signings</a></li>
            <?php endif; ?>
            <li role="presentation" data-tab="completed-signings" class="schedule-tab"><a href="#tab-completed-signings" aria-controls="tab-completed-signings" role="tab" data-toggle="tab" class="titlelink">Completed Signings</a></li>
            <li role="presentation" data-tab="deleted-signings" class="schedule-tab"><a href="#tab-deleted-signings" aria-controls="tab-deleted-signings" role="tab" data-toggle="tab" class="titlelink">Deleted Signings</a></li>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
            <!-- all past signings content -->
            <div role="tabpanel" class="tab-pane active" id="tab-all-signings">
              <div class="table-responsive mt20"> 
                <table id="allScheduleTable" class="table table-hover" style="margin-bottom:200px !important;">
                  <thead class="threadcolor"> 
                    <tr>
                      <th class="date-label width-100">Schedule</th>
                      <th class="date-label width-80" id="time-label">Time</th>
                      <th class="single-row width-150">Borrower</th>
                      <th class="single-row width-100">Title Order #</th>
                      <th class="single-row width-150"><span data-toggle="tooltip" title="Loan Officer">Loan Officer</span></th>
                      <th class="single-row width-150"><span data-toggle="tooltip" title="Processor">Processor</span></th>
                      <th class="single-row width-150"><span data-toggle="tooltip" title="Underwriter">Underwriter</span></th>
                      <th class="single-row width-150">Folder</th>
                      <th class="status-text single-row">Status</th>
                      <th class="status-text single-row">Signing Company</th>
                      <th class="comment-text text-center"><i class="fa fa-comments-o" data-toggle="tooltip" title="Comments"></i></th>
                      <?php if(is_loan_officer()): ?>
                        <th class="action-text-2 text-center"><span data-toggle="tooltip" title="Actions"><i class="fa fa-cog"></i></span></th>
                      <?php endif; ?>
                    </tr> 
                  </thead> 
                  <tbody></tbody> 
                </table>
              </div> 
            </div>
            <?php if(!is_lender_admin()): ?>
              <!-- my past signings content -->
              <div role="tabpanel" class="tab-pane" id="tab-my-signings">
                <div class="table-responsive mt20"> 
                  <table id="myScheduleTable" class="table table-hover" width="100%" style="margin-bottom:200px !important;">
                    <thead class="threadcolor"> 
                      <tr>
                        <th class="date-label width-100">Schedule</th>
                        <th class="date-label width-80" id="time-label">Time</th>
                        <th class="single-row width-150">Borrower</th>
                        <th class="single-row width-100">Title Order #</th>
                        <th class="single-row width-150"><span data-toggle="tooltip" title="Loan Officer">Loan Officer</span></th>
                        <th class="single-row width-150"><span data-toggle="tooltip" title="Processor">Processor</span></th>
                        <th class="single-row width-150"><span data-toggle="tooltip" title="Underwriter">Underwriter</span></th>
                        <th class="single-row width-150">Folder</th>
                        <th class="status-text single-row">Status</th>
                        <th class="status-text single-row">Signing Company</th>
                        <th class="comment-text text-center"><i class="fa fa-comments-o" data-toggle="tooltip" title="Comments"></i></th>
                        <?php if(is_loan_officer()): ?>
                          <th class="action-text-2 text-center"><span data-toggle="tooltip" title="Actions"><i class="fa fa-cog"></i></span></th>
                        <?php endif; ?>
                      </tr> 
                    </thead>
                    <tbody></tbody> 
                  </table>
                </div> 
              </div>
            <?php endif; ?>
            <!-- all completed signings content -->
            <div role="tabpanel" class="tab-pane" id="tab-completed-signings">
              <div class="table-responsive mt20"> 
                <table id="completedScheduleTable" class="table table-hover" style="margin-bottom:200px !important;">
                  <thead class="threadcolor"> 
                    <tr>
                      <th class="date-label width-100">Schedule</th>
                      <th class="date-label width-80" id="time-label">Time</th>
                      <th class="single-row width-150">Borrower</th>
                      <th class="single-row width-100">Title Order #</th>
                      <th class="single-row width-150"><span data-toggle="tooltip" title="Loan Officer">Loan Officer</span></th>
                      <th class="single-row width-150"><span data-toggle="tooltip" title="Processor">Processor</span></th>
                      <th class="single-row width-150"><span data-toggle="tooltip" title="Underwriter">Underwriter</span></th>
                      <th class="single-row width-150">Folder</th>
                      <th class="status-text single-row">Status</th>
                      <th class="status-text single-row">Signing Company</th>
                      <th class="comment-text text-center"><i class="fa fa-comments-o" data-toggle="tooltip" title="Comments"></i></th>
                      <?php if(is_loan_officer()): ?>
                        <th class="action-text-2 text-center"><span data-toggle="tooltip" title="Actions"><i class="fa fa-cog"></i></span></th>
                      <?php endif; ?>
                    </tr> 
                  </thead> 
                  <tbody></tbody> 
                </table>
              </div> 
            </div>
            <!-- all deleted signings content -->
            <div role="tabpanel" class="tab-pane" id="tab-deleted-signings">
              <div class="table-responsive mt20"> 
                <table id="deletedScheduleTable" class="table table-hover" style="margin-bottom:200px !important;">
                  <thead class="threadcolor"> 
                    <tr>
                      <th class="date-label width-100">Schedule</th>
                      <th class="date-label width-80" id="time-label">Time</th>
                      <th class="single-row width-150">Borrower</th>
                      <th class="single-row width-100">Title Order #</th>
                      <th class="single-row width-150"><span data-toggle="tooltip" title="Loan Officer">Loan Officer</span></th>
                      <th class="single-row width-150"><span data-toggle="tooltip" title="Processor">Processor</span></th>
                      <th class="single-row width-150"><span data-toggle="tooltip" title="Underwriter">Underwriter</span></th>
                      <th class="single-row width-150">Folder</th>
                      <th class="status-text single-row">Status</th>
                      <th class="status-text single-row">Signing Company</th>
                      <th class="comment-text text-center"><i class="fa fa-comments-o" data-toggle="tooltip" title="Comments"></i></th>
                      <?php if(is_loan_officer()): ?>
                        <th class="action-text-2 text-center"><span data-toggle="tooltip" title="Actions"><i class="fa fa-cog"></i></span></th>
                      <?php endif; ?>
                    </tr> 
                  </thead> 
                  <tbody></tbody> 
                </table>
              </div> 
            </div>
          </div>
          <!-- END CONTENT -->
        <?php else: ?>
          <div class="table-responsive mt10"> 
            <table id="allScheduleTable" class="table table-hover" style="margin-bottom:200px !important;">
              <thead class="threadcolor"> 
                <tr>
                  <th class="date-label width-100">Schedule</th>
                  <th class="date-label width-80" id="time-label">Time</th>
                  <th class="single-row width-150">Borrower</th>
                  <th class="single-row width-100">Title Order #</th>
                  <th class="single-row width-150"><span data-toggle="tooltip" title="Loan Officer">Loan Officer</span></th>
                  <th class="single-row width-150"><span data-toggle="tooltip" title="Processor">Processor</span></th>
                  <th class="single-row width-150"><span data-toggle="tooltip" title="Underwriter">Underwriter</span></th>
                  <th class="single-row width-150">Folder</th>
                  <th class="status-text single-row">Status</th>
                  <th class="status-text single-row">Signing Company</th>
                  <th class="comment-text text-center"><i class="fa fa-comments-o" data-toggle="tooltip" title="Comments"></i></th>
                </tr> 
              </thead> 
              <tbody></tbody> 
            </table>
          </div> 
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php endif; ?>

<script type="text/x-template" id="date-search-template">
  <label>Date: </label>
  <div class="form-group">
    <div class="input-group input-group-sm">
      <input type="text" class="form-control input-sm date-from-filter" placeholder="From">
      <span class="input-group-addon cursor-pointer date-from-filter-addon">
        <span class="fa fa-calendar-check-o"></span>
      </span>
    </div>
  </div>
  <div class="form-group ml10">
    <div class="input-group input-group-sm">
      <input type="text" class="form-control input-sm date-until-filter" placeholder="To">
      <span class="input-group-addon cursor-pointer date-until-filter-addon">
        <span class="fa fa-calendar-check-o"></span>
      </span>
    </div>
  </div>
</script>

<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>


