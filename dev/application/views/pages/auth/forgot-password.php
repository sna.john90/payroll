<div style='font-family: "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;font-size: 14px;line-height: 1.42857143;color: #555555;background-color: #fdfdfd;padding: 15px;border-radius:10px;'>
	<p style='text-align: center;font-size: 25px;'><span style='display: block;
	  margin-top: 14px;font-size: 25px;'>NOTARY<span style='color: #28b62c;font-size: 25px;'>CLOUD</span></span></p>
	<hr style='margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eeeeee;box-sizing: content-box;height: 0;'/>
	<br/>
	<p>Hello,</p>

	<div>
		You have requested to reset your password of your NotaryCloud account. Please click the link below to change your password.
		<br/>
		<br/>
		<a href="{base_url}auth/reset_password/{code}">{base_url}auth/reset_password/{code}</a>
		<br/>
		<br/>
		<br/>
		<p>Sincerely,</p>
	    <p>Notary Cloud</p>
	    <p>Phone: 702-710-4660</p>
	    <p>Email: <a href="mailto:support@NotaryCloud.com">support@NotaryCloud.com</a></p>
	    <br/>
	    <br/>
	    <div style="text-align:center;">
	      <p><a href="http://notarycloud.com">www.notarycloud.com</a>&nbsp;|&nbsp;<span>Phone: (702) 710-4660</span>&nbsp;|&nbsp;<span>Email: <a href="mailto:support@notarycloud.com">support@notarycloud.com</a></span></p>
	    </div>
	</div>
</div>