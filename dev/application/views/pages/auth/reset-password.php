<div class="page-content-container">
	<div class="modal-flipper mt30">
		<div class="flip-container">
	      <div id='loginCard' class="flipper register-flipper">
	        <div class="front" style="height:320px;">
	          <!-- front content -->
	          <div class="slider-login">
	            <h3 class="login-header font-thin text-center"><i class="fa fa-key"></i> RESET PASSWORD</h3>
	            <?php echo form_open(base_url()."auth/change_password/{code}", array("id" => "reset-password-form","class" => "form-minimalist")); ?>
	              <div class="message"></div>
	              <div class="form-group">
	                <div class="input-groupX">
	                  <span class="input-group-addon hidden"><i class="fa fa-key"></i></span>
	                  <input type="password" class="form-control text-left" name="new_password" placeholder="New Password">
	                </div>
	              </div>
	              <div class="form-group">
	                <div class="input-groupX">
	                  <span class="input-group-addon hidden"><i class="fa fa-key"></i></span>
	                  <input type="password" class="form-control text-left" name="confirm_new_password" placeholder="Confirm New Password">
	                </div>
	              </div>
	              <div class="text-right mt30">
	                <button type="submit" class="btn btn-success change-btn">Change Password <i class="fa fa-arrow-right"></i></button>
	              </div>
	            </form>
	          </div>
	        </div><!-- END front content -->
	      </div>
	    </div>
	</div>
</div>