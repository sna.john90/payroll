<div style='font-family: "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;font-size: 14px;line-height: 1.42857143;color: #555555;background-color: #fdfdfd;padding: 15px;border-radius:10px;'>
    <p style='text-align: center;font-size: 25px;'><span style='display: block;
      margin-top: 14px;font-size: 25px;'>NOTARY<span style='color: #28b62c;font-size: 25px;'>CLOUD</span></span></p>
    <hr style='margin-top: 20px;margin-bottom: 20px;border: 0;border-top: 1px solid #eeeeee;box-sizing: content-box;height: 0;'/>
    <br/>
	<p>Hello {first_name},</p>
	<br/>
	<p>Thank you for signing-up with Notary Cloud!</p>
	<br/>
	<p>To access your newly-created account, please refer to the following credentials:</p>
	<br/>
	<p><strong style="width:100px;">Username:</strong> {username}</p>
	<p><strong style="width:100px;">Password:</strong> {password}</p>
	<br/>
	<p>To login to Notary Cloud, visit <a href="{base_url}">{base_url}</a>. Please change you password from time to time for security purposes.</p>
	<br/>
	<p>Please reach us through email or phone for further inquiries.</p>
    <br/>
    <br/>
    <p>Thank you,</p>
    <p>Notary Cloud</p>
    <p>Phone: 702-710-4660</p>
    <p>Email: <a href="mailto:support@NotaryCloud.com">support@NotaryCloud.com</a></p>
    <br/>
    <br/>
    <div style="text-align:center;">
      <p><a href="http://notarycloud.com">www.notarycloud.com</a>&nbsp;|&nbsp;<span>Phone: (702) 710-4660</span>&nbsp;|&nbsp;<span>Email: <a href="mailto:support@notarycloud.com">support@notarycloud.com</a></span></p>
    </div>
</div>