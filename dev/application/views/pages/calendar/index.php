<div class="page-header container text-right clearfix">
  <h3 class="page-title pull-left mt15">Calendar of Signings</h3>
  <ul class="list-inline pull-right mt15">
    <li class="calendar-item">
      <span id="total-signings-label"><i class="fa fa-clone fa-fw"></i> Total Signings: <strong>0</strong></span>
    </li>
    <li class="calendar-item">
      <span id="in-transit-label"><i class="fa fa-truck fa-fw"></i> Estimated Deliveries: <strong>0</strong></span>
    </li>
    <li class="calendar-item">
      <span id="delivered-label"><i class="fa fa-bus fa-fw"></i> Delivered: <strong>0</strong></span>
    </li>
  </ul>
</div>
<div class="page-content pt0">
  <div class="container">
    <div class="row mb50 clearfix mb50">
      <div class="col-xs-12">

        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="schedule-tab active"><a href="javascript:void(0);" aria-controls="tab-s" role="tab" data-toggle="tab" class="titlelink">Signings</a></li>
          <li role="presentation" class="schedule-tab"><a href="{base_url}calendar/deliveries" aria-controls="tab-td" role="tab" class="titlelink">Tracked Deliveries</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- tab1-content -->
          <div role="tabpanel" class="tab-pane active" id="tab-s">

            <div class="min-height-300">
              <div id="scheduleCalendar" class="mt30" data-timezone="{timezone_local}" data-today="{date_today}"></div>
            </div>

          </div>
        </div>

      </div>
    </div>
  </div>
</div>
