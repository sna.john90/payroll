<!-- MODAL ADD CONTACT-->

<div class="modal-header">
<button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="panel-title">Add Contact</h4>
</div>
<div class="modal-body">
	<!-- {expenses_categories} -->
	<div class="form-modern row">
	      <div class="col-sm-12">
	          <ul class="list-table vmiddle list-unstyled">
	            <li>
	              <div class="list-table-label">Business Name:</div>
	              <div class="list-table-content"> <input class="form-control" placeholder=""></div>
	            </li>
	          </ul>
	      </div>
	      <div class="col-sm-12">
	      	  <h4>Contact Info</h4>
	          <ul class="list-table vmiddle list-unstyled">
	            <li>
	              <div class="list-table-label">First Name:</div>
	              <div class="list-table-content"> <input class="form-control" placeholder=""></div>
	            </li>
	            <li>
	              <div class="list-table-label">Last Name:</div>
	              <div class="list-table-content"> <input class="form-control" placeholder=""></div>
	            </li>
	            <li>
	              <div class="list-table-label">Email:</div>
	              <div class="list-table-content"> <input class="form-control" placeholder=""></div>
	            </li>
	            <li>
	              <div class="list-table-label">Phone:</div>
	              <div class="list-table-content"> <input class="form-control" placeholder=""></div>
	            </li>
	            <li>
	              <div class="list-table-label">Mobile:</div>
	              <div class="list-table-content"> <input class="form-control" placeholder=""></div>
	            </li>
	            <li>
	              <div class="list-table-label">Address:</div>
	              <div class="list-table-content"> <input class="form-control" placeholder=""></div>
	            </li>
	            <li>
	                <div class="list-table-label">Notes:</div>
	                <div class="list-table-content mt15"><textarea class="form-control"></textarea></div>
	            </li>
	          </ul>
	      </div>
	  </div>
</div>

<div class="modal-footer">
      <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
      <button type="button" class="btn btn-primary btn-sm">Add Contact</button>
  </div>