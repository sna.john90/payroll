<?php
  $names =array('Harry Portlock', 'Oliver Green','Hannah Bartlow','Majiku Dioske','Arnold Stelle','Steve Arduke','Martty Trevor','Grence Monreal', 'Amber Westley','Ross Yaellaz'); 
  $lo  =array('Ben Knudson','Ada Hamnon', 'Guy Wayman');
  $proc  =array('Calle Mcartny','Lorn Doer', 'Hoss Sandberg');
  $uw  =array('Sarrah Wong','Sherrie Davies', 'Melanie Griffin');
  $folder  =array('Scheduling','Back Proc', 'Underwriting', "VCI to VCI Processing","Maumee Underwriting","Maumee Processing","Maumee Initial Processing","Closing Prep");

  $statSked  =array('Docs Drawn (Jakie)','DD-Derrick', 'Error','Not Clear To Close','Reschedule');
  $statSkedLabel  =array('DD-Jackie','DD-Derrick', 'Error','NCTC','Reschedule');
  $statSkedColor  =array('btn-success','btn-primary', 'btn-warning','btn-danger','btn-info');

  $pstate  =array('Las Vegas, NV','Elko, NV', 'Reno, NV', 'Fallon, NV','Virginia City, NV','Lovelock, NV','Goldfield, NV','Tonopah, NV','Eureka, NV');

  $lender  =array('VCI','WellsFargo', 'Citi');
  $lenderLogo  =array('assets/images/logo-vci.jpg','assets/images/logo-wf.jpg', 'assets/images/logo-citi.png');

  $notary  =array('Allen Sheehan','Deborah Wilkinson', 'Joshua Wood');
  $notaryIcon  =array('fa-check-circle','fa-circle-o');
  $notaryColor  =array('color-success','color-default');
  $notaryStat  =array('Signing Complete',"Borrower Didn't Sign","Signed Docs Sent to Lender" );
  $notaryStatColor  =array('btn-success','btn-danger','btn-primary');
  $notaryStatColorText  =array('text-success','text-danger','text-primary');

  $userGroup  =array('Admin','Loan Officer','Processor','Underwriter','Notary','Subscriber');
?>
  
    <div class="page-header container text-right clearfix">
        <h1 class="page-title mt10 pull-left">Dashboard</h1>
        
        <ul class="list-inline pull-right">
          <li><a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#noxModal"><i class="fa fa-plus-circle"></i> Schedule Signing</a></li>
        </ul>  
    </div> 
    <div class="page-content pt0">
      <div class="dashboard-panel mb30 pt30">
        <div class="container">
          <!-- summary -->      
          <div class="row">
            <div class="col-sm-3">
                <div class="panel">
                    <div class="panel-body panel-oh bg-warning text-lite">
                       <div class="dash-icon text-warning pull-left"><i class="fa fa-calendar fa-4x"></i></div>
                       <div class="text-right">
                          <h2 class="dash-title mt5">7</h2>
                          <div class="dash-label font-sm">DUE TODAY <a href="#" style="color:#fff"><i class="fa fa-arrow-circle-right"></i></a></div>
                       </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-body panel-oh bg-success text-lite">
                       <div class="dash-icon text-success pull-left"><i class="fa fa-dollar fa-4x"></i></div>
                       <div class="text-right">
                          <h2 class="dash-title mt5">$24,577</h2>
                          <div class="dash-label font-sm">TOTAL SALES</div>
                       </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-body panel-oh bg-info text-lite">
                       <div class="dash-icon text-info pull-left"><i class="fa fa-shopping-cart fa-4x"></i></div>
                       <div class="text-right">
                          <h2 class="dash-title mt5">162</h2>
                          <div class="dash-label font-sm">TOTAL ORDERS</div>
                       </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
              <div class="panel panel-defaultX ">
                <div class="panel-heading bg-primary clearfix">
                  <div class="pull-right">
                      <div class="btn-group btn-group-xs" role="group" aria-label="...">
                        <a href="#" class="btn btn-default active">Day</a>
                        <a href="#" class="btn btn-default">Week</a>
                        <a href="#" class="btn btn-default">Month</a>
                      </div>
                      <button type="button" class="btn btn-default btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Print"><span class="fa fa-print"></span></button>

                    </div>
                      <h4 class="header-title text-lite"><i class="fa fa-line-chart"></i> Sales Analytics</h4> 
                  </div>



                <div class="panel-body clearfix">
                  <div class="col-sm-9">
                    <div id="morris-line" class="hiddenX" style="height:230px;"></div>
                    <div id="chart" class="hidden"><svg style="height:200px;"></svg></div>
                  </div>
                  <div class="col-sm-3">
                    <div class="avg-box bg-blue br3 text-center">
                      <div class=" text-success" style="font-size: 40px;">$3,525</div>
                      <div style="font-size: 10px;">May 1 - 31, 2016</div>
                    </div>
                    <ul class="list-unstyled mt15">
                      <li>Today <span class="pull-right">$405</span></li>
                      <li>Yesterday <span class="pull-right">$0</span></li>
                      <li>Prev Month <span class="pull-right">$2,255</span></li>
                      <li class="mt30">Customers <span class="pull-right">263</span></li>
                    </ul>
                  </div>  
                </div>
              </div>
            </div>
          </div>
        </div>
    </div> <!-- END dashboard panel -->
     <div class="container">
        <div class="row clearfix mb50">
            <!-- CONTENT -->
            <div class="col-xs-12">
               <div class="table-responsive"> 
                  <table data-page-length='15' class="tableSortableGroup table table-hover"> <thead class="threadcolor"> 
                   <tr> 
                     <th class="groupDateSort tableSortGuide">DateGuide</th>
                     <th class="groupDateSortDate tableSortGuide hidden">Date</th>
                     <th class="groupDateSortTime tableSortGuide">Time</th>
                     <th>Borrower</th>
                     <th>Loan #</th>
                     <th><span data-toggle="tooltip" title="Loan Officer">Loan Officer</span></th>
                     <th><span data-toggle="tooltip" title="Processor">Processor</span></th>
                     <th><span data-toggle="tooltip" title="Underwriter">Underwriter</span></th>
                     <th>Folder</th>
                     <th>Status</th>
                     <th class="text-center"><i class="fa fa-comments-o"></i></th>
                   </tr> 
                   </thead> 
                   <tbody> 
                   <?php for($q=1; $q<=45; $q++) { ?>
                    <?php for($k=1; $k<rand(5,10); $k++) { ?>
                   <tr> 
                        <td class="dateheader">
                            <h3 class="group-title">
                            <?php if($q==1) : ?>
                              <?php echo date('m-d-Y'); ?> <span class="label label-danger font-sm">DUE TODAY</span>
                           <?php else: ?>
                              <?php echo date('m-d-Y', mktime(0, 0, 0, date("m"), date("d")+$q, date("Y"))); ?>
                              </h3>
                           <?php endif; ?>
                        </td>
                        <td class="groupDateSortDate hidden" style="min-width: 100px;">
                            <?php if($q==1) : ?>
                                <span class="label label-danger"><?php echo date('m-d-Y'); ?> </span>
                           <?php else: ?>
                              <?php echo date('m-d-Y', mktime(0, 0, 0, date("m"), date("d")+$q, date("Y"))); ?>
                           <?php endif; ?>
                        </td>
                       <td><?php echo '0'.rand(0,9).':'.rand(10,59).'AM'; ?>
                     <td>
                        <a href="#" data-toggle="modal" data-target="#modalLoanDetail"><?php echo $names[array_rand($names)]; ?></a>
                        <div class="font-small muted">Las Vegas, NV</div>
                     </td>
                     <td>100<?php echo rand(2000,5000); ?></td>
                     <td><a href="#" data-toggle="modal" data-target="#modalProfileModern"><?php echo $lo[array_rand($lo)]; ?></a></td>
                     <td><a href="#" data-toggle="modal" data-target="#modalProfileModern"><?php echo $proc[array_rand($proc)]; ?></a></td>
                     <td><a href="#" data-toggle="modal" data-target="#modalProfileModern"><?php echo $uw[array_rand($uw)]; ?></a></td>
                     <td>
                        <div class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $folder[array_rand($folder)]; ?></a>
                            <ul class="dropdown-menu pull-right">
                              <?php foreach($folder as $key => $value) : ?>
                              <li><a href="#"><?php echo $value; ?></a></li>
                              <?php endforeach; ?>
                            </ul>
                          </div>
                     
                     </td>
                     <td>
                           <?php $randStatID =rand(0,4) ?>
                          <div class="btn-status-col dropdown" data-toggle="tooltip" title="<?php echo $statSked[$randStatID]; ?>">
                             <a href="#" class="btn-status btn btn-xs dropdown-toggle <?php echo $statSkedColor[$randStatID]; ?>"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $statSked[$randStatID]; ?>
                              <span class="caret pull-right"></span></a>
                            <ul class="dropdown-menu pull-right">
                              <li><a href="#"><span class="color-block fa fa-square text-info"></span> Reschedule</a></li>
                              <li><a href="#"><span class="color-block fa fa-square text-success"></span> Docs Drawn (Jakie)</a></li>
                              <li><a href="#"><span class="color-block fa fa-square text-primary"></span> Docs Drawn (Derrick)</a></li>
                              <li><a href="#"><span class="color-block fa fa-square text-warning"></span> Error</a></li>
                              <li><a href="#"><span class="color-block fa fa-square text-danger"></span> Not Clear To Close</a></li>
                            </ul>
                          </div>
                     </td>
                       <td><a href="#" data-toggle="modal" data-target="#modalComments" class="text-center center-block" titleX="View Comments" data-popover="true" data-content="This is a comment preview. Vivamus sagittis lacus vel augue laoreet rutrum faucibus..."><i class="fa fa-comments-o"></i> <span class="font-xs"><?php echo rand(0,9); ?></span></a></td>
                   </tr> 
                    <?php } ?>
                   <?php } ?>
                   </tbody> 
                  </table>
               </div> 

            </div>
        </div>
      </div>
    </div>  

<script type="text/javascript">
  $(document).ready(function(){
    //morris-line
    Morris.Line({
      element: 'morris-line',
      data: [
        <?php for($j=1;$j<=30;$j++){
          $randNo=rand(0,12);
          $sale=75*$randNo+5*rand(15,50);
        echo "{ y: 'May-".$j."', a: ".$sale.".00 ,customer:".$randNo."},";
        } ?>
        { y: 'May-31', a: "480.00",customer:7}
      ],
      xkey: 'y',
      ykeys: ['a','customer'],
      parseTime:false,
      xLabels:'days',
      resize:true,
      lineColors:['orange','blue'],
      labels: ['Profit','Customers']
    });    
  //*/
  });
</script>


