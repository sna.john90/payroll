<?php
#MAINTAINABLE - CATEGORY
?>

<div class="container expense-category-container">
  
	<div class="row">
	  	<div class="col-sm-12">
	  		<div class="panel panel-nox">
			    <div class="panel-heading">
					<h3 class="font-thin text-uppercase">Categories</h3>
			    </div>
			    <div class="panel-body">
			      
			      	<div class="table-responsive"> 
			      		{expenses_category_content}
				    </div> 
				    <div class="text-right mt15">
				        <button type="button" class="btn btn-primary add-expense-category"><i class="fa fa-plus-circle"></i> Add</button>
				    </div>  
			    </div>
			</div> <!-- END .panel -->

	  	</div>

	</div>

</div><!-- END: .cotainer*/


