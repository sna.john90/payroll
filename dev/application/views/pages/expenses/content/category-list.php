<table class="table table-hover">
				          <thead>
				            <tr>
				              <th>Name</th>
				              <th>Budget</th>
				              <th class="text-right">Action</th>
				            </tr>
				          </thead>
				          <tbody>
				          	<?php if(!empty($lists)): ?>
				          	<?php foreach($lists as $key=>$value): 
				          		$temp_id = encrypt($value['id']);
				          	?>
			                 <tr>   
			                    <td><?php echo $value['name'];?></td>
			                    <td><?php echo $value['estimated_amount'];?></td>
			                    <td class="text-right">
				                    <div class="btn-group btn-group-xs">
				                        <a href="#" class="btn btn-primary update-expense-category-btn" data-id="<?php echo $temp_id;?>"><i class="fa fa-pencil"></i></a>
				                        <a href="javascript:;" class="btn btn-danger delete-expense-category-btn" data-id="<?php echo $temp_id;?>"><i class="fa fa-trash-o"></i></a>
				                    </div>
			                    </td>
			                 </tr>
				                 <?php if(!empty($value['children'])): ?>
					                 <?php foreach($value['children'] as $skey=>$svalue): 
					                 	$temp_id = encrypt($svalue['id']);
					                 ?>
					                 <tr class="list-level-2">   
					                    <td><?php echo $svalue['name'];?></td>
					                    <td><?php echo $svalue['estimated_amount'];?></td>
					                    <td class="text-right">
						                    <div class="btn-group btn-group-xs">
						                        <a href="#" class="btn btn-primary update-expense-category-btn" data-id="<?php echo $temp_id;?>"><i class="fa fa-pencil"></i></a>
						                        <a href="javascript:;" class="btn btn-danger delete-expense-category-btn" data-id="<?php echo $temp_id;?>"><i class="fa fa-trash-o"></i></a>
						                    </div>
					                    </td>
					                 </tr> 
					                 <?php endforeach; ?>
				             	<?php endif;?>
			             	<?php endforeach; ?>
			             	<?php else: ?>
			             		<tr><td colspan="2" align="center">No expenses category</td></tr>
			             	<?php endif; ?>
				          </tbody>
				        </table>