
  <div class="table-responsive">
    <table id="expenseMonthlyTable" class="table table-group-colored-blue" cellspacing="0" width="100%">
      <thead>
          <tr>
              <th data-orderable="false">Particular</th>
              <th data-orderable="false">Category</th>
              <th data-orderable="false">Budget</th>
              <th data-orderable="false">Actual</th>
              <th data-orderable="false">Difference</th>
              <th data-orderable="false">Usage (%)</th>
          </tr>
      </thead>
      <tfoot>
          <tr>
              <th colspan="2">TOTAL</th>
              <th><?php echo number_format($overall_total['budget'],2);?></th>
              <th><?php echo number_format($overall_total['actual'],2);?></th>
              <th><?php echo number_format($overall_total['difference'],2);?></th>
              <th><?php echo number_format($overall_total['difference_percentage'],2);?></th>
          </tr>
      </tfoot>
      <tbody> 
          <?php   
            foreach($lists as $value): 

          ?>
          <tr>
              <td><?php echo $value['name'];?></td>
              <td><?php echo $value['type'];?></td>
              <td><?php echo number_format($value['estimated_amount'],2);?></td>
              <td><?php echo number_format($value['actual'],2);?></td>
              <td><?php echo number_format($value['difference'],2);?></td>
              <td><?php echo $value['difference_percentage'];?></td>
          </tr>
          <?php endforeach; ?>
      </tbody>
  </table>
  </div>



 <script type="text/javascript">
    $(document).ready(function() {


       /*grouped table*/ 
      var expenseTable = $('#expenseMonthlyTable').DataTable({
          "columnDefs": [
              { "visible": false, "targets": 1 }
          ],
          "order": [[ 1, 'asc' ]],
          "displayLength": 25,
          "drawCallback": function ( settings ) {
              var api = this.api();
              var rows = api.rows( {page:'current'} ).nodes();
              var last=null;
   
              api.column(1, {page:'current'} ).data().each( function ( group, i ) {
                  if ( last !== group ) {
                      $(rows).eq( i ).before(
                          '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                      );
   
                      last = group;
                  }
              } );
          },
        dom:"<'row data-table-header'<'col-xs-8'f><'col-xs-4'l>>" +
          "<'row'<'col-sm-12'tr>>"
       });
       
    });
</script>
