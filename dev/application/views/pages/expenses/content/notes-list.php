 <ul class="timeline list-unstyled">
          <?php if(!empty($notes)): ?>
          <?php foreach($notes as $note): ?>
          <li>
            <div class="photo-box pull-left"><img class="img-responsive" src="<?php echo profile_picture($note['profile_pic']); ?>"></div>
            <div class="timeline-body">
              <a href="#" class=""><?php echo ucfirst($note['firstname'])." ".ucfirst($note['lastname']); ?></a>
              <span class="font-thin font-xs pull-right"><i class="fa fa-clock-o"></i> <?php echo date('F j, Y g:i A',strtotime($note['date_added'])); ?></span>
              <div class="timeline-content note-attachment-content">
                <?php echo $note['notes'];?>
              </div>
            </div>  
          </li> 
        <?php endforeach; ?>
        <?php else: ?>
            <li>No notes added.</li>
        <?php endif;?>

        </ul>