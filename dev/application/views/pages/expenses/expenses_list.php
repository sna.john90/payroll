<?php
#ExPENSE LIST.
?>
<div class="container">
	<div class="panel panel-nox fullH">
	  <div class="panel-heading clearfix">
	      <div class="filter-option pull-right">
	            <ul class="list-inline">
	                 <li>
	                    <a href="#" class="btn btn-success btn-sm" id="add-expense-btn"><i class="fa fa-plus-circle"></i> Add Expense</a>
	                </li>
	            </ul>
	        </div>
	       <h4 class="panel-title pt10"><i class="fa fa-list-alt"></i> Expenses</h4>
	  </div>
	  <div class="panel-body">
	      <div class="mb15 clearfix">
	          <div class="filter-option pull-right">
	            <ul class="list-inline">
	                <li class="form-simple">
	                    <div class="form-inline">
	                       <div class="form-group">
	                         <label class="font-thinX">Month </label>
	                         <select class="form-control selectpicker select-simple select-simple-month select-picker-month" data-width="fit">
                           		<?php foreach(get_months() as $key=>$value): ?>
                           			<option <?php echo (($key+1)==date("m"))? "selected" : "";?> value="<?php echo $key+1; ?>"><?php echo $value; ?></option>
                           		<?php endforeach;?>
	                         </select>
	                       </div>
	                     </div>
	                </li>
	                <li class="form-simple">
	                    <div class="form-inline">
	                       <div class="form-group">
	                         <label class="font-thinX">Year </label>
	                         <select class="form-control selectpicker select-simple select-picker-year" data-width="fit">
	                            <?php for($i=(date("Y")-10);$i<=date("Y");$i++): ?>
	                            		<option <?php echo ($i==date("Y"))? "selected" : "";?> value="<?php echo $i;?>"><?php echo $i; ?></option>
	                            <?php endfor;?>
	                         </select>
	                       </div>
	                     </div>
	                </li>
	            </ul>
	        </div>
	        <div class="form-inline">
	          <label class="font-normal">Select Category: </label> 
	          <select class="form-control select-sm selectpicker select-simple  show-tick select-category" data-width="fit"  data-style="btn-primary"> 
	          		<option value="all">All</option>
            <?php foreach(get_expenses_category() as $key=>$value): ?>
                    <?php if(isset($value['children']) && !empty($value['children'])): ?>
                              <optgroup label="<?php echo $value['name']?>">
                                <?php foreach($value['children'] as $child): ?>
                                      <option value="<?php echo $child['id'];?>"><?php echo $child['name'];?></option>
                                <?php endforeach;?>
                              </optgroup>
                    <?php else: ?>
                         <option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
                    <?php endif;?>
            <?php endforeach; ?> 
                  </select>
	        </div>  
	      </div>
	    <div class="table-responsive">
	        <table class="table-nox dataTable table table-hover" id="expenseTable">
	          <thead>
	            <tr>
	              <th>Date</th>
	              <th class="text-left">Particular</th>
	              <th class="text-left">Receipt From</th>
	              <th class="text-left">Category</th>
	              <th>Amount</th>
	              <th data-orderable="false" class="text-center"><i class="fa fa-paperclip"></i></th>
	              <th data-orderable="false">Action</th>
	            </tr>
	          </thead>
	          <tbody>
	        <?php /*  <?php for($k=1;$k<=17;$k++): ?>
	            <tr>
	              <td class="text-left">2016-Nov-<?php echo rand(1,30);?></td>
	              <td class="text-left font-bold"><a href="javascript:void(0)" class="view-expense-btn">Bond Paper <?php echo $k; ?></a></td>
	              <td class="text-left">National Bookstore</td>
	              <td class="text-left">Office Supplies</td>
	              <td class="font-bold">100.00</td>
	              <td class="pay-note text-center"> 
	                <a href="#" data-toggle="modal" data-target="#modalNote"><i class="fa fa-wpforms"></i></a>
	              </td>
	              <td class="">
	                <div class="btn-group btn-group-xs">
	                    <a href="#" class="btn btn-primary edit-expense-btn" id="edit-expense-btn"><i class="fa fa-pencil"></i></a>
	                    <a href="#" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
	                </div>
	              </td>
	            </tr>
	          <?php endfor; ?> */ ?>
	          </tbody>
	        </table>

	      </div>
	        
	     <div class="filter-option pull-right mt30">
	            <ul class="list-inline">
	                 <li>
	                    <a href="#" class="btn btn-success btn-sm" id="add-expense-btn"><i class="fa fa-plus-circle"></i> Add Expense</a>
	                </li>
	            </ul>
	        </div>   
	  </div>
	</div>
</div>  

