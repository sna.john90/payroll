<?php
#Expense Monthly
?>
<div class="container expenses-monthly-summary">
  <div class="panel panel-nox fullH">
    <div class="panel-heading">
       <div class="filter-option pull-right">
            <ul class="list-inline">
                <li class="form-simple">
                    <div class="form-inline">
                       <div class="form-group">
                         <label class="font-thinX">Month </label>
                         <select class="form-control selectpicker select-simple select-picker-month" data-width="fit">
                           <?php foreach(get_months() as $key=>$value): ?>
                                <option <?php echo (($key+1)==$month)? "selected" : "";?> value="<?php echo $key+1; ?>"><?php echo $value; ?></option>
                              <?php endforeach;?>
                         </select>
                       </div>
                     </div>
                </li>
                <li class="form-simple">
                    <div class="form-inline">
                       <div class="form-group">
                         <label class="font-thinX">Year </label>
                         <select class="form-control selectpicker select-simple select-picker-year" data-width="fit">
                          <?php for($i=(date("Y")-10);$i<=date("Y");$i++): ?>
                                  <option <?php echo ($i==$year)? "selected" : "";?> value="<?php echo $i;?>"><?php echo $i; ?></option>
                              <?php endfor;?>
                         </select>
                       </div>
                     </div>
                </li>
            </ul>
	    </div>
	        
         <h4 class="panel-title pt10">Monthly Expense Summary</h4>
    </div>
    <div class="panel-body">
      
      {expenses_content_monthly}

    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
      var select_month_year = function(){
          var month = $('.select-picker-month').find("option:selected").val();
          var year  = $('.select-picker-year').find("option:selected").val();
          window.location.href = base_url+"expenses/monthly/?year="+year+"&month="+month;
      };
      $('.expenses-monthly-summary').on('change','.select-picker-month,.select-picker-year',function(){
          select_month_year();
      });
  });
</script>


