<?php
# OPERATING EXPENSE
?>
<div class="container-fluidX expenses-operating-summary">
  <div class="panel panel-nox">
    <div class="panel-body">
        <div class="form-inline pull-right">
         <div class="form-group">
           <label class="font-thinX">Year </label>
           <select class="form-control selectpicker select-simple select-picker-year" data-width="fit">
                 <?php for($i=(date("Y"));$i > date("Y")-11;$i--): ?>
                      <option <?php echo ($i==$year)? "selected" : "";?> value="<?php echo $i;?>"><?php echo $i; ?></option>
                  <?php endfor;?>
         </select>
         </div>
       </div>
        <h3 class="m0 mb15">Operating</h3>
        <div class="table-responsive mt30">
          <table id="expenseOperating" class="table table-nox" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th data-orderable="false">Particular</th>
                    <th>January</th>
                    <th>February</th>
                    <th>March</th>
                    <th>April</th>
                    <th>May</th>
                    <th>June</th>
                    <th>July</th>
                    <th>August</th>
                    <th>September</th>
                    <th>October</th>
                    <th>November</th>
                    <th>December</th>
                    <th>TOTAL</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>TOTALS</th>
                    <?php foreach($column_total as $value):?>
                    <th class="text-right"><?php echo number_format($value,2);?></th>
                    <?php endforeach; ?> 
                    <th class="text-right text-primary " style="font-size: 16px;"><?php echo number_format($grand_total,2); ?></th>
                </tr>
            </tfoot>
            <tbody> 
                <?php foreach($lists as $value): ?>
                <tr>
                    <td class="text-left font-bold"><?php echo $value['name'];?></td>
                    <?php if(!empty($value['data_permonth'])): ?>
                        <?php foreach($value['data_permonth'] as $svalue): ?>
                                <td>
                                    <?php if($svalue['total'] > 0 && $value['name']!="Salaries & Wages"): ?>
                                    <a href="#" class="view-expense-detail-btn" 
                                        data-month="<?php echo $svalue['month'];?>"
                                        data-year="<?php echo $svalue['year'];?>"
                                        data-category="<?php echo $value['id'];?>"
                                        ><?php echo number_format($svalue['total'],2);?></a>
                                    <?php else: ?>
                                        <span class="text-muted"><?php echo number_format($svalue['total'],2);?></span>
                                    <?php endif; ?>

                                    <?php if($value['name']!="Salaries & Wages"): ?>
                                    <div class="progress progress-xxs" data-toggle="tooltip" data-title="<?php echo $svalue['percent'];?>%">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $svalue['percent'];?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $svalue['percent'];?>%">
                                        </div>
                                    </div><div class="font-xs"><span class="text-muted"> of </span><?php echo $value['estimated_amount'];?></div>
                                    <?php endif; ?>
                                </td>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <td class="font-bold"><?php echo number_format($value['row_total'],2);?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        </div>
        
    </div>
  </div>
</div>    
<script type="text/javascript">
  $(document).ready(function(){
      var select_month_year = function(){ 
          var year  = $('.select-picker-year').find("option:selected").val();
          window.location.href = base_url+"expenses/operating/?year="+year;
      };
      $('.expenses-operating-summary').on('change','.select-picker-year',function(){
          select_month_year();
      });
  });
</script>

