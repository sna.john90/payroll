<?php
#PERSONNEL EXPENSE
?>

<div class="container-fluid expenses-personnel-summary">
  <div class="panel panel-nox">
    <div class="panel-body">
        <div class="form-inline pull-right">
         <div class="form-group">
           <label class="font-thinX">Year </label>
           <select class="form-control selectpicker select-simple personnel-year-dropdown" data-width="fit">
                   <?php for($i=(date("Y"));$i>(date("Y") - 11);$i--): ?>
                                        <option <?php echo ($i==$selected_year)? "selected" : "";?> value="<?php echo $i;?>"><?php echo $i; ?></option>
                                <?php endfor;?>
         </select>
         </div>
       </div>
        <h3 class="m0 mb15">Salaries & Wages</h3> 
        <div class="table-responsive">
          <table id="expensePersonnel" class="table table-nox" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th data-orderable="false"></th>
                    <th data-orderable="false">January</th>
                    <th data-orderable="false">February</th>
                    <th data-orderable="false">March</th>
                    <th data-orderable="false">April</th>
                    <th data-orderable="false">May</th>
                    <th data-orderable="false">June</th>
                    <th data-orderable="false">July</th>
                    <th data-orderable="false">August</th>
                    <th data-orderable="false">September</th>
                    <th data-orderable="false">October</th>
                    <th data-orderable="false">November</th>
                    <th data-orderable="false">December</th>
                    <th data-orderable="false" class="font-bold">Total</th>
                </tr>
            </thead> 
            <tbody>
                <?php if(!empty($lists)): ?>
                <?php foreach($lists as $value): ?>
                <tr>
                    <td class="text-left"><a href="#" class="g-view-profile" data-uid="<?php echo encrypt($value['id']); ?>"><?php echo $value["firstname"]." ".$value['lastname'];?></a></td>
                    <td><?php echo $value["month_duration"];?></td>
                    <?php
                        $total = 0; 
                        foreach($value['total_incentives'] as $val): 
                        $incentive = 0;   
                        if(is_array($val))
                        {
                            $incentive = $val['total_incentives'];
                           $total = $total+$val['total_incentives']; 
                        }
                        else
                        {
                             $total = $total+$val; 
                        }
                    ?>
                        <?php if(is_array($val)): ?>
                            <td><a href="javascript:;" class="review-payment-btn" data-payrunid="<?php echo encrypt($val['payrun_id']); ?>" data-uid="<?php echo encrypt($val['user_id']); ?>"><?php echo number_format($incentive,2); ?></a></td>
                        <?php else: ?>
                             <td><?php echo number_format($incentive,2); ?></td>
                        <?php endif; ?>
                    <?php endforeach; ?> 
                    <td class="font-bold"><?php echo number_format($total,2); ?></td>
                </tr>
                <?php endforeach;?>
                <?php endif; ?>
            </tbody>
            <tfoot class="pb30">
                <tr>
                    <th colspan="2">TOTALS</th>
                    <?php 
                     $total = 0;
                     foreach($total_pmonth as $val):
                        $total = $total+$val;
                      ?>
                    <th class="text-right"><?php echo number_format($val,2); ?></th>
                    <?php endforeach; ?> 
                    <th class="font-bold text-right"><?php echo number_format($total,2); ?></th>
                </tr>
            </tfoot>
        </table> <!-- END: #expensePersonnel -->
        </div>
        
    </div>
  </div>
</div>    



 <script type="text/javascript">
      $(document).ready(function() {

        /*personnel table- grouped by name*/
        var expensePersonnel   = $('#expensePersonnel').DataTable({
          "columnDefs": [
                { "visible": false, "targets": 0,}
            ],
            "order": [[ 0, 'asc' ]],
            "displayLength": 50,
            "drawCallback": function ( settings ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;
     
                api.column(0, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="13" class="text-left">'+group+'</td></tr>'
                        );
     
                        last = group;
                    }
                } );
            },
        dom:"<'row data-table-header'<'col-xs-8'f><'col-xs-4'l>>" +
          "<'row'<'col-sm-12'tr>>" +
          "<'row data-table-footer'<'col-sm-4 font-xs'i><'col-sm-8'p>>"
       });
       
    });
</script>
