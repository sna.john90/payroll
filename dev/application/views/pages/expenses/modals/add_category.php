<!-- MODAL ADD EXPENSE CATEGORIES-->

<div class="modal-header">
<button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="panel-title">Add Expense Category</h4>
</div>
<div class="modal-body">
	<!-- {expenses_categories} -->
	<div class="form-modern row">
	      <div class="col-sm-12">
	      	 <?php echo form_open("",array("class"=>"add-expense-category-form"));?>
	          <ul class="list-table vmiddle list-unstyled">
	            <li>
	              <div class="list-table-label">Category Name:</div>
	              <div class="list-table-content"> <input class="form-control" name="name" placeholder="Office Supply"></div>
	            </li>
	            <li>
	              <div class="list-table-label">Budget (Optional):</div>
	              <div class="list-table-content"> <input class="form-control"  name="estimated_amount" placeholder="Enter allocated budget"></div>
	            </li>
	            <li>
	              <div class="list-table-label">Parent Category:</div>
	              <div class="list-table-content">
	                <select class="form-control selectpicker select-simple" name="parent">
	                  	<option value="0">None</option>
	                  	<?php 
	                  		$cats = get_expenses_category(); 
	         			  if(!empty($cats))
	         			  {
	                  	?>
	                  	   <?php foreach($cats as $value): ?>
	                  	   			<option value="<?php echo $value['id']; ?>"><?php echo ucfirst($value['name']);?></option>
	                  	   			<?php if(!empty($value['children'])): ?>
	                  	   					<?php foreach($value['children'] as $svalue): ?>
	                  	   						<option value="<?php echo $svalue['id']; ?>">----<?php echo ucfirst($svalue['name']);?></option>
	                  	   					 <?php endforeach; ?>
	                  	   			<?php endif; ?>
	                  	   <?php endforeach; ?> 
	                 <?php } ?>
	                  </select>
	              </div>
	            </li>
	          </ul>
	          </form>
	      </div>
	  </div>
</div>

<div class="modal-footer">
  <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
  <button type="button" class="btn btn-primary btn-sm do-add-expense-category">Add Category</button>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker({
      liveSearchPlaceholder:'Search'
    });
    $('.bootstrap-select.hasAddVendorButton').on('loaded.bs.select', function (e) {
      $(this).find('.bs-searchbox').after('<a href="javascript:void(0)" class="select-add-btn add-vendor-btn"><span class="fa fa-plus-circle"></span> <span class="text">Add new Vendor</span></a>');
      console.log('addded');
    });

  })
</script>