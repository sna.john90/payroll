<!-- MODAL ADD EXPENSE -->
<style type="text/css">
  #editor {overflow:scroll; max-height:300px}
</style>
 <?php echo form_open(base_url()."expenses/do_add", array("id" => "add-expenses-form","class"=>"form-horizontal")); ?>
  <div class="modal-header bg-graylite">
    <button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="panel-title">Add Expense</h4>
  </div>
  <div class="modal-body">
   
    <div class="form-modern row table-grid">
        <div class="col-sm-7 table-grid-cell">
            <ul class="list-table vmiddle list-unstyled">
              <li>
                <div class="list-table-label">Entry Date:</div>
                <div class="list-table-content"> <input class="form-control date" name="entry_date" value=""  placeholder="<?php echo date('Y/m/d'); ?>"></div>
              </li>
              <li>
                <div class="list-table-label">Receipt From:</div>
                <div class="list-table-content"> 
                     <input class="form-control " type="text" name="receipt_from" placeholder="Ex: National Bookstore">
                    <!-- <select name="receipt_from" class="form-control selectpicker hasAddVendorButton select-simple" data-live-search="true">
                      <option>Ayala Mall</option>
                      <option>Marco Polo Hotel</option>
                      <option>National Bookstore</option>
                      <option>Nature Spring Water Inc.</option>
                      <option>Philippine Airlines</option>
                      <option selected>Robinsons Supermarket</option>
                      <option>Silicon Valley Inc.</option>
                    </select>  -->
                </div>
              </li>
              <li>
                <div class="list-table-label">Receipt Date:</div>
                <div class="list-table-content"> <input class="form-control date" name="receipt_date" placeholder="<?php echo date('Y/m/d'); ?>"></div>
              </li>
              <li>
                <div class="list-table-label">Description:</div>
                <div class="list-table-content"> <input class="form-control" name="description" placeholder="Name of Expense" value="Bond Paper"></div>
              </li>
              <li>
                <div class="list-table-label">Amount:</div>
                <div class="list-table-content"> <input class="form-control" name="amount" placeholder="Name of Expense" value="0.00"></div>
              </li>                         
              <li>
                <div class="list-table-label">Category:</div>
                <div class="list-table-content"> 
                  <select class="form-control selectpicker select-simple" name="category" data-live-search="true" data-width="fit"> 
                    <?php foreach(get_expenses_category() as $key=>$value): ?>
                            <?php if(isset($value['children']) && !empty($value['children'])): ?>
                                      <optgroup label="<?php echo $value['name']?>">
                                        <?php foreach($value['children'] as $child): ?>
                                              <option value="<?php echo $child['id'];?>"><?php echo $child['name'];?></option>
                                        <?php endforeach;?>
                                      </optgroup>
                            <?php else: ?>
                                 <option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
                            <?php endif;?>
                    <?php endforeach; ?> 
                  </select>
                  <a href="javascript:void(0)" class="add-category-btn btn"><i class="fa fa-plus-circle"></i></a>
                </div>
              </li>
              <!--<li>
                <div class="list-table-label">Note</div>
                <div class="list-table-content mt15"><textarea id="editor" name="note" class="form-control"></textarea></div>
                
              </li>-->

            </ul>
        </div>
        <div class="col-sm-5 table-grid-cell img-containerX">
          <!-- <div class="img-options">
              <a href="javascript:void(0)" class="remove-btn pull-right"><i class="fa fa-trash"></i> <span class="text-label">Remove</span></a>
              <a href="javascript:void(0)"><i class="fa fa-camera"></i> <span class="text-label">Upload</span></a>
          </div>
            <div class="img-wrap">
              <img class="img-responsive" src="{base_url}/assets/images/receipt.jpg">
            </div>
             -->
             <div class="text-label">Notes / Attachments:</div>
             <div class="mce-wrapper"><textarea id="editor" name="note" class="form-control"></textarea></div>
        </div>
    </div>
  
  </div><!-- /.modal-body -->
  <div class="modal-footer">
      <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
      <button type="submit" class="btn btn-primary btn-sm submit-btn"><i class="fa fa-plus-circle"></i> Add Expense</button>
  </div> 
    </form>
  <script type="text/javascript" src="{asset_url}lib/tinymce/tinymce.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
     $('[data-toggle="tooltip"]').tooltip();
       $('.date').datetimepicker({
        format: 'YYYY-MM-DD'
      }); 
       tinymce.init({
        selector: '#editor',
        menubar:false,
        plugins: 'image imagetools',
        height: '100%',
        //toolbar: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | link image',
        toolbar: 'undo redo | bold italic | link image',
        // enable title field in the Image dialog
        image_title: true, 
        // enable automatic uploads of images represented by blob or data URIs
        automatic_uploads: true, 
        // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
        images_upload_url: base_url+'image/tinymcepostacceptor',
        // here we add custom filepicker only to Image dialog
        file_picker_types: 'image', 
        statusbar:false,
        paste_date_images:true,
        // and here's our custom image picker
        file_picker_callback: function(cb, value, meta) {
          var input = document.createElement('input');
          input.setAttribute('type', 'file');
          input.setAttribute('accept', 'image/*');
          
          // Note: In modern browsers input[type="file"] is functional without 
          // even adding it to the DOM, but that might not be the case in some older
          // or quirky browsers like IE, so you might want to add it to the DOM
          // just in case, and visually hide it. And do not forget do remove it
          // once you do not need it anymore.

          input.onchange = function() {
            var file = this.files[0];
            
            // Note: Now we need to register the blob in TinyMCEs image blob
            // registry. In the next release this part hopefully won't be
            // necessary, as we are looking to handle it internally.
            var id = 'blobid' + (new Date()).getTime();
            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
            var blobInfo = blobCache.create(id, file);
            blobCache.add(blobInfo); 
            // call the callback and populate the Title field with the file name
            cb(blobInfo.blobUri(), { title: file.name });
          }; 
          input.click();
      }
    }); 
    $('.selectpicker').selectpicker({
      liveSearchPlaceholder:'Search'
    });
    $('.bootstrap-select.hasAddVendorButton').on('loaded.bs.select', function (e) {
      $(this).find('.bs-searchbox').after('<a href="javascript:void(0)" class="select-add-btn add-vendor-btn"><span class="fa fa-plus-circle"></span> <span class="text">Add new Vendor</span></a>');
    });


    //add expenses
     var form = $('#add-expenses-form');
    form.ajaxForm({ 
      dataType: 'json',
      beforeSend:function(){
        form.find('.submit-btn').html('<i class="fa fa-plus-circle"></i> Adding ... ');
        form.find('.submit-btn').prop("disabled",true);
      },
      success:function(response){
        form.find('.submit-btn').html('<i class="fa fa-plus-circle"></i> Add Expense');
        form.find('.submit-btn').prop("disabled",false);
        me_message_v2(response);
        console.log(response);
        if(response.error==0)
        { 
          // reload datatable users
          $("#expenseTable").DataTable().ajax.reload(null,false); 

          $("body #modalAddExpense").modal("hide");
          $("body #modalAddExpense").on('hidden.bs.modal',function () {
            $(this).data('bs.modal', null);
          });
        }
      }
    });
  })
</script>
