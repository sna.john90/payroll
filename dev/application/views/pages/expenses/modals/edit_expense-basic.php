<!-- MODAL VIEW EXPENSE -->
 <?php echo form_open(base_url()."expenses/do_update", array("id" => "update-expenses-form","class"=>"form-horizontal")); ?>
  <input type="hidden" name="id" value="<?php echo encrypt($id);?>">
  <div class="modal-header bg-graylite">
    <button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="panel-title">{description}</h4>
  </div>
  <div class="modal-body">
      
    <div class="form-modern row table-grid">
        <div class="col-sm-12 table-grid-cell">
            <ul class="list-table vmiddle list-unstyled">
              <li>
                <div class="list-table-label">Entry Date:</div>
                <div class="list-table-content"> <input class="form-control date" name="entry_date" value="{entry_date}"  /></div>
              </li>
              <li>
                <div class="list-table-label">Receipt From:</div>
                <div class="list-table-content"> 
                     <input class="form-control " type="text" name="receipt_from" value="{receipt_from}">
                    <!-- <select name="receipt_from" class="form-control selectpicker hasAddVendorButton select-simple" data-live-search="true">
                      <option>Ayala Mall</option>
                      <option>Marco Polo Hotel</option>
                      <option>National Bookstore</option>
                      <option>Nature Spring Water Inc.</option>
                      <option>Philippine Airlines</option>
                      <option selected>Robinsons Supermarket</option>
                      <option>Silicon Valley Inc.</option>
                    </select>  -->
                </div>
              </li>
              <li>
                <div class="list-table-label">Receipt Date:</div>
                <div class="list-table-content"> <input class="form-control date" name="receipt_date" value="{receipt_date}" /></div>
              </li>
              <li>
                <div class="list-table-label">Description:</div>
                <div class="list-table-content"> <input class="form-control" name="description" placeholder="Name of Expense" value="{description}"></div>
              </li>
              <li>
                <div class="list-table-label">Amount:</div>
                <div class="list-table-content"> <input class="form-control" name="amount" placeholder="Name of Expense" value="{amount}"></div>
              </li>                         
              <li>
                <div class="list-table-label">Category:</div>
                <div class="list-table-content"> 
                  <select class="form-control selectpicker select-simple" name="category" data-live-search="true" data-width="fit"> 
                    <?php foreach(get_expenses_category() as $key=>$value): 
                        $selected = ($category_id==$value['id'])? "selected" : "";
                    ?>
                            <?php if(isset($value['children']) && !empty($value['children'])): ?>
                                      <optgroup label="<?php echo $value['name']?>">
                                        <?php foreach($value['children'] as $child): 
                                          $selected = ($category_id==$svalue['id'])? "selected" : "";
                                        ?>
                                              <option <?php echo $selected; ?> value="<?php echo $child['id'];?>"><?php echo $child['name'];?></option>
                                        <?php endforeach;?>
                                      </optgroup>
                            <?php else: ?>
                                 <option <?php echo $selected; ?> value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
                            <?php endif;?>
                    <?php endforeach; ?> 
                  </select>
                  <!-- <a href="javascript:void(0)" class="add-category-btn btn"><i class="fa fa-plus-circle"></i></a> -->
                </div>
              </li>
              <!--<li>
                <div class="list-table-label">Note</div>
                <div class="list-table-content mt15"><textarea id="editor" name="note" class="form-control"></textarea></div>
                
              </li>-->

            </ul>
        </div>
       <!--  <div class="col-sm-5 table-grid-cell img-container">
            <div class="img-wrap">
              <img class="img-responsive" src="{base_url}/assets/images/receipt.jpg">
            </div>
        </div> -->
    </div>
        
  </div><!-- /.modal-body -->
  <div class="modal-footer">
     <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
      <button type="submit" class="btn btn-primary btn-sm update-btn"><i class="fa fa-pencil"></i> Update Expense</button>
  </div>
</form>
  <script type="text/javascript">
    
    $(document).ready(function(){

        $('.selectpicker').selectpicker({
      liveSearchPlaceholder:'Search'
    });
         var form = $('#update-expenses-form');
    form.ajaxForm({ 
      dataType: 'json',
      beforeSend:function(){
        form.find('.submit-btn').html('<i class="fa fa-pencil"></i> Updating ... ');
        form.find('.submit-btn').prop("disabled",true);
      },
      success:function(response){
        form.find('.submit-btn').html('<i class="fa fa-pencil"></i> Update Expense');
        form.find('.submit-btn').prop("disabled",false);
        me_message_v2(response); 
        if(response.error==0)
        { 
          // reload datatable users
          $("#expenseTable").DataTable().ajax.reload(null,false); 

          $("body #modalExpenseDetail").modal("hide");
          $("body #modalExpenseDetail").on('hidden.bs.modal',function () {
            $(this).data('bs.modal', null);
          });
        }
      }
    });
    });
  </script>
