<!-- MODAL EDIT EXPENSE -->

  <div class="modal-header bg-graylite">
    <button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="panel-title">Edit Expense</h4>
  </div>
  <div class="modal-body">
      
    <div class="form-modern row table-grid">
        <div class="col-sm-7 table-grid-cell">
            <ul class="list-table vmiddle list-unstyled">
              <li>
                <div class="list-table-label">Entry Date:</div>
                <div class="list-table-content"> <input class="form-control" placeholder="<?php echo date('Y/m/d'); ?>"></div>
              </li>
              <li>
                <div class="list-table-label">Receipt From:</div>
                <div class="list-table-content"> 
                    <input class="form-control hidden" placeholder="Ex: National Bookstore">
                    <select class="form-control selectpicker hasAddVendorButton select-simple" data-live-search="true">
                      <option>Ayala Mall</option>
                      <option>Marco Polo Hotel</option>
                      <option>National Bookstore</option>
                      <option>Nature Spring Water Inc.</option>
                      <option>Philippine Airlines</option>
                      <option selected>Robinsons Supermarket</option>
                      <option>Silicon Valley Inc.</option>
                    </select>
                </div>
              </li>
              <li>
                <div class="list-table-label">Receipt Date:</div>
                <div class="list-table-content"> <input class="form-control" placeholder="<?php echo date('Y/m/d'); ?>"></div>
              </li>
              <li>
                <div class="list-table-label">Description:</div>
                <div class="list-table-content"> <input class="form-control" placeholder="Name of Expense" value="Bond Paper"></div>
              </li>
              <li>
                <div class="list-table-label">Amount:</div>
                <div class="list-table-content"> <input class="form-control" placeholder="Name of Expense" value="0.00"></div>
              </li>                         
              <li>
                <div class="list-table-label">Category:</div>
                <div class="list-table-content"> 
                  <select class="form-control selectpicker select-simple" data-live-search="true" data-width="fit">
                    <option selected="">Office Supplies</option>
                    <option>Office Rental</option>
                    <optgroup label="Utilities">
                        <option>Internet</option>
                        <option>Electricity</option>
                    </optgroup>
                      <option>Transportation</option>
                      <option>Repair &amp; Maintenance</option>
                      <option>Representation</option>
                      <option>Others</option>
                  </select>
                  <a href="javascript:void(0)" class="add-category-btn btn"><i class="fa fa-plus-circle"></i></a>
                </div>
              </li>
              <li>
                <div class="list-table-label">Note</div>
                <div class="list-table-content mt15"><textarea class="form-control"></textarea></div>
                
              </li>

            </ul>
        </div>
        <div class="col-sm-5 table-grid-cell img-container">
          <div class="img-options">
              <a href="javascript:void(0)" class="remove-btn pull-right"><i class="fa fa-trash"></i> <span class="text-label">Remove</span></a>
              <a href="javascript:void(0)"><i class="fa fa-camera"></i> <span class="text-label">Upload</span></a>
          </div>
            <div class="img-wrap">
              <img class="img-responsive" src="{base_url}/assets/images/receipt.jpg">
            </div>
        </div>

    </div>
        
  </div><!-- /.modal-body -->
  <div class="modal-footer">
      <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
      <button type="button" class="btn btn-primary btn-sm">Save Changes</button>
  </div>


<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker({
      liveSearchPlaceholder:'Search'
    });
    $('.bootstrap-select.hasAddVendorButton').on('loaded.bs.select', function (e) {
      $(this).find('.bs-searchbox').after('<a href="javascript:void(0)" class="select-add-btn add-vendor-btn"><span class="fa fa-plus-circle"></span> <span class="text">Add new Vendor</span></a>');
      console.log('addded');
    });

  })
</script>