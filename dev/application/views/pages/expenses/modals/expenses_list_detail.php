<?php
/*
NOTE: this is the same as view/expenses/expense_list.php
 except this is a popup for: 
  -specific expense category
  -specific month/year
*/
?>


<!-- MODAL EXPENSE LIST DETAILS -->

<div class="modal-header bg-graylite">
<button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 	<!-- <div class="filter-option pull-right mt5 mr20">
        <ul class="list-inline">
             <li>
                <a href="#" class="btn btn-success btn-sm add-expense-btn"><i class="fa fa-plus-circle"></i> Add Expense</a>
            </li>
        </ul>
    </div> -->
   <h4 class="panel-title pt10">[{category_name}] <span class="label label-info">{month_name}</span></h4>
</div>
<div class="modal-body">
  
<div class="table-responsive">
    <table class="table-nox dataTable table table-hover">
      <thead>
        <tr>
          <th>Date</th>
          <th class="text-left">Description</th>
          <th class="text-left">Category</th>
          <th>Amount</th> 
        </tr>
      </thead>
      <tbody>
      <?php foreach($lists as $value): ?>
        <tr>
          <td class="text-left"><?php echo date("F m, Y",strtotime($value['receipt_date']));?></td>
          <td class="text-left"><?php echo $value['description'];?></td>
          <td class="text-left"><?php echo $value['category_name'];?></td>
          <td class="font-bold"><?php echo number_format($value['amount'],2); ?></td> 
        </tr>
      <?php endforeach; ?>
      </tbody>
    </table>

  </div>            
</div><!-- /.modal-body -->
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
</div>

