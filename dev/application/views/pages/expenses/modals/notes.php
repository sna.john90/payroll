<div class="modal-header bg-graylite">
        <button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="panel-title">Notes</h4>
      </div>
      <div class="modal-body">
        <div class="note-list-container">{note_lists}</div>
        <?php if(!isset($comment_setting)): ?>
        <?php echo form_open(base_url()."expenses/do_addnotes", array("id" => "add-expensesnote-form","class"=>"form-horizontal")); ?>
          <div class="section-divider"></div>
           <div class="reply-box clearfix">
              <input type="hidden" name="expenseid" value="<?php echo $target; ?>">
              <textarea class="form-control" id="editor" name="notes"   ></textarea>
              <div class="clearfix"><a data-toggle="tooltip" title="Send" class="reply-box-btn" href="#"><i class="fa fa-send"></i></a></div>
           </div>
         </form>
       <?php endif; ?>
      </div>
    </div><!-- /.modal-content -->
    <style type="text/css">
      .note-attachment-content IMG{
        max-width: 100%;
      }
    </style>
    <script type="text/javascript" src="{asset_url}lib/tinymce/tinymce.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
     $('[data-toggle="tooltip"]').tooltip();
       tinymce.init({
        selector: '#editor',
        menubar:false,
        plugins: 'image imagetools',
        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | link image',
        // enable title field in the Image dialog
        image_title: true,
        // enable automatic uploads of images represented by blob or data URIs
        automatic_uploads: true,
        // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
        images_upload_url: base_url+'image/tinymcepostacceptor',
        // here we add custom filepicker only to Image dialog
        file_picker_types: 'image',
        statusbar:false,
        paste_date_images:true,
        // and here's our custom image picker
        file_picker_callback: function(cb, value, meta) {
          var input = document.createElement('input');
          input.setAttribute('type', 'file');
          input.setAttribute('accept', 'image/*');

          // Note: In modern browsers input[type="file"] is functional without
          // even adding it to the DOM, but that might not be the case in some older
          // or quirky browsers like IE, so you might want to add it to the DOM
          // just in case, and visually hide it. And do not forget do remove it
          // once you do not need it anymore.

          input.onchange = function() {
            var file = this.files[0];

            // Note: Now we need to register the blob in TinyMCEs image blob
            // registry. In the next release this part hopefully won't be
            // necessary, as we are looking to handle it internally.
            var id = 'blobid' + (new Date()).getTime();
            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
            var blobInfo = blobCache.create(id, file);
            blobCache.add(blobInfo);
            // call the callback and populate the Title field with the file name
            cb(blobInfo.blobUri(), { title: file.name });
          };
          input.click();
      }
    });

    //trigger submit form
    $('.reply-box-btn').on('click',function(){
        tinyMCE.get("editor").save();
        $("#add-expensesnote-form").submit();
    });

    //add expenses
     var form = $('#add-expensesnote-form');
    form.ajaxForm({
      dataType: 'json',
      beforeSend:function(){
        me_message_v2({error:2,message:"Adding notes ..."});
      },
      success:function(response){
        me_message_v2(response);
        if(response.error==0)
        {
          $(".note-list-container").html(response.data);
          tinyMCE.activeEditor.setContent('');
          $("#expenseTable").DataTable().ajax.reload(null,false);
        }
      }
    });
  })
</script>
