<!-- MODAL VIEW EXPENSE -->

  <div class="modal-header bg-graylite">
    <button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="panel-title">{description}</h4>
  </div>
  <div class="modal-body">
      
    <div class="form-modern row table-grid">
        <div class="col-sm-12 table-grid-cell">
            <ul class="list-table vmiddle list-unstyled">
              <li>
                <div class="list-table-label">Entry Date:</div>
                <div class="list-table-content"><div class="data-container">{entry_date}</div></div>
              </li>
              <li>
                <div class="list-table-label">Receipt From:</div>
                <div class="list-table-content"><div class="data-container">{receipt_from}</div></div>
              </li>
              <li>
                <div class="list-table-label">Receipt Date:</div>
                <div class="list-table-content"><div class="data-container">{receipt_date}</div></div>
              </li>
              <li>
                <div class="list-table-label">Description:</div>
                <div class="list-table-content"><div class="data-container">{description}</div></div>
              </li>
              <li>
                <div class="list-table-label">Amount:</div>
                <div class="list-table-content"><div class="data-container">{amount}</div></div>
              </li>                         
              <li>
                <div class="list-table-label">Category:</div>
                <div class="list-table-content"><div class="data-container"><?php echo get_expenses_category($category_id)['name'];?></div></div>
              </li>
              <!-- <li>
                <div class="list-table-label">Note</div>
                <div class="list-table-content mt15"><div class="data-container h100"></div></div>
                
              </li>
 -->
            </ul>
        </div>
       <!--  <div class="col-sm-5 table-grid-cell img-container">
            <div class="img-wrap">
              <img class="img-responsive" src="{base_url}/assets/images/receipt.jpg">
            </div>
        </div> -->
    </div>
        
  </div><!-- /.modal-body -->
  <div class="modal-footer">
      <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
  </div>
