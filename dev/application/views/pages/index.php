<style>
body{
  background: url(assets/bg/bg<?php echo rand(1,4);?>.jpg) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
#main{
  margin-top: 0px;
  height: 95.4vh;
}
</style> 
<div class="bg-overlay">
  <div class="flexbox">
    <div class="container flexbox-item">
      <div class="login-page container-xs text-white">
        <h2 class="text-white mb30">Login</h2>
        <?php echo form_open(base_url()."auth/login/{redirect_url}", array("id" => "login-form","class" => "form-cozy form-cozy-sunken")); ?> 
            <div class="message"></div>
              <div class="form-group">
                <div class="input-group">
                  <input type="text" class="form-control form-control-sunkenX" placeholder="Username" name="username">
                  <div class="input-group-addon"><i class="fa fa-user"></i></div>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <input type="password" class="form-control form-control-sunkenX" placeholder="Password" name="password">
                  <div class="input-group-addon"><i class="fa fa-key"></i></div>
                </div>
              </div>
              <div class="text-right clearfix">
                <a href="#" class="text-white mt5 pull-left hidden">Forgot password?</a>
                <button type="submit" class="btn btn-primary">Log In <i class="fa fa-sign-in"></i></button>
              </div>
          </form>
      </div>
    </div>
  </div>
</div>




