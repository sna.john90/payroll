<button type="button" class="close close-tr text-dark" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
<div class="modal-header bg-graylite clearfix">
  <div class="table-grid">
    <div class="table-grid-cell" style="width:150px">
      <div class="btn-options-wrap img-avatar">
        <img class="img-responsive pull-left" src="{company_pic}" width="150" />
      </div>
    </div> 
    <div class="table-grid-cell vcenter pl15">
      <div class="btn-options-wrap">
        <h3 class="font-thin mt0 mb0" style="padding:5px">{name}</h3>
      </div>
      <div class="slogan btn-options-wrap mt15" style="padding:5px">{description}</div>
    </div>
  </div>  
</div>
<div class="modal-body">
  <div class="mb15 pb15" style="border-bottom: 1px solid #f2f2f2">
    <h4>Company Information</h4>
    <ul class="contact-list btn-options-wrap list-unstyled">
      <li data-toggle="tooltip" data-placement="left" title="Address"><span class="fa fa-map-marker"></span> <span>{address}</span></li>
      <li data-toggle="tooltip" data-placement="left" title="Email Address"><span class="fa fa-envelope-o"></span> <span><a href="mailto:{email}">{email}</a></span></li>
      <?php if($website!=""): ?>
        <li data-toggle="tooltip" data-placement="left" title="website"><span class="fa fa-globe"></span> <span>{website}</span></li>
      <?php endif; ?>
      <?php if($telephone_number!=""): ?>
        <li data-toggle="tooltip" data-placement="left" title="Telephone"><span class="fa fa-phone"></span> <span>{telephone_number}</span></li>
      <?php endif; ?>
      <?php if($fax_number!=""): ?>
        <li data-toggle="tooltip" data-placement="left" title="Fax No."><span class="fa fa-fax"></span> <span>{fax_number}</span></li>
      <?php endif; ?>
    </ul>
  </div>
</div>
