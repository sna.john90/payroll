<div id="cropImageModal" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" id="close-image-modal" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <div class="row pt20">
        <div class="profile-preview-container m20">
          <img id="preview-image">
        </div>
      </div>
      <div class="mt10 text-center">
        <button id="change-image" type="button" class="btn btn btn-primary"><span class="fa fa-image"></span> Change Image</button>
        <button id="save-image" type="button" class="btn btn btn-success" data-dismiss="modal"><span class="fa fa-save"></span> Save Cropped Image</button>
      </div>
    </div>
  </div>
</div>