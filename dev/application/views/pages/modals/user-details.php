<div class="modal-header bg-blue clearfix">
  <button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
  <div class="table-grid">
    <div class="table-grid-cell" style="width:150px">
      <div class="btn-options-wrap img-avatar img-circle">
        <img class="img-responsive pull-left" src="{profile_pic}" width="150" />
      </div>
    </div>  
    <div class="table-grid-cell vcenter pl15">
      <div class="btn-options-wrap">
        <h3 class="font-thin mt0 mb0 p5">{name}</h3>
      </div>
      <div class="slogan btn-options-wrap mt15 p5">{description}</div>
    </div>
  </div>
</div>
<div class="modal-body">
  <div class="mb15 pb15" style="border-bottom: 1px solid #f2f2f2">
    <ul class="contact-list btn-options-wrap list-unstyled">
      <?php if($company_name!=""): ?>
        <li data-toggle="tooltip" data-placement="left" title="Company"><span class="fa fa-university"></span> <span>{company_name}</span></li>
      <?php endif; ?>
      <?php if($address!=""): ?>
        <li data-toggle="tooltip" data-placement="left" title="Address"><span class="fa fa-map-marker"></span> <span>{address}</span></li>
      <?php endif; ?>
      <li data-toggle="tooltip" data-placement="left" title="Email Address"><span class="fa fa-envelope-o"></span> <span><a href="mailto:{email}">{email}</a></span></li>
      <?php if($work_phone!=""): ?>
        <li data-toggle="tooltip" data-placement="left" title="Office Phone No."><span class="fa fa-phone"></span> <span>{work_phone}</span></li>
      <?php endif; ?>
      <?php if($cell_phone!=""): ?>
        <li data-toggle="tooltip" data-placement="left" title="Cell Phone No."><span class="fa fa-mobile"></span> <span>{cell_phone}</span></li>
      <?php endif; ?>
    </ul>
  </div>
</div>