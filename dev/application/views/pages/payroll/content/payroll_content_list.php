<?php
#PAYROLL LIST
?>
<div class="mb30">Process new or pending pay runs.</div>
<div class="table-responsive">
  <table class="table-nox table-payroll table table-hover payrun-lists">
    <thead>
      <tr>
        <th class="text-left">Pay Date</th>
        <th class="text-left">Description</th>
        <th class="text-left">Payroll #</th>
        <th class="text-left">Pay Period</th>
        <th class="text-left">Personnels</th>
        <th class="text-left">Status</th>
        <th class="">Total Cost</th>
         <th class="font-bold">Deductions</th>
        <th class="font-bold">Total Pay</th>
        <th class="text-center"><i class="fa fa-wpforms"></i></th>
        <th class="text-center">Action</th>
      </tr>
    </thead>
    <tbody>

   <?php foreach($payruns as $value): ?>
     <?php
        $link = base_url('payroll/view-payrun/'.encrypt($value['id']));
        if($value['status']==0)
        {
          $link = base_url('payroll/payrun-employees/'.encrypt($value['id']));
        }
     ?>
      <tr class="valign-middle">
        <td class="pay-date">
        <a href="<?php echo $link;?>"><?php echo $value['payroll_date'];?></a>
      </td>
        <td class="text-left"><?php echo $value['description'];?></td>
        <td class="text-left"><a href="<?php echo $link;?>"><?php echo $value['payroll_number'];?></a></td>
        <td class="text-left"><?php echo $value['period_from'];?> <span class="text-label">—</span> <?php echo $value['period_to'];?></td>
        <td class="text-left"><?php echo payrun_personnel_count($value['id']); ?></td>
        <td class="text-left">
            <?php  if($value['status']==0):  ?><button type="button" class="btn btn-xs btn-warning"> <i class="fa fa-bookmark-o"></i> Draft</div> <?php endif; ?>
            <?php  if($value['status']==1):  ?><div class="btn btn-xs btn-success"><i class="fa fa-check"></i> Complete</div></td><?php endif; ?>
        <td class=""><?php echo number_format(payrun_total($value['id']),2); ?></td> 
        <td class="text-danger"><?php echo number_format(payrun_total($value['id'],"deduction"),2); ?>
        <td class="pay-total"><?php echo number_format(payrun_total($value['id'],"pay"),2); ?>
        </td>
        <td class="pay-note text-center">
          <a href="#" class="notes" data-id="<?php echo encrypt($value['id']); ?>" data-toggle="modal" data-target="#modalNote"><i class="fa fa-wpforms"></i></a>
        </td>
        <td class="text-center">
          <div class="btn-group-xs">
            <a href="javascript:void(0)" data-payrun-id="<?php echo encrypt($value['id']); ?>" class="remove-payment-btn btn btn-danger" data-toggle="tooltip" data-container="body" data-title="Delete" data-placement="left"><i class="fa fa-close"></i>   </a>
          </div>
        </td>

      </tr>
    <?php endforeach; ?>
    </tbody>
  </table>

</div>
