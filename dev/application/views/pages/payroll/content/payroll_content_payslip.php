<!-- PAYSLIP SECTION -->
<div class="panel">
	<div class="panel-body">
	    <div class="row">
	    	<div class="col-sm-6">
	    		<div class="p15">
		    		<h3 class="m0"><?php echo $company_details['name'];?></h3>
		    		<div><?php echo $company_details['address'];?></div>
		    		<div><?php echo $company_details['telephone_number'];?></div>
	    		</div>
	    	</div>
	    	<div class="col-sm-6 text-right">
	    		<img class="img-responsive mt10" style="margin-left: auto;height:60px"   src="<?php echo company_picture($company_details['company_pic']);?>" />
	    	</div>
	    </div>

	    <div class="row mt30">
	    	<div class="col-sm-6">
	    		<ul class="list-table vmiddle list-table-payslip list-unstyled pl15 pr15">
			        <li>
			          <div class="list-table-label">Name:</div>
			          <div class="list-table-content font-bold"><?php echo ucfirst($get_employee_details['firstname'])." ".ucfirst($get_employee_details['lastname']);?></div>
			        </li>
			        <li>
			          <div class="list-table-label">Designation:</div>
			          <div class="list-table-content"><?php echo ucfirst(trim($get_employee_details['designation']))?></div>
			        </li>
			        <li>
			          <div class="list-table-label">Department:</div>
			          <div class="list-table-content"><?php echo ucfirst(trim($get_employee_details['name']))?></div>
			        </li>
			        <li>
			          <div class="list-table-label">Employee ID: </div>
			          <div class="list-table-content"><?php echo ucfirst(trim($get_employee_details['emp_id']))?></div>
			        </li>
			      </ul>
	    	</div>
	    	<div class="col-sm-6">
	    		<ul class="list-table vmiddle list-table-payslip list-unstyled pl15 pr15">
			        <li>
			          <div class="list-table-label">Pay Date:</div>
			          <div class="list-table-content"><?php echo format_date($payrun_details['payroll_date'],"M. j, Y") ?></div>
			        </li>
			        <li>
			          <div class="list-table-label">Pay Period: </div>
			          <div class="list-table-content"><?php echo format_date($payrun_details['period_from'],"M. j, Y")." - ".format_date($payrun_details['period_to'],"M. j, Y"); ?></div>
			        </li>
			        <li>
			          <div class="list-table-label">Pay Mode: </div>
			          <div class="list-table-content"><?php echo ucfirst(trim($get_employee_details['payment_type']))?></div>
			        </li>
			        <!-- <li>
			          <div class="list-table-label">Days Worked:</div>
			          <div class="list-table-content">23.0</div>
			        </li> -->
			      </ul>
	    	</div>
	    </div>
	</div>
</div>
<?php echo form_open(base_url()."payroll/payrun/rpayment-review-update/{payrun_id}/{user_id}",array("id" => "review-payment-form")); ?>
<div class="row">
	<div class="col-sm-6 p5">
	    <div class="panelX panel-noborder">
	    	<div class="panel-body">
	    		<table class="table form-modern table-incentives">
	    			<thead>
	    				<tr class="dark">
	    					<th class="payslip-label-th">EARNINGS</th>
	    					<th class="payslip-amount-th">AMOUNT</th>
	    				</tr>
	    			</thead>
		    		<tbody>
		    			<tr class="s-last">
		    				<td class="payslip-label">Basic Pay</td>
		    				<td class="payslip-amount">
		    					<div class="input-group">
											<?php if($editable): ?>
					                <input id="basicPay" name="incentives[basic_pay]" type="text" class="form-control text-right updateTaxComputation cls-basic-pay" value="<?php echo $earnings['basic_pay'];?>">
										  <?php else: ?>
													<?php echo $earnings['basic_pay'];?>
											<?php endif; ?>
					            </div>
		    				</td>
		    			</tr>
		    		<!-- 	<tr>
		    				<td class="payslip-label">Holiday Pay</td>
		    				<td class="payslip-amount">
		    					<div class="input-group">
					                <input id="holidayPay" type="text" class="form-control text-right updateTaxComputation" value="500.00">
					                <a href="#" class="field-minus-btn input-group-addon" data-toggle="tooltip" title="Delete"><i class="fa fa-minus-circle"></i></a>
					            </div>
		    				</td>
		    			</tr>
		    			<tr>
		    				<td class="payslip-label">Overtime Pay</td>
		    				<td class="payslip-amount">
		    					<div class="input-group">
					                <input id="overtimePay" type="text" class="form-control text-right updateTaxComputation" value="250.00">
					                <a href="#" class="field-minus-btn input-group-addon" data-toggle="tooltip" title="Delete"><i class="fa fa-minus-circle"></i></a>
					            </div>
		    				</td>
		    			</tr>
		    			<tr>
		    				<td class="payslip-label">Nigh-shift Differential</td>
		    				<td class="payslip-amount">
		    					<div class="input-group">
					                <input id="nightDifferential" type="text" class="form-control text-right updateTaxComputation" value="700.00">
					                <a href="#" class="field-minus-btn input-group-addon" data-toggle="tooltip" title="Delete"><i class="fa fa-minus-circle"></i></a>
					            </div>
		    				</td>
		    			</tr> -->
							<?php foreach ($earnings as $key => $value): ?>
								<?php if(!in_array($key,types_default())): ?>
								<tr class="s-last">
									<td class="payslip-label"><?php echo $pay_types[$key];?></td>
									<td class="payslip-amount">
										<div class="input-group">
													<?php if($editable): ?>
														<input  name="incentives[<?php echo $key; ?>]" type="text" class="form-control text-right" value="<?php echo $value; ?>" />
														<a href="#" class="field-minus-btn input-group-addon"><i class="fa fa-minus-circle"></i></a>
													<?php else: ?>
														<?php echo $value;?>
													<?php endif;?>
												</div>
									</td>
								</tr>
							<?php endif;?>
							<?php endforeach; ?>
							<?php if($editable): ?>
		    			<tr class="cls-incentive-wrapper">
		    				<td class="payslip-label">
		    					<select class="form-control cls-incentive-list"   data-live-search="true" data-container="body">
		                          	<?php foreach(get_payroll_mnt() as $key=>$value): ?>
		                          		<option value="<?php echo $value['name'];?>"><?php echo $value['description'];?></option>
		                          	<?php endforeach; ?>
		                         </select>
		    				</td>
		    				<td class="payslip-amount">
		    					<div class="input-group">
					                <input id="" type="text" class="form-control text-right cls-incentive-input" value="0.00">
					                <a href="#" class="field-plus-btn input-group-addon" data-toggle="tooltip" title="Add"><i class="fa fa-plus-circle"></i></a>
					            </div>
		    				</td>
		    			</tr>
						<?php endif; ?>
		    			<tr class="">
		    				<td class="payslip-label font-bold">TOTAL EARNINGS</td>
		    				<td class="payslip-amount font-bold total-earnings-wrapper">{total_income}</td>
		    			</tr>
		    		</tbody>
	    		</table>
			</div>
	    </div>

	</div>
	<div class="col-sm-6 p5">
		<div class="panelX panel-noborder">
	    	<div class="panel-body">
	    		<table class="table form-modern table-deductions">
	    			<thead>
	    				<tr class="dark">
	    					<th class="payslip-label-th">DEDUCTIONS</th>
	    					<th class="payslip-amount-th">AMOUNT</th>
	    				</tr>
	    			</thead>
		    		<tbody>
		    			<tr>
		    				<td class="payslip-label">Income Tax</td>
		    				<td class="payslip-amount">
		    					<div class="input-group">
		    						<?php if($editable): ?>
					                <input id="totalTax" name="deductions[income_tax]" type="text" class="form-control text-right updateTaxComputation" value="<?php echo $deductions['income_tax'];?>">
					            	<?php else: ?>
					            			<?php echo $deductions['income_tax']?>
					            	<?php endif; ?>
					            </div>
		    				</td>
		    			</tr>
		    			<tr>
		    				<td class="payslip-label">SSS Contribution</td>
		    				<td class="payslip-amount">
		    					<div class="input-group">
		    						<?php if($editable): ?>
					                <input id="sss" type="text" name="deductions[sss]" class="form-control text-right updateTaxComputation" value="<?php echo $deductions['sss'];?>">
					            	<?php else: ?>
					            			<?php echo $deductions['sss']?>
					            	<?php endif; ?>
					            </div>
		    				</td>
		    			</tr>
		    			<tr>
		    				<td class="payslip-label">Philhealth Contribution</td>
		    				<td class="payslip-amount">
		    					<div class="input-group">
		    						<?php if($editable): ?>
					                <input id="philhealth" name="deductions[philhealth]" type="text" class="form-control text-right updateTaxComputation" value="<?php echo $deductions['philhealth'];?>">
					            	<?php else: ?>
					            			<?php echo $deductions['philhealth']?>
					            	<?php endif; ?>
					            </div>
		    				</td>
		    			</tr>
		    			<tr class="s-last">
		    				<td class="payslip-label">Pag-ibig Contribution</td>
		    				<td class="payslip-amount">
		    					<div class="input-group">
		    						<?php if($editable): ?>
					                <input id="pagibig" name="deductions[pagibig]" type="text" class="form-control text-right updateTaxComputation" value="<?php echo $deductions['pagibig'];?>">
					            	<?php else: ?>
					            			<?php echo $deductions['pagibig']?>
					            	<?php endif; ?>
					            </div>
		    				</td>
		    			</tr>
							<?php foreach ($deductions as $key => $value): ?>
								<?php if(!in_array($key,types_default("deductions"))): ?>
								<tr class="s-last">
									<td class="payslip-label"><?php echo $pay_types[$key];?></td>
									<td class="payslip-amount">
										<div class="input-group">
														<?php if($editable): ?>
														<input name="deductions[<?php echo $key; ?>]" type="text" class="form-control text-right" value="<?php echo $value; ?>" />
														<a href="#" class="field-minus-btn input-group-addon"><i class="fa fa-minus-circle"></i></a>
													<?php else: ?>
														<?php echo $value; ?>
													<?php endif;?>
												</div>
									</td>
								</tr>
							<?php endif;?>
							<?php endforeach; ?>
							<?php if($editable): ?>
		    			<tr class="cls-deduction-wrapper">
		    				<td class="payslip-label">
		    					<select class="form-control cls-deduction-list" data-live-search="true" data-container="body">
		                           	<?php foreach(get_payroll_mnt("deduction") as $key=>$value): ?>
		                          		<option value="<?php echo $value['name'];?>"><?php echo $value['description'];?></option>
		                          	<?php endforeach; ?>
		                         </select>
		    				</td>
		    				<td class="payslip-amount">
		    					<div class="input-group">
					                <input id="" type="text" class="form-control text-right cls-deduction-input" value="0.00">
					                <a href="#" class="field-plus-btn input-group-addon" data-toggle="tooltip" title="Add"><i class="fa fa-plus-circle"></i></a>
					            </div>
		    				</td>
		    			</tr>
		    		<?php endif; ?>
		    			<tr class="">
		    				<td class="payslip-label font-bold">TOTAL DEDUCTIONS</td>
		    				<td class="payslip-amount font-bold total-deductions-wrapper">{total_deductions}</td>
		    			</tr>
		    		</tbody>
	    		</table>

			</div>
	    </div>

	</div>
</div>
</form>
<div class="panel">
	<div class="panel-body">
	    <div class="row">
	    	<div class="col-sm-6">
	    		<table class="table table-outer-border">
	    			<tbody>
		    			<tr>
		    				<td class="payslip-label">Previous Balance</td>
		    				<td class="payslip-amount">0.00</td>
		    			</tr>
		    			<tr>
		    				<td class="payslip-label">Carry Over (excess)</td>
		    				<td class="payslip-amount">0.00</td>
		    			</tr>
		    		</tbody>
	    		</table>
	    	</div>
	    	<div class="col-sm-6">
	    		<table class="table table-outer-border">
	    			<tbody>
		    			<tr class="bg-primary">
		    				<td class="payslip-label font-bold"><h3 class="m0 p15">NET PAY</h3></td>
		    				<td class="payslip-amount font-bold"><h3 class="m0 p15"><span class="font-thin">Php</span><span class="total-netpay-wrapper"> {net_pay}</span></h3></td>
		    			</tr>
		    		</tbody>
	    		</table>
	    	</div>
	    </div>

	    <div class="row">
	    	<div class="col-sm-3 mt50">
	    		<table class="table text-center">
	    			<tbody>
		    			<tr>
		    				<td class="font-bold">Employer's Signature</td>
		    			</tr>
		    		</tbody>
	    		</table>
	    	</div>
	    	<div class="col-sm-3 col-sm-offset-6 mt50">
	    		<table class="table text-center">
	    			<tbody>
		    			<tr>
		    				<td class="font-bold">Employee's Signature</td>
		    			</tr>
		    		</tbody>
	    		</table>
	    	</div>
	    </div>


	</div>
</div>
<script src="{asset_url}lib/jquery.form.min.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		$('#review-payment-form table input').inputmask({ alias : "currency", prefix: '' });
		var htmls = '<tr>'+
			'<td class="payslip-label">{field_name}</td>'+
			'<td class="payslip-amount">'+
				'<div class="input-group">'+
	                '<input  type="text" name="{type}[{field_value}]" class="form-control text-right" value="{value}">'+
	                '<a href="#" class="field-minus-btn input-group-addon"><i class="fa fa-minus-circle"></</a>'+
	            '</div>'+
			'</td>'+
		   '</tr>';
		var updateData = function(trigger){
			var trigger_handler = "";
			if(typeof trigger !="undefined")
			{
				trigger_handler = "/?trigger="+trigger;
			}
			preloader_saving_status({error:2,message:" Saving changes ..."});
			$.post("{base_url}payroll/rpayment-review-update/{payrun_id}/{user_id}"+trigger_handler,$("#review-payment-form").serialize(),function(data){
					$('.total-netpay-wrapper').text(data.data.total_netpay);
					$('.total-deductions-wrapper').text(data.data.total_deductions);
					$('.total-earnings-wrapper').text(data.data.total_earnings);
					for(var i in data.data.deductions)
					{
					  $('input[name="deductions['+i+']"]').val(data.data.deductions[i]);
					}
					preloader_saving_status(data);

			});
		};
		$('#review-payment-form').on('change blur','input',function(){
			if(!$(this).hasClass("cls-incentive-input") && !$(this).hasClass("cls-deduction-input"))
			{
				updateData();
			}
			
		});
		$('.cls-incentive-wrapper').on('click','.field-plus-btn',function(){
			var type = "incentives";
			var field_name 		= $('.cls-incentive-list option:selected').text();
			var field_value 	= $('.cls-incentive-list').val();

			var value = $('.cls-incentive-input').val();
			var html = htmls;
			html = html.replace("{field_name}",field_name);
			html = html.replace("{field_value}",field_value);
			html = html.replace("{value}",value);
			html = html.replace("{type}",type);

			$('.table-incentives > tbody > tr.s-last').after(html);
			$('table input').inputmask({ alias : "currency", prefix: '' });
			$('.cls-incentive-input').val("");
			updateData();

		});
		$('.cls-deduction-wrapper').on('click','.field-plus-btn',function(){
			var type = "deductions";
			var field_name 		= $('.cls-deduction-list option:selected').text();
			var field_value 	= $('.cls-deduction-list').val();

			var value = $('.cls-deduction-input').val();
			var html = htmls;
			html = html.replace("{field_name}",field_name);
			html = html.replace("{field_value}",field_value);
			html = html.replace("{value}",value);
			html = html.replace("{type}",type);

			$('.table-deductions > tbody > tr.s-last').after(html);
			$('table input').inputmask({ alias : "currency", prefix: '' });
			updateData();
			$('.cls-deduction-input').val("");

		});
		$('table').on('click','.field-minus-btn',function(){
			$(this).parents("tr").fadeOut(1000,function(){
				$(this).remove();
				updateData();
			});
		});
	});
</script>
