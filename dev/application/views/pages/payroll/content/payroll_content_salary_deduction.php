<?php
#Salary and Deductions
?>
<div class="section form-modern">
  <h3 class="section-title mb15 hidden"><i class="fa fa-money"></i> Salary &amp; Allowances</h3>
  <div class="section-content row">
    <div class="col-sm-5">
      <h4>Salary</h4>
      <ul class="list-table list-table-payslip vmiddle list-unstyled">
        <li>
          <div class="list-table-label">Basic Pay:</div>
          <div class="list-table-content font-bold"><input type="text" id="basicPay" data-value="20000" class="form-control" value="20000"></div>
        </li>
        <li>
          <div class="list-table-label">Status:</div>
          <div class="list-table-content font-bold">
            <select id="civilStatus" name="civilStatus" class="form-control" required="">
              <option value="" selected="selected"></option>
              <option value="S">Single</option>
              <option value="ME">Married</option>
              <option value="S1">Single w/ 1 Dependent</option>
              <option value="S2">Single w/ 2 Dependent</option>
              <option value="S3">Single w/ 3 Dependent</option>
              <option value="S4">Single w/ 4 Dependent</option>
              <option value="ME1">Married w/ 1 Dependent</option>
              <option value="ME2">Married w/ 2 Dependent</option>
              <option value="ME3">Married w/ 3 Dependent</option>
              <option value="ME4">Married w/ 4 Dependent</option>
            </select>
          </div>
        </li>
        <li>
          <div class="list-table-label">Pay Mode:</div>
          <div class="list-table-content">
            <select id="payMode" name="payMode" class="form-control" required="">
              <option value="weekly">Weekly</option>
              <option value="semimonthly">Semi-monthly</option>
              <option value="monthly" selected="selected">Monthly</option>
            </select>
          </div>
        </li>
        <li>
          <div class="list-table-label">TAX:</div>
          <div class="list-table-content">
            <div id="taxableAllowances" class="font-xs"></div>
            <div id="taxTotal" class="font-lg"></div>
          </div>
        </li>

      </ul>
    </div>
    <div class="col-sm-6 col-sm-offset-1">
        <h4 class="">Allowances <a href="#" class="field-add-btn  btn-simple font-md pull-right"><i class="fa fa-plus"></i></a></h4>
        <ul class="list-table vmiddle list-unstyled">
          <li>
            <div class="list-table-label">Holiday Pay</div>
            <div class="list-table-content">
              <div class="input-group">
                <input id="holidayPay" type="text" class="form-control updateTaxComputation" placeholder="Holiday Pay" value="1,500.00">
                <a href="#" class="field-minus-btn input-group-addon"><i class="fa fa-minus-circle"></i></a>
              </div>
            </div>
          </li>
          <li>
            <div class="list-table-label">Overtime Pay</div>
            <div class="list-table-content">
              <div class="input-group">
                <input id="overtimePay" type="text" class="form-control updateTaxComputation" placeholder="0.00">
                <a href="#" class="field-minus-btn input-group-addon"><i class="fa fa-minus-circle"></i></a>
              </div>
            </div>    
          </li>
          <li>
            <div class="list-table-label">Night Shift Differential</div>
            <div class="list-table-content">
              <div class="input-group">
                <input id="nightDifferential" type="text" class="form-control updateTaxComputation" placeholder="0.00">
                <a href="#" class="field-minus-btn input-group-addon"><i class="fa fa-minus-circle"></i></a>
              </div>
            </div>
          </li>
          <li>
            <div class="list-table-label">Other</div>
            <div class="list-table-content">
              <div class="input-group">
                <input id="otherAllowance" type="text" class="form-control updateTaxComputation" placeholder="0.00" >
                <a href="#" class="field-minus-btn input-group-addon"><i class="fa fa-minus-circle"></i></a>
              </div>
            </div>
          </li>
        </ul>
    </div>
  </div><!-- END: .section-content -->

  <div class="section-divider"></div>


  <h3 class="section-title mb15 hidden"><i class="fa fa-money"></i> Deductions</h3>
  <div class="section-content row mt30">
    <div class="col-sm-6">
      <h4 class="">Deductions <a href="#" class="field-add-btn  btn-simple font-md pull-right"><i class="fa fa-plus"></i></a></h4>
        <ul class="list-table vmiddle list-unstyled">
          <li>
            <div class="list-table-label">SSS</div>
            <div class="list-table-content">
              <div class="input-group">
                <input type="text" class="form-control updateTaxComputation" id="sss" name="sss" placeholder="SSS Contribution" data-value="500">
                <a href="#" class="field-minus-btn input-group-addon"><i class="fa fa-minus-circle"></i></a>
              </div>
            </div>
          </li>
          <li>
            <div class="list-table-label">Philhealth</div>
            <div class="list-table-content">
              <div class="input-group">
                <input type="text" class="form-control updateTaxComputation" id="philhealth" name="philhealth" placeholder="Philhealth Contribution" data-value="500">
                <a href="#" class="field-minus-btn input-group-addon"><i class="fa fa-minus-circle"></i></a>
              </div>
            </div>
          <li>
            <div class="list-table-label">PAGIBIG</div>
            <div class="list-table-content">
              <div class="input-group">
                <input type="text" class="form-control updateTaxComputation" id="pagibig" name="pagibig" placeholder="PAGIBIG Contribution" data-value="500">
                <a href="#" class="field-minus-btn input-group-addon"><i class="fa fa-minus-circle"></i></a>
              </div>
            </div>
          </li>
        </ul>

    </div>

    <div class="col-sm-5 col-sm-offset-1">
        <h4 class="">Miscellaneous <a href="#" class="field-add-btn  btn-simple font-md pull-right"><i class="fa fa-plus"></i></a></h4>
        <ul class="list-table vmiddle list-unstyled">
          <li>
            <div class="list-table-label">Absences</div>
            <div class="list-table-content">
              <div class="input-group">
                <input id="absences" type="text" class="form-control updateTaxComputation" placeholder="Absences">
                <a href="#" class="field-minus-btn input-group-addon"><i class="fa fa-minus-circle"></i></a>
              </div>
            </div>
          </li>
          <li>
            <div class="list-table-label">Tardiness</div>
            <div class="list-table-content">
              <div class="input-group">
                <input id="tardiness" type="text" class="form-control updateTaxComputation" placeholder="Tardiness">
                <a href="#" class="field-minus-btn input-group-addon"><i class="fa fa-minus-circle"></i></a>
              </div>
            </div>
          </li>
          <li>
            <div class="list-table-label">Undertime</div>
            <div class="list-table-content">
              <div class="input-group">
                <input id="undertime" type="text" class="form-control updateTaxComputation" placeholder="Undertime">
                <a href="#" class="field-minus-btn input-group-addon"><i class="fa fa-minus-circle"></i></a>
              </div>
            </div>
          </li>
          <li>
            <div class="list-table-label">Loan</div>
            <div class="list-table-content">
              <div class="input-group">
                <input id="loan" type="text" class="form-control" placeholder="Loan" value="1,500.00">
                <a href="#" class="field-minus-btn input-group-addon"><i class="fa fa-minus-circle"></i></a>
              </div>
            </div>
          </li>
        </ul>
   </div>
    
  </div><!-- END: .section-content -->



</div><!-- END: .section -->