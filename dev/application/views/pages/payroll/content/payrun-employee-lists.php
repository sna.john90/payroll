
      <tr class="valign-middle">
        <td class="text-center"><input type="checkbox" class="checkbox-employee" name="employees[]" value="{enc_id}"></td>
        <td class="pay-name">
          <a href="javascript:void(0)" class="view-employee-btn profile-btn" data-payrunid="{payrun_id_enc}"
          data-uid="{enc_id}">
              <div class="photo-box photo-box-xs pull-left"><img class="img-responsive" src="{employee_pic}"></div>
              <div class="ml40 mt5">{name}</div>
          </a>
              <div class="font-xs ml40">{designation}</div>
        </td>
        <td class="text-left hidden-xs">{last_payperiod}</td>
       <!--  <td class="text-center">
          <div class="label label-danger">Not Paid</div>
        </td> -->
        <td class="pay-basic">{basic_pay}</td>
        <td class="pay-incentive">{incentives}</td>
        <td class="pay-deduction">{total_deductions}</td>
        <td class="pay-total">{take_homepay}</td>
        <td class="pay-note text-center">
          <a href="#" data-toggle="modal" data-target="#modalNote"><i class="fa fa-wpforms"></i></a>
        </td>
        <td>
          <div class="btn-group-xs">
            <a href="javascript:void(0)" class="review-payment-btn btn btn-primary" data-toggle="tooltip" data-title="Details"
                data-placement="left"
                data-payrunid="{payrun_id_enc}"
                data-uid="{enc_id}"
                ><i class="fa fa-search-plus"></i>   </a>
          </div>
        </td>
      </tr>
