<tr class="valign-middle">
  <td class="pay-name">
    <a href="javascript:void(0)" class="view-employee-btn" data-payrunid="{payrun_id_enc}"
    data-uid="{enc_id}">
        <div class="photo-box photo-box-xs pull-left"><img class="img-responsive" src="{employee_pic}"></div>
        <div class="ml40 mt5">{name}</div>
    </a>
        <div class="font-xs ml40">{designation}</div>
  </td>
<td class="text-left hidden-xs">{last_payperiod}</td>
 <td class="text-center">
    <?php if(!$is_paid): ?><div class="label label-danger">Not Paid</div> <?php endif; ?>
      <?php if($is_paid): ?><div class="label label-success"><i class="fa fa-check-circle"></i> Paid</div><?php endif; ?>
 </td>
 <td class="pay-basic">{basic_pay}</td>
 <td class="pay-incentive">{incentives}</td>
 <td class="pay-deduction">{total_deductions}</td>
 <td class="pay-total">{take_homepay}</td>
  <td class="pay-note text-center">
    <a href="#" class="notes" data-toggle="modal" data-target="#modalNote"><i class="fa fa-wpforms"></i></a>
  </td>
  <td>
    <div class="btn-group-xs">
      <a href="javascript:void(0)" class="download-payslip-btn btn btn-default" data-payrunid="{payrun_id_enc}"
      data-uid="{enc_id}" data-toggle="tooltip" data-container="body" data-title="Print Payslip"><i class="fa fa-print"></i>   </a>
      <a href="javascript:void(0)" data-payrunid="{payrun_id_enc}"
      data-uid="{enc_id}" class="review-payment-btn btn btn-primary"  data-toggle="tooltip" data-container="body" data-title="Details"><i class="fa fa-search-plus"></i>   </a>
    </div>
  </td>
</tr>
