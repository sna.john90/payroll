
<div class="mt15 mb30 pl10">
  <h4 class="mb5">Select employees to pay</h4>
  <div>Choose your employees to be included in this pay run and click on <a href="#">generate payroll</a>.</div>
</div>


<div class="row form-modern mb15 clearfix">
    <div class="col-sm-4">
        <ul class="list-table vmiddle mb0 list-unstyled">
          <li>
            <div class="text-label"><span class="pl5 pr5">PERIOD: </span></div>
            <div><input class="form-control payrun-details date-range date-from"  value="{period_from}" data-location-target="{payrun_id_enc}" data-field="period_from"> </div>
            <div class="text-label"><span class="pl5 pr5">TO</span></div>
            <div><input class="form-control payrun-details date-range date-to" value="{period_to}" data-location-target="{payrun_id_enc}" data-field="period_to"></div>
          </li>
        </ul>
    </div>
    <div class="col-sm-4 col-sm-offset-4">
        <ul class="col-sm-12 list-table vmiddle mb0 list-inline">
          <li class="">
                <div class="text-label text-right font-bold">PAY DATE:</div>
                <div class=""><input class="form-control date payrun-details" type="text" value="{payroll_date}" data-location-target="{payrun_id_enc}" data-field="payroll_date" ></div>
          </li>
          <li class="hidden">
                <div class="text-label">NOTES:</div>
                <div class=""><textarea class="form-control"></textarea></div>
          </li>
        </ul>
    </div>
</div>


<div class="row form-modern clearfix ">
  <div class="quickview-panel well row bg-gray ml15 mr15">
      <div class="col-xs-6 col-sm-2 pl5 pr5">
        <div class="panel panel-default mb5">
          <div class="panel-body pl10 text-right">
            <div class="font-xlg text-primary"><span class="font-md text-gray">PHP </span> <span class="salaries">{total_salaries}</span></div>
            <div class="font-xs">TOTAL EARNINGS</div>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-2 pl5 pr5">
        <div class="panel panel-default mb5">
          <div class="panel-body pl10 text-right">
            <div class="font-xlg"><span class="font-md text-gray ">PHP </span> <span class="salaries"> {total_salaries}</span></div>
            <div class="font-xs">TOTAL SALARIES</div>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-2 pl5 pr5">
        <div class="panel panel-default mb5">
          <div class="panel-body pl10 text-right">
            <div class="font-xlg text-success"><span class="font-md text-gray ">PHP </span><span class="taxes"> {total_taxes}</span></div>
            <div class="font-xs text-muted">TOTAL TAXES</div>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-2 pl5 pr5">
        <div class="panel panel-default mb5">
          <div class="panel-body pl10 text-right">
            <div class="font-xlg"><span class="font-md text-gray ">PHP </span> <span class="incentives">{total_incentives}</span></div>
            <div class="font-xs text-muted">INCENTIVES</div>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-2 pl5 pr5">
        <div class="panel panel-default mb5">
          <div class="panel-body pl10 text-right">
            <div class="font-xlg"><span class="font-md text-gray ">PHP </span> <span class="deductions">{total_deductions}</span></div>
            <div class="font-xs text-muted">DEDUCTIONS</div>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-2 pl5 pr5">
        <div class="panel panel-default mb5">
          <div class="panel-body pl10 text-right">
            <div class="font-xlg text-info"><span class="font-md ">PHP </span> <span class="takehomepay">{total_takehome}</span></div>
            <div class="font-xs text-muted">TOTAL TAKE HOME PAY</div>
          </div>
        </div>
      </div>
    </div>
</div>

<?php echo form_open(base_url()."payroll/generate/{payrun_id_enc}",array("id" => "generate-payment-form")); ?>
<div class="table-responsive">
  <table id="tablePayroll" class="table-nox table table-hover">
    <thead>
      <tr>
        <th data-orderable="false" class="text-center">
              <input type="checkbox" id="checkAll">
        </th>
        <th class="text-left">Name</th>
        <th class="text-left hidden-xs">Last Pay Period</th>
        <th>Basic Pay</th>
        <th>Incentives</th>
        <th>Deductions</th>
        <th>Take Home Pay</th>
        <th data-orderable="false" class="text-center"><i class="fa fa-wpforms"></i></th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody class="payrun-employee-lists">
        <tr><td colspan="8" align="center" class="text-center"><i class="fa fa-circle-o-notch text-success fa-spin"></i></td></tr>
    </tbody>
  </table>
</div>
<?php echo form_close();?>
  <div id="payrunNotes" class="form-modern">
    <ul class="list-table vmiddle  mb0 list-unstyled">
        <li>
          <div class="text-label text-right">NOTES FOR THIS PAY RUN: </div>
          <div><textarea class="form-control payrun-details"   data-location-target="{payrun_id_enc}" data-field="notes">{notes}</textarea></div>
        </li>
      </ul>
  </div>

 <script type="text/javascript" src="{asset_url}lib/tinymce/tinymce.min.js"></script>
  <script type="text/javascript">
      function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
      $(document).ready(function(){
          //get lists of employee
          $.get(base_url+"payroll/payrun_employees/{payrun_id_enc}/lists",function(data){
              $('.payrun-employee-lists').html(data.data.employee_list_html);

              $.each(eval(data.data.statistics), function(key, val) {
                  var num = parseFloat(val);
                  val = num.toFixed(2);
                  $('.quickview-panel').find('span.'+key).text(numberWithCommas(val));
              });
          });
        <?php /*  tinymce.init({
            forced_root_block:'',
            selector: '#editor',
            menubar:false,
            plugins: 'image imagetools',
            toolbar: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | link image',
            // enable title field in the Image dialog
            image_title: true,
            // enable automatic uploads of images represented by blob or data URIs
            automatic_uploads: true,
            // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
            images_upload_url: 'index.php/image/tinymcepostacceptor',
            // here we add custom filepicker only to Image dialog
            file_picker_types: 'image',
            statusbar:false,
            paste_date_images:true,
            // and here's our custom image picker
            file_picker_callback: function(cb, value, meta) {
              var input = document.createElement('input');
              input.setAttribute('type', 'file');
              input.setAttribute('accept', 'image/*');

              // Note: In modern browsers input[type="file"] is functional without
              // even adding it to the DOM, but that might not be the case in some older
              // or quirky browsers like IE, so you might want to add it to the DOM
              // just in case, and visually hide it. And do not forget do remove it
              // once you do not need it anymore.

              input.onchange = function() {
                var file = this.files[0];

                // Note: Now we need to register the blob in TinyMCEs image blob
                // registry. In the next release this part hopefully won't be
                // necessary, as we are looking to handle it internally.
                var id = 'blobid' + (new Date()).getTime();
                var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                var blobInfo = blobCache.create(id, file);
                blobCache.add(blobInfo);
                // call the callback and populate the Title field with the file name
                cb(blobInfo.blobUri(), { title: file.name });
              };
              input.click();
          }
    });  */?>
      });

  </script>
