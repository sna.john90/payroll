<div class="container-fluid payroll-wrapper">
  <div class="panel panel-nox fullH">
    <div class="panel-heading clearfix">
        <a href="<?php echo base_url('payroll'); ?>" class="btn-page-prev" data-toggle="tooltip" data-container="body" title="Back"><i class="fa fa-angle-left"></i> </a>
        <div class="filter-option pull-right">
            <ul class="list-inline m0">
              <li>
                  <span class="font-xs text-gray preloader-message-status"></span>
              </li>
               <li>
	            <div class="btn-group-sm">
                  <a href="#payrunNotes" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" data-title="View Notes"><i class="fa fa-file-text-o"></i></a>
                  <a href="" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" data-title="Print Payslips"><i class="fa fa-print"></i></a>
	            </div>
              </li>
            </ul>
        </div>
        <h4 class="panel-title pt10">
          <div class="contenteditable-wrap">
            <div class="title-editable payrun-details" contenteditable="true" data-field="description" data-location-target="{payrun_id_enc}">{description}</div>
          </div>
        </h4>
    </div>
    <div class="panel-body">
        {payrun_content}
    </div><!-- END .panel-body -->
  </div>
</div>
