
<div class="container-fluid">
  <div class="panel panel-nox fullH">
    <div class="panel-heading clearfix">
        <div class="filter-option pull-right">
            <ul class="list-inline">
                 <li>
                    <a href="javascript:void(0)" id="add-user-btn" class="btn btn-success btn-sm"><i class="fa fa-plus-circle"></i> Add Employee</a>
                </li>
                <!-- <li>
                    <div class="btn-group btn-group-sm">
                        <a href="" class="btn btn-default active"><i class="fa fa-bars"></i></a>
                        <a href="" class="btn btn-default"><i class="fa fa-th"></i></a>
                    </div>
                </li> -->
            </ul>
        </div>
        <h3 class="m0 mb15">Employees</h3>
    </div>
    <div class="panel-body">
        <div class="row mb30">
        	<div class="col-sm-8">Manage your employees.</div>
        	<div class="col-sm-4 text-right">
        		<div class="checkbox">
        			<label><input type="checkbox"> Show inactive employees</label>
        		</div>
        	</div>
        </div>
        <div class="table-responsive"> 
        <table id="userTable" class="table table-nox dataTable vmiddle table-hover"> 
           <thead>
                <tr>
                    <th>Name</th>
                    <th>ID#</th>
                    <th>Department</th>
                    <th>Designation</th>
                    <th>Rate</th>
                    <th>Date Hired</th>
                    <th data-orderable="false">Phone</th>
                    <th>Pay Mode</th>
                    <th>Status</th>
                    <th data-orderable="false">Action</th>
                </tr>
            </thead>
           <tbody>
           		<?php for($k=1;$k<9;$k++): ?>
                <tr class="valign-middle">
                    <td class="text-left">
                        <a href="#" data-toggle="modal" data-target="#modalProfile">
                            <div class="photo-box photo-box-xs pull-left"><img class="img-responsive" src="{base_url}assets/images/avatar.jpg"></div>
                            <div class="ml40 mt5">John McSmith</div>
                        </a>
                    </td>
                    <td>0001</td>
                    <td>Web Dev</td>
                    <td>Programmer</td>
                    <td>20,000.00</td>
                    <td>2014/10/15</td>
                    <td>123-456987</td>
                    <td>Semi-monthly</td>
                    <td>Active</td>
                    <td>
                        <div class="btn-group btn-group-xs">
                            <a href="" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                            <a href="" class="btn btn-danger"><i class="fa fa-close"></i></a>
                        </div>
                    </td>
                </tr>
                <?php endfor; ?>

           </tbody> 
        </table>
      </div>
        
    </div>

    <div class="panel-body">
        <div class="filter-option pull-right">
            <ul class="list-inline">
                 <li>
                    <a href="" class="btn btn-success btn-sm"><i class="fa fa-plus-circle"></i> Add Employee</a>
                </li>
            </ul>
        </div>
    </div>
  </div>
</div>    
{crop_modal}