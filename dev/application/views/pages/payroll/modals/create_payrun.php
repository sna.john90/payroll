<!-- MODAL generate payroll -->


  <div class="modal-header bg-graylite">
    <button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="panel-title">New Pay Run</h4>
  </div>
  <?php echo form_open(base_url()."payroll/do_create", array("id" => "add-payroll-form","class"=>"form-horizontal")); ?>
  <div class="modal-body">

    <div class="form-modern row">
        <div class="col-sm-12">
            <ul class="list-table vtop list-unstyled">
              <li>
                <div class="list-table-label">Pay Period:</div>
                <div class="list-table-content">
                  <div class="row">
                  <div class="col-sm-5"><input class="form-control date date-from" name="period_from" value="<?php echo date('Y/m/d'); ?>" placeholder="<?php echo date('Y/m/d'); ?>"> </div>
                  <div class="col-sm-1 text-right text-label mt10">TO</div>
                  <div class="col-sm-5"><input class="form-control date date-to" name="period_to" value="<?php echo date('Y/m/d'); ?>" placeholder="<?php echo date('Y/m/d'); ?>"></div>
                  </div>
                </div>
              </li>
              <li>
                <div class="list-table-label">Payroll Date:</div>
                <div class="list-table-content"> <input class="form-control date" name="payroll_date" value="<?php echo date('Y/m/d'); ?>"  name="p"></div>
              </li>
              <li>
                <div class="list-table-label">Description:</div>
                <div class="list-table-content"> <input class="form-control" name="description"  name="description"  ></div>
              </li>
              <li>
                <div class="list-table-label">Notes</div>
                <div class="list-table-content mt15"><textarea class="form-control" id="editor" name="notes"></textarea></div>
              </li>
            </ul>
        </div>
    </div>

  </div><!-- /.modal-body -->
  <div class="modal-footer">
      <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
      <button type="submit" class="btn btn-primary btn-sm" >Continue <i class="fa fa-arrow-right"></i></button>
  </div>
  </form>
<script type="text/javascript" src="{asset_url}lib/tinymce/tinymce.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
     $('[data-toggle="tooltip"]').tooltip();
      $('.date').datetimepicker({
          format: 'YYYY-MM-DD'
      }).on('dp.show',function(){
          if($(this).hasClass("date-to"))
          {
            $(this).data("DateTimePicker").minDate($('.date-from').val());
          }
      }).on('dp.change',function(e){
          if($(this).hasClass("date-from")){
                $('.date-to').data("DateTimePicker").show();
          }
      });
       tinymce.init({
        forced_root_block:'',
        selector: '#editor',
        menubar:false,
        plugins: 'image imagetools',
        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | link image',
        // enable title field in the Image dialog
        image_title: true,
        // enable automatic uploads of images represented by blob or data URIs
        automatic_uploads: true,
        // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
        images_upload_url: 'index.php/image/tinymcepostacceptor',
        // here we add custom filepicker only to Image dialog
        file_picker_types: 'image',
        statusbar:false,
        paste_date_images:true,
        // and here's our custom image picker
        file_picker_callback: function(cb, value, meta) {
          var input = document.createElement('input');
          input.setAttribute('type', 'file');
          input.setAttribute('accept', 'image/*');

          // Note: In modern browsers input[type="file"] is functional without
          // even adding it to the DOM, but that might not be the case in some older
          // or quirky browsers like IE, so you might want to add it to the DOM
          // just in case, and visually hide it. And do not forget do remove it
          // once you do not need it anymore.

          input.onchange = function() {
            var file = this.files[0];

            // Note: Now we need to register the blob in TinyMCEs image blob
            // registry. In the next release this part hopefully won't be
            // necessary, as we are looking to handle it internally.
            var id = 'blobid' + (new Date()).getTime();
            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
            var blobInfo = blobCache.create(id, file);
            blobCache.add(blobInfo);
            // call the callback and populate the Title field with the file name
            cb(blobInfo.blobUri(), { title: file.name });
          };
          input.click();
      }
    });

    //add expenses
     var form = $('#add-payroll-form');
    form.ajaxForm({
      dataType: 'json',
      beforeSend:function(){
        form.find('.submit-btn').html('<i class="fa fa-plus-circle"></i> Adding ... ');
        form.find('.submit-btn').prop("disabled",true);
      },
      success:function(response){
        form.find('.submit-btn').html('<i class="fa fa-plus-circle"></i> Add Expense');
        form.find('.submit-btn').prop("disabled",false);
        me_message_v2(response);
        if(response.error==0)
        {
          window.location.href = "{base_url}payroll/payrun-employees/"+response.data;
        }
      }
    });
  })
</script>
