<!-- MODAL edit payroll -->


  <div class="modal-header bg-graylite">
    <button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="panel-title">Edit Payroll</h4>
  </div>
  <div class="modal-body">
      
    <div class="form-modern row">
        <div class="col-sm-12">
            <ul class="list-table vtop list-unstyled">
              <li>
                <div class="list-table-label">Pay Run:</div>
                <div class="list-table-content"> 
                  <div class="row">
                  <div class="col-sm-5"><input class="form-control" placeholder="<?php echo date('Y/m/d'); ?>"> </div>
                  <div class="col-sm-1 text-right text-label mt10">TO</div>
                  <div class="col-sm-5"><input class="form-control" placeholder="<?php echo date('Y/m/d'); ?>"></div>
                  </div>
                </div>
              </li>
              <li>
                <div class="list-table-label">Payroll Date:</div>
                <div class="list-table-content"> <input class="form-control" placeholder="<?php echo date('Y/m/d'); ?>"></div>
              </li>
              <li>
                <div class="list-table-label">Title:</div>
                <div class="list-table-content"> <input class="form-control" placeholder="Payroll Title" value="Smartstart Payroll"></div>
              </li>
              <li>
                <div class="list-table-label">Notes</div>
                <div class="list-table-content mt15"><textarea class="form-control"></textarea></div>
              </li>
            </ul>
        </div>
    </div>
        
  </div><!-- /.modal-body -->
  <div class="modal-footer">
      <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
      <button type="button" class="btn btn-primary btn-sm">Save</button>
  </div>
