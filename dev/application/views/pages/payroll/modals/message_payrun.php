<?php
/*
Message when pay run is completed.
*/
?>

  <div class="modal-header bg-graylite">
    <button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="panel-title">Pay Run Complete!</h4>
  </div>
  <div class="modal-body">
      
    <p>Your pay run for the period <strong>2016-Nov-15</strong> to <strong>2016-Nov-30</strong> is complete!</p>

    <p>Number of employees paid: <strong>15</strong></p>
        
  </div><!-- /.modal-body -->
  <div class="modal-footer">
      <button type="button" class="btn btn-primary btn-sm">Done</button>
  </div>
