<?php if(isset($media) && $media=="print"): ?>
  <style type="text/css">
    @media print {
        .modal-header{
          display: none;
        }
        .nav-tabs{
          display: none;
        }
        .prof-widget{
          display: none;
        }
        .right-pane{
          width: 100%;
        }
        INPUT,SELECT{
          display: none;
        }
    }
  </style>
<?php endif; ?>
<!-- MODAL MAKEPAYMENT -->
<div class="modal-header bg-graylite">
  <button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <div class="filter-option pull-right mr30">
      <ul class="list-inline m0">
         <li>
            <span class="font-xs text-gray preloader-message-status"></span>
        </li>
        <li>
            <div class="btn-group btn-group-sm">
              <a href="#" class="btn btn-default review-payment-prev review-payment-btn" <?php echo ($prev_id=="0")? "disabled" : ""; ?>
                data-prev-id="{prev_id}"
                data-payrun-id="{payrun_id}"
                data-trigger="hover"
                data-action="prev"
                rel="tooltip" data-container="body" data-placement="bottom"  title="Previous Employee"><i class="fa fa-chevron-left"></i></a>
              <a href="#" class="btn btn-default review-payment-next review-payment-btn" <?php echo ($next_id=="0")? "disabled" : ""; ?>
                   data-next-id="{next_id}"
                   data-payrun-id="{payrun_id}"
                   data-action="next"
                  rel="tooltip" data-trigger="hover" data-container="body" data-placement="bottom"  title="Next Employee"><i class="fa fa-chevron-right"></i></a>
            </div>
        </li>
      </ul>
  </div>
  <h4 class="">Review Payment</h4>
</div>
<div class="modal-body modal-body-gray clearfix">

    <div class="row">
        <div class="left-pane col-sm-4 col-md-3 prof-widget">
          {profile_widget}
        </div><!-- END: .left-pane -->
        <div class="right-pane col-sm-8 col-md-9">
          <div class="panel-nav">
            <ul class="nav nav-tabs nav-tabs-nox" role="tablist">
              <li role="presentation" class="<?php echo ($active_tab=="profile")? "active" : "";?>"><a href="#userProfile" aria-controls="userProfile" role="tab" data-toggle="tab"><i class="fa fa-user-circle-o"></i> Profile</a></li>
              <li role="presentation" class="<?php echo ($active_tab!="profile")? "active" : "";?>"><a href="#payslip" aria-controls="payroll" role="tab" data-toggle="tab"><i class="fa fa-dollar"></i> Payslip</a></li>
            </ul>
          </div>
          <div class="panel panel-nox">
            <div class="panel-body">
                  <!-- Tab panes -->
                  <div class="tab-content pl10 pr10">
                    <div role="tabpanel" class="tab-pane <?php echo ($active_tab=="profile")? "active" : "";?>" id="userProfile">{profile_content} </div>
                    <div role="tabpanel" class="tab-pane <?php echo ($active_tab!="profile")? "active" : "";?>" id="payslip">{payroll_payslip}</div>
                  </div>
            </div>
          </div>
        </div><!-- END: .right-pane -->
      </div>



</div> 
