<?php
#PAYROLL PaymentLIST
?>
<div class="container-fluid">
  <div class="panel panel-nox fullH">
    <div class="panel-heading clearfix">
        <div class="filter-option pull-right">
            <ul class="list-inline m0">
              <li>
                  <a href="javascript:void(0)" id="create-payroll-btn" class="btn btn-success btn-sm"><i class="fa fa-plus-circle"></i> Start New Pay Run</i></a>
              </li>
            </ul>
        </div>
        <h3 class="mt0 pt10">Pay Runs</h3>
    </div>
    <div class="panel-body">

      {payroll_content_list}

    </div>
  </div> 
</div>

<script type="text/javascript">
  //data tables
      // var payDataTable   = $('.dataTable').DataTable({
      //   scrollCollapse: true,
      //   //scrollY: '90vh',
      //   "language": {
      //     "lengthMenu": "SHOW _MENU_",
      //     "paginate": {
      //       "previous": "&lt;",
      //       "next": "&gt;"
      //     }
      //  },
      //   dom:"<'row data-table-header'<'col-xs-8'f><'col-xs-4'l>>" +
      //     "<'row'<'col-sm-12'tr>>" +
      //     "<'row data-table-footer'<'col-sm-4 font-xs'i><'col-sm-8'p>>"
      //  });

      /*NOTE: data range filter
      http://yadcf-showcase.appspot.com/DOM_source.html
      */
</script>
