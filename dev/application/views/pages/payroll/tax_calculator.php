<!-- PROFILE SECTION -->   
<div class="container-fluid">
	<div class="row">
		<div class="left-pane col-sm-4 col-md-3"> 
		  {profile_widget}
		</div><!-- END: .left-pane -->
		<div class="right-pane col-sm-8 col-md-9">
		  <div class="panel-nav">
		    <ul class="nav nav-tabs nav-tabs-nox" role="tablist">
		      <li role="presentation"><a href="#userProfile" aria-controls="userProfile" role="tab" data-toggle="tab"><i class="fa fa-user-circle-o"></i> Profile</a></li>
		      <li role="presentation" class="active"><a href="#salaryDeductions" aria-controls="salaryDeductions" role="tab" data-toggle="tab"><i class="fa fa-address-card-o"></i> Salary &amp; Deductions </a></li>
		      <li role="presentation"><a href="#payslip" aria-controls="payroll" role="tab" data-toggle="tab"><i class="fa fa-dollar"></i> Payslip</a></li>
		    </ul>
		  </div>
		  <div class="panel panel-nox">
		    <div class="panel-body">
		          <!-- Tab panes -->
		          <div class="tab-content pl10 pr10">
		            <div role="tabpanel" class="tab-pane" id="userProfile">{profile_content}</div>
		            <div role="tabpanel" class="tab-pane active" id="salaryDeductions">{salary_dedecutions}</div>
		            <div role="tabpanel" class="tab-pane" id="payslip">{payroll_payslip}</div>
		          </div>
		    </div>
		  </div>
		</div><!-- END: .right-pane -->    
	</div>
</div>  