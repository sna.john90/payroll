<div class="row">
	<div class="col-xs-12 mb20">
		<ul class="contact-list contact-list-medium btn-options-wrap list-unstyled">
	      	<li data-toggle="tooltip" data-placement="left" title="Username">
	      		<span class="fa fa-user"></span> 
	      		<span class="display-inline-block">{username}</span>
	      		<button type="button" class="btn btn-primary btn-sm display-inline-block pull-right" id="edit-username-btn"><i class="fa fa-pencil"></i> Edit</button>
	      	</li>
	      	<li data-toggle="tooltip" data-placement="left" title="Password">
	      		<span class="fa fa-key"></span> 
	      		<span class="font-italic display-inline-block">Your password</span>
	      		<button type="button" class="btn btn-primary btn-sm display-inline-block pull-right" id="change-password-btn"><i class="fa fa-pencil"></i> Change Password</button>
	      	</li>
	    </ul>
	</div>
</div>