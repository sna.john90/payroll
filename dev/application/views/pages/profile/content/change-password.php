<?php echo form_open(base_url()."profile/change_password/{id}",array("class" => "form-horizontal","id" => "change-password-form")); ?>
	<input type="hidden" name='original_username' value="{username}">
	<div class="row">
		<div class="col-xs-12 mb30">
			<div class="form-group mr15">
	          <label class="col-sm-3 control-label">New Password</label>
	          <div class="input-group col-sm-9">
	            <input type="password" class="form-control" name="new_password">
	            <div class="input-group-addon"><i class="fa fa-key"></i></div>
	          </div>
	        </div>
	        <div class="form-group mr15">
	          <label class="col-sm-3 control-label">Confirm New Password</label>
	          <div class="input-group col-sm-9">
	            <input type="password" class="form-control" name="confirm_new_password">
	            <div class="input-group-addon"><i class="fa fa-key"></i></div>
	          </div>
	        </div>
	        <div class="form-group mr15">
	        	<div class="pull-right">
	        		<button type="button" class="btn btn-default btn-sm cancel-password-btn"><i class="fa fa-times-circle-o"></i> Cancel</button>
	        		<button type="submit" class="btn btn-primary btn-sm submit-btn"><i class="fa fa-check-circle-o"></i> Save</button>
	        	</div>
	        </div>
		</div>
	</div>
</form>

<script type="text/javascript">
	$(document).ready(function(){
		var form = $('#change-password-form');
	    // submit form
	    $('#change-password-form').ajaxForm({
	      dataType: 'json',
	      beforeSend:function(){
	        form.find('.submit-btn').html('<i class="fa fa-check-circle-o"></i> Saving ... ');
	        form.find('.submit-btn').prop("disabled",true);
	      },
	      success:function(response){
	        form.find('.submit-btn').html('<i class="fa fa-check-circle-o"></i> Save');
	        form.find('.submit-btn').prop("disabled",false);
	        response_message(response);
	        if(response.error==0)
	        {
	        	$(".cancel-password-btn").click();
	        }
	      }
	    });
	});
</script>