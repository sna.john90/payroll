<div class="row">
	<div class="col-xs-12 mb20">
		<ul class="contact-list contact-list-medium btn-options-wrap list-unstyled">
      		<?php if($email!=""): ?>
	      		<li data-toggle="tooltip" data-placement="left" title="Email Address"><span class="fa fa-envelope-o"></span> <span><a href="mailto:{email}">{email}</a></span></li>
	      	<?php endif; ?>
      		<?php if($home_phone!=""): ?>
	      		<li data-toggle="tooltip" data-placement="left" title="Home Phone No."><span class="fa fa-phone"></span> <span>{home_phone}</span></li>
	      	<?php endif; ?>
      		<?php if($office_phone!=""): ?>
	        	<li data-toggle="tooltip" data-placement="left" title="Office Phone No."><span class="fa fa-phone"></span> <span>{office_phone}</span></li>
	        <?php endif; ?>
      		<?php if($cell_phone!=""): ?>
	        	<li data-toggle="tooltip" data-placement="left" title="Cell Phone No."><span class="fa fa-mobile"></span> <span>{cell_phone}</span></li>
	        <?php endif; ?>
      		<?php if($efax!=""): ?>
	        	<li data-toggle="tooltip" data-placement="left" title="Efax"><span class="fa fa-fax"></span> <span>{efax}</span></li>
	        <?php endif; ?>
	    </ul>
	</div>
</div>