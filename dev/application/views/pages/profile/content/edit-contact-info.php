<?php echo form_open(base_url()."profile/update_contact_info/{id}",array("class" => "form-horizontal","id" => "edit-contact-form")); ?>
	<input type="hidden" name='original_email' value="{email}">
	<div class="row">
		<div class="col-xs-12 mb30">
			<div class="form-group mr15">
	          <label class="col-sm-3 control-label">Email</label>
	          <div class="input-group col-sm-9">
	            <input type="email" class="form-control" name="email" value="{email}">
	            <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
	          </div>
	        </div>
	        <div class="form-group mr15">
	          <label class="col-sm-3 control-label">Home Phone</label>
	          <div class="input-group col-sm-9">
	            <input type="text" class="form-control phone" name="home_phone" value="{home_phone}">
	            <div class="input-group-addon"><i class="fa fa-phone"></i></div>
	          </div>
	        </div>
	        <div class="form-group mr15">
	          <label class="col-sm-3 control-label">Office Phone</label>
	          <div class="input-group col-sm-9">
	            <input type="text" class="form-control phone" name="office_phone" value="{office_phone}">
	            <div class="input-group-addon"><i class="fa fa-phone"></i></div>
	          </div>
	        </div>
	        <div class="form-group mr15">
	          <label class="col-sm-3 control-label">Cell Phone</label>
	          <div class="input-group col-sm-9">
	            <input type="text" class="form-control phone" name="cell_phone" value="{cell_phone}">
	            <div class="input-group-addon"><i class="fa fa-mobile"></i></div>
	          </div>
	        </div>
	        <div class="form-group mr15">
	          <label class="col-sm-3 control-label">Efax</label>
	          <div class="input-group col-sm-9">
	            <input type="text" class="form-control phone" name="efax" value="{efax}">
	            <div class="input-group-addon"><i class="fa fa-fax"></i></div>
	          </div>
	        </div>
	        <div class="form-group mr15">
	        	<div class="pull-right">
	        		<button type="button" class="btn btn-default btn-sm cancel-contact-btn"><i class="fa fa-times-circle-o"></i> Cancel</button>
	        		<button type="submit" class="btn btn-primary btn-sm submit-btn"><i class="fa fa-check-circle-o"></i> Save</button>
	        	</div>
	        </div>
		</div>
	</div>
</form>

<script type="text/javascript">
	$(document).ready(function(){
		$(".phone").inputmask("999-999-9999");
		var form = $('#edit-contact-form');
	    // submit form
	    $('#edit-contact-form').ajaxForm({
	      dataType: 'json',
	      beforeSend:function(){
	        form.find('.submit-btn').html('<i class="fa fa-check-circle-o"></i> Saving ... ');
	        form.find('.submit-btn').prop("disabled",true);
	      },
	      success:function(response){
	        form.find('.submit-btn').html('<i class="fa fa-check-circle-o"></i> Save');
	        form.find('.submit-btn').prop("disabled",false);
	        response_message(response);
	        if(response.error==0)
	        {
	        	$(".cancel-contact-btn").click();
	        }
	      }
	    });
	});
</script>