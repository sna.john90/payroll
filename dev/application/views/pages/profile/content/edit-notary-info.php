<?php echo form_open(base_url()."profile/update_notary_info/{id}",array("class" => "form-horizontal","id" => "edit-notary-form")); ?>
	<div class="row">
		<input type="hidden" name="w9_form_file" value="{w9_form_file}">
		<div class="col-xs-12 mb30">
			<div class="form-group mr15">
	          <label class="col-sm-3 control-label">E&O Insurance Amount</label>
	          <div class="input-group col-sm-9">
	            <input type="number" class="form-control" min="0" step="0.01" name="eo_insurance" value="{eo_insurance}">
	            <div class="input-group-addon"><i class="fa fa-dollar"></i></div>
	          </div>
	        </div>
	        <div class="form-group mr15">
	          <label class="col-sm-3 control-label">Commission</label>
	          <div class="input-group col-sm-9">
	            <input type="number" class="form-control" min="0" step="0.01" name="commission" value="{commission}">
	            <div class="input-group-addon"><i class="fa fa-money"></i></div>
	          </div>
	        </div>
	        <div class="form-group mr15">
	          <label class="col-sm-3 control-label">W9 Form</label>
	          <div class="input-group col-sm-9">
	          	<button type="button" class="btn btn-success btn-xs" id="upload-w9-form-btn"><span class="fa fa-upload"></span> Upload File</button>
	          	<?php if($w9_form==""): ?>
	          		<span class="file-desc">No file chosen.</span>
	          	<?php else: ?>
	          		<span class="file-desc">{w9_form}</span>
	          	<?php endif; ?>
	            <input type="file" class="form-control hidden" name="w9_form" id="w9-form-file">
	          </div>
	        </div>
	        <div class="form-group mr15">
	        	<div class="pull-right">
	        		<button type="button" class="btn btn-default btn-sm cancel-notary-btn"><i class="fa fa-times-circle-o"></i> Cancel</button>
	        		<button type="submit" class="btn btn-primary btn-sm submit-btn"><i class="fa fa-check-circle-o"></i> Save</button>
	        	</div>
	        </div>
		</div>
	</div>
</form>

<script type="text/javascript">
	$(document).ready(function(){
		$(".phone").inputmask("999-999-9999");
		var form = $('#edit-notary-form');
	    // submit form
	    $('#edit-notary-form').ajaxForm({
	      dataType: 'json',
	      beforeSend:function(){
	        form.find('.submit-btn').html('<i class="fa fa-check-circle-o"></i> Saving ... ');
	        form.find('.submit-btn').prop("disabled",true);
	      },
	      success:function(response){
	        form.find('.submit-btn').html('<i class="fa fa-check-circle-o"></i> Save');
	        form.find('.submit-btn').prop("disabled",false);
	        response_message(response);
	        if(response.error==0)
	        {
	        	$(".cancel-notary-btn").click();
	        }
	      }
	    });
	});
</script>