<?php echo form_open(base_url()."profile/update_personal_info/{id}",array("class" => "form-horizontal","id" => "edit-personal-form","onsubmit" => "return addUserEditableContent()")); ?>
	
	<div class="table-grid">
		<div class="table-grid-cell" style="width:150px">
			<div class="btn-options-wrap img-avatar img-circle img-avatar border-light-gray">
		    	<img class="img-responsive pull-left" src="{profile_pic}" width="150" cropper-canvas-image/>
		    	<ul class="btn-options btn-options-boxed list-inline pull-right">
		            <li class="mt10 mr20">
		              <a href="javascript:void(0)" id="add-profile-picture"><i class="fa fa-camera pr5"></i></a>
		            </li>
		         </ul>
		  	</div>
		</div>  
		<div class="table-grid-cell vcenter pl15">
		 	<div class="btn-options-wrap">
		    	<h3 id="name-editable" class="font-thin mt0 mb0 p5" contenteditable="true">{name}</h3>
		  	</div>
		  	<div  id="description-editable" class="slogan btn-options-wrap mt15 p5" contenteditable="true">{description}</div>
		</div>
	</div>
	<div class="row mt20 mb30">
		<input type="file" cropper data-reset="false" data-upload="false" class="hidden">
	    <!-- Coordinates for Files -->
	    <input type="hidden" id="image_url" name="profile_picture">
	    <input type="hidden" name="description" id="description-value">
	    <input type="hidden" name="name" id="name-value">
		<div id="address-details" class="form-group mr15">
          <label class="col-sm-3 control-label">Address</label>
          <input type="hidden" data-geo="lat" name="latitude">
          <input type="hidden" data-geo="lng" name="longitude">
          <input type="hidden" data-geo="locality" name="locality">
          <input type="hidden" data-geo="administrative_area_level_1_short" name="state_short">
          <input type="hidden" data-geo="administrative_area_level_1" name="state">
          <div class="input-group col-sm-9">
            <input type="text" class="form-control address-geo" name="address" value="{address}">
            <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
          </div>
        </div>
        <div class="form-group mr15">
          <label class="col-sm-3 control-label">Website</label>
          <div class="input-group col-sm-9">
            <input type="url" class="form-control" name="website" value="{website}">
            <div class="input-group-addon"><i class="fa fa-globe"></i></div>
          </div>
        </div>
        <div class="form-group mr15">
        	<div class="pull-right">
        		<button type="button" class="btn btn-default btn-sm cancel-personal-btn"><i class="fa fa-times-circle-o"></i> Cancel</button>
        		<button type="submit" class="btn btn-primary btn-sm submit-btn"><i class="fa fa-check-circle-o"></i> Save</button>
        	</div>
        </div>
	</div>
</form>

<script type="text/javascript">
	$(document).ready(function(){
		$("#name-editable").focus();
		
		$(".address-geo").geocomplete({
	      details: "#address-details",
	      detailsAttribute: "data-geo"
	    });
	    $(".address-geo").trigger("geocode");

		var form = $('#edit-personal-form');
	    // submit form
	    $('#edit-personal-form').ajaxForm({
	      dataType: 'json',
	      beforeSend:function(){
	        form.find('.submit-btn').html('<i class="fa fa-check-circle-o"></i> Saving ... ');
	        form.find('.submit-btn').prop("disabled",true);
	      },
	      success:function(response){
	        form.find('.submit-btn').html('<i class="fa fa-check-circle-o"></i> Save');
	        form.find('.submit-btn').prop("disabled",false);
	        response_message(response);
	        if(response.error==0)
	        {
	        	$(".cancel-personal-btn").click();
	        }
	      }
	    });
	    // add profile picture
	    $("#add-profile-picture").click(function(){
	      form.find("input[cropper]").click();
	    });
	    form.on("change","input[cropper]",function(){
	      var reset = $(this).data("reset");
	      var upload = $(this).data("upload");
	      if($(this).val().length!=0)
	      {
	        previewFile(this, reset);
	      }
	      $(this).replaceWith('<input type="file" cropper data-reset="false" data-upload="false" class="hidden">');
	      form.find("input[cropper]").data("reset",reset);
	      form.find("input[cropper]").data("upload",upload);
	    });
	});
	function addUserEditableContent()
	{
	    document.getElementById("name-value").value =  document.getElementById("name-editable").innerHTML;
	    document.getElementById("description-value").value =  document.getElementById("description-editable").innerHTML;
	    return true;
	}
</script>