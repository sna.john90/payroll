<div class="row">
	<div class="col-xs-12 mb20">
		<ul class="contact-list contact-list-medium btn-options-wrap list-unstyled">
			<?php if($eo_insurance!=""): ?>
	      		<li data-toggle="tooltip" data-placement="left" title="E&O Insurance"><span class="fa fa-dollar"></span> <span>$ <?php echo number_format($eo_insurance, 2, '.', ','); ?></span></li>
	      	<?php else: ?>
	      		<li data-toggle="tooltip" data-placement="left" title="E&O Insurance"><span class="fa fa-dollar"></span> <span>None</span></li>
	      	<?php endif; ?>
	      	<?php if($commission!=""): ?>
	      		<li data-toggle="tooltip" data-placement="left" title="Commission"><span class="fa fa-money"></span> <span>$ <?php echo number_format($commission, 2, '.', ','); ?></span></li>
	      	<?php else: ?>
	      		<li data-toggle="tooltip" data-placement="left" title="Commission"><span class="fa fa-money"></span> <span>None</span></li>
	      	<?php endif; ?>
	      	<?php if($w9_form!=""): ?>
	      		<li data-toggle="tooltip" data-placement="left" title="W9-Form"><span class="fa fa-file-text-o"></span> <span><a href="{base_url}profile/download_w9/?file={w9_form_file}&name={w9_form}">{w9_form}</a></span></li>
	      	<?php else: ?>
	      		<li data-toggle="tooltip" data-placement="left" title="W9-Form"><span class="fa fa-file-text-o"></span> <span>None</span></li>
	      	<?php endif; ?>
	    </ul>
	</div>
</div>