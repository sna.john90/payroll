<?php /*<?php for($k=1;$k<=20;$k++): ?>
  <tr>
    <td class="pay-date">
      <a href="javascript:void(0)" class="view-payslip-btn"><?php echo '2016-Nov-'.rand(1,2)*15; ?></a>
    </td>
    <td class="pay-basic">30,000.00</td>
    <td class="pay-incentive">
      <a href="#" data-toggle="modal" data-target="#modalPopup">2,500.00</a>
    </td>
    <td class="pay-deduction">
      <a href="#" data-toggle="modal" data-target="#modalPopup">1,000.00</a>
      <div class="pay-more-info">more info...</div>
    </td>
    <td class="pay-total">25,500.00</td>
    <td class="pay-note text-center">
      <a href="#" data-toggle="modal" data-target="#modalNote"><i class="fa fa-wpforms"></i></a>
    </td>
  </tr>
<?php endfor; ?>

*/ ?>

<tr>
  <td class="pay-date">
    <a href="javascript:void(0)" class="view-payslip-btn" data-payrun-id="<?php echo encrypt($payrun_id); ?>">{date}</a>
  </td>
  <td class="pay-basic">{basic_pay}</td>
  <td class="pay-incentive">
    <a href="#">{total_incentives}</a>
  </td>
  <td class="pay-deduction">
    <a href="#">{total_deductions}</a>
    <div class="pay-more-info">more info...</div>
  </td>
  <td class="pay-total">{total}</td>
  <td class="pay-note text-center">
    <a href="#"><i class="fa fa-wpforms"></i></a>
  </td>
  <td>
    <a href="#"  class="download-payslip-btn btn btn-default" data-payrunid="{payrun_id_enc}"
      data-uid="{enc_id}"><i class="fa fa-print"></i></a>
  </td>
</tr>
