<!-- PAYSLIP SECTION -->
<button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<div class="panel">
	<div class="panel-body">
	    <div class="row">
	    	<div class="col-sm-6">
	    		<div class="p15">
		    		<h3 class="m0">Smartstart Inc.</h3>
		    		<div>Ols Bldg, Gorordo Ave., Cebu City</div>
		    		<div>Cebu Philippines 6000</div>
	    		</div>
	    	</div>
	    	<div class="col-sm-6 text-right">
	    		<img class="img-responsive mt10" style="margin-left: auto;" src="<?php echo base_url();?>assets/logo.png" />
	    	</div>
	    </div>

	    <div class="row mt30">
	    	<div class="col-sm-6">
	    		<ul class="list-table vmiddle list-table-payslip list-unstyled pl15 pr15">
			        <li>
			          <div class="list-table-label">Name:</div>
			          <div class="list-table-content font-bold">John Smith</div>
			        </li>
			        <li>
			          <div class="list-table-label">Designation:</div>
			          <div class="list-table-content">Web Developer</div>
			        </li>
			        <li>
			          <div class="list-table-label">Department:</div>
			          <div class="list-table-content">IT Department</div>
			        </li>
			        <li>
			          <div class="list-table-label">Employee ID: </div>
			          <div class="list-table-content">s1001</div>
			        </li>
			      </ul>
	    	</div>
	    	<div class="col-sm-6">
	    		<ul class="list-table vmiddle list-table-payslip list-unstyled pl15 pr15">
			        <li>
			          <div class="list-table-label">Pay Date:</div>
			          <div class="list-table-content">Nov. 30, 2016</div>
			        </li>
			        <li>
			          <div class="list-table-label">Pay Period: </div>
			          <div class="list-table-content">2016/11/15 - 2016/11/30</div>
			        </li>
			        <li>
			          <div class="list-table-label">Pay Mode: </div>
			          <div class="list-table-content">Semi-monthly</div>
			        </li>
			        <li>
			          <div class="list-table-label">Days Worked:</div>
			          <div class="list-table-content">23.0</div>
			        </li>
			      </ul>
	    	</div>
	    </div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
	    <div class="panel panel-noborder">
	    	<div class="panel-body panel-body-nopad">

	    		<table class="table">
	    			<thead>
	    				<tr class="dark">
	    					<th class="payslip-label-th">EARNINGS</th>
	    					<th class="payslip-amount-th">AMOUNT</th>
	    				</tr>
	    			</thead>
		    		<tbody>
		    			<tr>
		    				<td class="payslip-label">Basic Pay</td>
		    				<td class="payslip-amount">10,000.00</td>
		    			</tr>
		    			<tr>
		    				<td class="payslip-label">Medical Allowance</td>
		    				<td class="payslip-amount">1,500.00</td>
		    			</tr>
		    			<tr>
		    				<td class="payslip-label">House Rent Allowance</td>
		    				<td class="payslip-amount">2,000.00</td>
		    			</tr>
		    			<tr>
		    				<td class="payslip-label">Food Allowance</td>
		    				<td class="payslip-amount">600</td>
		    			</tr>
		    			<tr class="">
		    				<td class="payslip-label font-bold">TOTAL EARNINGS</td>
		    				<td class="payslip-amount font-bold">14,100.00</td>
		    			</tr>
		    		</tbody>
	    		</table>
			</div>
	    </div>

	</div>
	<div class="col-sm-6">
		<div class="panel panel-noborder">
	    	<div class="panel-body panel-body-nopad">
	    		<table class="table">
	    			<thead>
	    				<tr class="dark">
	    					<th class="payslip-label-th">DEDUCTIONS</th>
	    					<th class="payslip-amount-th">AMOUNT</th>
	    				</tr>
	    			</thead>
		    		<tbody>
		    			<tr>
		    				<td class="payslip-label">Income Tax</td>
		    				<td class="payslip-amount">1,000.00</td>
		    			</tr>
		    			<tr>
		    				<td class="payslip-label">SSS</td>
		    				<td class="payslip-amount">300.00</td>
		    			</tr>
		    			<tr>
		    				<td class="payslip-label">Pag-ibig</td>
		    				<td class="payslip-amount">500.00</td>
		    			</tr>
		    			<tr>
		    				<td class="payslip-label">Philhealth</td>
		    				<td class="payslip-amount">100</td>
		    			</tr>
		    			<tr class="">
		    				<td class="payslip-label font-bold">TOTAL DEDUCTIONS</td>
		    				<td class="payslip-amount font-bold">1,900.00</td>
		    			</tr>
		    		</tbody>
	    		</table>

			</div>
	    </div>

	</div>
</div>

<div class="panel">
	<div class="panel-body">
	    <div class="row">
	    	<div class="col-sm-6">
	    		<table class="table table-outer-border">
	    			<tbody>
		    			<tr>
		    				<td class="payslip-label">Previous Balance</td>
		    				<td class="payslip-amount">0.00</td>
		    			</tr>
		    			<tr>
		    				<td class="payslip-label">Carry Over (excees)</td>
		    				<td class="payslip-amount">0.00</td>
		    			</tr>
		    		</tbody>
	    		</table>
	    	</div>
	    	<div class="col-sm-6">
	    		<table class="table table-outer-border">
	    			<tbody>
		    			<tr class="bg-primary">
		    				<td class="payslip-label font-bold"><h3 class="m0 p15">NET PAY</h3></td>
		    				<td class="payslip-amount font-bold"><h3 class="m0 p15"><span class="font-thin">Php</span> 12,200.00</h3></td>
		    			</tr>
		    		</tbody>
	    		</table>
	    	</div>
	    </div>

	    <div class="row">
	    	<div class="col-sm-3 mt50">
	    		<table class="table text-center">
	    			<tbody>
		    			<tr>
		    				<td class="font-bold">Employer's Signature</td>
		    			</tr>
		    		</tbody>
	    		</table>
	    	</div>
	    	<div class="col-sm-3 col-sm-offset-6 mt50">
	    		<table class="table text-center">
	    			<tbody>
		    			<tr>
		    				<td class="font-bold">Employee's Signature</td>
		    			</tr>
		    		</tbody>
	    		</table>
	    	</div>
	    </div>


	</div>
</div>
