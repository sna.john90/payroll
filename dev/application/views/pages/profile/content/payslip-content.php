<!-- PAYSLIP SECTION -->
<button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<div class="panel">
	<div class="panel-body">
	    <div class="row">
	    	<div class="col-sm-6">
	    		<div class="p15">
		    		<h3 class="m0"><?php echo $company_details['name'];?></h3>
		    		<div><?php echo $company_details['address'];?></div>
		    		<div><?php echo $company_details['telephone_number'];?></div>
	    		</div>
	    	</div>
	    	<div class="col-sm-6 text-right">
	    		<img class="img-responsive mt10" style="margin-left: auto;height:60px;" src="<?php echo company_picture($company_details['company_pic']);?>" />
	    	</div>
	    </div>

	    <div class="row mt30">
	    	<div class="col-sm-6">
					<ul class="list-table vmiddle list-table-payslip list-unstyled pl15 pr15">
			        <li>
			          <div class="list-table-label">Name:</div>
			          <div class="list-table-content font-bold"><?php echo ucfirst($get_employee_details['firstname'])." ".ucfirst($get_employee_details['lastname']);?></div>
			        </li>
			        <li>
			          <div class="list-table-label">Designation:</div>
			          <div class="list-table-content"><?php echo ucfirst(trim($get_employee_details['designation']))?></div>
			        </li>
			        <li>
			          <div class="list-table-label">Department:</div>
			          <div class="list-table-content"><?php echo ucfirst(trim($get_employee_details['name']))?></div>
			        </li>
			        <li>
			          <div class="list-table-label">Employee ID: </div>
			          <div class="list-table-content"><?php echo ucfirst(trim($get_employee_details['emp_id']))?></div>
			        </li>
			      </ul>
	    	</div>
	    	<div class="col-sm-6">
		    		<ul class="list-table vmiddle list-table-payslip list-unstyled pl15 pr15">
				        <li>
				          <div class="list-table-label">Pay Date:</div>
				          <div class="list-table-content"><?php echo format_date($payrun_details['payroll_date'],"M. j, Y") ?></div>
				        </li>
				        <li>
				          <div class="list-table-label">Pay Period: </div>
				          <div class="list-table-content"><?php echo format_date($payrun_details['period_from'],"M. j, Y")." - ".format_date($payrun_details['period_to'],"M. j, Y"); ?></div>
				        </li>
				        <li>
				          <div class="list-table-label">Pay Mode: </div>
				          <div class="list-table-content"><?php echo ucfirst(trim($get_employee_details['payment_type']))?></div>
				        </li>
				        <!-- <li>
				          <div class="list-table-label">Days Worked:</div>
				          <div class="list-table-content">23.0</div>
				        </li> -->
				      </ul>
	    	</div>
	    </div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
	    <div class="panel panel-noborder">
	    	<div class="panel-body panel-body-nopad">

	    		<table class="table">
	    			<thead>
	    				<tr class="dark">
	    					<th class="payslip-label-th">EARNINGS</th>
	    					<th class="payslip-amount-th">AMOUNT</th>
	    				</tr>
	    			</thead>
		    		<tbody>
							{incentive_content}
		    			<tr class="">
		    				<td class="payslip-label font-bold">TOTAL EARNINGS</td>
		    				<td class="payslip-amount font-bold">{total_earnings}</td>
		    			</tr>
		    		</tbody>
	    		</table>
			</div>
	    </div>

	</div>
	<div class="col-sm-6">
		<div class="panel panel-noborder">
	    	<div class="panel-body panel-body-nopad">
	    		<table class="table">
	    			<thead>
	    				<tr class="dark">
	    					<th class="payslip-label-th">DEDUCTIONS</th>
	    					<th class="payslip-amount-th">AMOUNT</th>
	    				</tr>
	    			</thead>
		    		<tbody>
		    			{deduction_content}
		    			<tr class="">
		    				<td class="payslip-label font-bold">TOTAL DEDUCTIONS</td>
		    				<td class="payslip-amount font-bold">{total_deductions}</td>
		    			</tr>
		    		</tbody>
	    		</table>

			</div>
	    </div>

	</div>
</div>

<div class="panel">
	<div class="panel-body">
	    <div class="row">
	    	<div class="col-sm-6">
	    		<table class="table table-outer-border">
	    			<tbody>
		    			<tr>
		    				<td class="payslip-label">Previous Balance</td>
		    				<td class="payslip-amount">0.00</td>
		    			</tr>
		    			<tr>
		    				<td class="payslip-label">Carry Over (excees)</td>
		    				<td class="payslip-amount">0.00</td>
		    			</tr>
		    		</tbody>
	    		</table>
	    	</div>
	    	<div class="col-sm-6">
	    		<table class="table table-outer-border">
	    			<tbody>
		    			<tr class="bg-primary">
		    				<td class="payslip-label font-bold"><h3 class="m0 p15">NET PAY</h3></td>
		    				<td class="payslip-amount font-bold"><h3 class="m0 p15"><span class="font-thin">Php</span> {total_takehome}</h3></td>
		    			</tr>
		    		</tbody>
	    		</table>
	    	</div>
	    </div>

	    <div class="row">
	    	<div class="col-sm-3 mt50">
	    		<table class="table text-center">
	    			<tbody>
		    			<tr>
		    				<td class="font-bold">Employer's Signature</td>
		    			</tr>
		    		</tbody>
	    		</table>
	    	</div>
	    	<div class="col-sm-3 col-sm-offset-6 mt50">
	    		<table class="table text-center">
	    			<tbody>
		    			<tr>
		    				<td class="font-bold">Employee's Signature</td>
		    			</tr>
		    		</tbody>
	    		</table>
	    	</div>
	    </div>


	</div>
</div>
