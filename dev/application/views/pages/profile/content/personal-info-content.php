<div class="table-grid">
	<div class="table-grid-cell" style="width:150px">
		<div class="btn-options-wrap img-avatar img-circle img-avatar border-light-gray">
	    	<img class="img-responsive pull-left" src="{profile_pic}" width="150" />
	  	</div>
	</div>  
	<div class="table-grid-cell vcenter pl15">
	 	<div class="btn-options-wrap">
	    	<h3 class="font-thin mt0 mb0 p5">{name}</h3>
	  	</div>
	  	<div class="slogan btn-options-wrap mt15 p5">{description}</div>
	</div>
	</div>
	<div class="row">
	<div class="col-xs-12 mt20 mb20">
		<ul class="contact-list contact-list-medium btn-options-wrap list-unstyled">
			<?php if($company_name!=""): ?>
	    		<li data-toggle="tooltip" data-placement="left" title="Company"><span class="fa fa-university"></span> <span>{company_name}</span></li>
	    	<?php endif; ?>
      		<?php if($address!=""): ?>
	        	<li data-toggle="tooltip" data-placement="left" title="Address"><span class="fa fa-map-marker"></span> <span>{address}</span></li>
	        <?php endif; ?>
      		<?php if($website!=""): ?>
	        	<li data-toggle="tooltip" data-placement="left" title="Website"><span class="fa fa-globe"></span> <span><a href="{website}" target="_blank">{website}</a></span></li>
	        <?php endif; ?>
	        <?php if($timezone!=""): ?>
	        	<li data-toggle="tooltip" data-placement="left" title="Timezone"><span class="fa fa-clock-o"></span> <span>{timezone}</span></li>
	        <?php else: ?>
	        	<li data-toggle="tooltip" data-placement="left" title="Timezone"><span class="fa fa-clock-o"></span> <span>PST – Pacific Standard Time</span></li>
	        <?php endif; ?>
	        <?php if($user_type_description!=""): ?>
	        	<li data-toggle="tooltip" data-placement="left" title="User Type"><span class="fa fa-user"></span> <span>{user_type_description}</span></li>
	        <?php endif; ?>
	    </ul>
	</div>
</div>