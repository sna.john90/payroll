<tr>
  <td class="pay-date">
  	<?php if($payrun_id==0): ?>
    	<a href="javascript:void(0)" class="text-muted">{payroll_date}</a>
	<?php else: ?>
		<a href="javascript:void(0)" class="view-payslip-btn" data-payrun-id="<?php echo encrypt($payrun_id); ?>">{payroll_date}</a>
	<?php endif; ?>
  </td>
  <td class="pay-label text-left">{notes}</td>
  <td class="pay-label">{amount}</td>
  <td class="pay-total">{total}</td>
  <td>{type}</td>
  <?php if(is_admin()): ?>
  <td>
  		<?php if($payrun_id==0): ?>
  			<a href="#" class="text-danger remove-manual-savings" data-user-id="{user_id_enc}" data-id="<?php echo $id; ?>"><i class="fa fa-times"></i></a>
  		<?php endif; ?>
  </td>
  <?php endif; ?>
</tr>