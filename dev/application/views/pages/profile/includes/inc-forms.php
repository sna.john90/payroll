<?php
#FORMs
?>
<div class="section">
	<div class="section-header mb15 clearfix">
		<div class="filter-option mt15 pull-right">
		    <ul class="list-inline">
            <li>
                <a href="#" class="btn btn-primary btn-sm" id="create-form-btn"><i class="fa fa-plus-circle"></i> New Leave Form</a>
            </li>
		         <li class="hidden">
		            <a href="#" class="btn btn-primary btn-sm" id="create-leave-form-btn"><i class="fa fa-plus-circle"></i> Leave Form</a>
		        </li>
		    </ul>
		</div>
		<h3 class="section-title"><i class="fa fa-files-o"></i> Forms</h3>
	</div>
	<div class="section-content">
	    <div class="table-responsive">
        <table id="dataTableLeave" class="table-nox table table-hover">
          <thead>
            <tr>
              <th>Date</th>
              <th class="text-left">Subject</th>
              <th class="text-left">Type</th>
              <th class="text-left">Period</th>
              <th class="text-left">Status</th>
              <th class="text-center">Notes</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="text-left"><?php echo rand(2014,2016).'/'.sprintf("%02d", rand(1,12)).'/'.sprintf("%02d", rand(1,30)); ?></td>
              <td class="text-left font-bold"><a href="#" class="view-leave-form-btn">Trip to Palawan</a></td>
              <td class="text-left">Vacation</td>
              <td class="text-left">Nov 15 - Nov 20</td>
              <td class="text-left">
                <div class="label label-success">Approved</div>
              </td>
              <td class="pay-note text-center"> 
                <a href="#" data-toggle="modal" data-target="#modalNote"><i class="fa fa-wpforms"></i></a>
              </td>
              <td class="">
                  <!-- NOTE: Visible only to admin -->
                  <div class="btn-group btn-group-xs">
                        <a href="" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                    </div>
              </td>

            </tr>
          <?php for($k=1;$k<=4;$k++): ?>
            <tr>
              <td class="text-left"><?php echo rand(2014,2016).'/'.sprintf("%02d", rand(1,12)).'/'.sprintf("%02d", rand(1,30)); ?></td>
              <td class="text-left font-bold"><a href="#" class="view-leave-form-btn">Sick</a></td>
              <td class="text-left">Sick Leave</td>
              <td class="text-left">Oct 1 - Oct 2</td>
              <td class="text-left">
                <div class="label label-success">Approved</div>
                <div class="label label-danger">Rejected</div>
                <div class="label label-default">Cancelled</div>
              </td>
              <td class="pay-note text-center"> 
                <a href="#" data-toggle="modal" data-target="#modalNote"><i class="fa fa-wpforms"></i></a>
              </td>
              <td class="">
              		<!-- NOTE: Visible only to admin -->
              		<div class="btn-group btn-group-xs">
                        <a href="" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                    </div>
              </td>

            </tr>
          <?php endfor; ?>
          </tbody>
        </table>
        <div>Start by clicking creating on <a href="#" class="create-form-btn">general form</a> or  <a href="#" class="create-leave-form-btn">leave form</a>.</div>
      </div>
	</div><!-- END: .section-content -->
</div>

