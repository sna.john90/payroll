<?php
#PAYROLL DETAILS
?>

<div class="section">
  <div class="pull-right"><span class="text-label">NEXT PAY DAY: </span><strong>{next_payday}</strong></div>
  <h3 class="section-title"><i class="fa fa-dollar"></i> My Payroll</h3>
  <div class="section-content quickview-panel well mt30">
  <div class="row m0">
    <div class="col-sm-4 pl5 pr5">
      <div class="panel panel-default mb5">
        <div class="panel-body text-right">
          <div class="font-xlg text-primary"><span class="font-md text-gray">PHP </span> {total_alltime_earning}</div>
          <div class="font-xs">ALL-TIME EARNINGS</div>
        </div>
      </div>
    </div>
    <div class="col-sm-4 pl5 pr5">
      <div class="panel panel-default mb5">
        <div class="panel-body text-right">
          <div class="font-xlg text-success"><span class="font-md text-gray">PHP </span> {total_year_earning}</div>
          <div class="font-xs">EARNINGS THIS YEAR</div>
        </div>
      </div>
    </div>
    <div class="col-sm-4 pl5 pr5">
      <div class="panel panel-default mb5">
        <div class="panel-body text-right">
          <div class="font-xlg text-info"><span class="font-md text-gray">PHP </span> {total_month_earning}</div>
          <div class="font-xs">EARNINGS THIS MONTH</div>
        </div>
      </div>
    </div>
  </div>
  </div>
  <div class="section-divider"></div>
  <div class="section-content">
      <div class="table-responsive">
        <table id="myPayroll2" class="table-nox table table-hover">
          <thead>
            <tr>
              <th>Date</th>
              <th>Basic Pay</th>
              <th>Incentives</th>
              <th>Deductions</th>
              <th>Total</th>
              <th class="text-center">Notes</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {payroll_lists}
          </tbody>
        </table>

      </div>
  </div><!-- END: .section-content -->



</div><!-- END: .section -->
