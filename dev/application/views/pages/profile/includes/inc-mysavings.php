<?php
#MY SAVINGS 
?>

<div class="section">
  <h3 class="section-title"><i class="fa fa-trophy"></i> My Savings</h3>

  <div class="section-content quickview-panel well mt30">
  <div class="row m0">
    <div class="col-sm-4 pl5 pr5">
      <div class="panel panel-default mb5">
				<div class="panel-body text-right">
					<div class="font-xlg text-primary"><span class="font-md text-gray">PHP </span> <?php echo number_format($overall_savings,2); ?></div>
					<div class="font-xs">TOTAL SAVINGS</div>
				</div>
			</div>
		</div>
   <!--  <div class="col-sm-4 pl5 pr5 ">
      <div class="panel panel-default mb5">
				<div class="panel-body text-right">
					<div class="font-xlg text-success"><span class="font-md text-gray">PHP </span> <?php echo number_format($this_year,2); ?></div>
					<div class="font-xs">SAVINGS THIS YEAR</div>
				</div>
			</div>
		</div>
    <div class="col-sm-4 pl5 pr5">
      <div class="panel panel-default mb5">
				<div class="panel-body text-right">
					<div class="font-xlg text-warning"><span class="font-md text-gray">PHP </span> <?php echo number_format($this_month,2); ?></div>
					<div class="font-xs">SAVINGS THIS MONTH</div>
				</div>
			</div>
		</div> -->
	</div>
	</div>
	<div class="section-divider"></div>
  <div class="section-content">
      <div class="table-responsive">
        <table class="table-nox table-payroll table table-hover">
          <thead>
            <tr>
              <th>Date</th>
              <th class="text-left">Particular</th>
              <th>Amount</th>
              <th>Total</th> 
              <th>Transaction Type</th>
              <?php if(is_admin()): ?>
              <th>Action</th>
              <?php endif; ?>
            </tr>
          </thead>
          <tbody>{mysavings_content}</tbody>
        </table>

      </div>
      <?php if(is_admin()): ?>
      <div class="">
          <a href="#" class="btn btn-primary add-saving-transaction pull-right btn-sm" data-id="{user_id_enc}"> <i class="fa-dollar fa"></i> ADD SAVING TRANSACTION</a>
      </div>
    <?php endif;?>
  </div><!-- END: .section-content -->
</div><!-- END: .section -->
<script type="text/javascript">
    
    $(document).ready(function(){
        $('#mysavings').on('click','.remove-manual-savings',function(){
          var this_ = $(this);
            var savings_id = $(this).attr("data-id");
            var id = $(this).attr("data-user-id");
            if(typeof savings_id != "undefined" && typeof id !="undefined")
            {
              me_message_v2({error:2,message:"Removing the transaction ..."});
              $.get(base_url+"savings/delete/"+id+"/"+savings_id,function(response){
                  me_message_v2(response);
                  if(response.error==0)
                  {
                    this_.parents("tr").fadeOut(1000,function(){
                      $(this).remove();
                    });
                  }
              });
            }
      })
    });

</script>