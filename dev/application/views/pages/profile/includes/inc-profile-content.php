<?php
#PROFILE DETAILS

$user_info      = get_user_info($id);
$emp_details    = get_employee_details($id); 
?>
<div class="section">
  <h3 class="section-title"><i class="fa fa-user-circle-o"></i> Personal Information</h3>
  <div class="section-content row mt30">
    <div class="col-sm-6">
      <ul class="list-table vmiddle list-unstyled">
        <li>
          <div class="list-table-label">First Name:</div>
          <div class="list-table-content"><?php echo $user_info['firstname'];?></div>
        </li>
        <li>
          <div class="list-table-label">Last Name:</div>
          <div class="list-table-content"><?php echo $user_info['lastname'];?></div>
        </li>
        <li>
          <div class="list-table-label">Middle Name:</div>
          <div class="list-table-content"><?php echo $user_info['middlename'];?></div>
        </li>
        <li>
          <div class="list-table-label">Birthday:</div>
          <div class="list-table-content"><?php echo format_date($user_info['birthdate'],"F j, Y"); ?></div>
        </li>
        <li>
          <div class="list-table-label">Address:</div>
          <div class="list-table-content"><?php echo $user_info['address'];?></div>
        </li>
      </ul>
    </div>
    <div class="col-sm-6">
      <ul class="list-table list-unstyled">
        <li>
          <div class="list-table-label">Email:</div>
          <div class="list-table-content"><a href="mailto:johnmcsmith@work.com"><?php echo $user_info['email'];?></a></div>
        </li>
        <li>
          <div class="list-table-label">Phone:</div>
          <div class="list-table-content"><?php echo $user_info['phone'];?></div>
        </li>
        <!-- <li>
          <div class="list-table-label">Blood Type:</div>
          <div class="list-table-content">B+</div>
        </li>
        <li>
          <div class="list-table-label">Emergency Contact Person:</div>
          <div class="list-table-content">Jane McSmith</div>
        </li>
        <li>
          <div class="list-table-label">Emergency Contact No.:</div>
          <div class="list-table-content">+63.917.987.6543</div>
        </li> -->
      </ul>

    </div>
  </div><!-- END: .section-content -->

  <div class="section-divider hidden"></div>

  <div class="section-content row hidden">
    <div class="col-sm-5">
      <h3 class="section-title"><i class="fa fa-flask"></i> Skills <a href="#" class="btn-simple font-md mt5 pull-right"><i class="fa fa-plus"></i></a></h3>
      <ul class="list-info list-unstyled mt30">
        <li>
          <div class="list-info-title">PHP</div>
          <div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
            </div>
          </div>
        </li>
        <li>
          <div class="list-info-title">Javascrip</div>
          <div class="progress">
            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
            </div>
          </div>
        </li>
        <li>
          <div class="list-info-title">MySQL</div>
          <div class="progress">
            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%">
            </div>
          </div>
        </li>
        <li>
          <div class="list-info-title">Photoshop</div>
          <div class="progress">
            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%">
            </div>
          </div>
        </li>
        <li>
          <div class="list-info-title">Angular JS</div>
          <div class="progress">
            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col-sm-6 col-sm-offset-1">
       <h3 class="section-title"><i class="fa fa-black-tie"></i> Work Experience <a href="#" class="btn-simple font-md mt5 pull-right"><i class="fa fa-plus"></i></a></h3>
       <ul class="list-info list-unstyled mt30">
          <li>
            <div class="list-info-title">Goodle Inc. <span class="font-thin font-xs">Jun 2015 - Present</span></div>
            <div class="list-info-content font-xs">Web Developer</div>
          </li>
          <li>
            <div class="list-info-title">Macrosoft Creatives <span class="font-thin font-sm">Feb 2013 - Jun 2015</span></div>
            <div class="list-info-content font-xs">Web Developer</div>
          </li>
          <li>
            <div class="list-info-title">Bluebox Media <span class="font-thin font-sm">Nov 2012 - Feb 2013</span></div>
            <div class="list-info-content font-xs">Junior Web Developer</div>
          </li>
        </ul>

    </div>
  </div><!-- END: .section-content -->

  <div class="section-divider hidden"></div>

  <div class="section-content row hidden">
    <div class="col-sm-6">
     <h3 class="section-title"><i class="fa fa-graduation-cap"></i> Education <a href="#" class="btn-simple font-md mt5 pull-right"><i class="fa fa-plus"></i></a></h3>
     <ul class="list-info list-unstyled mt30">
        <li>
          <div class="list-info-title">University of Harvard <span class="font-thin font-xs">Jun 2008 - March 2012</span></div>
          <div class="list-info-content font-xs">Information Technology</div>
        </li>
        <li>
          <div class="list-info-title">Awesome High School <span class="font-thin font-sm">Jun 2004 - Jun 2008</span></div>
          <div class="list-info-content font-xs">Web Developer</div>
        </li>
        <li>
          <div class="list-info-title">Bright Academy <span class="font-thin font-sm">Jun 1998 - Mar 2004</span></div>
          <div class="list-info-content font-xs">Junior Web Developer</div>
        </li>

      </ul>
    </div>
    <div class="col-sm-5 col-sm-offset-1">


    </div>
  </div><!-- END: .section-content -->

</div><!-- END: .section -->
