<?php
#PROFILE WIDGET
?>
<?php 
  $userinfo     = get_user_info($id);
  $emp_details  = get_employee_details($id); 
?>
<div class="panel panel-nox">
  <div class="panel-heading panel-heading-nox bg-blue-grad">
      <div class="pt30 pb15">
        <a href="#" class="avatar-img photo-box photo-box-hovered photo-box-lg text-center center-block">
          <img class="img-responsive" src="<?php echo profile_picture($userinfo['profile_pic']);?>">
        </a>
      </div>
      <div class=" text-center text-white pb10">
        <div class="profile-name font-xlg font-thin"><?php echo get_name($id); ?></div>
        <div class="profile-title font-xs text-uppercase letter-space2"><?php echo $emp_details['designation'];?></div>
      </div>
  </div>
  <div class="panel-body">
    <div class="profile-mini pt15">
      <ul class="list-table list-unstyled">
        <li>
          <div class="list-table-label">ID #:</div>
          <div class="list-table-content"><?php echo $userinfo['emp_id'];?></div>
        </li>
        <li>
          <div class="list-table-label">Department:</div>
          <div class="list-table-content"><?php echo $emp_details['name'];?></div>
        </li>
        <li>
          <div class="list-table-label">Date Hired:</div>
          <div class="list-table-content"><?php echo date("M j Y",strtotime($emp_details['date_hired'])); ?></div>
        </li>
        <li>
          <div class="list-table-label">Pay Mode:</div>
          <div class="list-table-content"><?php echo ucfirst($emp_details['status']);?></div>
        </li>
        <li>
          <div class="list-table-label">Status:</div>
          <div class="list-table-content text-success"><?php echo ($userinfo['status']==1)? "Active" : "Inactive"; ?></div>
        </li>
        <li class="divider"><div></div><div></div>
        </li>
        <li>
          <div class="list-table-label">SSS:</div>
          <div class="list-table-content"><?php echo $userinfo['sss'];?></div>
        </li>
        <li>
          <div class="list-table-label">TIN:</div>
          <div class="list-table-content"><?php echo $userinfo['tin'];?></div>
        </li>
        <li>
          <div class="list-table-label">Philhealth:</div>
          <div class="list-table-content"><?php echo $userinfo['philhealth'];?></div>
        </li>
        <li>
          <div class="list-table-label">Pag-ibig:</div>
          <div class="list-table-content"><?php echo $userinfo['pagibig'];?></div>
        </li>
        <li>
          <div class="list-table-label">Bank Account:</div>
          <div class="list-table-content"><?php echo $userinfo['bankaccount'];?></div>
        </li>
      </ul>
    </div>
  </div>
</div>
