<?php
#SETTINGS DETAILS

$user_info      = get_user_info($id);
?>
<div class="section">
  <div class="section-content row">
    <div class="col-sm-6">
      <?php echo form_open("",array("class" => "changepassword-form"));?>
	     <h3 class="section-title"><i class="fa fa-key"></i> Account Settings</h3>
      <ul class="list-table form-modern list-table-borderedX list-unstyled">
        <?php if(is_admin()): ?>
        <li>
          <div class="list-table-label">Firstname:</div>
          <div class="list-table-content"><input type="text" class="form-control" name="firstname" value="<?php echo $user_info['firstname'];?>"></div>
        </li>
        <li>
          <div class="list-table-label">Lastname:</div>
          <div class="list-table-content"><input type="text" class="form-control" name="lastname" value="<?php echo $user_info['lastname'];?>"></div>
        </li>
        <li>
          <div class="list-table-label">Username:</div>
          <div class="list-table-content"><input type="text" class="form-control" name="username" value="<?php echo $user_info['username'];?>"></div>
        </li>  
        <?php else: ?>
        <li>
          <div class="list-table-label">Username:</div>
          <div class="list-table-content"><span class="text-muted"><?php echo $user_info['username'];?></span></div>
        </li>
        <?php endif; ?>
        <li>
          <div class="list-table-label">Password:</div>
          <div class="list-table-content"><input type="Password" class="form-control" name="new_password" placeholder="*******"></div>
        </li>

        <li>
          <div class="list-table-label"></div>
          <div class="list-table-content text-right"><button type="button" class="btn btn-primary btn-sm changepassword-btn">Save Changes</button></div>
        </li>
      </ul>
      </form>
    </div>
    <div class="col-sm-5 col-sm-offset-1">
		
      
    </div>
  </div><!-- END: .section-content -->

  

</div><!-- END: .section -->


<script type="text/javascript">
  $(document).ready(function(){
      $('.changepassword-btn').on('click',function(){
          me_message_v2({error:2,message:"Changing password."});
          $.post(base_url+"profile/change_password",$('.changepassword-form').serialize(),function(response){
            me_message_v2(response);
          }); 
             <?php if(is_admin()): ?>
          $.post(base_url+"profile/update_basic_info",$('.changepassword-form').serialize(),function(response){
            me_message_v2(response);
          }); 
          <?php endif; ?>
      });
  });
</script>