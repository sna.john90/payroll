<!-- PROFILE SECTION -->   
<div class="container-fluid">
    <div class="row">
        <div class="left-pane col-sm-4 col-md-3"> 
          {widget}
        </div><!-- END: .left-pane -->
       <div class="right-pane col-sm-8 col-md-9">
          <div class="panel-nav">
            <ul class="nav nav-tabs nav-tabs-nox" role="tablist">
              <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-user-circle-o"></i> <span class="hidden-xs">Profile</span></a></li>
              <li role="presentation"><a href="#payroll" aria-controls="payroll" role="tab" data-toggle="tab"><i class="fa fa-dollar"></i> <span class="hidden-xs">Payroll</span></a></li>
              <li role="presentation"><a href="#mysavings" aria-controls="mysavings" role="tab" data-toggle="tab"><i class="fa fa-trophy"></i> <span class="hidden-xs"><span class="hidden-sm">My</span> Savings</span></a></li>
              <!-- <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><i class="fa fa-files-o"></i> <span class="hidden-xs">Forms</span></a></li> -->
              <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa-cog"></i> <span class="hidden-xs hidden-sm">Settings</span></a></li>
            </ul>
          </div>
          <div class="panel panel-nox fullH">
            <div class="panel-body">
                  <!-- Tab panes -->
                  <div class="tab-content pl10 pr10">
                    <div role="tabpanel" class="tab-pane active" id="profile">{content}</div>
                    <div role="tabpanel" class="tab-pane" id="payroll">{my_payroll}</div>
                    <div role="tabpanel" class="tab-pane" id="mysavings">{my_savings}</div>
                    <div role="tabpanel" class="tab-pane" id="messages">{forms}</div>
                    <div role="tabpanel" class="tab-pane" id="settings">{settings}</div>
                  </div>
            </div>
          </div>
       </div><!-- END: .right-pane -->    
    </div>
</div>  