<!-- MODAL ADD EXPENSE CATEGORIES-->

<div class="modal-header">
<button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="panel-title">Add Savings</h4>
</div>
<div class="modal-body">
	<!-- {expenses_categories} -->
	<div class="form-modern row">
	      <div class="col-sm-12">
	      	 <?php echo form_open("",array("class"=>"add-savings-form"));?>
	          <ul class="list-table vmiddle list-unstyled">
	            <li>
	              <div class="list-table-label">Date:</div>
	              <div class="list-table-content"> <input class="form-control date" name="date" value="<?php echo date("Y-m-d");?>"></div>
	            </li>
	            <li>
	              <div class="list-table-label">Amount:</div>
	              <div class="list-table-content"> <input type="text" class="form-control" name="value"></div>
	            </li>
	            <li>
	              <div class="list-table-label">Transaction:</div>
	              <div class="list-table-content">
	              		<select name="type" class="form-control">
	              			<option value="manual-deposit">Deposit</option>
	              			<option value="manual-withdraw">Withdraw</option>
	              		</select>
	              </div>
	            </li>
	            <li>
	              <div class="list-table-label">Notes (Optional):</div>
	              <div class="list-table-content"> <input class="form-control"  name="notes" ></div>
	            </li> 
	          </ul>
	          </form>
	      </div>
	  </div>
</div>

<div class="modal-footer">
  <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
  <button type="button" class="btn btn-primary btn-sm do-add-saving-transaction"> <i class="fa-plus fa"></i> Add Transaction</button>
</div> 

<script type="text/javascript">
	$(document).ready(function(){
		 $('.date').datetimepicker({
       		 format: 'YYYY-MM-DD'
      	}); 
	});
</script>