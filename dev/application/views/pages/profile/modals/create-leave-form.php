<?php
#LEAVE FORM
?>
<div class="modal-body">
  <button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<div class="section">
  <div class="section-content">
      <div class="row">
        <div class="col-sm-6">
          <div class="p15">
            <h3 class="m0">Smartstart Inc.</h3>
            <div>Ols Bldg, Gorordo Ave., Cebu City</div>
            <div>Cebu Philippines 6000</div>
          </div>
        </div>
        <div class="col-sm-6 text-right">
          <img class="img-responsive mt10" style="margin-left: auto;" src="{base_url}assets/logo.png" />
        </div>
      </div>

       <h2 class="section-title mt50 mb50 text-center">Leave Request Form</h2>

      <div class="row mt30">
        <div class="col-sm-6">
          <ul class="list-table vmiddle list-table-payslip list-unstyled pl15 pr15">
              <li>
                <div class="list-table-label">Name:</div>
                <div class="list-table-content font-bold">John Smith</div>
              </li>
              <li>
                <div class="list-table-label">Designation:</div>
                <div class="list-table-content">Web Developer</div>
              </li>
              <li>
                <div class="list-table-label">Department:</div>
                <div class="list-table-content">IT Department</div>
              </li>
            </ul>
        </div>
        <div class="col-sm-6">
          <ul class="list-table list-table-payslip list-unstyled pl15 pr15">
              <li>
                <div class="list-table-label">Employee ID: </div>
                <div class="list-table-content">s1001</div>
              </li>
              <li>
                <div class="list-table-label">Application Date:</div>
                <div class="list-table-content">Dec. 30, 2016</div>
              </li>
              <li>
                <div class="list-table-label">Reporting to:</div>
                <div class="list-table-content">Rodrigo Chan</div>
              </li>
            </ul>
        </div>
      </div>
  </div><!-- END: .section-content -->
</div>

<div class="section-divider"></div>

<div class="section pl15 pr15">
  <div class="section-content row">
    <div class="col-sm-6">
      <h5 class="mt30 mb15">Date of Absense:</h5>
      <div class="form-modern pl15">
        <div class="form-inline">
          <div class="form-group">
              <label>From</label>
              <input type="text" class="form-control" placeholder="From date..." style="max-width:150px;">
          </div>
          <div class="form-group">
              <label>To</label>
              <input type="text" class="form-control" placeholder="To date..." style="max-width:150px;">
          </div>
        </div>  
      </div>
      <h5 class="mt50 mb15">Type of Absence Requested <small>(Please choose the relevant reason)</small></h5>
      <div class="form-modern pl15">
        <div class="radio">
          <label>
            <input type="radio" name="optionsTypeOfLeave" id="optionsTypeOfLeave1" value="option1" checked>
            Sick
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="optionsTypeOfLeave" id="optionsTypeOfLeave2" value="option1">
            Bereavement
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="optionsTypeOfLeave" id="optionsTypeOfLeave2" value="option1">
            Time Off Without Pay
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="optionsTypeOfLeave" id="optionsTypeOfLeave2" value="option1">
            Personal Leave
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="optionsTypeOfLeave" id="optionsTypeOfLeave2" value="option1">
            Maternity/Paternity
          </label>
        </div>
        <div class="radio form-inline">
          <label>
            <input type="radio" name="optionsTypeOfLeave" id="optionsTypeOfLeave2" value="option1">
            Others
          </label>
           <input type="text" class="form-control" name="" placeholder="Please Specify" style="max-width:300px;">
        </div>
      </div>
      

    </div>

    <div class="col-sm-5 col-sm-offset-1">
      <h5 class="mt30 mb15">Reason for Absense:</h5>
      <div class="form-modern">
        <textarea class="form-control"></textarea>  
      </div>
    </div>
    
  </div><!-- END: .section-content -->

  <div class="row">
      <div class="col-sm-3 mt50">
        <table class="table text-center mt35">
          <tbody>
            <tr>
              <td class="font-bold">Employee's Signature</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="col-sm-3 col-sm-offset-6 mt50">
      <div class="form-simple">
        <div class="form-inline">
              <input type="text" class="form-control text-center" placeholder="" style="max-width:200px;">
        </div>  
      </div>
        <table class="table text-center">
          <tbody>
            <tr>
            </tr>
            <tr>
              <td class="font-bold">Date</td>
            </tr>
          </tbody>
        </table>

      </div>
    </div>

</div>

<div class="section-divider"></div>

<div class="section pl15 pr15">
  <div class="section-content">
    <h4 class="p15 bg-darkgray text-center mb30">Recommendation</h4>
    <div class="row">
        <div class="col-sm-5 col-sm-offset-1">
          
            <ul class="list-inline pl30">
              <li>
                <div class="text-label">STATUS: </div>
              </li>
              <li>
                    <label class="radio-inline text-success">
                      <input type="radio" name="leaveApproval" id="optionsTypeOfLeave1" value="option1">
                      Approve
                    </label>
                    <label class="radio-inline text-danger">
                      <input type="radio" name="leaveApproval" id="optionsTypeOfLeave2" value="option2">
                      Disapproved
                    </label>
              </li>
            </ul>
        
        </div>
        <div class="col-sm-5 col-sm-offset-1">
        <div class="form form-modern">
          <label class="text-label">Comments:</label>
          <textarea class="form-control" placeholder=""></textarea>
        </div>
        </div>
      </div>

    <div class="row">
        <div class="col-sm-3 mt50">
          <table class="table text-center mt35">
            <tbody>
              <tr>
                <td class="font-bold">Authorized Signature</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="col-sm-3 col-sm-offset-6 mt50">
        <div class="form-simple">
          <div class="form-inline">
                <input type="text" class="form-control text-center" placeholder="" style="max-width:200px;">
          </div>  
        </div>
          <table class="table text-center">
            <tbody>
              <tr>
              </tr>
              <tr>
                <td class="font-bold">Date</td>
              </tr>
            </tbody>
          </table>

        </div>
      </div>
  </div><!-- END: .section-content -->
</div>


</div>