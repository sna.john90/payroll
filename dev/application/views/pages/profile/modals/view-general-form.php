<div class="modal-body">
	<button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<div class="section">
  <div class="section-content">
      <div class="row">
        <div class="col-sm-6">
          <div class="p15">
            <h3 class="m0">Smartstart Inc.</h3>
            <div>Ols Bldg, Gorordo Ave., Cebu City</div>
            <div>Cebu Philippines 6000</div>
          </div>
        </div>
        <div class="col-sm-6 text-right">
          <img class="img-responsive mt10" style="margin-left: auto;" src="{base_url}assets/logo.png" />
        </div>
      </div>

       <h2 class="section-title mt50 mb50 text-center">Request Form</h2>

      <div class="row mt30">
        <div class="col-sm-6">
          <ul class="list-table vmiddle list-table-payslip list-unstyled pl15 pr15">
              <li>
                <div class="list-table-label">Name:</div>
                <div class="list-table-content font-bold">John Smith</div>
              </li>
              <li>
                <div class="list-table-label">Designation:</div>
                <div class="list-table-content">Web Developer</div>
              </li>
              <li>
                <div class="list-table-label">Department:</div>
                <div class="list-table-content">IT Department</div>
              </li>
            </ul>
        </div>
        <div class="col-sm-6">
          <ul class="list-table list-table-payslip list-unstyled pl15 pr15">
              <li>
                <div class="list-table-label">Employee ID: </div>
                <div class="list-table-content">s1001</div>
              </li>
              <li>
                <div class="list-table-label">Application Date:</div>
                <div class="list-table-content">Dec. 30, 2016</div>
              </li>
              <li>
                <div class="list-table-label">Reporting to:</div>
                <div class="list-table-content">Rodrigo Chan</div>
              </li>
            </ul>
        </div>
      </div>
  </div><!-- END: .section-content -->

  <div class="section-divider"></div>
          

    <div class="mt30 mb30">
        <div class="form-horizontal form-modern pr15"> 
            <div class="form-group">  
              <label class="col-sm-2 control-label text-label">Subject: </label>
              <div class="col-sm-10">
                  <div class="data-container">Ex. Request for information/recommendation</div>
              </div>
          </div> 
            <div class="form-group">  
                <label class="col-sm-2 control-label text-label">Message: </label>
                <div class="col-sm-10">
                    <div class="data-container h100">wysywig textarea</div>
                    <div class="form-underline"></div>
                </div>
            </div> 
        </div> 
    </div>
</div>
<div class="text-right">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
</div>
</div>