<!-- MODAL MAKEPAYMENT -->
<div class="modal-header bg-blue">
  <button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
  <h4 class="">{fullname}'s Profile</h4>
</div>
<div class="modal-body modal-body-gray clearfix">

    <div class="row">
        <div class="left-pane col-sm-4 col-md-3">
          {profile_widget}
        </div><!-- END: .left-pane -->
        <div class="right-pane col-sm-8 col-md-9">
          <div class="panel-nav">
            <ul class="nav nav-tabs nav-tabs-nox" role="tablist">
              <li role="presentation" class="<?php echo ($active_tab=="profile")? "active" : "";?>"><a href="#userProfile" aria-controls="userProfile" role="tab" data-toggle="tab"><i class="fa fa-user-circle-o"></i> Profile</a></li> 
               <li role="presentation" class="<?php echo ($active_tab=="savings")? "active" : "";?>"><a href="#mysavings" aria-controls="mysavings" role="tab" data-toggle="tab"><i class="fa fa-trophy"></i> <span class="hidden-xs"><span class="hidden-sm">My</span> Savings</span></a></li>
            </ul>
          </div>
          <div class="panel panel-nox">
            <div class="panel-body">
                  <!-- Tab panes -->
                  <div class="tab-content pl10 pr10">
                    <div role="tabpanel" class="tab-pane <?php echo ($active_tab=="profile")? "active" : "";?>" id="userProfile">{profile_content} </div> 
                     <div role="tabpanel" class="tab-pane <?php echo ($active_tab=="savings")? "active" : "";?>"" id="mysavings">{my_savings}</div>
                  </div>
            </div>
          </div>
        </div><!-- END: .right-pane -->
      </div>



</div>
<script type="text/javascript">
  jQuery(document).ready(function(){
     //$('[rel=tooltip]').tooltip({ trigger: "hover" });
  });
</script>
