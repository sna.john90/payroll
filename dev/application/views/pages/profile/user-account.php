<div id="profileContainer" class="page-content">
  <input type="hidden" id="user-id-val" value="{id}">
  <div class="container">
    <div class="row">
      <!-- LEFT -->
      <div class="col-sm-3 col-xs-12">
        <h1 class="font-thin mt0">PROFILE</h1>
        <ul class="company-list list-unstyled mb30">
            <li><a href="{base_url}profile/edit">Profile Information</a></li>
            <li class="active"><a href="javascript:void(0)">User Accounts</a></li>
          	<!-- <li><a href="javascript:void(0)">Signings</a></li>
          	<li><a href="javascript:void(0)">Reviews</a></li>
          	<li><a href="javascript:void(0)">Signings</a></li>
          	<li><a href="javascript:void(0)">Credentials</a></li> -->
        </ul>
      </div> 
      <!-- RIGHT -->
      <div class="col-sm-9 col-xs-12 mb100">  
       	<div class="profile-container">
       		<h4>Account Information</h4>
       		<hr/>
       		<div id="account-info-container">
       			{account_info_content}
       		</div>
       	</div>
      </div>
    </div>
  </div>
</div>