<?php
#MAINTAINABLE - DEPARTMENT
?>

<div class="container department-container">
  
	<div class="row">

	  	<div class="col-sm-12">
	  		<div class="panel panel-nox">
			    <div class="panel-heading">
			       <h3 class="font-thin text-uppercase">Department</h3>
			    </div>
			    <div class="panel-body">
			      
			      	<div class="table-responsive">
				        <table class="table table-hover">
				          <thead>
				            <tr>
				              <th>Name</th>
				              <th>Description</th>
				              <th class="text-right">Action</th>
				            </tr>
				          </thead>
				          <tbody>
				          	<?php foreach($lists as $value): 
				          		$temp_id = encrypt($value['id']);
				          	?>
				            <tr>
				              	<td><?php echo $value['name'];?></td>
				              	<td><?php echo $value['description'];?></td>
			                    <td class="text-right">
			                    	<?php if($value['company_id']!=0 OR is_admin()): ?>
				                    <div class="btn-group btn-group-xs">
				                        <a href="#" class="btn btn-primary update-department-btn" data-id="<?php echo $temp_id; ?>"><i class="fa fa-pencil"></i></a>
				                        <a href="" class="btn btn-danger delete-department-btn" data-id="<?php echo $temp_id; ?>"><i class="fa fa-trash-o"></i></a>
				                    </div>
				                	<?php endif; ?>
			                    </td>
			                 </tr>
			                  <?php if(!empty($value['children'])): ?>
					                 <?php foreach($value['children'] as $skey=>$svalue): 
					                 	$temp_id = encrypt($svalue['id']);
					                 ?>
					                 <tr class="list-level-2">   
					                    <td><?php echo $svalue['name'];?></td>
					                    <td><?php echo $svalue['description'];?></td>
					                    <td class="text-right">
						                    <div class="btn-group btn-group-xs">
						                        <a href="#" data-id="<?php echo $temp_id; ?>" class="btn btn-primary update-department-btn" ><i class="fa fa-pencil"></i></a>
				                        <a href="#" data-id="<?php echo $temp_id; ?>" class="btn btn-danger delete-department-btn"><i class="fa fa-trash-o"></i></a>
						                    </div>
					                    </td>
					                 </tr> 
					                 <?php endforeach; ?>
				             	<?php endif;?>
			             	<?php endforeach; ?>
				          </tbody>
				        </table>

				    </div>
				    <div class="text-right mt15">
				        <button type="button" class="btn btn-primary add-department-btn" ><i class="fa fa-plus-circle"></i> Add</button>
				    </div>
			    </div>
			</div> <!-- END .panel -->

	  	</div>

	</div>


</div><!-- END: .cotainer*/


