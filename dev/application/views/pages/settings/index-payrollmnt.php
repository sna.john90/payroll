<?php
#MAINTAINABLE - DEPARTMENT
?>

<div class="container department-container">
  
	<div class="row">

	  	<div class="col-sm-12">
	  		<div class="panel panel-nox">
			    <div class="panel-heading">
			       <h3 class="font-thin text-uppercase">PAYROLL INCENTIVES & DEDUCTIONS</h3>
			    </div>
			    <div class="panel-body">
			      
			      	<div class="table-responsive">
				        <table class="table table-hover">
				          <thead>
				            <tr>
				              <th>Name</th> 
				              <th>Type</th>
				              <th>Is Taxable</th>
				              <th class="text-right hidden">Action</th>
				            </tr>
				          </thead>
				          <tbody>
				          	<?php foreach($lists as $value): 
				          		$temp_id = encrypt($value['id']);
				          	?>
				            <tr> 
				              	<td><?php echo $value['description'];?></td>
				              	<td><?php echo $value['type'];?></td>
				              	<td><?php echo $value['is_taxable'];?></td>
			                    <td class="text-right hidden">
			                    	<?php if($value['company_id']!=0): ?>
				                    <div class="btn-group btn-group-xs">
				                        <a href="#" class="btn btn-primary update-department-btn" data-id="<?php echo $temp_id; ?>"><i class="fa fa-pencil"></i></a>
				                        <a href="" class="btn btn-danger delete-department-btn" data-id="<?php echo $temp_id; ?>"><i class="fa fa-trash-o"></i></a>
				                    </div>
				                	<?php endif; ?>
			                    </td>
			                 </tr>
			             	<?php endforeach; ?>
				          </tbody>
				        </table>

				    </div>
				    <div class="text-right mt15 hidden">
				        <button type="button" class="btn btn-primary add-department-btn" ><i class="fa fa-plus-circle"></i> Add</button>
				    </div>
			    </div>
			</div> <!-- END .panel -->

	  	</div>

	</div>


</div><!-- END: .cotainer*/


