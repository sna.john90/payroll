<div id="settings-container" class="page-content">
  <input type="hidden" id="user-id-val" value="{id}">
  <div class="container">
    <div class="row">
      <!-- LEFT -->
      <div class="col-sm-3 col-xs-12">
        <h1 class="font-thin mt0">Settings</h1>
        <ul class="company-list list-unstyled mb30">
          <li class="active"><a href="javascript:void(0)">User Privileges</a></li>
        </ul>
      </div> 
      <!-- RIGHT -->
      <div class="col-sm-9 col-xs-12 mb50">  
        <div class="profile-container">
          <div class="pull-right">
            <button type="button" class="btn btn-primary btn-sm" id="add-user-role-btn"><i class="fa fa-plus-circle"></i> Add</button>
          </div>
          <h4>User Special Roles</h4>
          <hr/>
          <div id="personal-info-container">
            <div class="table-responsive" style="margin-top:-26px;">
              <table id="specialRoleTable" class="table table-hover">
                <thead class="threadcolor"> 
                  <tr> 
                    <th class="width-250">Name</th>
                    <th>Special Role</th>
                    <th>Date Added</th>
                    <th class="no-sort width-60">Action</th>
                  </tr> 
                </thead>
                <tbody></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
