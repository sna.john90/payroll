<div class="modal-header bg-graylite">
        <button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="panel-title">Add Department</h4>
      </div>
      <div class="modal-body"> 
      <div class="form-minimalist row mb30">
          <div class="col-sm-12">
              <?php echo form_open("",array("class" => "add-department-form")); ?>
              <ul class="list-table list-unstyled">
                <li>
                  <div class="list-table-label">Department Name:</div>
                  <div class="list-table-content"> <input class="form-control" name="name" placeholder="Software Engineering"></div>
                </li>
                 <li>
                  <div class="list-table-label">Description:</div>
                  <div class="list-table-content"> <input class="form-control" name="description" ></div>
                </li>
                <li>
                  <div class="list-table-label">Parent Category:</div>
                  <div class="list-table-content">
                    <select class="form-control" name="parent_id">
                      <option value="0">None</option>
                      <?php foreach(get_departments() as $key=>$value): ?>
                            <?php if(isset($value['children']) && !empty($value['children'])): ?>
                                      <optgroup label="<?php echo $value['name']?>">
                                        <?php foreach($value['children'] as $child): ?>
                                              <option value="<?php echo $child['id'];?>"><?php echo $child['name'];?></option>
                                        <?php endforeach;?>
                                      </optgroup>
                            <?php else: ?>
                                 <option value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
                            <?php endif;?>
                    <?php endforeach; ?> 
                      </select>
                  </div>
                </li>
              </ul>
              </form>
          </div>
      </div> 
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary btn-sm doadd-department-btn">Add Department</button>
      </div>