<div class="modal-body">
  <input type="hidden" id="company-id" value="{company_id}">
  <div class="mb15 pb15" style="border-bottom: 1px solid #f2f2f2">
    <h4 class="mb0">Add Special Role</h4>
    <small>You can add special priviliges to certain users.</small>
    <?php echo form_open(base_url()."settings/add_special_role",array("id" => "add-special-role-form", "class" => "form-horizontal mt30 pr15")); ?>
      <div class="row form-group">
        <label class="col-xs-12 col-sm-3 control-label">Search User</label>
        <div class="col-xs-12 col-sm-9 pl0 pr0">
          <select name="user_id" id="search-user-id-val"></select>
        </div>
      </div>
      <div class="row form-group">
        <label class="col-xs-12 col-sm-3 control-label">User Privilege</label>
        <div class="col-xs-12 col-sm-9 pl0 pr0">
          <select name="user_group_id" id="user-group-id-val">
            <option></option>
            <option value="7">Lender Admin</option>
            <option value="2">Loan Officer</option>
            <option value="3">Processor</option>
            <option value="4">Underwriter</option>
          </select>
        </div>
      </div>
      <div class="form-group text-right mt30 clearfix">
        <button type="button" class="btn btn-default btn-sm mr3" data-dismiss="modal"><i class="fa fa-times-circle"></i> Cancel</a>
        <button type="submit" class="btn btn-primary btn-sm submit-btn"><i class="fa fa-plus-circle"></i> Save</button>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){

    var company_id = $("#company-id").val();

    $("#user-group-id-val").selectize({
      placeholder: "Select user type"
    });

    $("#search-user-id-val").selectize({
      plugins:['no_results'],
      valueField: 'id',
      labelField: 'name',
      searchField: 'name',
      placeholder: "Select user",
      create: false,
      preload: true,
      render: {
        option: function(item, escape){
          return '<div class="options-container"><img class="img-responsive pull-left pr15" src="'+item.profile_pic+'" width="48"/><a href="javascript:void(0)" class="options-text">'+escape(item.name)+'</a></div>';
        }
      },
      load: function(query, callback) {
        $.ajax({
          url: base_url+'users/users_lists/'+company_id+'/'+encodeURIComponent(query),
          type: 'GET',
          dataType: 'json',
          error: function(){
            callback();
          },
          success: function(response){
            callback(response.data);
          }
        });
      }
    });

    var form = $('#add-special-role-form');
    // submit admin status form
    $('#add-special-role-form').ajaxForm({
      dataType: 'json',
      beforeSend:function(){
        form.find('.submit-btn').html('<i class="fa fa-plus-circle"></i> Saving ... ');
        form.find('.submit-btn').prop("disabled",true);
      },
      success:function(response){
        form.find('.submit-btn').html('<i class="fa fa-plus-circle"></i> Save');
        form.find('.submit-btn').prop("disabled",false);
        response_message(response);
        if(response.error==0)
        {
          // reload datatable special status
          $("#specialRoleTable").DataTable().ajax.reload(null,false); 

          $("body #modalAddSpecialRole").modal("hide");
          $("body #modalAddSpecialRole").on('hidden.bs.modal',function () {
            $(this).data('bs.modal', null);
          });
        }
      }
    });

  });
</script>