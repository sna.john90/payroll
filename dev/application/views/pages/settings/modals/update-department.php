<div class="modal-header bg-graylite">
        <button type="button" class="close close-tr" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="panel-title">Update Department</h4>
      </div>
      <div class="modal-body"> 
      <div class="form-minimalist row mb30">
          <div class="col-sm-12">
              <?php echo form_open("",array("class" => "update-department-form")); ?>

              <input type="hidden" name="id" value="{user_id}" />
              <ul class="list-table list-unstyled">
                <li>
                  <div class="list-table-label">Department Name:</div>
                  <div class="list-table-content"> <input class="form-control" name="name" value="{name}" placeholder="Software Engineering"></div>
                </li>
                 <li>
                  <div class="list-table-label">Description:</div>
                  <div class="list-table-content"> <input class="form-control" value="{description}" name="description" ></div>
                </li>
                <li>
                  <div class="list-table-label">Parent Category:</div>
                  <div class="list-table-content">
                    <select class="form-control" name="parent_id">
                      <option value="0">None</option>
                     <?php 
                        $cats = get_departments();  
                          if(!empty($cats))
                          {
                              ?>
                                 <?php foreach($cats as $value): 
                                      $selected = ($value['id']==$parent_id)? "selected" : ""; 
                                 ?>
                                      <option <?php echo $selected; ?> value="<?php echo $value['id']; ?>" ><?php echo ucfirst($value['name']);?></option>
                                      <?php if(!empty($value['children'])): ?>
                                          <?php foreach($value['children'] as $svalue): 
                                            $selected = ($svalue['id']==$parent_id)? "selected" : ""; 
                                          ?>
                                            <option value="<?php echo $svalue['id']; ?>" <?php echo $selected; ?>>----<?php echo ucfirst($svalue['name']);?></option>
                                           <?php endforeach; ?>
                                      <?php endif; ?>
                                 <?php endforeach; ?> 
                   <?php } ?>
                      </select>
                  </div>
                </li>
              </ul>
              </form>
          </div>
      </div> 
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary btn-sm doupdate-department-btn">Update Department</button>
      </div>