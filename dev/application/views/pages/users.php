<div class="page-header container text-left clearfix no-border-bottom mb5">
  <h1 class="page-title title-border mt10 mlneg15 mrneg15">USERS</h1>
  <div class="row clearfix">
    <div class="col-xs-12">
      <label class="pull-left company-label mlneg15">Company: </label>
      <div class="form-group pull-left">
        <select id="company-select" style="display:none;">
          <?php if(!empty($companies)): ?>
            <?php foreach($companies as $key => $value): ?>
              <option value="<?php echo encrypt($value['id']); ?>" <?php if($value['is_current']==1):?>selected<?php endif; ?>><?php echo $value['name']?></option>
            <?php endforeach; ?>
          <?php endif; ?>
        </select>
      </div>
      <?php if(is_lender_admin()): ?>
        <ul class="list-inline pull-right mrneg20">
          <li><a href="javascript:void(0)" id="add-user-btn" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Add New</a></li>
        </ul> 
        <?php echo form_open(base_url()."users/add_existing_user",array("id"=>"add-existing-user-form")); ?>
          <input type="hidden" class="existing-id" name="id">
          <ul class="list-inline pull-right mr10">
            <li><input id="add-existing-user" class="form-control form-control-sm width-220" placeholder="Search existing name or email"/></li>
            <li><button type="submit" class="btn btn-primary btn-sm submit-btn" style="margin-top:-4px;"><i class="fa fa-plus"></i> Add</button></li>
          </ul>
        </form>
      <?php endif; ?>
    </div>
  </div>
</div>

<div class="page-content pt0">
  <div class="container">
    <div class="row clearfix mb50">
      <div class="table-responsive"> 
        <table id="userTable" class="table table-hover"> 
          <thead class="threadcolor"> 
            <tr> 
              <th style="width:200px;">Name</th>
              <th>Email</th>
              <th>User Group</th>
              <th>Date Added</th>
              <th>Status</th>
              <?php if(is_lender_admin()): ?>
                <th class="no-sort">Action</th>
              <?php endif; ?>
            </tr> 
          </thead> 
          <tbody></tbody> 
        </table>
      </div> 
    </div>
  </div>
</div> 

{crop_modal}