$(document).ready(function(){
	
	/* SCHEDULING */
    var schedule_container = $("#schedule-tables-container");

    $('#company-select').niceSelect();

    var company_id = $('#company-select').val();
    // if there is company id
    if(typeof company_id == "undefined")
    {
        company_id = 0;
    }
    var filters = {
        lender_id : company_id
    };

    function get_all_sched_filters()
    {
        var all_sched_filters = { 
            date_search_from : $("#allScheduledTable_wrapper .date-from-filter").val(),
            date_search_to : $("#allScheduledTable_wrapper .date-until-filter").val()
        };
        $.extend(all_sched_filters,filters);
        return JSON.stringify(all_sched_filters);
    }
    function get_not_sched_filters()
    {
        var not_sched_filters = { 
            date_search_from : $("#notScheduledTable_wrapper .date-from-filter").val(),
            date_search_to : $("#notScheduledTable_wrapper .date-until-filter").val()
        };
        $.extend(not_sched_filters,filters);
        return JSON.stringify(not_sched_filters);
    }
    function get_sched_filters()
    {
        var sched_filters = { 
            date_search_from : $("#scheduledTable_wrapper .date-from-filter").val(),
            date_search_to : $("#scheduledTable_wrapper .date-until-filter").val()
        };
        $.extend(sched_filters,filters);
        return JSON.stringify(sched_filters);
    }
    function get_sched_notary_assurance_filters()
    {
        var sched_filters = { 
            date_search_from : $("#scheduledAssuranceTable_wrapper .date-from-filter").val(),
            date_search_to : $("#scheduledAssuranceTable_wrapper .date-until-filter").val()
        };
        $.extend(sched_filters,filters);
        return JSON.stringify(sched_filters);
    }
    function get_completed_filters()
    {
        var sched_filters = { 
            date_search_from : $("#completedScheduledTable_wrapper .date-from-filter").val(),
            date_search_to : $("#completedScheduledTable_wrapper .date-until-filter").val()
        };
        $.extend(sched_filters,filters);
        return JSON.stringify(sched_filters);
    }
    function get_deleted_filters()
    {
        var sched_filters = { 
            date_search_from : $("#deletedScheduledTable_wrapper .date-from-filter").val(),
            date_search_to : $("#deletedScheduledTable_wrapper .date-until-filter").val()
        };
        $.extend(sched_filters,filters);
        return JSON.stringify(sched_filters);
    }

	// select company
	$("#company-select").on("change",function(){
		var company_id = $(this).val();
		filters['lender_id'] = company_id;
        if(company_id==0)
        {
            $("#add-schedule-btn").prop("disabled",true);
        }else
        {
            $("#add-schedule-btn").prop("disabled",false);
        }
		// load data tables
		loadScheduleTables();
	});

	// switch tabs
	$(".schedule-tab").click(function(){
		var tab = $(this).data("tab");
		if(!$(this).hasClass('active'))
		{
			if(tab=="sched")
			{
				$("#scheduledTable").DataTable().ajax.url( base_url+"admin/schedule/scheduled_listing/?filters="+get_sched_filters() ).load();
			}else if(tab=="not-sched")
			{
				$("#notScheduledTable").DataTable().ajax.url( base_url+"admin/schedule/not_scheduled_listing/?filters="+get_not_sched_filters() ).load();
			}else if(tab=="sched-notary-assurance")
            {
                $("#scheduledAssuranceTable").DataTable().ajax.url( base_url+"admin/schedule/scheduled_notary_assurance_listing/?filters="+get_sched_notary_assurance_filters() ).load();
            }else if(tab=="all-sched")
            {
                $("#allScheduledTable").DataTable().ajax.url( base_url+"admin/schedule/all_scheduled_listing/?filters="+get_all_sched_filters() ).load();
            }else if(tab=="completed-sched")
            {
                $("#completedScheduledTable").DataTable().ajax.url( base_url+"admin/schedule/completed_scheduled_listing/?filters="+get_completed_filters() ).load();
            }else if(tab=="deleted-sched")
            {
                $("#deletedScheduledTable").DataTable().ajax.url( base_url+"admin/schedule/deleted_scheduled_listing/?filters="+get_deleted_filters() ).load();
            }
		}
	});
    
	// add schedule
	$("#add-schedule-btn").click(function(){
		var company_id = $('#company-select').val();
		modalBox( base_url + "schedule/add_schedule_view/"+company_id,{
            width : '',
            header: "Schedule Signing",
            id : "noxModal",
            class : "add-schedule-modal modal-wide",
            backdrop : "static",
            keyboard : false,
  			buttons: [
  			{
                text: "Cancel",
                type: "default",
                additional_class: "",
                click: function() {
                    closeModalbox("noxModal");
                }
            },
        	{
	            text: "<i class='fa fa-plus-circle'></i> Add Schedule",
	            type: "primary",
	            additional_class: "scheduleplus submit-btn",
	            click: function(){
	            	var form = $("body").find("#add-schedule-form").serializeArray();

	            	$("body").find(".add-schedule-modal").find(".submit-btn").html("<i class='fa fa-plus-circle'></i> Adding Schedule ...");
	            	$("body").find(".add-schedule-modal").find(".submit-btn").prop("disabled",true);
	   
	            	$.post(base_url+"schedule/add_schedule/"+company_id,form,function(response){
	            		$("body").find(".add-schedule-modal").find(".submit-btn").html("<i class='fa fa-plus-circle'></i> Add Schedule");
	            		$("body").find(".add-schedule-modal").find(".submit-btn").prop("disabled",false);

	            		response_message(response);
	            		if(response.error==0)
	            		{
                            // reload data tables
	            			reloadScheduleTables();

	            			closeModalbox("noxModal");
	            		}
	            	},"json");
	            }
            }]
		});
	});

    // all scheduled datatables
    var all_scheduled = $("#allScheduledTable").DataTable({
        initComplete: function(){
            var api = this.api();
            var wrapper = $('#allScheduledTable_wrapper');
            wrapper.find(".date-search-container").html($("#date-search-template").html()).promise().done(function(){
                $("#allScheduledTable_wrapper .date-from-filter").datetimepicker({
                    format: "LL",
                    minDate: moment(schedule_container.data("today"),"MM-DD-YYYY"),
                    useCurrent: false,
                    showClear: true
                });
                $("#allScheduledTable_wrapper .date-until-filter").datetimepicker({
                    format: "LL",
                    showClear: true,
                    minDate: moment(schedule_container.data("today"),"MM-DD-YYYY"),
                    useCurrent: false
                });
                $("#allScheduledTable_wrapper").on("dp.change",".date-from-filter",function (e) {
                    wrapper.find(".date-until-filter").data("DateTimePicker").minDate(e.date);
                    all_scheduled.ajax.url( base_url+"admin/schedule/all_scheduled_listing/?filters="+get_all_sched_filters() ).load();
                });
                $("#allScheduledTable_wrapper").on("dp.change",".date-until-filter",function (e) {
                    all_scheduled.ajax.url( base_url+"admin/schedule/all_scheduled_listing/?filters="+get_all_sched_filters() ).load();
                });
                wrapper.find(".date-from-filter").data("DateTimePicker").clear();
                wrapper.find(".date-until-filter").data("DateTimePicker").clear();
                wrapper.on("click",".date-from-filter-addon",function(){
                    wrapper.find(".date-from-filter").data("DateTimePicker").toggle();
                });
                wrapper.on("click",".date-until-filter-addon",function(){
                    wrapper.find(".date-until-filter").data("DateTimePicker").toggle();
                });
            });
            $('#allScheduledTable_filter input').off('.DT').on('keyup.DT', function (e) {
                if(e.keyCode == 13 || this.value.length==0) 
                {
                    api.search(this.value).draw();
                }
            });
        },
        "dom": "<'row'<'col-sm-12 mb10'lf<'form-group date-search-container'>>><'row'<'col-sm-12't>><'row'<'col-sm-5'i><'col-sm-7'p>>",
        "lengthMenu": [10,25,50,75,100,200,300,500],
        "pageLength": 100,
        "processing": true,
        "serverSide": true,
        "responsive": true,
        "deferRender": true,
        "ajax": {
            url: base_url+"admin/schedule/all_scheduled_listing/?filters="+get_all_sched_filters()
        },
        "columns": [
            { "data": "date_scheduled_label" },
            { "data": "time_label" },
            { "data": "borrower" },
            { "data": "title_order_number" },
            { "data": "folder" },
            { "data": "schedule_status" },
            { "data": "status" },
            { "data": "signing_company" },
            { "data": "comments_label" },
            { "data": "notes_label" },
            { "data": "actions" }
        ],
        "order": [[ 0, 'asc' ]],
        "columnDefs":[
            {
                "targets": 8,
                "searchable": false,
                "orderable": false
            },
            {
                "targets": 9,
                "searchable": false,
                "orderable": false
            },
            {
                "targets": 10,
                "searchable": false,
                "orderable": false
            },
            {
                "targets": 0,
                "visible": false,
                "render": function ( data, type, full, meta ) {
                    if(full.due_today==true)
                    {
                        return '<span>'+data+'</span><br/><span class="label label-danger">DUE TODAY</span>';
                    }
                    return data;
                }
            }
        ],
        "drawCallback": function ( settings ) {
            if($(this).DataTable().column(0).visible()==false)
            {
                var api = this.api();
                var data = api.rows( {page:'current'} ).data();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;
                api.column(0, {page:'current'} ).data().each(function(group,i) {
                    if(last!==group)
                    {
                        var extra = "";
                        if(data[i].due_today==true)
                        {
                            // if due today
                            extra = '<span class="label label-danger font-sm ml10">DUE TODAY</span>';
                        }
                        $(rows).eq(i).before(
                            '<tr class="group"><td colspan="10"><h3 class="group-title">'+group+' '+extra+'</h3></td></tr>'
                        );
                        last = group;
                    }
                });
            }
        }
    });

	// not scheduled datatables
	var not_scheduled = $("#notScheduledTable").DataTable({
        initComplete: function(){
            var api = this.api();
            var wrapper = $('#notScheduledTable_wrapper');
            wrapper.find(".date-search-container").html($("#date-search-template").html()).promise().done(function(){
                $("#notScheduledTable_wrapper .date-from-filter").datetimepicker({
                    format: "LL",
                    minDate: moment(schedule_container.data("today"),"MM-DD-YYYY"),
                    useCurrent: false,
                    showClear: true
                });
                $("#notScheduledTable_wrapper .date-until-filter").datetimepicker({
                    format: "LL",
                    showClear: true,
                    minDate: moment(schedule_container.data("today"),"MM-DD-YYYY"),
                    useCurrent: false
                });
                $("#notScheduledTable_wrapper").on("dp.change",".date-from-filter",function (e) {
                    wrapper.find(".date-until-filter").data("DateTimePicker").minDate(e.date);
                    not_scheduled.ajax.url( base_url+"admin/schedule/not_scheduled_listing/?filters="+get_not_sched_filters() ).load();
                });
                $("#notScheduledTable_wrapper").on("dp.change",".date-until-filter",function (e) {
                    not_scheduled.ajax.url( base_url+"admin/schedule/not_scheduled_listing/?filters="+get_not_sched_filters() ).load();
                });
                wrapper.find(".date-from-filter").data("DateTimePicker").clear();
                wrapper.find(".date-until-filter").data("DateTimePicker").clear();
                wrapper.on("click",".date-from-filter-addon",function(){
                    wrapper.find(".date-from-filter").data("DateTimePicker").toggle();
                });
                wrapper.on("click",".date-until-filter-addon",function(){
                    wrapper.find(".date-until-filter").data("DateTimePicker").toggle();
                });
            });
            $('#notScheduledTable_filter input').off('.DT').on('keyup.DT', function (e) {
                if(e.keyCode == 13 || this.value.length==0) 
                {
                    api.search(this.value).draw();
                }
            });
        },
        "dom": "<'row'<'col-sm-12 mb10'lf<'form-group date-search-container'>>><'row'<'col-sm-12't>><'row'<'col-sm-5'i><'col-sm-7'p>>",
		"lengthMenu": [10,25,50,75,100,200,300,500],
		"pageLength": 100,
		"processing": true,
        "serverSide": true,
        "responsive": true,
        "deferRender": true,
        "ajax": {
        	url: base_url+"admin/schedule/not_scheduled_listing/?filters="+get_not_sched_filters()
        },
        "columns": [
            { "data": "date_scheduled_label" },
            { "data": "time_label" },
            { "data": "borrower" },
            { "data": "title_order_number" },
            { "data": "folder" },
            { "data": "schedule_status" },
            { "data": "status" },
            { "data": "signing_company" },
            { "data": "comments_label" },
            { "data": "notes_label" },
            { "data": "actions" }
        ],
        "order": [[ 0, 'asc' ]],
        "columnDefs":[
            {
                "targets": 7,
                "searchable": false,
                "orderable": false
            },
			{
				"targets": 8,
				"searchable": false,
				"orderable": false
			},
			{
				"targets": 9,
				"searchable": false,
				"orderable": false
			},
            {
                "targets": 10,
                "searchable": false,
                "orderable": false
            },
            {
                "targets": 0,
                "visible": false,
                "render": function ( data, type, full, meta ) {
                    if(full.due_today==true)
                    {
                        return '<span>'+data+'</span><br/><span class="label label-danger">DUE TODAY</span>';
                    }
                    return data;
                }
            }
		],
        "drawCallback": function ( settings ) {
            if($(this).DataTable().column(0).visible()==false)
            {
                var api = this.api();
                var data = api.rows( {page:'current'} ).data();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;
                api.column(0, {page:'current'} ).data().each(function(group,i) {
                    if(last!==group)
                    {
                        var extra = "";
                        if(data[i].due_today==true)
                        {
                            // if due today
                            extra = '<span class="label label-danger font-sm ml10">DUE TODAY</span>';
                        }
                        $(rows).eq(i).before(
                            '<tr class="group"><td colspan="10"><h3 class="group-title">'+group+' '+extra+'</h3></td></tr>'
                        );
                        last = group;
                    }
                });
            }
        }
	});
	// scheduled with Notary Cloud datatables
	var scheduled = $("#scheduledTable").DataTable({
        initComplete: function(){
            var api = this.api();
            var wrapper = $('#scheduledTable_wrapper');
            wrapper.find(".date-search-container").html($("#date-search-template").html()).promise().done(function(){
                $("#scheduledTable_wrapper .date-from-filter").datetimepicker({
                    format: "LL",
                    minDate: moment(schedule_container.data("today"),"MM-DD-YYYY"),
                    useCurrent: false,
                    showClear: true
                });
                $("#scheduledTable_wrapper .date-until-filter").datetimepicker({
                    format: "LL",
                    showClear: true,
                    minDate: moment(schedule_container.data("today"),"MM-DD-YYYY"),
                    useCurrent: false
                });
                $("#scheduledTable_wrapper").on("dp.change",".date-from-filter",function (e) {
                    wrapper.find(".date-until-filter").data("DateTimePicker").minDate(e.date);
                    scheduled.ajax.url( base_url+"admin/schedule/scheduled_listing/?filters="+get_sched_filters() ).load();
                });
                $("#scheduledTable_wrapper").on("dp.change",".date-until-filter",function (e) {
                    scheduled.ajax.url( base_url+"admin/schedule/scheduled_listing/?filters="+get_sched_filters() ).load();
                });
                wrapper.find(".date-from-filter").data("DateTimePicker").clear();
                wrapper.find(".date-until-filter").data("DateTimePicker").clear();
                wrapper.on("click",".date-from-filter-addon",function(){
                    wrapper.find(".date-from-filter").data("DateTimePicker").toggle();
                });
                wrapper.on("click",".date-until-filter-addon",function(){
                    wrapper.find(".date-until-filter").data("DateTimePicker").toggle();
                });
            });
            $('#scheduledTable_filter input').off('.DT').on('keyup.DT', function (e) {
                if(e.keyCode == 13 || this.value.length==0) 
                {
                    api.search(this.value).draw();
                }
            });
        },
        "dom": "<'row'<'col-sm-12 mb10'lf<'form-group date-search-container'>>><'row'<'col-sm-12't>><'row'<'col-sm-5'i><'col-sm-7'p>>",
		"lengthMenu": [10,25,50,75,100,200,300,500],
		"pageLength": 100,
		"processing": true,
        "serverSide": true,
        "responsive": true,
        "deferRender": true,
        "ajax": {
        	url: base_url+"admin/schedule/scheduled_listing/?filters="+get_sched_filters()
        },
        "columns": [
            { "data": "date_scheduled_label" },
            { "data": "time_label" },
            { "data": "borrower" },
            { "data": "title_order_number" },
            { "data": "notary_name" },
            { "data": "folder" },
            { "data": "schedule_status" },
            { "data": "status" },
            { "data": "signing_company" },
            { "data": "comments_label" },
            { "data": "notes_label" },
            { "data": "actions" }
        ],
        "order": [[ 0, 'asc' ]],
        "columnDefs":[
            {
                "targets": 8,
                "searchable": false,
                "orderable": false
            },
			{
				"targets": 9,
				"searchable": false,
				"orderable": false
			},
			{
				"targets": 10,
				"searchable": false,
				"orderable": false
			},
            {
                "targets": 11,
                "searchable": false,
                "orderable": false
            },
			{
				"targets": 0,
				"visible": false,
				"render": function ( data, type, full, meta ) {
					if(full.due_today==true)
					{
						return '<span>'+data+'</span><br/><span class="label label-danger">DUE TODAY</span>';
					}
			    	return data;
			    }
			}
		],
		"drawCallback": function ( settings ) {
			if($(this).DataTable().column(0).visible()==false)
			{
				var api = this.api();
				var data = api.rows( {page:'current'} ).data();
	            var rows = api.rows( {page:'current'} ).nodes();
	            var last=null;
	            api.column(0, {page:'current'} ).data().each(function(group,i) {
	                if(last!==group)
	                {
	                	var extra = "";
	                	if(data[i].due_today==true)
	                	{
	                		// if due today
	                		extra = '<span class="label label-danger font-sm ml10">DUE TODAY</span>';
	                	}
	                    $(rows).eq(i).before(
	                        '<tr class="group"><td colspan="10"><h3 class="group-title">'+group+' '+extra+'</h3></td></tr>'
	                    );
	                    last = group;
	                }
	            });
			}
        }
	});
    // scheduled with Notary Assurance datatables
    var sched_notary_assurance = $("#scheduledAssuranceTable").DataTable({
        initComplete: function(){
            var api = this.api();
            var wrapper = $('#scheduledAssuranceTable_wrapper');
            wrapper.find(".date-search-container").html($("#date-search-template").html()).promise().done(function(){
                $("#scheduledAssuranceTable_wrapper .date-from-filter").datetimepicker({
                    format: "LL",
                    minDate: moment(schedule_container.data("today"),"MM-DD-YYYY"),
                    useCurrent: false,
                    showClear: true
                });
                $("#scheduledAssuranceTable_wrapper .date-until-filter").datetimepicker({
                    format: "LL",
                    showClear: true,
                    minDate: moment(schedule_container.data("today"),"MM-DD-YYYY"),
                    useCurrent: false
                });
                $("#scheduledAssuranceTable_wrapper").on("dp.change",".date-from-filter",function (e) {
                    wrapper.find(".date-until-filter").data("DateTimePicker").minDate(e.date);
                    sched_notary_assurance.ajax.url( base_url+"admin/schedule/scheduled_notary_assurance_listing/?filters="+get_sched_notary_assurance_filters() ).load();
                });
                $("#scheduledAssuranceTable_wrapper").on("dp.change",".date-until-filter",function (e) {
                    sched_notary_assurance.ajax.url( base_url+"admin/schedule/scheduled_notary_assurance_listing/?filters="+get_sched_notary_assurance_filters() ).load();
                });
                wrapper.find(".date-from-filter").data("DateTimePicker").clear();
                wrapper.find(".date-until-filter").data("DateTimePicker").clear();
                wrapper.on("click",".date-from-filter-addon",function(){
                    wrapper.find(".date-from-filter").data("DateTimePicker").toggle();
                });
                wrapper.on("click",".date-until-filter-addon",function(){
                    wrapper.find(".date-until-filter").data("DateTimePicker").toggle();
                });
            });
            $('#scheduledAssuranceTable_filter input').off('.DT').on('keyup.DT', function (e) {
                if(e.keyCode == 13 || this.value.length==0) 
                {
                    api.search(this.value).draw();
                }
            });
        },
        "dom": "<'row'<'col-sm-12 mb10'lf<'form-group date-search-container'>>><'row'<'col-sm-12't>><'row'<'col-sm-5'i><'col-sm-7'p>>",
        "lengthMenu": [10,25,50,75,100,200,300,500],
        "pageLength": 100,
        "processing": true,
        "serverSide": true,
        "responsive": true,
        "deferRender": true,
        "ajax": {
            url: base_url+"admin/schedule/scheduled_notary_assurance_listing/?filters="+get_sched_notary_assurance_filters()
        },
        "columns": [
            { "data": "date_scheduled_label" },
            { "data": "time_label" },
            { "data": "borrower" },
            { "data": "title_order_number" },
            { "data": "folder" },
            { "data": "schedule_status" },
            { "data": "status" },
            { "data": "signing_company" },
            { "data": "comments_label" },
            { "data": "notes_label" },
            { "data": "actions" }
        ],
        "order": [[ 0, 'asc' ]],
        "columnDefs":[
            {
                "targets": 7,
                "searchable": false,
                "orderable": false
            },
            {
                "targets": 8,
                "searchable": false,
                "orderable": false
            },
            {
                "targets": 9,
                "searchable": false,
                "orderable": false
            },
            {
                "targets": 10,
                "searchable": false,
                "orderable": false
            },
            {
                "targets": 0,
                "visible": false,
                "render": function ( data, type, full, meta ) {
                    if(full.due_today==true)
                    {
                        return '<span>'+data+'</span><br/><span class="label label-danger">DUE TODAY</span>';
                    }
                    return data;
                }
            }
        ],
        "drawCallback": function ( settings ) {
            if($(this).DataTable().column(0).visible()==false)
            {
                var api = this.api();
                var data = api.rows( {page:'current'} ).data();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;
                api.column(0, {page:'current'} ).data().each(function(group,i) {
                    if(last!==group)
                    {
                        var extra = "";
                        if(data[i].due_today==true)
                        {
                            // if due today
                            extra = '<span class="label label-danger font-sm ml10">DUE TODAY</span>';
                        }
                        $(rows).eq(i).before(
                            '<tr class="group"><td colspan="10"><h3 class="group-title">'+group+' '+extra+'</h3></td></tr>'
                        );
                        last = group;
                    }
                });
            }
        }
    });
    // completed scheduled datatables
    var completed = $("#completedScheduledTable").DataTable({
        initComplete: function(){
            var api = this.api();
            var wrapper = $('#completedScheduledTable_wrapper');
            wrapper.find(".date-search-container").html($("#date-search-template").html()).promise().done(function(){
                $("#completedScheduledTable_wrapper .date-from-filter").datetimepicker({
                    format: "LL",
                    minDate: moment(schedule_container.data("today"),"MM-DD-YYYY"),
                    useCurrent: false,
                    showClear: true
                });
                $("#completedScheduledTable_wrapper .date-until-filter").datetimepicker({
                    format: "LL",
                    showClear: true,
                    minDate: moment(schedule_container.data("today"),"MM-DD-YYYY"),
                    useCurrent: false
                });
                $("#completedScheduledTable_wrapper").on("dp.change",".date-from-filter",function (e) {
                    wrapper.find(".date-until-filter").data("DateTimePicker").minDate(e.date);
                    completed.ajax.url( base_url+"admin/schedule/completed_scheduled_listing/?filters="+get_completed_filters() ).load();
                });
                $("#completedScheduledTable_wrapper").on("dp.change",".date-until-filter",function (e) {
                    completed.ajax.url( base_url+"admin/schedule/completed_scheduled_listing/?filters="+get_completed_filters() ).load();
                });
                wrapper.find(".date-from-filter").data("DateTimePicker").clear();
                wrapper.find(".date-until-filter").data("DateTimePicker").clear();
                wrapper.on("click",".date-from-filter-addon",function(){
                    wrapper.find(".date-from-filter").data("DateTimePicker").toggle();
                });
                wrapper.on("click",".date-until-filter-addon",function(){
                    wrapper.find(".date-until-filter").data("DateTimePicker").toggle();
                });
            });
            $('#completedScheduledTable_filter input').off('.DT').on('keyup.DT', function (e) {
                if(e.keyCode == 13 || this.value.length==0) 
                {
                    api.search(this.value).draw();
                }
            });
        },
        "dom": "<'row'<'col-sm-12 mb10'lf<'form-group date-search-container'>>><'row'<'col-sm-12't>><'row'<'col-sm-5'i><'col-sm-7'p>>",
        "lengthMenu": [10,25,50,75,100,200,300,500],
        "pageLength": 100,
        "processing": true,
        "serverSide": true,
        "responsive": true,
        "deferRender": true,
        "ajax": {
            url: base_url+"admin/schedule/completed_scheduled_listing/?filters="+get_completed_filters()
        },
        "columns": [
            { "data": "date_scheduled_label" },
            { "data": "time_label" },
            { "data": "borrower" },
            { "data": "title_order_number" },
            { "data": "notary_name" },
            { "data": "folder" },
            { "data": "schedule_status" },
            { "data": "status" },
            { "data": "signing_company" },
            { "data": "comments_label" },
            { "data": "notes_label" },
            { "data": "actions" }
        ],
        "order": [[ 0, 'asc' ]],
        "columnDefs":[
            {
                "targets": 8,
                "searchable": false,
                "orderable": false
            },
            {
                "targets": 9,
                "searchable": false,
                "orderable": false
            },
            {
                "targets": 10,
                "searchable": false,
                "orderable": false
            },
            {
                "targets": 11,
                "searchable": false,
                "orderable": false
            },
            {
                "targets": 0,
                "visible": false,
                "render": function ( data, type, full, meta ) {
                    if(full.due_today==true)
                    {
                        return '<span>'+data+'</span><br/><span class="label label-danger">DUE TODAY</span>';
                    }
                    return data;
                }
            }
        ],
        "drawCallback": function ( settings ) {
            if($(this).DataTable().column(0).visible()==false)
            {
                var api = this.api();
                var data = api.rows( {page:'current'} ).data();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;
                api.column(0, {page:'current'} ).data().each(function(group,i) {
                    if(last!==group)
                    {
                        var extra = "";
                        if(data[i].due_today==true)
                        {
                            // if due today
                            extra = '<span class="label label-danger font-sm ml10">DUE TODAY</span>';
                        }
                        $(rows).eq(i).before(
                            '<tr class="group"><td colspan="10"><h3 class="group-title">'+group+' '+extra+'</h3></td></tr>'
                        );
                        last = group;
                    }
                });
            }
        }
    });
    // deleted scheduled datatables
    var deleted = $("#deletedScheduledTable").DataTable({
        initComplete: function(){
            var api = this.api();
            var wrapper = $('#deletedScheduledTable_wrapper');
            wrapper.find(".date-search-container").html($("#date-search-template").html()).promise().done(function(){
                $("#deletedScheduledTable_wrapper .date-from-filter").datetimepicker({
                    format: "LL",
                    minDate: moment(schedule_container.data("today"),"MM-DD-YYYY"),
                    useCurrent: false,
                    showClear: true
                });
                $("#deletedScheduledTable_wrapper .date-until-filter").datetimepicker({
                    format: "LL",
                    showClear: true,
                    minDate: moment(schedule_container.data("today"),"MM-DD-YYYY"),
                    useCurrent: false
                });
                $("#deletedScheduledTable_wrapper").on("dp.change",".date-from-filter",function (e) {
                    wrapper.find(".date-until-filter").data("DateTimePicker").minDate(e.date);
                    deleted.ajax.url( base_url+"admin/schedule/deleted_scheduled_listing/?filters="+get_deleted_filters() ).load();
                });
                $("#deletedScheduledTable_wrapper").on("dp.change",".date-until-filter",function (e) {
                    deleted.ajax.url( base_url+"admin/schedule/deleted_scheduled_listing/?filters="+get_deleted_filters() ).load();
                });
                wrapper.find(".date-from-filter").data("DateTimePicker").clear();
                wrapper.find(".date-until-filter").data("DateTimePicker").clear();
                wrapper.on("click",".date-from-filter-addon",function(){
                    wrapper.find(".date-from-filter").data("DateTimePicker").toggle();
                });
                wrapper.on("click",".date-until-filter-addon",function(){
                    wrapper.find(".date-until-filter").data("DateTimePicker").toggle();
                });
            });
            $('#deletedScheduledTable_filter input').off('.DT').on('keyup.DT', function (e) {
                if(e.keyCode == 13 || this.value.length==0) 
                {
                    api.search(this.value).draw();
                }
            });
        },
        "dom": "<'row'<'col-sm-12 mb10'lf<'form-group date-search-container'>>><'row'<'col-sm-12't>><'row'<'col-sm-5'i><'col-sm-7'p>>",
        "lengthMenu": [10,25,50,75,100,200,300,500],
        "pageLength": 100,
        "processing": true,
        "serverSide": true,
        "responsive": true,
        "deferRender": true,
        "ajax": {
            url: base_url+"admin/schedule/deleted_scheduled_listing/?filters="+get_deleted_filters()
        },
        "columns": [
            { "data": "date_scheduled_label" },
            { "data": "time_label" },
            { "data": "borrower" },
            { "data": "title_order_number" },
            { "data": "folder" },
            { "data": "schedule_status" },
            { "data": "status" },
            { "data": "signing_company" },
            { "data": "comments_label" },
            { "data": "notes_label" },
            { "data": "actions" }
        ],
        "order": [[ 0, 'asc' ]],
        "columnDefs":[
            {
                "targets": 8,
                "searchable": false,
                "orderable": false
            },
            {
                "targets": 9,
                "searchable": false,
                "orderable": false
            },
            {
                "targets": 10,
                "searchable": false,
                "orderable": false
            },
            {
                "targets": 0,
                "visible": false,
                "render": function ( data, type, full, meta ) {
                    if(full.due_today==true)
                    {
                        return '<span>'+data+'</span><br/><span class="label label-danger">TODAY</span>';
                    }
                    return data;
                }
            }
        ],
        "drawCallback": function ( settings ) {
            if($(this).DataTable().column(0).visible()==false)
            {
                var api = this.api();
                var data = api.rows( {page:'current'} ).data();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;
                api.column(0, {page:'current'} ).data().each(function(group,i) {
                    if(last!==group)
                    {
                        var extra = "";
                        if(data[i].due_today==true)
                        {
                            // if due today
                            extra = '<span class="label label-danger font-sm ml10">TODAY</span>';
                        }
                        $(rows).eq(i).before(
                            '<tr class="group"><td colspan="10"><h3 class="group-title">'+group+' '+extra+'</h3></td></tr>'
                        );
                        last = group;
                    }
                });
            }
        }
    });
    
    $('#allScheduledTable thead').on( 'click', 'th.single-row', function () {
        var is_date_visible = $("#allScheduledTable").DataTable().column(0).visible();
        if(is_date_visible==false)
        {
            $("#allScheduledTable").DataTable().column(0).visible(true);
            $("#allScheduledTable").DataTable().columns.adjust().draw(false);
        }
    });
    $('#allScheduledTable thead').on( 'click', 'th.date-label', function () {
        var is_date_visible = $("#allScheduledTable").DataTable().column(0).visible();
        if(is_date_visible==true)
        {
            $("#allScheduledTable").DataTable().column(0).visible(false);
            $("#allScheduledTable").DataTable().columns.adjust().draw(false);
        }
    });
    $('#notScheduledTable thead').on( 'click', 'th.single-row', function () {
        var is_date_visible = $("#notScheduledTable").DataTable().column(0).visible();
        if(is_date_visible==false)
        {
            $("#notScheduledTable").DataTable().column(0).visible(true);
            $("#notScheduledTable").DataTable().columns.adjust().draw(false);
        }
    });
    $('#notScheduledTable thead').on( 'click', 'th.date-label', function () {
        var is_date_visible = $("#notScheduledTable").DataTable().column(0).visible();
        if(is_date_visible==true)
        {
            $("#notScheduledTable").DataTable().column(0).visible(false);
            $("#notScheduledTable").DataTable().columns.adjust().draw(false);
        }
    });
	$('#scheduledTable thead').on( 'click', 'th.single-row', function () {
        var is_date_visible = $("#scheduledTable").DataTable().column(0).visible();
        if(is_date_visible==false)
        {
        	$("#scheduledTable").DataTable().column(0).visible(true);
        	$("#scheduledTable").DataTable().columns.adjust().draw(false);
        }
    });
    $('#scheduledTable thead').on( 'click', 'th.date-label', function () {
        var is_date_visible = $("#scheduledTable").DataTable().column(0).visible();
        if(is_date_visible==true)
        {
        	$("#scheduledTable").DataTable().column(0).visible(false);
        	$("#scheduledTable").DataTable().columns.adjust().draw(false);
        }
    });
    $('#scheduledAssuranceTable thead').on( 'click', 'th.single-row', function () {
        var is_date_visible = $("#scheduledAssuranceTable").DataTable().column(0).visible();
        if(is_date_visible==false)
        {
            $("#scheduledAssuranceTable").DataTable().column(0).visible(true);
            $("#scheduledAssuranceTable").DataTable().columns.adjust().draw(false);
        }
    });
    $('#scheduledAssuranceTable thead').on( 'click', 'th.date-label', function () {
        var is_date_visible = $("#scheduledAssuranceTable").DataTable().column(0).visible();
        if(is_date_visible==true)
        {
            $("#scheduledAssuranceTable").DataTable().column(0).visible(false);
            $("#scheduledTAssuranceable").DataTable().columns.adjust().draw(false);
        }
    });
    $('#completedScheduledTable thead').on( 'click', 'th.single-row', function () {
        var is_date_visible = $("#completedScheduledTable").DataTable().column(0).visible();
        if(is_date_visible==false)
        {
            $("#completedScheduledTable").DataTable().column(0).visible(true);
            $("#completedScheduledTable").DataTable().columns.adjust().draw(false);
        }
    });
    $('#completedScheduledTable thead').on( 'click', 'th.date-label', function () {
        var is_date_visible = $("#completedScheduledTable").DataTable().column(0).visible();
        if(is_date_visible==true)
        {
            $("#completedScheduledTable").DataTable().column(0).visible(false);
            $("#completedScheduledTable").DataTable().columns.adjust().draw(false);
        }
    });
    $('#deletedScheduledTable thead').on( 'click', 'th.single-row', function () {
        var is_date_visible = $("#deletedScheduledTable").DataTable().column(0).visible();
        if(is_date_visible==false)
        {
            $("#deletedScheduledTable").DataTable().column(0).visible(true);
            $("#deletedScheduledTable").DataTable().columns.adjust().draw(false);
        }
    });
    $('#deletedScheduledTable thead').on( 'click', 'th.date-label', function () {
        var is_date_visible = $("#deletedScheduledTable").DataTable().column(0).visible();
        if(is_date_visible==true)
        {
            $("#deletedScheduledTable").DataTable().column(0).visible(false);
            $("#deletedScheduledTable").DataTable().columns.adjust().draw(false);
        }
    });

    // remove schedule 
    schedule_container.on("click",".remove-schedule-btn",function(){
        var id = $(this).data("id");
        jConfirm("Confirmation","You are going to remove this entry from the scheduled signings. Would you like to remove it?",function(){
            modalBox( base_url + "schedule/add_reason_deletion_view/"+id,{
                width : '',
                class : '',
                id: 'modalAddReason',
                changeModalContent: true
            });
        });
    });
    // permanently delete schedule
    schedule_container.on("click",".permanently-delete-schedule-btn",function(){
        var id = $(this).data("id");
        jConfirm("Confirmation","You are going to delete this schedule permanently from this system. Would you like to permanently delete it?",function(){
            $.get(base_url+"schedule/permanently_remove_schedule/"+id,function(response){
                response_message(response);
                if(response.error==0)
                {
                    // reload data tables
                    reloadScheduleTables();
                }
            },'json');
        });
    });
    // put back schedule
    schedule_container.on("click",".undo-schedule-btn",function(){
        var id = $(this).data("id");
        jConfirm("Confirmation","You are going to put back this schedule to the active schedules. Would you like to put it back?",function(){
            $.get(base_url+"schedule/put_back_schedule/"+id,function(response){
                response_message(response);
                if(response.error==0)
                {
                    // reload data tables
                    reloadScheduleTables();
                }
            },'json');
        });
    });
    
    // select admin status
    schedule_container.on("click",".select-admin-status",function(){
    	var id = $(this).data("id");
    	var status = $(this).data("status");
    	var schedule_id = $(this).data("schedule-id");
    	var formData = [];
    	formData.push({'name':csrf_token,'value':csrf_hash});
    	formData.push({'name':'admin_status_id','value':id});
    	formData.push({'name':'admin_status','value':status});
    	$.post(base_url+"schedule/change_admin_status/"+schedule_id,formData,function(response){
    		response_message(response);
    		if(response.error==0)
    		{
    			// reload data tables
                reloadScheduleTables(); 
    		}
    	},'json');
    });
    // select folder
    schedule_container.on("click",".select-schedule-folder",function(){
    	var id = $(this).data("id");
    	var schedule_id = $(this).data("schedule-id");
    	var formData = [];
    	formData.push({'name':csrf_token,'value':csrf_hash});
    	formData.push({'name':'folder_id','value':id});
    	$.post(base_url+"schedule/change_folder/"+schedule_id,formData,function(response){
    		response_message(response);
    		if(response.error==0)
    		{
    			// reload data tables
                reloadScheduleTables();
    		}
    	},'json');
    });
    // select schedule status
    schedule_container.on("click",".select-schedule-status",function(){
        var id = $(this).data("id");
        var schedule_id = $(this).data("schedule-id");
        var formData = [];
        formData.push({'name':csrf_token,'value':csrf_hash});
        formData.push({'name':'schedule_status_id','value':id});
        $.post(base_url+"schedule/change_schedule_status/"+schedule_id,formData,function(response){
            response_message(response);
            if(response.error==0)
            {
                // reload data tables
                reloadScheduleTables();
            }
        },'json');
    });
    // select signing company
    schedule_container.on("click",".select-signing-company",function(){
    	var name = $(this).data("name");
    	var schedule_id = $(this).data("schedule-id");
    	var formData = [];
    	formData.push({'name':csrf_token,'value':csrf_hash});
    	formData.push({'name':'signing_company','value':name});
    	$.post(base_url+"schedule/change_signing_company/"+schedule_id,formData,function(response){
    		response_message(response);
    		if(response.error==0)
    		{
                // reload data tables
    			reloadScheduleTables();
    		}
    	},'json');
    });

	function reloadScheduleTables()
	{
		if($("#tab-s").hasClass("active"))
		{
			$("#scheduledTable").DataTable().ajax.reload(null,false);
		}else if($("#tab-ns").hasClass("active"))
		{
			$("#notScheduledTable").DataTable().ajax.reload(null,false);
		}else if($("#tab-as").hasClass("active"))
        {
            $("#scheduledAssuranceTable").DataTable().ajax.reload(null,false);
        }else if($("#tab-alls").hasClass("active"))
        {
            $("#allScheduledTable").DataTable().ajax.reload(null,false);
        }else if($("#tab-completed").hasClass("active"))
        {
            $("#completedScheduledTable").DataTable().ajax.reload(null,false);
        }else if($("#tab-deleted").hasClass("active"))
        {
            $("#deletedScheduledTable").DataTable().ajax.reload(null,false);
        } 
	}
    function reloadScheduleTables2()
    {
        $("#allScheduledTable").DataTable().ajax.reload(null,false);
        $("#scheduledTable").DataTable().ajax.reload(null,false);
        $("#notScheduledTable").DataTable().ajax.reload(null,false);
        $("#scheduledAssuranceTable").DataTable().ajax.reload(null,false);
        $("#completedScheduledTable").DataTable().ajax.reload(null,false);
        $("#deletedScheduledTable").DataTable().ajax.reload(null,false);
    }
    function loadScheduleTables()
    {
        if($("#tab-s").hasClass("active"))
        {
            $("#scheduledTable").DataTable().ajax.url( base_url+"admin/schedule/scheduled_listing/?filters="+get_sched_filters() ).load();
        }else if($("#tab-ns").hasClass("active"))
        {
            $("#notScheduledTable").DataTable().ajax.url( base_url+"admin/schedule/not_scheduled_listing/?filters="+get_not_sched_filters() ).load();
        }else if($("#tab-as").hasClass("active"))
        {
            $("#scheduledAssuranceTable").DataTable().ajax.url( base_url+"admin/schedule/scheduled_notary_assurance_listing/?filters="+get_sched_notary_assurance_filters() ).load();
        }else if($("#tab-alls").hasClass("active"))
        {
            $("#allScheduledTable").DataTable().ajax.url( base_url+"admin/schedule/all_scheduled_listing/?filters="+get_all_sched_filters() ).load();
        }else if($("#tab-completed").hasClass("active"))
        {
            $("#completedScheduledTable").DataTable().ajax.url( base_url+"admin/schedule/completed_scheduled_listing/?filters="+get_completed_filters() ).load();
        }else if($("#tab-deleted").hasClass("active"))
        {
            $("#deletedScheduledTable").DataTable().ajax.url( base_url+"admin/schedule/deleted_scheduled_listing/?filters="+get_deleted_filters() ).load();
        }
    }

    // reload data every 5 minutes
    setInterval(function(){
        reloadScheduleTables2();
    },300000);
});