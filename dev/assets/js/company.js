$(document).ready(function(){
	// add lending company
	$("#add-company-btn").click(function(){
		modalBox( base_url + "admin/company/add_company_view",{
            id : "modalCompany",
            class : "modal-centered modal-bg-white",
            changeModalContent : true
		});
	});
	// initialize data tables
	$("#companyTable").DataTable({
		initComplete: function(){
            var api = this.api();
            $('#companyTable_filter input').off('.DT').on('keyup.DT', function (e) {
                if(e.keyCode == 13 || this.value.length==0) 
                {
                    api.search(this.value).draw();
                }
            });
        },
		"processing": true,
        "serverSide": true,
        "responsive": true,
        "deferRender": true,
        "ajax": base_url+"admin/company/company_listing",
        "columns": [
            { "data": "company_name" },
            { "data": "address" },
            { "data": "email" },
            { "data": "website" },
            { "data": "date_added" },
            { "data": "action" }
        ],
        "columnDefs":[
			{
				"targets": 4,
				"searchable": false,
				"orderable": false
			} 
		]
	});
	
	// remove company
	$("#companyTable").on("click",".remove-company-btn",function(){
		var id = $(this).data("id");
		jConfirm("Confirmation","You are going to delete this company from the list. Would you like to permanently remove it?",function(){
			$.get(base_url+"admin/company/remove_company/"+id,function(response){
				me_message_v2(response);
				if(response.error==0)
				{
					// reload admin status data table
	          		$("#companyTable").DataTable().ajax.reload(null,false); 
				}
			},'json');
		});
	});
});