$(document).ready(function(){

// modal ADD EXPENSE
$("#add-expense-btn,.add-expense-btn").click(function(){
	modalBox( base_url + "expenses/add/",{
        id : "modalAddExpense",
        class : "modal-centered modal-bg-white",
        width:"50%",
        changeModalContent : true
	});
});

// modal EDIT EXPENSE
$("#edit-expense-btn, .edit-expense-btn").click(function(){
	modalBox( base_url + "expenses/edit/",{
        id : "modalEditExpense",
        class : "modal-centered modal-bg-white",
        changeModalContent : true
	});
});

// modal VIEW EXPENSE 
$("#expenseTable").on('click','.view-btn',function(){
        var id = $(this).data("id"); 
        modalBox( base_url + "expenses/view/?id="+id,{
        id : "modalExpenseDetail",
        class : "modal-centered modal-bg-white",
        changeModalContent : true
        });
});
$("#expenseTable").on('click','.edit-btn',function(){
        var id = $(this).data("id"); 
        modalBox( base_url + "expenses/edit_expense/?id="+id,{
        id : "modalExpenseDetail",
        class : "modal-centered modal-bg-white",
        changeModalContent : true
        });
});

// modal VIEW EXPENSE DETAIL - monthly
$(".view-expense-detail-btn").click(function(){
    var month       = $(this).data("month");
    var year        = $(this).data("year");
    var category    = $(this).data("category");
    if(typeof month!="undefined" && typeof category!="undefined" && typeof year!="undefined")
    {
        var filter = "?month="+month+"&year="+year+"&category="+category;
        modalBox( base_url + "expenses/view_details/"+filter,{
            id : "modalExpenseDetail",
            class : "modal-centered modal-bg-white",
            changeModalContent : true
         });
    }
	
});
// modal VIEW EXPENSE DETAIL - monthly
$("#expenseTable").on('click','.note-entry',function(){
    var id = $(this).data("id");
    modalBox( base_url + "expenses/notes/?id="+id,{
        id : "modalExpensenotes",
        class : "modal-centered modal-bg-white",
        changeModalContent : true
    });
});

// add-category-btn
$("body").on('click','.add-category-btn',function(){
        modalBox( base_url + "expenses/add_category/",{
        id : "modalAddCategory",
        class : "modal-centered modal-bg-white",
        changeModalContent : true
        });
});

// bootstrap-select: button to add new vendor
$("body").on('click','.add-vendor-btn',function(){
        modalBox( base_url + "expenses/add_vendor/",{
        id : "modalAddVendor",
        class : "modal-centered modal-bg-white",
        changeModalContent : true
        });
});




$("#expenseTable").on("click",".remove-btn",function(){
        var id = $(this).data("id"); 
        $.confirm({
            title:"Delete expense confirmation",
            text: "You are going to delete this expense from the list. Would you like to permanently remove it?",
            confirm: function() {
                $.get(base_url+"expenses/do_delete/?id="+id,function(response){
                    me_message_v2(response);
                    if(response.error==0)
                    {
                        // reload admin status data table
                        $("#expenseTable").DataTable().ajax.reload(null,false); 
                    }
                },'json');
            },
            cancel: function() {
                // nothing to do
            }
        });
    });
        // initialize data tables
       var expenseTable = $("#expenseTable").DataTable({
                initComplete: function(){
            var api = this.api();
            $('#companyTable_filter input').off('.DT').on('keyup.DT', function (e) {
                if(e.keyCode == 13 || this.value.length==0) 
                {
                    api.search(this.value).draw();
                }
            });
        },
        "processing": true,
        "serverSide": true,
        "responsive": true,
        "deferRender": true,
        bFilter:true,
        sDom:"rtpf",
        "ajax": base_url+"expenses/default_listing",
        "columns": [
            { "data": "date","className":"text-left" },
            { "data": "particular","className":"text-left text-bold" },
            { "data": "receipt_from" ,"className":"text-left"},
            { "data": "category","className":"text-left" },
            { "data": "amount" ,"className":"text-bold"},
             { "data": "note","className":"pay-note text-center" },
            { "data": "action"}
        ],
        "columnDefs":[
                        {
                                "targets": 5,
                                "searchable": false,
                                "orderable": false
                        } 
                ]
        });
       $('.select-category').on('change',function(){ 
            expenseTable
                .columns(3)
                .search(this.value)
                .draw();
        });
       $('.select-picker-year').on('change',function(){ 
             var selected = $(this).find("option:selected").val();
             var month    = $('.select-picker-month').find("option:selected").val();
             var format = selected+"-"+month+"-01";
            expenseTable
                .columns(0)
                .search(format)
                .draw();
        });
        $('.select-picker-month').on('change',function(){ 
             var selected = $(this).find("option:selected").val();
             var year    = $('.select-picker-year').find("option:selected").val();
             var format = year+"-"+selected+"-01";
            expenseTable
                .columns(0)
                .search(format)
                .draw();
        });
       // expenseTable.fnFilter('');


       //expense category
       $(".expense-category-container").on('click','.add-expense-category',function(){
            modalBox( base_url + "expenses/add_category/",{
                id : "modalAddExpenseCategory",
                class : "modal-centered modal-bg-white",
                width:"50%",
                changeModalContent : true,
                afterLoad:function(){
                    $('.do-add-expense-category').on('click',function(){
                        var formcontent = $('.add-expense-category-form').serialize();
                        me_message_v2({error:2,message:"Adding ..."});
                        $.post(base_url+"expenses/doAdd_category",formcontent,function(response){
                            me_message_v2(response);
                            if(response.error==0)
                            {
                                window.location.reload();
                            }
                        });
                    });
                }
            });
        });
       $(".expense-category-container").on('click','.update-expense-category-btn',function(){
        var id = $(this).data("id");
            var this_ = $(this); 
            if(typeof id !="undefined")
            {
                modalBox( base_url + "expenses/update_category/?id="+id,{
                    id : "modalUpdateExpenseCategory",
                    class : "modal-centered modal-bg-white",
                    width:"50%",
                    changeModalContent : true,
                    afterLoad:function(){
                        $('.do-update-expense-category').on('click',function(){
                            var formcontent = $('.update-expense-category-form').serialize();
                            me_message_v2({error:2,message:"Updating ..."});
                            $.post(base_url+"expenses/doUpdate_category",formcontent,function(response){
                                me_message_v2(response);
                                if(response.error==0)
                                {
                                    window.location.reload();
                                }
                            });
                        });
                    }
                });
            }
        });

        $(".expense-category-container").on('click','.delete-expense-category-btn',function(){
            var id = $(this).data("id");
            var this_ = $(this); 
            if(typeof id !="undefined")
            {
                $.confirm({
                    title:"Delete ?",
                    text: "You are going to delete this from the list. Would you like to permanently remove it?",
                    confirm: function() {
                        $.get(base_url+"expenses/doDelete_category/?id="+id,function(response){
                            me_message_v2(response);
                            if(response.error==0)
                            {
                                 setTimeout(function(){
                                    this_.parents("tr").fadeOut(500,function(){
                                        $(this).remove();
                                    });
                                 },500);
                            }
                        },'json');
                    },
                    cancel: function() {
                        // nothing to do
                    }
                });
            }
        });

        $('.expenses-personnel-summary').on('change','.personnel-year-dropdown',function(){
             var selected = $(this).find("option:selected").val();
            window.location.href = base_url+"expenses/personnel/?year="+selected;
        });
          $('body').on('click',".review-payment-btn",function(){
                var payrunid = $(this).data("payrunid");
                var employee = $(this).data("uid");
                ///if no data check first the table
                if(typeof payrunid!="undefined" && typeof employee!="undefined")
                {
                  modalBox( base_url + "payroll/review_payment/"+payrunid+"/"+employee,{
                      id : "modalMakePayment",
                      class : "modal-fullscreen modal-centered modal-bg-white",
                      changeModalContent : true,
                      afterLoad:function(data){
                      }
                  });
                }

        });
});