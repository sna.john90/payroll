$(document).ready(function(){
	// submit login form
	$('#login-form').ajaxForm({
		dataType: 'json',
		beforeSend:function(){
			$('.login-btn').html("Logging in ...");
			$('.login-btn').prop("disabled",true);
		},
		success:function(response){
			$('.login-btn').html('Login <i class="fa fa-sign-in"></i>');
			$('.login-btn').prop("disabled",false);
			if(response.error==0)
			{
				location.href = response.data.url;
			}
			else
			{
				$("#login-form").find("input[type='password']").val("");
				$("#login-form").find(".message").html("<div class='alert alert-danger alert-dismissable text-left'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+response.message+"</div>");
			}
			setTimeout(function(){
				$("#login-form").find(".message").find('.alert').fadeOut("fast");
				$(this).remove();
			},3000);
		}
	});
	// submit forgot password form
	$('#forgot-password-form').ajaxForm({
		dataType: 'json',
		beforeSend:function(){
			$('.forgot-btn').html("Sending ...");
			$('.forgot-btn').prop("disabled",true);
		},
		success:function(response){
			$('.forgot-btn').html('Send Email <i class="fa fa-send"></i>');
			$('.forgot-btn').prop("disabled",false);
			if(response.error==0)
			{
				$("#forgot-password-form").find(".message").html("<div class='alert alert-success alert-dismissable text-left'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+response.message+"</div>");
			}
			else
			{
				$("#forgot-password-form").find(".message").html("<div class='alert alert-danger alert-dismissable text-left'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+response.message+"</div>");
			}
			setTimeout(function(){
				$("#forgot-password-form").find(".message").find('.alert').fadeOut("fast");
				$(this).remove();
			},5000);
		}
	});
	// submit forgot password form 2
	$('#forgot-password-register-form').ajaxForm({
		dataType: 'json',
		beforeSend:function(){
			$('.forgot-register-btn').html("Sending ...");
			$('.forgot-register-btn').prop("disabled",true);
		},
		success:function(response){
			$('.forgot-register-btn').html('Send Email <i class="fa fa-send"></i>');
			$('.forgot-register-btn').prop("disabled",false);
			if(response.error==0)
			{
				$("#forgot-password-register-form").find(".message").html("<div class='alert alert-success alert-dismissable text-left'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+response.message+"</div>");
			}
			else
			{
				$("#forgot-password-register-form").find(".message").html("<div class='alert alert-danger alert-dismissable text-left'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+response.message+"</div>");
			}
			setTimeout(function(){
				$("#forgot-password-register-form").find(".message").find('.alert').fadeOut("fast");
				$(this).remove();
			},5000);
		}
	});
	// submit register notary form
	$('#register-notary-form').ajaxForm({
		dataType: 'json',
		beforeSend:function(){
			$('.register-btn').html("Saving ...");
			$('.register-btn').prop("disabled",true);
		},
		success:function(response){
			$('.register-btn').html('Create Account <i class="fa fa-arrow-right"></i>');
			$('.register-btn').prop("disabled",false);
			if(response.error==0)
			{
				$("#registerNotaryModal").modal("hide");

				$("#loginModal").modal("show");

				reset_register_form();

				$("#login-form").find(".message").html("<div class='alert alert-success alert-dismissable text-left'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+response.message+"</div>");
				setTimeout(function(){
					$("#login-form").find(".message").find('.alert').fadeOut("fast");
					$(this).remove();
				},3000);
			}
			else
			{
				$("#register-notary-form").find("input[type='password']").val("");
				$("#register-notary-form").find("input[type='confirm_password']").val("");
				$("#register-notary-form").find(".message").html("<div class='alert alert-danger alert-dismissable text-left'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+response.message+"</div>");
				setTimeout(function(){
					$("#register-notary-form").find(".message").find('.alert').fadeOut("fast");
					$(this).remove();
				},3000);
			}
		}
	});
	
	$('#loginModal').on('show.bs.modal', function() {
		reset_login_form();
	});

	$('#registerNotaryModal').on('show.bs.modal', function() {
		reset_register_form();
	});

	function reset_register_form()
	{
		$('#register-notary-form')[0].reset();
		$('.register-btn').html('Create Account <i class="fa fa-arrow-right"></i>');
		$('.register-btn').prop("disabled",false);
		$("#register-notary-form").find(".message").html('');

		// $('#forgot-password-register-form')[0].reset();
		// $('.forgot-register-btn').html('Send Email <i class="fa fa-send"></i>');
		// $('.forgot-register-btn').prop("disabled",false);
		// $("#forgot-password-register-form").find(".message").html('');
		
	}
	function reset_login_form()
	{
		$('#login-form')[0].reset();
		$('.login-btn').html('Login <i class="fa fa-sign-in"></i>');
		$('.login-btn').prop("disabled",false);
		$("#login-form").find(".message").html('');

		$('#forgot-password-form')[0].reset();
		$('.forgot-btn').html('Send Email <i class="fa fa-send"></i>');
		$('.forgot-btn').prop("disabled",false);
		$("#forgot-password-form").find(".message").html('');
	}
});