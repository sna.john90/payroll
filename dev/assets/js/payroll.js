jQuery(document).ready(function(){
        // modal generate payroll
        $("#create-payroll-btn").click(function(){
                modalBox( base_url + "payroll/create_payroll/",{
                id : "modalCreatePayroll",
                class : "modal-centered modal-bg-white",
                changeModalContent : true
                });
        });


        // modal: show payroll employee info
        $(".view-employee-btn").click(function(){
                modalBox( base_url + "payroll/payroll_employee_view/",{
                id : "modalPayrollEmployee",
                class : "modal-fullscreen modal-centered modal-bg-white",
                changeModalContent : true
                });
        });

        //adding new fields

        // modal: make payment
        $('.payroll-wrapper').on('click',".review-payment-btn",function(){
                var payrunid = $(this).data("payrunid");
                var employee = $(this).data("uid");
                ///if no data check first the table
                if(typeof payrunid!="undefined" && typeof employee!="undefined")
                {
                  modalBox( base_url + "payroll/review_payment/"+payrunid+"/"+employee,{
                      id : "modalMakePayment",
                      class : "modal-fullscreen modal-centered modal-bg-white",
                      changeModalContent : true,
                      afterLoad:function(data){
                          $("#modalMakePayment").on('hide.bs.modal',function(){
                         // window.location.reload();
                        });
                      }
                  });
                }

        });
          
        // modal: make payment
        $('.payroll-wrapper').on('click',".view-employee-btn",function(){
                var payrunid = $(this).data("payrunid");
                var employee = $(this).data("uid");
                ///if no data check first the table
                if(typeof payrunid!="undefined" && typeof employee!="undefined")
                {
                  modalBox( base_url + "payroll/review_payment/"+payrunid+"/"+employee+"?profile_content=true",{
                      id : "modalMakePayment",
                      class : "modal-fullscreen modal-centered modal-bg-white",
                      changeModalContent : true,
                      afterLoad:function(data){
                        $("#modalMakePayment").on('hide.bs.modal',function(){
                         // window.location.reload();
                        });
                      }
                  });
                }

        });

        //generate payment
        $('.payroll-wrapper').on('click',".generate-payrun",function(){
            preloader_saving_status({error:2,message:"Generating payroll ..."});
            $.post($("#generate-payment-form").attr("action"),$("#generate-payment-form").serialize(),function(data){
              preloader_saving_status(data);
              if(data.error==0)
              {
                  setTimeout(function(){
                    window.location.href = base_url+"payroll";
                  },2000);
              }
            });
        });
        $("body").on('click','.modal .review-payment-btn',function(){
              var class_  = $(this).data("action");
              var data_   = $(this).data(class_+"-id");
              var payrunid = $(this).data("payrun-id");
              if(data_!="0")
              {
                 closeModalbox("modalMakePayment",function(data){
                     modalBox( base_url + "payroll/review-payment/"+payrunid+"/"+data_,{
                      id : "modalMakePayment",
                      class : "modal-fullscreen modal-centered modal-bg-white",
                      changeModalContent : true,
                      afterLoad:function(data){
                      }
                    });
                 });
              }
       });
        var save_payrun_info = function(target,field,value){
                if(target !="undefined" && field !="undefined" && value !="undefined")
                {
                  preloader_saving_status({error:2,message:" Saving changes ..."});
                  var params = [];
                  params.push({name:csrf_token,value:csrf_hash});
                  params.push({name:'primarykey',value:target});
                  params.push({name:'value',value:value});
                  params.push({name:'container',value:field});

                  var data = JSON.stringify(params);
                  $.post(base_url+"payroll/do-update-payrun-details",params,function(response){
                        preloader_saving_status(response);
                  });
                }
        }
        //saving
        $('.payroll-wrapper').on('keyup change','.payrun-details',function(){
                var this_ = $(this);
                var field = this_.data("field");
                var target = this_.data("location-target");
                var is_contenteditable = false;
                var value = "";
                if(this_[0].localName=="div" && this_.attr("contenteditable")!="undefined")
                {
                  value = this_.text();
                }
                else
                {
                  value = this_.val();
                }
                setTimeout(function(){
                        save_payrun_info(target,field,value);
                },2000);

        });
      $('.date-range').datetimepicker({
        format: 'YYYY-MM-DD'
      }).on('dp.show',function(){
          if($(this).hasClass("date-to"))
          {
            $(this).data("DateTimePicker").minDate($('.date-from').val());
          }
      }).on('dp.change',function(e){
          if($(this).hasClass("date-from")){
                $('.date-range.date-to').data("DateTimePicker").show();
          }
          var field = $(this).data("field");
          var target = $(this).data("location-target");
          var value = $(this).val();
          save_payrun_info(target,field,value);
      });
      $('.date').datetimepicker({
        format: 'YYYY-MM-DD',
        // minDate: new Date()
      }).on('dp.change',function(e){
          if($(this).hasClass("payrun-details")){
                 var field = $(this).data("field");
                  var target = $(this).data("location-target");
                  var value = $(this).val();
                  save_payrun_info(target,field,value);
          }
      });

      $("#checkAll").click(function () {
          $('input:checkbox').not(this).prop('checked', this.checked);
      });

      $('#tablePayroll').on('click','input[type="checkbox"].checkbox-employee',function(){
         var count_checkbox = $('#tablePayroll .checkbox-employee').length;
         var check_checkbox = 0;
         $('.checkbox-employee').each(function(){
            if($(this).is(":checked"))
            {
              check_checkbox++;
            }
         });
         console.log(count_checkbox+" "+check_checkbox);
         if(check_checkbox == count_checkbox)
         {
           $('#checkAll').prop("checked",true);
         }
         else {
           $('#checkAll').prop("checked",false);
         }
      });

      // remove company
    	$(".payrun-lists").on("click",".remove-payment-btn",function(){
    		var id = $(this).data("payrun-id");
        var this_ = $(this);
        $.confirm({
            title:"Delete expense confirmation",
            text: "You are going to delete this expense from the list. Would you like to permanently remove it?",
            confirm: function() {
                $.get(base_url+"payroll/do-remove/"+id,function(response){
                    me_message_v2(response);
                    if(response.error==0)
            				{
                      this_.parents("tr").fadeOut(1000,function(){
                        $(this).remove();
                      });
            				}
                },'json');
            },
            cancel: function() {
                // nothing to do
            }
        });
    	});

      //view notes
      // modal VIEW EXPENSE DETAIL - monthly
      $(".payrun-lists").on('click','.notes',function(){
          var id = $(this).data("id");
          modalBox( base_url + "payroll/notes/?id="+id,{
              id : "modalExpensenotes",
              class : "modal-centered modal-bg-white",
              changeModalContent : true
          });
      });
});
