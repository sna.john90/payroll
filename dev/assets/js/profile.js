// VIEW PAYSLIP
$("#view-payslip-btn,.view-payslip-btn").click(function(){
        var payrun_id = $(this).data("payrun-id");
        modalBox( base_url + "profile/payslip/"+payrun_id,{
        id : "modalViewPayslip",
        class : "modal-fullscreen modal-centered modal-bg-white",
        changeModalContent : true
        });
});

// modal CREATE FORM
$("#create-form-btn,.create-form-btn").click(function(){
	modalBox( base_url + "profile/create_form/",{
        id : "modalCreateForm",
        class : "modal-centered modal-bg-white",
        changeModalContent : true
	});
});

// modal VIEW FORM
$("#view-form-btn,.view-form-btn").click(function(){
        modalBox( base_url + "profile/view_form/",{
        id : "modalViewForm",
        class : "modal-centered modal-bg-white",
        changeModalContent : true
        });
});

// modal CREATE LEAVE FORM
$("#create-leave-form-btn,.create-leave-form-btn").click(function(){
        modalBox( base_url + "profile/create_leave_form/",{
        id : "modalCreateLeaveForm",
        class : "modal-wide modal-centered modal-bg-white",
        changeModalContent : true
	});
});
// modal VIEW LEAVE FORM
$("#view-leave-form-btn,.view-leave-form-btn").click(function(){
        modalBox( base_url + "profile/create_leave_form/",{
        id : "modalViewLeaveForm",
        class : "modal-wide modal-centered modal-bg-white",
        changeModalContent : true
        });
});
