$(document).ready(function(){
	// modal EDIT EXPENSE
	$(".department-container").on('click','.add-department-btn',function(){
		modalBox( base_url + "settings/addDepartment/",{
	        id : "modalEditExpense",
	        class : "modal-centered modal-bg-white",
	        changeModalContent : true,
	        afterLoad:function(){
	        	$('.doadd-department-btn').on('click',function(){
	        		me_message_v2({error:2,message:"Saving ..."});
	        		$.post(base_url+"settings/addDepartment",$('.add-department-form').serialize(),function(response){
	        			me_message_v2(response);
	        			if(response.error==0)
	        			{
	        				setTimeout(function(){
	        					window.location.reload();
	        				},1000);
	        			}
	        		});
	        	});
	        }
		});
	});
	$(".department-container").on('click','.update-department-btn',function(){
       		 var id = $(this).data("id");
            var this_ = $(this);  
                modalBox( base_url + "settings/updateDepartment/?id="+id,{
                    id : "modalUpdateDepartment",
                    class : "modal-centered modal-bg-white",
                    width:"50%",
                    changeModalContent : true,
                    afterLoad:function(){
                        $('.doupdate-department-btn').on('click',function(){
                            var formcontent = $('.update-department-form').serialize();
                            me_message_v2({error:2,message:"Updating ..."});
                            $.post(base_url+"settings/doUpdateDepartment",formcontent,function(response){
                                me_message_v2(response);
                                if(response.error==0)
                                {
                                    window.location.reload();
                                }
                            });
                        });
                    }
                }); 
        });
	$(".department-container").on('click','.delete-department-btn',function(){
            var id = $(this).data("id");
            var this_ = $(this); 
            if(typeof id !="undefined")
            {
                $.confirm({
                    title:"Delete ?",
                    text: "You are going to delete this from the list. Would you like to permanently remove it?",
                    confirm: function() {
                        $.get(base_url+"settings/deleteDepartment/?id="+id,function(response){
                            me_message_v2(response);
                            if(response.error==0)
                            {
                                 setTimeout(function(){
                                    this_.parents("tr").fadeOut(500,function(){
                                        $(this).remove();
                                    });
                                 },500);
                            }
                        },'json');
                    },
                    cancel: function() {
                        // nothing to do
                    }
                });
            }
        });
});