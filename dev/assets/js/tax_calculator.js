/*
* TAX CALCULATOR
*
* BOR MATRIX: 
* SSS MATRIX: https://www.sss.gov.ph/sss/appmanager/pages.jsp?page=scheduleofcontribution
* PHILHEALTH MATRIX: https://www.philhealth.gov.ph/partners/employers/contri_tbl.html
* PAG-IBIG/HDMF: 
*     P1500 and below = 1%
*     Over = 2% (or P100)
*  
*/

$(document).ready(function(){

	function formatNumber(n, currency='') {
	    return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	}

	function parseNumber(n){
		var val = n.replace(/,/g, "");
		//var num = parseFloat(n.replace(/^\s+|\s+$/g,"")); 
		console.log('num: '+ val);
		if(typeof val == 'string' && !val.trim() || typeof val == 'undefined' || val === null ) {
			val = 0;
		}else{
		    val = parseFloat(val);
		}
		console.log('num2: '+ val);
		return val;
	}


	//
	function getCivilStatus(cstatus){
		if(cstatus =='S' || cstatus =='ME'){
			status	='S/ME';	
		}else if(cstatus =='S1' || status =='ME1'){
			status	='S1/ME1';
		}else if(cstatus =='S2' || status =='ME2'){
			status	='S2/ME2';
		}else if(cstatus =='S3' || status =='ME3'){
			status	='S3/ME3';
		}else if(cstatus =='S4' || status =='ME4'){
			status	='S4/ME4';
		}
		return status;
	}

	//get payment mode
	function getPeriod(paymode){
		var payModeVal;
		if(paymode =='monthly'){
			payModeVal	='monthly';
		}else if(paymode =='semiMonthly'){
			payModeVal	='semiMonthly';
		}
		return payModeVal;
	}

	/*
	* SSS Contributions
	* Contributions = 11% of mo. salary (not more than P16000)
	* Employee Share (EE) = 3.63%
	* Employer Share (ER) = 7.37%
	*/
	function getSSSContribution(basicPay, memStatus, period){
		//var memStatus = "employed";
		//var period = "monthly";
		$.getJSON(base_url + 'assets/js/sss_table.json', function(sssData) {	
			$.each(eval(sssData), function(sssKey, sssVal) {
				//console.log(JSON.stringify(sssVal));
				//console.log("sss status: " + sssVal.membershipStatus[0].status);
				var $dataTable		= sssVal.membershipStatus[0].dataTable;
				var bracketCount	= parseInt($dataTable.length-1);
				var minRate			= parseFloat($dataTable[0].salaryRangeEnd);
				var maxRate			= parseFloat($dataTable[bracketCount].salaryRangeStart);
				
				//console.log(basicPay,bracketCount,minRate,maxRate);

				if(memStatus === sssVal.membershipStatus[0].status){
					// employed
					var membershipType 		= sssVal.membershipStatus[0].dataTable;

					for (var k = 0; k < membershipType.length; k++){

						var salaryRangeStart	= membershipType[k].salaryRangeStart;
						var salaryRangeEnd		= membershipType[k].salaryRangeEnd;
						//console.log('sss: ' + membershipType[k], salaryRangeStart, salaryRangeEnd);

						if(basicPay <= minRate && minRate == salaryRangeEnd){
							var sssEE 		= membershipType[k].ee;
							var sssER 		= membershipType[k].er;
							var sssShare	= getPeriod(period, sssEE);
						}else if(basicPay >= salaryRangeStart && basicPay <= salaryRangeEnd){
							var sssEE 		= membershipType[k].ee;
							var sssER 		= membershipType[k].er;
							var sssShare	= getPeriod(period, sssEE);	
						}else if(basicPay>=salaryRangeEnd){
							var sssEE 		= membershipType[k].ee;
							var sssER 		= membershipType[k].er;
							var sssShare	= getPeriod(period, sssEE);	
						}
					}

				}else if(memStatus === sssVal.membershipStatus[1].status){
					// selfemployed

				}

				//console.log('sssShare: ' + sssEE);
				$('#sss').val(sssShare);
			});
		});
	}

	/*
	* Withholding Tax
	*/
	function getWithholdingTax(basicPay, cStatus, period){
		var cStatus 		= getCivilStatus(cStatus);
		//var period 			= "monthly";
		//var taxableIncome 	= basicPay-993.8;
		$.getJSON(base_url + 'assets/js/tax_table.json', function(taxData) {	
			$.each(eval(taxData), function(taxKey, taxVal) {

				var $taxPeriod	= taxVal.withholdingTax[0].period;
				var $taxTable 	= taxVal.withholdingTax[0].dataTable;

				var grossIncome = getGrossIncome(basicPay);
				var deductions 	= getTotalDeductions();
				var taxableIncome = grossIncome - deductions;

				console.log('getWithholdingTax: '+ getGrossIncome(basicPay), getTotalDeductions(), taxableIncome);

				//console.log(JSON.stringify(taxVal));
				//console.log("employee status: " + taxVal.withholdingTax[0].dataTable[0].status);
				//console.log(basicPay,$taxPeriod, $taxTable.length);
				//console.log('deductions: '+ deductions);

				if(period === $taxPeriod){
					// employed
					var membershipType 	= taxVal.withholdingTax[0].dataTable;

					for (var k = 0; k < $taxTable.length; k++){
						var compensationLevel = $taxTable[k].compensationLevel;
						if(cStatus === $taxTable[k].status  ){
							if(taxableIncome >= compensationLevel){
								var percent = parseFloat($taxTable[k].percent)/100;
								var excess	= (taxableIncome - parseFloat(compensationLevel));
								var tax 	= (excess * percent) + parseFloat($taxTable[k].tax);
								console.log('status: '+ taxableIncome, $taxTable[k].status, compensationLevel, percent, excess, tax);
							}else{
								var tax  	= 0;
								console.log('zero tax');
							}
						}
					}

				}

				console.log('taxTotal: ' + tax);
				$('#taxTotal').html(tax);
			});
		});
	}

	/*
	* PAG-IBIG / HDMF  Contributions
	* Contributions = 1% of monthly compensation/salary if less than 1500, otherwise, 2%
	* Employer share = 2% of monthly compensation/salary            
	* Max compensations = 5000    
	*/
	function getPagibigContribution(monthlyCompensation){
		const maxMonthlyCompensation 	= 5000.00;
		const bracket 	= 1500.00;
		if (monthlyCompensation <= bracket){
			return monthlyCompensation * 0.01;
		}else if (monthlyCompensation > bracket){
			return maxMonthlyCompensation * 0.02;
		}
	}

	/*
	* Frequency of payment/payroll
	*/
	function getPeriod(period, ee) {
		if(period == "monthly") {
			return ee;			
		} else if (period == "semimonthly") {
			return (ee/2);			
		} else if (period == "weekly") {
			return (ee/4);			
		}
	}	

    /* PHILHEALTH / PHIC Contributions
    * Contribution or premium = 2.5% of salary rate
    * Employee Share (EE) = 50% of premium
    * Employer Share (ER) = 50% of premium
    * Max. Rate = 35,000
    */
	function getPhilhealthContribution(basicPay, period){
		//var period = "monthly";
		$.getJSON(base_url + 'assets/js/philhealth_table.json', function(philhealthData) {	
			$.each(philhealthData, function(PhilhealthKey, philhealthVal) {

					var bracketCount	= parseInt(philhealthVal.philhealth.length-1);
					var minRate			= parseFloat(philhealthVal.philhealth[0].salaryRangeEnd);
					var maxRate			= parseFloat(philhealthVal.philhealth[bracketCount].salaryRangeStart);
					//console.log(minRate, maxRate, bracketCount);

					for(var k = 0; k < philhealthVal.philhealth.length; k++){
						var salaryRangeStart 	= parseFloat(philhealthVal.philhealth[k].salaryRangeStart);
						var salaryRangeEnd 		= parseFloat(philhealthVal.philhealth[k].salaryRangeEnd);		
						
						if(basicPay >= salaryRangeStart && basicPay <= salaryRangeEnd){
							var philhealthPremium 	= philhealthVal.philhealth[k].premium;
							var philhealthEE 		= philhealthVal.philhealth[k].ee;
							var philhealthER 		= philhealthVal.philhealth[k].er;
							var philHealthShare		= getPeriod(period, philhealthEE);																
						}else if (basicPay <= minRate && minRate == salaryRangeEnd){
							var philhealthPremium 	= philhealthVal.philhealth[k].premium;
							var philhealthEE 		= philhealthVal.philhealth[k].ee;
							var philhealthER 		= philhealthVal.philhealth[k].er;
							var philHealthShare		= getPeriod(period, philhealthEE);								
						}else if (basicPay >= maxRate){
							var philhealthPremium 	= philhealthVal.philhealth[k].premium;
							var philhealthEE 		= philhealthVal.philhealth[k].ee;
							var philhealthER 		= philhealthVal.philhealth[k].er;
							var philHealthShare		= getPeriod(period, philhealthEE);																					
						}				
					}
					$('#philhealth').val(philHealthShare);
			});			
		});	
	}

	function getGrossIncome(basicPay){
		//var basicPay, holidayPay, overtimePay, nightDifferential, otherAllowance;
		var holidayPay			= parseNumber($('#holidayPay').val());
		var overtimePay			= parseNumber($('#overtimePay').val());
		var nightDifferential	= parseNumber($('#nightDifferential').val());
		var otherAllowance		= parseNumber($('#otherAllowance').val());
		var grossIncome 		= basicPay + holidayPay + overtimePay + nightDifferential + otherAllowance;
		console.log("grossIncome: "+ grossIncome, basicPay, holidayPay, overtimePay, nightDifferential, otherAllowance);
		return grossIncome;
	}

	function getTotalDeductions(){
		//sss, philhealth, pagibig, absences, tardiness, undertime;
		var sss 		= parseNumber($('#sss').val());
		var philhealth 	= parseNumber($('#philhealth').val());
		var pagibig 	= parseNumber($('#pagibig').val());
		var absences 	= parseNumber($('#absences').val());
		var tardiness 	= parseNumber($('#tardiness').val());
		var undertime 	= parseNumber($('#undertime').val());
		//var totalDeductions = sss + philhealth + pagibig;
		var totalDeductions = sss + philhealth + pagibig + absences + tardiness + undertime;
		console.log("totalDeductions: "+totalDeductions);
		return totalDeductions;	
	}

	//compute SSS/Philhealth/Pagibig
	function computeContributions(basicPay){
		if (typeof basicPay === "undefined" || basicPay === null) { 
		    basicPay = parseFloat($('#basicPay').attr('data-value')); 
		}
		var period	= $('#payMode').val();

		getSSSContribution(basicPay,"employed",period);

		var pagibig 	= getPagibigContribution(basicPay);
		$('#pagibig').val(formatNumber(pagibig));

		getPhilhealthContribution(basicPay, period);

		var civilStatus = $('#civilStatus').val();
		getWithholdingTax(basicPay, civilStatus, period);
	}

	//compute tax total
	function computeTaxTotal(){

	}

	$('#basicPay, .updateTaxComputation').on('change propertychange keyup paste input',function(){
		//$(this).attr('data-value', parseNumber($(this).val()));
		var basicPay = parseFloat($('#basicPay').attr('data-value'));
		computeContributions(basicPay);
	})


	//civi status
	$('#civilStatus').on('change',function(){
		console.log($(this).val());
		$('#taxTotal').html(getTotalDeductions());
	})

	//paymode
	$('#payMode').on('change',function(){
		console.log($(this).val());
		computeContributions();
	})

	//pass value to data-value attrib
	$('#basicPay, #philhealth, #pagibig, #sss').on('change propertychange keyup paste input',function(){
		var dataVal 	=$(this).val();
		$(this).attr('data-value', parseNumber(dataVal));
	})


	//add number format
	$('#basicPay').on('blur',function(){
		var val =	parseNumber($(this).attr('data-value'));
		$(this).val(formatNumber(val));
	})


})