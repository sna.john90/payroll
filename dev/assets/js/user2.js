$(document).ready(function(){

	function get_filters()
	{
		var company_id = $("#company-select").val();
		// if there is company id
		if(typeof company_id == "undefined")
		{
			company_id = 0;
		}
		var filters = {
			not_user_id : 1,
			company_id : company_id
		};

		return JSON.stringify(filters);
	}
	function get_company_id()
	{
		return $("#company-select").val();
	}

	$('#company-select').niceSelect();

	// select company
	$("#company-select").on("change",function(){
		// load user tables
		$("#userTable").DataTable().ajax.url( base_url+"admin/users/users_listing/?filters="+get_filters() ).load();
	});

	var user_sources = new Bloodhound({
	  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
	  queryTokenizer: Bloodhound.tokenizers.whitespace,
	  remote: {
	    url: base_url+'users/get_company_users/'+get_company_id()+"/?search=%QUERY",
	    wildcard: '%QUERY',
	    replace: function(url, uriEncodedQuery) {
	        return base_url+'users/get_company_users/'+get_company_id()+"/?search="+uriEncodedQuery;
	    },
	    filter: function (response) {
	    	return response.data;
	    }
	  }
	});

	var user_typeahead = $('#add-existing-user').typeahead(null, {
	  name: 'existing-users',
	  display: 'name',
	  source: user_sources,
	  limit: 100,
	  templates: {
	  	empty: [
	      '<div class="empty-message">',
	        'No results found.',
	      '</div>'
	    ].join('\n'),
	    suggestion: Handlebars.compile('<div class="options-container"><img class="img-responsive pull-left pr15" src="{{profile_pic}}" width="48"/><a href="javascript:void(0)" class="options-text">{{name}}</a></div>')
	  }
	});
	user_typeahead.on('typeahead:selected',function(evt,data){
	    $("#add-existing-user-form").find(".existing-id").val(data.id);
    });

    $('#add-existing-user-form').ajaxForm({
		dataType: 'json',
		beforeSubmit: function(arr, $form, options) { 
		    arr.push({name:"company_id",value:get_company_id()});              
		},
		beforeSend:function(){
			$('.submit-btn').html('<i class="fa fa-plus"></i> Adding ...');
			$('.submit-btn').prop("disabled",true);
		},
		success:function(response){
			response_message(response);
			$('.submit-btn').html('<i class="fa fa-plus"></i> Add');
			$('.submit-btn').prop("disabled",false);
			if(response.error==0)
			{
				user_typeahead.typeahead('val','');
				$("#add-existing-user-form").find(".existing-id").val('');

				$("#userTable").DataTable().ajax.reload(null,false);
			}
		}
	});

	 
	// edit user button
	$("#userTable").on("click",".edit-user-btn", function(){
		var id = $(this).data("id");
		modalBox( base_url + "admin/users/edit_users_view/"+id,{
            id : "modalAddUser1",
            class : "",
            changeModalContent : true
		});
	});
	$("#userTable").on("click",".remove-user-btn",function(){
		var id = $(this).data("id");
		var company_id = $(this).data("company-id");
		jConfirm("Confirmation","You are going to delete this user from the list. Would you like to permanently remove it?",function(){
			$.get(base_url+"admin/users/remove/"+id+"/"+company_id,function(response){
				response_message(response);
				if(response.error==0)
				{
					// reload admin status data table
	          		$("#userTable").DataTable().ajax.reload(null,false); 
				}
			},'json');
		});
	});
	// user datatable
	$("#userTable").DataTable({
		initComplete: function(){
            var api = this.api();
            $('#userTable_filter input').off('.DT').on('keyup.DT', function (e) {
                if(e.keyCode == 13 || this.value.length==0) 
                {
                    api.search(this.value).draw();
                }
            });
        },
		"processing": true,
        "serverSide": true,
        "responsive": true,
        "deferRender": true,
        "ajax": base_url+"admin/users/users_listing/?filters="+get_filters(),
        "columns": [
            { "data": "name" },
            { "data": "email" },
            { "data": "user_group" },
            { "data": "date_added" },
            { "data": "status" },
            { "data": "action" }
        ],
        "columnDefs":[
			{
				"targets": 5,
				"searchable": false,
				"orderable": false
			} 
		]
	});
});