/* load page */
function loadPage(src_file){
	$('#page-content').load(src_file,function(){
		console.log(src_file + " is file is loaded");
		
		/*reenable on load of page*/
		/*bootstrap-select selectpicker*/
			$('.selectpicker').selectpicker();
			/*tooltip*/
			$('[data-toggle="tooltip"]').tooltip();
			$('[data-toggle="tooltip-body"]').tooltip({container:'body'});
	})
}


jQuery(function($){
	//=== function to check if element existed
	jQuery.fn.exists = function() { return this.length > 0; };
	jQuery.fn.existNot = function() { return this.length == 0; };


	Inputmask.extendAliases({
	  pesos: {
	            prefix: "₱ ",
	            groupSeparator: ".",
	            alias: "numeric",
	            placeholder: "0",
	            autoGroup: !0,
	            digits: 2,
	            digitsOptional: !1,
	            clearMaskOnLostFocus: !1
	        }
	});

	/*enable underline on form-control*/
	function enableFormUnderline($elem, $event){
		 if($elem.next().existNot()){
	    	$elem.after('<div class="form-underline"></div>');
	    }
		if($event =='focus'){
			$elemW	=$elem.outerWidth();
		}else{
			$elemW	=0;
		}
		$elemLeft	=$elem.position().left;
		$elemTop	=$elem.position().top + $elem.outerHeight();
		$elem.next().css({
			'width':$elemW,
			'left':$elemLeft+"px",
			'top':$elemTop+"px"
		})
	}/*END: enableFormUnderline*/

	$(document).ready(function(){
		
		/* load initial page */
		//loadPage("page-profile.php");
		//loadPage("page-maintainables.php");

		/*load function*/
		$('.hasPage').on('click',function(){
			loadPage($(this).data('target'));
			$(this).addClass('active').parent().siblings().find(".hasPage").removeClass('active');
		})

		/*set sticky nav*/
		//$('.navbar-nox').affix();
		$('.table-payroll td>a').on('click',function(e){
			//e.preventDefault();
			$(this).next('.pay-more-info').slideToggle('fast');
		})

		/*tooltip*/
     	$('[data-toggle="tooltip"]').tooltip();

     	/*bootstrap-select selectpicker*/
     	$('.selectpicker').selectpicker();

		//data tables
	   //  var smartDataTable 	= $('.dataTable').DataTable({
	   //   	scrollCollapse: true,
	   //   	//scrollY: '90vh',
	   //   	"language": {
	   //   		"lengthMenu": "SHOW _MENU_",
	   //   		"paginate": {
			 //      "previous": "&lt;",
			 //      "next": "&gt;"
			 //    }
			 // },
	   //   	dom:"<'row data-table-header'<'col-xs-8'f><'col-xs-4'l>>" +
				// 	"<'row'<'col-sm-12'tr>>" +
				// 	"<'row data-table-footer'<'col-sm-4 font-xs'i><'col-sm-8'p>>"
	   //   });

	    /*enable underline on form-control*/
	    $('body').on('focus','.form-minimalist .form-control',function(event){
	    	event.stopPropagation();
		    enableFormUnderline($(this),'focus');

	    })
	    $('body').on('blur focusout','.form-minimalist .form-control',function(event){
	    	event.stopPropagation();
		    enableFormUnderline($(this),'blur');
	    })


	    //profile display
	     $('body').on('click',".g-view-profile",function(){  
                var employee = $(this).data("uid");
                var active_tab = $(this).data("active-tab"); 
                ///if no data check first the table
                if(typeof employee!="undefined")
                {
                  var filter = "";
                  if(typeof active_tab!="undefined" && active_tab!="")
                  {
                  	filter = "/?active_tab="+active_tab;
                  }
                  modalBox( base_url + "profile/details/"+employee+filter,{
                      id : "modalProfileDetails",
                      class : "modal-fullscreen modal-centered modal-bg-white",
                      changeModalContent : true,
                      afterLoad:function(data){
                      }
                  });
                }

        });

	     // edit company
		$("body").on("click",".edit-company-btn",function(){
			var id = $(this).data("id");
			if(typeof id!="undefined")
			{
				modalBox( base_url + "admin/company/edit_company_view/"+id,{
	            id : "modalCompany",
	            class : "modal-centered modal-bg-white",
	            changeModalContent : true
			});
			}
			
		});
		$('body').on('click',".download-payslip-btn",function(){
                var payrunid = $(this).data("payrunid");
                var employee = $(this).data("uid"); 
                ///if no data check first the table
                if(typeof payrunid!="undefined" && typeof employee!="undefined")
                {
                  $.get(base_url + "payroll/review_payment/"+payrunid+"/"+employee+"/print",function(data){
                    $('body').find(".for-printing").html(data);
                    window.print();
                    window.onafterprint = function(){
                      $('body').find(".for-printing").html("");
                    };
                  });
                }

        });

          // modal: show payroll employee info
       $('body').on('click',".add-saving-transaction",function(){
       		var id = $(this).attr("data-id");
       		if(typeof id != "undefined")
       		{
       			 modalBox( base_url + "savings/add/"+id,{
	                id : "modalSavingsAdd",
	                class : "modal-centered modal-bg-white",
	                changeModalContent : true,
	                afterLoad:function(){
	                	$('.do-add-saving-transaction').on('click',function(){
	                		 me_message_v2({error:2,message:"Adding the transaction ..."});
	                		 $.post(base_url+"savings/add/"+id,$('.add-savings-form').serialize(),function(response){
	                		 	me_message_v2(response);
	                		 	if(response.error==0)
	                		 	{
	                		 		closeModalbox("modalSavingsAdd");
	                		 		$.get(base_url+"profile/savings_list/"+id,function(response){
	                		 			$("#mysavings").html(response);
	                		 		});
	                		 	}
	                		 });
	                	}); 
	                }
                });
       		}
               
        });

	})/*END: document.ready*/
	$(document).load(function(){
		
	})/*END: document.load*/
	$(document).resize(function(){
		
	})/*END: document.resize*/



})