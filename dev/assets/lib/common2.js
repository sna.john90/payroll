 

/*=========================
    MODAL - MULTIPLE OVERLAY
    ===========================*/
    $(document).on({
        'show.bs.modal': function () {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            $(this).find('.modal-dialog').css('z-index', zIndex-1);
            setTimeout(function() {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 2).addClass('modal-stack');
            }, 10);
        },
        /*'shown.bs.modal': function () {
            var this_ = $(this);
            setTimeout(function(){

                    var heightP       = $(window).height();
                    var contentHeight = this_.find('.modal-content').height();
                    this_.css({"overflow":"hidden"});
                    this_.find('.modal-stack').css({width:"100%"});
                    if(contentHeight>=heightP)
                    {
                        this_.find('.modal-custom-wrapper').slimScroll({
                                    height:heightP+"px",
                                    alwaysVisible: true
                                });
                        setTimeout(function(){  
                                this_.find('.modal-custom-wrapper').parent('.slimScrollDiv').find('> .slimScrollBar').addClass('cssSlimscroll');
                            },100);
                    }
            },500);
        },*/
        'hidden.bs.modal': function() {
            var this_ = $(this);
            if ($('.modal:visible').length > 0) {
                //restore proper scrolling
                setTimeout(function() {
                    $(document.body).addClass('modal-open');
                    this_.find('.modal-stack').css({width:"100%"});
                }, 0);
            }
        }
    }, '.modal');

/*bid timer*/
setInterval(function() {
    $('.bid-sec').each(function(){
        var bidSec =parseInt($(this).html());
        if(bidSec<=0){
            bidSec=60;
        }
        var bidSecLeft  =(bidSec-1);
        $(this).html(bidSecLeft);

    })
},1000); 

// Confirm
function jConfirm(title,content,confirmation,cancel)
{
    $.confirm({
        title : "<h2 class='page-title'>"+title+"</h2>",
        content : content,
        confirm : confirmation,
        cancel : cancel,
        confirmButtonClass: 'btn-primary',
        cancelButtonClass: 'btn-default',
        confirmButton: 'Yes',
        cancelButton: 'No'
    });
}

// Modalbox 
var $dynamic_modal = {};
function modalBox(url, options)
{
	// initialize defaults
	var defaults = {
        header : "Title",
        id : "noxModal",
        class : "modal-wide",
        width: "",
        modal_dialog_class: "",
        noclose : false,
        changeModalContent: false,
        content : false,
        footer : true,
        button : true,
        drag : false,
        backdrop : true,
        keyboard : true,
        afterLoad : function(data){},
        buttons: [
        	{
	            text: "OK",
                additional_class: "",
	            type: "success",
	            click: function(){

	            }
            },
            {
                text: "Close",
                additional_class: "",
                type: "default",
                click: function() {
                    closeModalbox();
                }
            }
        ]
    }
    // extend options
    var opts = $.extend({},defaults,options);
    // check modal existence
    var lengthmodal = $('body').find('div[id*="'+opts.id+'"]').length;
    if(lengthmodal<1)
    {
        lengthmodal = "";
    }
    opts.id = opts.id+lengthmodal;
    // add width on modal dialog
    var width = "";
    if(typeof opts.width != "undefined" && opts.width!="" && opts.drag==false)
    {
        width = 'style="width:'+opts.width+'"';
    }
    // initialize modal html and check if the whole model content will be filled or not
    if(opts.changeModalContent == true)
    {
    	var modal_html = '<div id="'+opts.id+'" class="'+opts.class+' modal fade" tabindex="-1" role="dialog"><div class="modal-dialog '+opts.modal_dialog_class+'" '+width+'><div class="modal-content"></div></div></div>';
    }else
    {
    	var modal_html = '<div id="'+opts.id+'" class="'+opts.class+' modal fade" tabindex="-1" role="dialog"><div class="modal-dialog '+opts.modal_dialog_class+'" '+width+'><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title"></h4></div><div class="modal-body"></div><div class="modal-footer"></div></div></div></div>';
    }
    
    // append modal to body
    $('body').append(modal_html).promise().done(function(){
    	$dynamic_modal  = $('body').find("#"+opts.id);
    	// check for modal backdrop
    	if(typeof opts.backdrop != "undefined")
	    {
	        $dynamic_modal.modal({
	        	backdrop : opts.backdrop
	        });
	    }
    	// check for modal keyboard
    	if(typeof opts.keyboard != "undefined")
	    {
	        $dynamic_modal.modal({
	        	keyboard : opts.keyboard
	        });
	    }

	    if(opts.changeModalContent == false)
	    {
	    	// check modal header and set modal head title
		    if(opts.header)
	        {
	            //set header
	            var head = opts.header;
	            if(head == "&nbsp;" || head == "" || head == " ")
	            {
	                $dynamic_modal.find('.modal-header').find('h4.modal-title').remove();
	            }
	            else
	            {
	                $dynamic_modal.find('.modal-header').find("h4.modal-title").html(head);
	            }
	        }
	        else
	        {
	            $dynamic_modal.find('.modal-header').hide();
	        }
	        //close button on the title bar
	        if(opts.noclose)
	        {
	            $dynamic_modal.find('button.close').css("display", "none");
	        }else
	        {
	            $dynamic_modal.find('button.close').css("display", "inline");
	        }
	        // footer buttons
	        var btns = opts.buttons;
        	var footer = $dynamic_modal.find(".modal-footer");
	        //initiate
	        footer.html("");
        	if(btns.length == 0 || opts.footer==false || opts.button==false) {
            	footer.hide();
        	}
        	else
        	{
            	footer.show();
        	}
        	for (var i = 0; i < btns.length; i++) {
            	btns[i].type = btns[i].type || 'default';
            	btn = $('<button type="button" class="btn btn-' + btns[i].type + ' ' + btns[i].additional_class + '">' + btns[i].text + '</button>');
            	btn.on("click",btns[i].click);
            	footer.append(btn);
        	}
	    }
	    // show modal
	    $dynamic_modal.modal('show');
        if(opts.changeModalContent == false)
        {
            $dynamic_modal.find('.modal-body').html("<h1 style='text-align:center;margin-top:30px;'><span class='fa fa-spin fa-circle-o-notch'></span></h1>");
        }else
        {
            $dynamic_modal.find('.modal-content').html("<h1 style='text-align:center;margin-top:30px;'><span class='fa fa-spin fa-circle-o-notch'></span></h1>");
        }
        if (url == "" && opts.content === false) {
            return;
        }

        if((url == "" || url == false) && opts.content != "")
        {
        	if(opts.changeModalContent == true)
        	{
        		$dynamic_modal.find('.modal-content').html(opts.content);
        	}else
        	{
        		$dynamic_modal.find('.modal-body').html(opts.content);
        	}

            // $dynamic_modal.find(".overflow-cont").slimScroll({
            //     height: '100%',
            //     size: '5px',
            //     wheelStep: 10,
            //     touchScrollStep: 75
            // });
        }else 
        {
            var reqid = "reqid="+new Date().getTime();
            var url_data = (url.indexOf("?") > -1)? url+"&"+reqid : url+"?"+reqid; 
            $.get(url_data, function(data) {

            	if(opts.changeModalContent == true)
	        	{
	        		$dynamic_modal.find('.modal-content').html(data);
	        	}else
	        	{
	        		$dynamic_modal.find('.modal-body').html(data);
	        	}

                // $dynamic_modal.find(".overflow-cont").slimScroll({
                //     height: '100%',
                //     size: '5px',
                //     wheelStep: 10,
                //     touchScrollStep: 75
                // });

                setTimeout(function(){
                    try
                    {
                        if(opts.drag)
                        {
                            $('#'+opts.id).draggable({
                                cursor: "move"
                            });
                        }
                    }
                    catch(err)
                    {
                        console.log('Draggable is not included.');
                    }

                    if(typeof(opts.afterLoad) == 'function')
                    {
                        opts.afterLoad.call(undefined,data);
                    }
                },100);

            }, "text");
        }
        $dynamic_modal.on('hidden.bs.modal', function () {
            $(this).remove();
            $(this).data('bs.modal', null);
        });
    });
}

function closeModalbox(modal_id,callback) {
    var id = "";
    if(typeof modal_id === 'undefined')
    {
        id = $('body').find("#noxModal");
    }
    else
    {
        id = $('body').find("#"+modal_id);
    }
    id.modal('hide');
    id.on('hidden.bs.modal',function () {
        $(this).data('bs.modal', null);
        if(typeof callback == "function" && typeof callback!="undefined")
        {
            callback.call(undefined,modal_id);
        }
    });
}

function response_message(response)
{
    var message = response.message;
    var type = ["success","danger"];
    $.notify(
    {
        message: "<strong>"+message+"</strong>"
    },{
        type: type[response.error],
        animate: {
            enter: 'animated slideInRight',
            exit: 'animated slideOutRight'
        },
        z_index: 2000,
        delay: 4000,
        template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert" style="width:370px;">' +
        '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
        '<div data-notify="message" style="margin-bottom:-15px;margin-right:10px;">{2}</div>' +
        '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
        '</div>' +
        '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
}

function randString(id){
    var dataSet = $(id).attr('data-character-set').split(',');  
    var possible = '';
    if($.inArray('a-z', dataSet) >= 0){
        possible += 'abcdefghijklmnopqrstuvwxyz';
    }
    if($.inArray('A-Z', dataSet) >= 0){
        possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
    if($.inArray('0-9', dataSet) >= 0){
        possible += '0123456789';
    }
    if($.inArray('#', dataSet) >= 0){
        possible += '![]{}()%&*$#^<>~@|';
    }
    var text = '';
    for(var i=0; i < $(id).attr('data-size'); i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}


// get schedule details
$("body").on("click","[data-toggle='loan-detail']",function(){
    var id = $(this).data("id");
    modalBox( base_url + "schedule/get_schedule_details/"+id,{
        width : '',
        id: 'modalLoanDetail',
        class : "modal-70",
        changeModalContent: true
    });
});

// get billing details
$("body").on("click","[data-toggle='billing-detail']",function(){
    var id = $(this).data("id");
    modalBox( base_url + "admin/billing/get_billing_details/"+id,{
        width : '',
        id: 'modalLoanDetail',
        class : "modal-70",
        changeModalContent: true
    });
});

// get user info details and display in modal
$("body").on("click","[data-toggle='user-info']",function(){
    var id = $(this).data("id");
    modalBox( base_url + "users/get_user_info/"+id,{
        width : '',
        class : '',
        id: 'modalProfileModern',
        changeModalContent: true
    });
});

// get company details and display in modal
$("body").on("click","[data-toggle='company-info']",function(){
    var id = $(this).data("id");
    modalBox( base_url + "admin/company/get_company_details/"+id,{
        width : '',
        class : '',
        id: 'modalCompany',
        changeModalContent: true
    });
});

// get notary details and display in modal
$("body").on("click","[data-toggle='notary-info']",function(){
    var id = $(this).data("id");
    modalBox( base_url + "notary/get_notary_details/"+id,{
        width : '',
        class : '',
        id: 'modalAgentInfo',
        changeModalContent: true
    });
});

// get comments
$("body").on("click","[data-toggle='comment-details']",function(){
    var schedule_id = $(this).data("schedule-id");
    modalBox( base_url + "comments/comment_details/"+schedule_id,{
        width : '',
        class : '',
        id: 'modalComments',
        changeModalContent: true
    });
});

// get internal notes
$("body").on("click","[data-toggle='notes-details']",function(){
    var schedule_id = $(this).data("schedule-id");
    modalBox( base_url + "comments/notes_details/"+schedule_id,{
        width : '',
        class : '',
        id: 'modalInternalNotes',
        changeModalContent: true
    });
});

// modalbox
$("body").on("click","[data-toggle='modalbox']",function(){
    var url = $(this).data("url");
    var attributes = $(this).data();
    console.log(attributes);
    // remove other attributes
    delete attributes.url;
    delete attributes.toggle;
    modalBox(url,attributes);
});

/* CROP IMAGE */
// preview profile image file
function previewFile(image, is_reset) {
    var preview = document.querySelector('#preview-image');
    var file    = image.files[0];
    // check for image type (jpg and png are allowed)
    var rFilter = /^(image\/jpeg|image\/png)$/i;
    if(file)
    {
        if (!rFilter.test(file.type)) {
        response = {"error":1,"message":'<p>Please select a valid image file (jpg and png are allowed)</p>',"data":[]};
        response_message(response);
        return;
        }

        if(is_reset==false)
        {
            $("#cropImageModal").modal("show");
        }
    }

    var reader  = new FileReader();
    reader.addEventListener("load", function () {
        preview.src = reader.result;
        if(is_reset==true)
        {
            $("#cropImageModal").find('#preview-image').cropper('reset',true).cropper('replace',reader.result);
        }else{
            $("#cropImageModal #preview-image").cropper({ 
                autoCropArea: 0.65,
                viewMode: 3,
                dragCrop: false,
                dragMode: 'move'
            });
        }
    }, false);
    if (file) {
        reader.readAsDataURL(file);
    }
}

$("#cropImageModal").on('show.bs.modal', function () {
    
}).on('hidden.bs.modal', function (){
    $("body").find("input[cropper]").data("reset",false);
    $(this).find('#preview-image').cropper('destroy');
});

$("#change-image").click(function(){
    $("body").find("input[cropper]").data("reset",true);
    $("body").find("input[cropper]").click();
});
$("#save-image").click(function(){
    $("body").find("input[cropper]").data("upload",true);
    var cropped = $("#cropImageModal").find('#preview-image').cropper('getCroppedCanvas',{width: 200, height: 200}).toDataURL();
    $("body").find("#image_url").val(cropped);
    $("body").find("img[cropper-canvas-image]").prop("src",cropped);
    // hide modal
    $("#cropImageModal").hide();
});
$("#close-image-modal").click(function(){
  var upload = $("input[cropper]").data("upload");
  if(upload == false)
  {
    $("body").find("img[cropper-canvas-image]").prop("src", base_url+"assets/img/avatar-default.jpg");
  }
});
/* END CROP IMAGE */


$("body").on('click','.collapsible',function(){
    $(this).toggleClass('collapsible-close').next('.collapsible-content').slideToggle();
    $(this).parents('.schedule-details-parent').siblings(".schedule-details-parent").find('.collapsible').addClass("collapsible-close").next('.collapsible-content').slideUp();
})

/*tooltip*/
$('[data-toggle="tooltip"]').tooltip();
$('[tooltip]').tooltip();
/*popover*/
$('[data-popover="true"]').popover({container:'body', trigger:'hover ', placement:'left'});

// if there is ajax completed
$( document ).ajaxStop(function() {
    /*tooltip*/
    $('[data-toggle="tooltip"]').tooltip();
    $('[tooltip]').tooltip();
    /*popover*/
    $('[data-popover="true"]').popover({container:'body', trigger:'hover ', placement:'left'});
});

// view documents
$("body").on('click','[data-toggle="viewer"]',function(e){
    e.preventDefault();
    var title = $(this).data('title');
    var url   = $(this).data('url');
    var height = $(window).height()-200;
    var modal_content = '<iframe id="iframeprint" src="//docs.google.com/viewer?url='+url+'&embedded=true" border="0" style="width:100%;border:0px;height:'+height+'px;"></iframe>';
    modalBox('',{
        header : title,
        id : 'viewDocs',
        content : modal_content,
        footer: false
    });
});

jQuery.fn.selectText = function() {
  var range, selection;
  return this.each(function() {
    if (document.body.createTextRange) {
      range = document.body.createTextRange();
      range.moveToElementText(this);
      range.select();
    } else if (window.getSelection) {
      selection = window.getSelection();
      range = document.createRange();
      range.selectNodeContents(this);
      selection.removeAllRanges();
      selection.addRange(range);
    }
  });
};
jQuery.fn.hasAttr = function( name ) {
  for ( var i = 0, l = this.length; i < l; i++ ) {
      if ( !!( this.attr( name ) !== undefined ) ) {
          return true;
      }
  }
  return false;
};


