function me_message_v2(response)
{
    var status_message = new Array();
    var message = "";
    status_message[0] = "fa-check";
    status_message[1] = "fa-times";
    status_message[2] = "fa-info";
    status_message[3] = "alert-warning";
    
    if(typeof response===undefined)
    {
        status = 2;
    }
    else
    {
        message = response.message;
        status  = (response.error<3)? response.error : 2;
    } 
    var icons = "<span class='profile-photo-nav' style='height: 40px;width: 40px;margin-top: 0px;'><img style='max-width:100%' src='"+response.custom_icon+"' /></span>";
    if( response.custom_icon === undefined)
    {
        icons = '<span class="fa '+status_message[status]+'"></span>';
    }
     html_data = '<div class="col-xs-2 preloader-body-icon">'+icons+'</div>'+
                 '<div class="col-xs-8 preloader-body-content">'+message+'</div>'+
                 '<div class="col-xs-2"><a class="close" data-dismiss="alert" href="#"><span class="fa fa-times-circle"></span></a></div>';
    var container_open  = '<div id="preloader-message" class="col-xs-4">';
    var container_close = "</div>";
    if(jQuery('body').find('#preloader-message').length>0)
    {
        jQuery("#preloader-message").html(html_data);
    }
    else
    {
        jQuery('body').prepend(container_open+html_data+container_close);
    }
    jQuery('#preloader-message').animate({right:"0"},"100");
    if(status!=2)
    {
        setTimeout(function(){
           jQuery('#preloader-message').fadeOut(1000,function(){
               jQuery(this).remove();
           });
        },7000);
    }
    jQuery('#preloader-message').on('click','.close',function(){
        jQuery('#preloader-message').fadeOut(1000,function(){
            jQuery(this).remove();
        });
    });
     
}
function preloader_saving_status(response)
{
    var status_message = new Array();
    var message = "";
    status_message[0] = "fa-check text-success";
    status_message[1] = "fa-times text-danger";
    status_message[2] = "fa-spin fa-circle-o-notch text-primary";
    status_message[3] = "alert-warning";
    
    if(typeof response===undefined)
    {
        status = 2;
    }
    else
    {
        message = response.message;
        status  = (response.error<3)? response.error : 2;
    }  
    var icons = '<span class="fa '+status_message[status]+'"></span>';
    jQuery('body').find('.preloader-message-status').html("<span class='preloader-message-status-content'>"+icons+" "+response.message+"</span>");
    if(status!=2)
    {
        setTimeout(function(){
           jQuery('body').find('.preloader-message-status').find(".preloader-message-status-content").fadeOut(1000,function(){
            $(this).remove();
           });
        },4000);
    } 
}