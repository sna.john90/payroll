(function() {
  // see https://github.com/brianreavis/selectize.js/issues/470
  // extended from https://gist.github.com/bkosborne/56ee6a6559ac66d64490
  // gist at https://gist.github.com/thiago-negri/132bf33b5312e2da823c

  Selectize
      .define('no_results', function(options) {
        var self = this;

        options = $
            .extend({message: 'No results found.', html: function(data) {
              return ('<div class="selectize-dropdown ' + data.classNames + ' dropdown-empty-message" style="top: 34px; width:100%;">' + '<div class="selectize-dropdown-content"><div class="options-container" style="padding: 5px 8px;">' + data.message + '</div></div>' + '</div>');
            }}, options);

        self.displayEmptyResultsMessage = function() {
          this.$empty_results_container.css('top', this.$control.outerHeight());
          this.$empty_results_container.show();
        };

        self.on('type', function(str) {
            if (!self.hasOptions) {
                self.$empty_results_container.show();
            } else {
                self.$empty_results_container.hide();
            }
        });

        // self.on('focus', function(str) {
        //     if (!self.hasOptions) {
        //         self.$empty_results_container.show();
        //     } else {
        //         self.$empty_results_container.hide();
        //     }
        // });

        self.onKeyDown = (function() {
          var original = self.onKeyDown;

          return function(e) {
            original.apply(self, arguments);
            this.$empty_results_container.hide();
          }
        })();

        self.onBlur = (function() {
          var original = self.onBlur;

          return function() {
            original.apply(self, arguments);
            this.$empty_results_container.hide();
          };
        })();

        self.setup = (function() {
          var original = self.setup;
          return function() {
            original.apply(self, arguments);
            self.$empty_results_container = $(options.html($.extend({classNames: self.$input.attr('class')}, options)));
            self.$empty_results_container.insertBefore(self.$dropdown);
            self.$empty_results_container.hide();
          };
        })();
      });
})();